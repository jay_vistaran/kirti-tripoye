<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['city-page'] = 'home/city_page';
$route['detail-page'] = 'home/detail_page';
$route['category'] = 'home/category';
$route['b2b-destinations'] = 'b2B_handler';

$route['checkout'] = 'checkout';
$route['payment'] = 'home/payment';
$route['about'] = 'home/about';
$route['careers'] = 'home/career';
$route['contact-us'] = 'home/contact';
$route['frequently-asked-questions'] = 'home/faq';
$route['privacy-policy'] = 'home/privacypolicy';
$route['terms-of-use'] = 'home/termscondition';
$route['booking-modal'] = 'home/booking_modal';
$route['confirmation'] = 'home/confirmation';
//$route['thankyou'] = 'home/thankyou';

$route['thank-you'] = 'home/sendMail';


$route['home/admin'] = 'admin/home';

$route['booking-modal'] = 'home/booking_modal';

$route['thankyou'] = 'B2B_handler/sendMail';


$route['user-profile'] = 'authenticate/user_profile';
$route['user-tour-booking'] = 'authenticate/user_tour_booking';
$route['travel-booking-detail/(:num)'] = 'authenticate/travel_booking_detail/$1';
$route['edit-user-profile'] = 'authenticate/edit_user_profiloe';





$route['transaction-fail'] = 'home/transaction_fail';

////// .....URL   ROUTING...........

$route['tours/(:any)'] = 'city_manager/city/$1';


// Home tour list url
$route['tours/(:any)/(:any)-(:num)'] = 'Detailspage_manager/detailspageHomeTour/$1/$2/$3';


// city tour list url
$route['tours/(:any)/(:any)/(:any)-(:num)'] = 'Detailspage_manager/detailspage/$1/$2/$3/$4';


// city tourcategory list url
$route['tours/(:any)/(:num)-(:any)'] = 'city_manager/categorypage/$1/$2/$3';


