<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Detailspage_manager extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Detailspage_manager_model','detail_manager');
		
    }
	
	public function detailspageHomeTour($tourCat,$tour,$tourid)
	{
		      $TotalRatingUsers=$this->detail_manager->get_users_by_rating_tours($tourid);
			  $TotalRatinCount=$this->detail_manager->get_count_by_rating_tours($tourid);
			
			   $TotalRatinCountArray = array(); 
		       $TotalRatinCountArray[] =$TotalRatinCount;
			
			
		       $tourRating=$this->detail_manager->get_tourRating_by_selected_tours($tourid);
		 
		       $tourInfo=$this->detail_manager->get_tour_by_selected_tours($tourid);
			
			   $tour_prices = array(); 
					   
			   $tour=$this->detail_manager->get_tourdetails_by_id($tourid);
			   $tour_prices[] =$tour;
			 
		      
		      
			  $tourTransferInfo=$this->detail_manager->get_tour_transfer_category_by_selected_tours($tourid);
			 
	          $Ticket_tourTransferInfo=$this->detail_manager->get_tour_Ticket_transfer_category_by_selected_tours($tourid);
	
	
	          $tour_optionInfo=$this->detail_manager->get_tour_Option_by_selected_tours($tourid);
			 
              $No_Ticket="NoTicket";
			 
			    $data['tourInfo']=$tourInfo;
			    $data['tour_prices']=$tour_prices;
			    $data['tour_transfer']=$tourTransferInfo;
			   
			   if($Ticket_tourTransferInfo == null)
			   {
				   $data['tour_transfer_Ticket']=$No_Ticket;
			   }else
			   {
				   $data['tour_transfer_Ticket']=$Ticket_tourTransferInfo;
			   }
			      
			    $data['tour_option']=$tour_optionInfo;	
			    $data['tourRating']=$tourRating;

                $data['TotalRatingUsers']=$TotalRatingUsers;	
		        $data['TotalRatinCount']=$TotalRatinCountArray;				
				
	           $this->load->view('detail_page',$data);
		
	}
	
	
	public function detailspage($city,$tourCat,$tour,$tourid)
	{
		
		    $TotalRatingUsers=$this->detail_manager->get_users_by_rating_tours($tourid);
			$TotalRatinCount=$this->detail_manager->get_count_by_rating_tours($tourid);
			
			$TotalRatinCountArray = array(); 
		    $TotalRatinCountArray[] =$TotalRatinCount;
		
		
	           $tourRating=$this->detail_manager->get_tourRating_by_selected_tours($tourid);
		
		      $tourInfo=$this->detail_manager->get_tour_by_selected_tours($tourid);
			
		      $tour_prices = array(); 
					   
			  $tour=$this->detail_manager->get_tourdetails_by_id($tourid);
			  $tour_prices[] =$tour;
			  
			  
			  
	$tourTransferInfo=$this->detail_manager->get_tour_transfer_category_by_selected_tours($tourid);
			 
	$Ticket_tourTransferInfo=$this->detail_manager->get_tour_Ticket_transfer_category_by_selected_tours($tourid);
	
	
	$tour_optionInfo=$this->detail_manager->get_tour_Option_by_selected_tours($tourid);
	
	
			 
             $No_Ticket="NoTicket";
			  
			 
		       $data['tourInfo']=$tourInfo;
			   $data['tour_prices']=$tour_prices;
			   $data['tour_transfer']=$tourTransferInfo;
			   
			   if($Ticket_tourTransferInfo == null)
			   {
				  $data['tour_transfer_Ticket']=$No_Ticket;
			   }else
			   {
				  $data['tour_transfer_Ticket']=$Ticket_tourTransferInfo;
			   }
			      
			 $data['tour_option']=$tour_optionInfo;	

             $data['tourRating']=$tourRating;	
			 
		     $data['TotalRatingUsers']=$TotalRatingUsers;	
		     $data['TotalRatinCount']=$TotalRatinCountArray;	
				
				
				//echo json_encode($data);
	        $this->load->view('detail_page',$data);
		
	}
	
	
	//////////.....ADD RATING TOURS.............
	
		public function add_rating()
	{
		$user_email=$this->input->post('user_email');
		$Exiting_users_id=$this->detail_manager->exiting_users_check($user_email);
		
		 if($Exiting_users_id == [])
		 {
			    
				
				$data['user_name']=$this->input->post('user_name');
				$data['user_email']=$this->input->post('user_email');
				$data['user_mobile']=$this->input->post('user_mobile');
				
				$data['user_status']=1;
				$data['user_account_disabled']=0;
				$data['date_created	']=date('Y-m-d H:i:s');
				
			    $insert_users_id=$this->detail_manager->save_users($data);
		 
		 
		        $data_rating['TourId']=$this->input->post('TourId');
		        $data_rating['UserId']=$insert_users_id;
		        $data_rating['tour_rating']=$this->input->post('tour_rating');
		        $data_rating['tour_comment']=$this->input->post('tour_comment');
		        $data_rating['approve']=0;
		 
				$data_rating['created'] = date('Y-m-d H:i:s');
				$insert=$this->detail_manager->save_rating($data_rating); 
				
				echo json_encode(array("status" => $insert_users_id));
				
				
			 
		 }
		 else
		 {
			    $data_rating['TourId']=$this->input->post('TourId');
				
				$users_id=$Exiting_users_id->user_id;
				
		        $data_rating['UserId']=$users_id;
				
		        $data_rating['tour_rating']=$this->input->post('tour_rating');
		        $data_rating['tour_comment']=$this->input->post('tour_comment');
		        $data_rating['approve']=0;
		 
				$data_rating['created'] = date('Y-m-d H:i:s');
				$insert=$this->detail_manager->save_rating($data_rating); 
				
				echo json_encode(array("status2" => $data_rating));
		 }
		
	  
	}
	
	
	
}
?>