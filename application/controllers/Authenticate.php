<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Controller
 {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		
		$this->load->library('google');
		
		
		
		
		$this->load->model('Users_model','users_model');
		
		
    }

	
			
			public function user_profile()
		 {
			 
			
			   $User_id=$this->session->userdata('user_id');
			  
			   
			   $oauth_uid=$this->session->userdata('loggedIn');
			   
			   if($User_id == '' && $oauth_uid == false)  
			   {
				  redirect('home');
			   }
			   else
			   {
				    $data['notification'] = $this->users_model->get_tour_discount_notification();
				   
				   $data['profile'] = $this->users_model->get_user_profile($User_id);
			   
			      $this->load->view('user_profile',$data);
			   }
			   
		 } 
 
 
		 public function user_tour_booking()
		 {
			 
			    $User_id=$this->session->userdata('user_id');
			   
			   $oauth_uid=$this->session->userdata('loggedIn');
			   
			   if($User_id == '' && $oauth_uid == false)  
			   {
				   redirect('home');  
			   }
			   else
			   {
				   $user_email=$this->session->userdata('user_email');
			   
			       $data['booking'] = $this->users_model->get_user_all_booking($user_email);
			   
			      $data['notification'] = $this->users_model->get_tour_discount_notification();
			   
			       $this->load->view('user_tour_booking',$data);
			   }
			 
			   
		 }
 
 
		 public function travel_booking_detail($order_id)
		 { 
		    $User_id=$this->session->userdata('user_id');
			   
			   $oauth_uid=$this->session->userdata('loggedIn');
			   
			   if($User_id == '' && $oauth_uid == false)  
			   {
				   redirect('home');  
			   }
			   else
			   {
				   $data['order_info'] = $this->users_model->get_order_details_by_id($order_id);
			   
			       $data['notification'] = $this->users_model->get_tour_discount_notification();
				   
				   
			       $this->load->view('travel_booking_detail',$data);
			   }
		 
		       
		 }
 
 
		 public function edit_user_profiloe()
		 {
			 
			  $User_id=$this->session->userdata('user_id');
			   
			   $oauth_uid=$this->session->userdata('loggedIn');
			   
			   if($User_id == '' && $oauth_uid == false)  
			   {
				   redirect('home');  
			   }
			   else
			   {
				   $data['profile'] = $this->users_model->get_user_profile($User_id);
			   
			        $data['notification'] = $this->users_model->get_tour_discount_notification();
			       $this->load->view('edit_user_profiloe',$data);
			   }
			 
			   
		 }
		 
		 
	public function update_user()
	{    
		
		
							 
		$data1['user_name']=$this->input->post("user_name");		
		
        $data1['user_surname']=$this->input->post("user_surname");
		$data1['user_mobile']=$this->input->post("user_mobile");
		
		$data1['user_dob']=$this->input->post("user_dob");
		$data1['user_city']=$this->input->post("user_city");
		
		$data1['user_address']=$this->input->post("user_address");
		
		
	    $User_id=$this->session->userdata('user_id');
	   
		 
	    $insert1 = $this->users_model->update_profile($User_id,$data1);
		 
			
	    $mgs='<div id="succssssMgs"><div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span> Your Profile is Successfully Updated.</span></div></div>';
            	
        $this->session->set_flashdata('err_message',$mgs);
            	
        redirect('edit-user-profile'); 
		
		
		
	}
		 
		 
		 
		 
		 
		 
		 
		 
	
	
	public function login()
	{
		 if ($_POST)
        {
              /* $name = $this->input->post('user_email', true);
			  $pass = $this->input->post('user_password', true);
			  
	         $data['user_email'] = ($name);
	         $data['user_password'] = md5(($pass)); */
	
	
	
            $data['user_email'] = $this->input->post('user_email', true);
            $data['user_password'] = $this->input->post('user_password', true);

		 
		   
            if ($this->users_model->resolve_user_login($data))
            {
				
                $user_email = $data['user_email'];

                $user_id = $this->users_model->get_user_id_from_username($user_email);


                $user = $this->users_model->get_user($user_id);
             

                foreach ($user as $row)
                {                
                    $userdata['user_id'] = $row->user_id;
                    $userdata['user_name'] = $row->user_name;
                    $userdata['user_mobile'] = $row->user_mobile;                   
                    $userdata['user_email'] = $row->user_email;                   
                  
                }
				
                $this->session->set_userdata($userdata);
				
                 
		        echo json_encode(true);
			//	redirect('user-profile'); 
		
            } 
			else
            {
                 echo json_encode(false);
				
				  /* $mgs='<div id="succssssMgs"><div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>User Email or Passsword Is incorrect.</span></div></div>';    

                    $this->session->set_flashdata('err_message',$mgs);                     
                 
                  redirect('home');  */ 
            }


        }
		
	}
	
	
	public function sign_up()
	{
       
		$data['user_name']=$this->input->post("user_name");
		
		$data['user_email']=$this->input->post("user_email");
		
		$data['user_password']=$this->input->post("user_password");
		
		
		
		if($this->users_model->get_exits_user_id_from_username($data))
		{
			echo json_encode(false);
			        /*  $mgs='<div id="succssssMgs"><div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><span>User all ready Exists ........</span></div></div>';    

                     $this->session->set_flashdata('err_messages',$mgs);                     
                 
                     redirect('home'); */
		}
		else
		{
			$insert = $this->users_model->save($data);
			
			 echo json_encode(true);
		    //redirect('home');	
		}
		
		
						
   }
	
	
	
	public function logout()    
    {

        $this->session->sess_destroy($userdata);

        $this->session->unset_userdata('loggedIn');
		//$this->session->unset_userdata('userData');
        $this->session->sess_destroy();		
			
		 redirect('home');  
     }
	
	
	////................SIGN UP WITH GOOGLE ACCOUNT,.,.............
	
	public function sign_up_google()
	{
	
	   /*  if($this->session->userdata('loggedIn') == true)
			
			{
				redirect('user-profile');
			} */
		
		if(isset($_GET['code']))
		{
			
			$this->google->getAuthenticate();
			
			
			$gpInfo = $this->google->getUserInfo();
			
           
			$userData['oauth_provider'] = 'google';
			$userData['oauth_uid'] 		= $gpInfo['id'];
            $userData['first_name'] 	= $gpInfo['given_name'];
            $userData['last_name'] 		= $gpInfo['family_name'];
            $userData['email'] 			= $gpInfo['email'];
			$userData['gender'] 		= !empty($gpInfo['gender'])?$gpInfo['gender']:'';
			$userData['locale'] 		= !empty($gpInfo['locale'])?$gpInfo['locale']:'';
            $userData['profile_url'] 	= !empty($gpInfo['link'])?$gpInfo['link']:'';
            $userData['picture_url'] 	= !empty($gpInfo['picture'])?$gpInfo['picture']:'';
			
			
            $userID = $this->users_model->checkUser($userData);
			
			
			
			
             $data['user_name']     = $gpInfo['given_name'];           
            $data['user_email']     = $gpInfo['email'];
			$data['user_image'] 	= !empty($gpInfo['picture'])?$gpInfo['picture']:'';
			
		      $user_email     = $gpInfo['email'];
			
			if($this->users_model->get_exits_user_id_from_username($data))
				{
					
							
				}
				else
				{
					$insert = $this->users_model->save($data);
					
				}
			
			    $user_id = $this->users_model->get_user_id_from_username($user_email);


                $user = $this->users_model->get_user($user_id);
             

                foreach ($user as $row)
                {                
                    $userdata['user_id'] = $row->user_id;
                    $userdata['user_name'] = $row->user_name;
                    $userdata['user_mobile'] = $row->user_mobile;                   
                    $userdata['user_email'] = $row->user_email;                   
                  
                }
				
                $this->session->set_userdata($userdata);
			
			
			$this->session->set_userdata('loggedIn', true);
			$this->session->set_userdata($userData);			
			
			redirect('user-profile');
			
		} 
		
		
		//$data['loginURL'] = $this->google->loginURL();
		
		
		//$this->load->view('user_authentication/index',$data);
	
	     //redirect('home');
	
	}
	
	
	

}
?>
