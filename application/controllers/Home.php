<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
         $this->load->library('google');
		
		$this->load->helper('url');
		$this->load->model('Section_manager_model','section_manager');
		
		
		}

	public function index()
	{
			
		
		
		
		 //.......COUNTRY SEARCH BAR SELLING
		  $countrylist=$this->section_manager->get_all_country();
		  
		  $citylist=$this->section_manager->get_all_city();
		  
		  
		  
		  
		  
		 //.......TRENDING SECTION
		 
		  $Exits_trend_str=$this->section_manager->get_all_treading_destinations_city();			
		     $trendingitems = array(); 
			 
			  if(!empty($Exits_trend_str))
		{	
             $cityid=explode(",",$Exits_trend_str);
         foreach ($cityid as $key => $value)
          {
              $id= $value;
              $datas=$this->section_manager->get_city_by_id($id);
              $trendingitems[] = $datas;
          } 
		 }
          else{
	      
              }	
		 
		 
		 //.......HOT SELLING
		 
		  $Exits_Hot_str=$this->section_manager->get_all_hot_selling();	
              $items1 = array(); 	
              $items = array(); 	
              $TourTotalRatingArray = array(); 		 	  
		     if(!empty($Exits_Hot_str))
		{	
		    
             $tourid=explode(",",$Exits_Hot_str);
			 
         foreach ($tourid as $key => $value)
          {
               $id= $value;	
               $tourorder_array[] =$id;	
               $tourorder=$this->section_manager->get_tourdetails_by_id($id);

             $TotalRatingUsers=$this->section_manager->get_users_by_rating_tours($id);
			 $TotalRatinCount=$this->section_manager->get_count_by_rating_tours($id);
			 
			        $tour_rating=$TotalRatinCount['tour_rating'];
			 
			             if($tour_rating == null)
							  {
								$TourReview =0;
							  }
							  else
							  {   
						       $TourReviews= $tour_rating /  $TotalRatingUsers; 
						       $TourReview = round($TourReviews);
						       }
			
              $TourTotalRatingArray[] =  $TourReview;  
			  
              $items1[] =  $tourorder;  
			  $items = array_values(array_filter($items1));
		  }	
		  
		   }
          else{
	      
              }	
		  
		   //.......COMBO
		  
		  $Exits_Combo_str=$this->section_manager->get_all_exclusive_combo();	
          $comboitems1 = array(); 
          $comboitems = array(); 
					  
		    if(!empty($Exits_Combo_str))
		{	
			 $tourorder_array = array(); 
             $comboid=explode(",",$Exits_Combo_str);
			 
         foreach ($comboid as $key => $value)
          {
               $id= $value;			   
			   $tourorder=$this->section_manager->get_tourorder_by_combo_by_id($id);
			   $tours_ids=explode(",",$tourorder);
			   
		 	   
          foreach ($tours_ids as $key => $value)
          {
			  $tours_id= $value;			  
			  $tourorder_array[] =$tours_id;
			  
		  }	 	   
		       
               $tour_otheroption_idlist=$this->section_manager->get_tour_otheroption_base_pack_by_tour_id($tours_ids);
			    
			   $dd=array_shift($tour_otheroption_idlist);
			
		  foreach ($dd as $key => $tour_otheroption_base_pack)
            {
			  $tour_otheroption_ids= $tour_otheroption_base_pack;			  
	    	}	
			
			
			  $tour_otheroption_id=explode(",",$tour_otheroption_ids);
			
              $array=$this->section_manager->get_all_tour_combo_by_id($id,$tourorder_array,$tour_otheroption_id);
		      $comboitems1[] =  $array;
			  $comboitems = array_values(array_filter($comboitems1));
		  } 
              
          }
          else{
	      
              }		  
		 
		  
		  
		  
		  
		 $data['trending']=$trendingitems;
		 $data['hotselling']=$items;
		 $data['hotselling_tour_rating']=$TourTotalRatingArray;
		 
		 $data['country']=$countrylist;
		  $data['city']=$citylist;
		 $data['combo']=$comboitems; 
		 
		$data['loginURL'] = $this->google->loginURL();
		
	    // echo json_encode($data);
	
      $this->load->view('home',$data);  
	  
	}
	
	
	
	
	
	
	
	
	
    public function city_page()
	{
      $this->load->view('city_page');
	}
	
	
	public function detail_page()
	{
      $this->load->view('detail_page');
	}
	
		public function category()
	{
      $this->load->view('category');
	}
	
	 public function b2b()
	{
       $this->load->view('b2b');
	}
	 public function about()
	{
       $this->load->view('about');
	}
	 public function career()
	{
       $this->load->view('career');
	}
	
	 public function contact()
	{
       $this->load->view('contact');
	}
	 public function faq()
	{
       $this->load->view('faq');
	} 
	public function privacypolicy()
	{
       $this->load->view('privacypolicy');
	}
	public function termscondition()
	{
       $this->load->view('termscondition');
	}
	
	public function confirmation()
	{
       $this->load->view('confirmation');
	}
	
	
	
	

	
	

	
	public function booking_modal()
	{
		$cityname=$this->input->post("cityname");
		
		$data['cityname'] = $cityname;
		
		
        $this->load->view('booking_modal',$data);
	}
	
	
	 public function payment()
	{
       $this->load->view('payment');
	}
	
	 public function thankyou()
	{
       $this->load->view('thankyou');
	}


		public function saveNewletter()
	{
		//....Newsletter Add
		
		$data['newsletter']=$this->input->post('newsletter');
		$data['created_date'] = date('Y-m-d H:i:s');
		$insert=$this->section_manager->saveNewsletter($data);
		
      	echo json_encode(array("status" => TRUE));
	  
	}
	
	
	
	
	function sendMail()
	
    {
		
		
		        $name=$this->input->post('name');
				$email_id=$this->input->post('email_id');
				$subject=$this->input->post('subject');
				
				
				
				$query=$this->input->post('query');
				
				
				$ci = get_instance();
				$ci->load->library('email');
				$config['protocol'] = "smtp";
				$config['smtp_host'] = "mail.tripoye.com";
				$config['smtp_port'] = "25";
				$config['smtp_user'] = "infotest@tripoye.com";
				$config['smtp_pass'] = "tripoye@123";
				$config['charset'] = "utf-8";
				$config['mailtype'] = "html";
				$config['newline'] = "\r\n";
				$config['crlf'] = "\r\n";

				$ci->email->initialize($config);

			   
				$ci->email->from('info@tripoye.com');

				
				$ci->email->to('info@tripoye.com');
			   
				$ci->email->subject('Contact us Enquiry ');
				$ci->email->message('Dear Employee,<br> We have recieved an enquiry with below mentioned requirements.<br><br>
				
				
				Customer Name: '.$name.'<br>
				
				
				Email ID: .'.$email_id.'<br>
				Subject: '.$subject.'<br>			
				
			
				Message: '.$query.'<br> ');

			
				$ci->email->send();
			
			
		      $this->load->view('thankyou');	
			
			}
	
	
	
	
	
	
	
				
 public function imagefile_upload( $field_name )    // this function does the uploads
    {
        if ( ! $this->upload->do_upload( $field_name ))    // ** do_upload() is a member function of upload class, and it is responsible for the uploading files with the given configuration in the config array
        {
               // now if there's is some error in uploading files, then errors are stored in the member variable 'data'
        }
        else
        {
            $upload_data = $this->upload->data();    // if succesful, then infomation about the uploaded file is stored in the $upload_data variable
             $data['filepath']=$upload_data['file_name'];
			 return $data['filepath'];
           
        }
    }
	
		 public function setupfile_upload( $field_name )    // this function does the uploads
    {
        if ( ! $this->upload->do_upload( $field_name ))    // ** do_upload() is a member function of upload class, and it is responsible for the uploading files with the given configuration in the config array
        {
            $this->data['notification'] .= $this->upload->display_errors();    // now if there's is some error in uploading files, then errors are stored in the member variable 'data'
        }
        else
        {
            $upload_data = $this->upload->data();    // if succesful, then infomation about the uploaded file is stored in the $upload_data variable
             $data['setupfile']= $upload_data['file_name'];
							return $data['setupfile'];
			 
           
        }
    }
	
	
	
	


}
?>
