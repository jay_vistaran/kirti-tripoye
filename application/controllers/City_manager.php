<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class City_manager extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('City_manager_model','city_manager');
		
    }
	
	
	
	public function city($city)
	{
		  $cityName = str_replace("-", " ", $city);
		  $cityid=$this->city_manager->get_CityId_Cityname($cityName);
		      $tourorder_array = array(); 
		    $cityInfo=$this->city_manager->get_city_by_selected_cityid($cityid,$city);
			
			 ////.....collection
			
        $Exits_id=$this->city_manager->get_exits_id_by_city($cityid);	
		$collection_array = array(); 
		if(!empty($Exits_id))
		{
			  
			   $str=$this->city_manager->get_tourcatorder_by_city($cityid);			
		    
			  
             $tourcatid=explode(",",$str);
			    
      foreach ($tourcatid as $key => $value)
          {
               $id= $value;			   
			  $tour=$this->city_manager->get_tourcatdetails_by_id($id);
			  $collection_array[] =$tour;	         		 
		  } 
		     
			  
		}
		else
		{
			
		    
		}
		
		
		
		
					////.....hot Selling
					
		$Exits_Selling_id=$this->city_manager->get_Selling_exits_id_by_city($cityid);
		$Selling_array2 = array(); 
		$Selling_array3 = array(); 
		$Selling_title = array(); 
		
		$TourTotalRatingArray = array(); 	
		if(!empty($Exits_Selling_id))
		{
			$str=$this->city_manager->get_tourorder_by_city($cityid);
			$title=$this->city_manager->get_hot_selling_title_by_city($cityid);
			
			 
             $tourid=explode(",",$str);
			    
          foreach ($tourid as $key => $value)
            {
               $id= $value;			   
			  $tour=$this->city_manager->get_tourdetails_by_id($id);
			  
			    $TotalRatingUsers=$this->city_manager->get_users_by_rating_tours($id);
			    $TotalRatinCount=$this->city_manager->get_count_by_rating_tours($id);
			 
			        $tour_rating=$TotalRatinCount['tour_rating'];
			 
			             if($tour_rating == null)
							  {
								$TourReview =0;
							  }
							  else
							  {   
						       $TourReviews= $tour_rating /  $TotalRatingUsers; 
						       $TourReview = round($TourReviews);
						       }
							   
			
              $TourTotalRatingArray[] =  $TourReview; 
			  
			  $Selling_array3[] =$tour;
			  $Selling_title[] =$title;
	         		 
			 
			  $Selling_array2 = array_values(array_filter($Selling_array3));		 
		     }	  
	   }
		else
		{
			
		    
		}
		 
		  
		  ////.....Combo Section
		  
		  $Exits_Combo_id=$this->city_manager->get_Combo_exits_id_by_city($cityid);
		   $comboitems1 = array(); 
		   $comboitems = array(); 
		   $combotitle = array(); 	
		if(!empty($Exits_Combo_id))
		{
			$title=$this->city_manager->get_combo_title_by_city($cityid);
			$str=$this->city_manager->get_all_exclusive_combo($cityid);			    
		    		 
			 $tourorder_arraycombo = array(); 
             $comboid=explode(",",$str);
			 
         foreach ($comboid as $key => $value)
          {
               $id= $value;			   
			   $tourorder=$this->city_manager->get_tourorder_by_combo_by_id($id);
			   $tours_ids=explode(",",$tourorder);
			   
		 	   
              foreach ($tours_ids as $key => $value)
               {
			      $tours_id= $value;			  
			      $tourorder_arraycombo[] =$tours_id;
			  
		        }	 	   
		       
               $tour_otheroption_idlist=$this->city_manager->get_tour_otheroption_base_pack_by_tour_id($tours_ids);
			    
			   $dd=array_shift($tour_otheroption_idlist);
			
		         foreach ($dd as $key => $tour_otheroption_base_pack)
                {
			         $tour_otheroption_ids= $tour_otheroption_base_pack;			  
	    	    }	
			
			
			$tour_otheroption_id=explode(",",$tour_otheroption_ids);

              $array=$this->city_manager->get_all_tour_combo_by_id($id,$tourorder_arraycombo,$tour_otheroption_id);
			  
              $comboitems1[] =  $array; 
			  $comboitems = array_values(array_filter($comboitems1));
			  $combotitle[] =  $title;

            } 	
			
	}
	else
	{
			
		    
	}
		 
		  
		  
		  ///.....CREATED Section.....
		  
		   $Exits_Created_Section_id=$this->city_manager->get_Created_Sections_exits_id_by_city($cityid);
		   $Createitems1 = array(); 	
		   $Createitems = array(); 	
		   
		   $CREATED_Section_TourTotalRatingArray = array(); 
		   
		if(!empty($Exits_Created_Section_id))
		{
			 $strs=$this->city_manager->get_Created_tourcatorder_by_city($cityid);	         
		     		 
			 $tourorder_array11 = array(); 
             $tourcatid=explode(",",$strs);
			 
			 
         foreach ($tourcatid as $key => $value)
          {
               $cat_id= $value;		
               $Exits_tourorder=$this->city_manager->get_tourcat_tour_exits_by_id($cat_id);
			   
			   if(empty($Exits_tourorder))
			{
				   
			}
			else
			{
			
			$tourorder=$this->city_manager->get_tourcat_tour_by_id($cat_id);
			   
			   $tours_ids=explode(",",$tourorder);
			   
		 	   
               foreach ($tours_ids as $key => $value)
                {
			      $tours_id= $value;			  
			      $tourorder_array11[] =$tours_id;	



          		    $TotalRatingUsers=$this->city_manager->get_users_by_rating_tours($tours_id);
			        $TotalRatinCount=$this->city_manager->get_count_by_rating_tours($tours_id);
			 
			        $tour_rating=$TotalRatinCount['tour_rating'];
			 
			             if($tour_rating == null)
							  {
								$TourReview =0;
							  }
							  else
							  {   
						       $TourReviews= $tour_rating /  $TotalRatingUsers; 
						       $TourReview = round($TourReviews);
						       }
							   
			
              $CREATED_Section_TourTotalRatingArray[] =  $TourReview; 
				  
		      }	 	   
		       
              $array=$this->city_manager->get_Created_all_tour_by_id($cat_id,$tourorder_array11);
              $Createitems1[] =  $array;
			  $Createitems = array_values(array_filter($Createitems1));

           } 
		   
		   
		   
		   
		  }
		   
		}
	else
	{
			
		    
	}
		  
		  
		         $data['cityInfo1']=$cityInfo;
                 $data['collection']=$collection_array;
				
				 $data['hotselling']=$Selling_array2;
				 $data['hotselling_title']=$Selling_title;
				 $data['hotselling_tour_rating']=$TourTotalRatingArray;
				  
				 $data['combo']=$comboitems;
				 $data['combo_title']=$combotitle; 
				 
				 $data['create_section']=$Createitems; 
				 $data['create_section_tour_rating']=$CREATED_Section_TourTotalRatingArray;
				 
				$data['cityInfo1']=$cityInfo;
			//echo json_encode($data);
				
	       $this->load->view('city_page',$data); 

	}
	
	
	
	
	
	
	
	
	//....... COLLECTION  SECTION ........
	
	
	 public function get_all_collection_by_city()
	{     
	      $id=$this->input->post("id");
		    $Exits_id=$this->city_manager->get_exits_id_by_city($id);	
		$tourorder_array = array(); 
		if(!empty($Exits_id))
		{
			  
			   $str=$this->city_manager->get_tourcatorder_by_city($id);			
		    
			  
             $tourcatid=explode(",",$str);
			    
      foreach ($tourcatid as $key => $value)
          {
               $id= $value;			   
			  $tour=$this->city_manager->get_tourcatdetails_by_id($id);
			  $tourorder_array[] =$tour;	         		 
		  } 
			  echo json_encode($tourorder_array); 
			  
		}
		else
		{
			
		   echo json_encode($tourorder_array); 
		}
		
		   
		  
	}
	
	
	
	
	
	//....... HOT SELLING SECTION ........
	
  public function get_all_hot_selling_by_city()
	{     
	     $id=$this->input->post("id");
		   $str=$this->city_manager->get_tourorder_by_city($id);			
		    
		     $items = array(); 
			 $items1 = array(); 
	 
			 $tourorder_array = array(); 
             $tourid=explode(",",$str);
			    
foreach ($tourid as $key => $value)
          {
               $id= $value;			   
			  $tour=$this->city_manager->get_tourdetails_by_id($id);
			  $tourorder_array[] =$tour;
	         		 
		  }	  
		  
		  echo json_encode($tourorder_array); 
	}
	
	
	//....... EXCLUSIVE COMBO DEALS SECTION ........		
	
	public function get_all_exclusive_combo()
	{
		   $id=$this->input->post("id");
		 $str=$this->city_manager->get_all_exclusive_combo($id);			
		    
		     $items = array(); 
			 
			 $tourorder_array = array(); 
             $comboid=explode(",",$str);
			 
         foreach ($comboid as $key => $value)
          {
               $id= $value;			   
			   $tourorder=$this->city_manager->get_tourorder_by_combo_by_id($id);
			   $tours_ids=explode(",",$tourorder);
			   
		 	   
          foreach ($tours_ids as $key => $value)
          {
			  $tours_id= $value;			  
			  $tourorder_array[] =$tours_id;
			  
		  }	 	   
		       
               $tour_otheroption_idlist=$this->city_manager->get_tour_otheroption_base_pack_by_tour_id($tours_ids);
			    
			   $dd=array_shift($tour_otheroption_idlist);
			
		  foreach ($dd as $key => $tour_otheroption_base_pack)
            {
			  $tour_otheroption_ids= $tour_otheroption_base_pack;			  
	    	}	
			
			
			$tour_otheroption_id=explode(",",$tour_otheroption_ids);

              $array=$this->city_manager->get_all_tour_combo_by_id($id,$tourorder_array,$tour_otheroption_id);
			  
              $items[] =  $array;

          } 		  
	         echo json_encode($items); 			 
	  }
	  
	  
	  
	  
	  
	  
	  	//....... CREATED  SECTION ........
	
  public function get_all_created_section_by_city()
	{     
	
	     	   $id=$this->input->post("id");
			  
			 $strs=$this->city_manager->get_Created_tourcatorder_by_city($id);			
		              
		     $Createitems = array(); 
			 
			 $tourorder_array = array(); 
             $tourcatid=explode(",",$strs);
			 
         foreach ($tourcatid as $key => $value)
          {
               $cat_id= $value;			   
			   $tourorder=$this->city_manager->get_tourcat_tour_by_id($cat_id);
			   $tours_ids=explode(",",$tourorder);
			   
		 	   
          foreach ($tours_ids as $key => $value)
          {
			  $tours_id= $value;			  
			  $tourorder_array[] =$tours_id;
			  
		  }	 	   
		       
              $array=$this->city_manager->get_Created_all_tour_by_id($cat_id,$tourorder_array);
			  
              $Createitems[] =  $array;

          } 
		  
		  
		  echo json_encode($Createitems); 
		  
	}
	
	
	
	
	///////  Collection Tour category details page(category page)
	
	  public function categorypage($city,$tourCatTd,$tourCategory)
	  {
		 $categorName_array = array(); 
		 $category_array = array(); 
		 $TourTotalRatingArray = array(); 
		
		
		    $city_Id=$this->city_manager->get_CityId_Cityname($city);
			
		    $CategoryName=$this->city_manager->get_CategoryName_by_tourCatTd($tourCatTd);
			
			$TourList=$this->city_manager->get_tour_by_tourCatTd($city_Id,$tourCatTd);
			
			
			  $categorName_array[] =$CategoryName;
			  $category_array[] =$TourList;
	         		 
			 foreach ($TourList as $key)
          {
			      $tours_id= $key->id;		
			  
			  	    $TotalRatingUsers=$this->city_manager->get_users_by_rating_tours($tours_id);
			        $TotalRatinCount=$this->city_manager->get_count_by_rating_tours($tours_id);
			 
			        $tour_rating=$TotalRatinCount['tour_rating'];
			 
			             if($tour_rating == null)
							  {
								$TourReview =0;
							  }
							  else
							  {   
						       $TourReviews= $tour_rating /  $TotalRatingUsers; 
						       $TourReview = round($TourReviews);
						       }
							   
			
              $TourTotalRatingArray[] =  $TourReview; 
			  
		  }			 
					 
					 
			  $data['city_array']=$city;
		      $data['TourcategoryName_array']=$categorName_array;			 
		      $data['Tourcategory_array']=$category_array;
		      $data['TourcategoryTour_rating']=$TourTotalRatingArray;
	 
	          
		     $this->load->view('category',$data);
	  }
	  
	
}
?>