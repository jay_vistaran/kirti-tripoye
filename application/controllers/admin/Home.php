<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Enquries_model','enquries');
		$this->load->model('Subscriber_model','subscriber');
		}

	public function index()
	{
      $this->load->view('admin/home');
	}
    public function dashboard()
	{
      $this->load->view('admin/dashboard');
	}
	  public function enquiry()
	{
      $this->load->view('admin/enquiry');
	}
	  public function subsciber()
	{
      $this->load->view('admin/subsciber');
	}
	
		public function ajax_list()
	{
		$list = $this->enquries->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $enquries) {
			
			$no++;
			$row = array();
			$row[] = $enquries->enquiry_id;
			$row[] = $enquries->fname;
			$row[] = $enquries->mobile_no;
			$row[] = $enquries->email_id;
			$row[] = $enquries->sub;
			$row[] = $enquries->msg;
			$row[] = '<a href="'.base_url('assets/uploads/'.$enquries->file).'" style="color: blue" download>'."".$enquries->file."".'</a>';
			
		
			
			//add html for action
				$row[] = '<a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Hapus" onclick="delete_enquries('."'".$enquries->enquiry_id."'".')">Delete</a>';

			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->enquries->count_all(),
						"recordsFiltered" => $this->enquries->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	 public function delete_enquries($id)
	{
		$this->enquries->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_list_subscriber()
	{
		$list = $this->subscriber->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $subscriber) {
			
			$no++;
			$row = array();
			
			$row[] = $subscriber->subscriber_id;
			
			$row[] = $subscriber->email_id;
			
			
			
			//add html for action
				$row[] = '<a class="btn btn-xs btn-warning" href="javascript:void(0)" title="Hapus" onclick="delete_subscriber('."'".$subscriber->subscriber_id."'".')">Delete</a>';

			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->subscriber->count_all(),
						"recordsFiltered" => $this->subscriber->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	 public function delete_subscriber($id)
	{
		$this->subscriber->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	
	
	public function total_enquries()
	
 {
	 
   $data=$this->enquries->get_total_enquries();
       echo json_encode($data);
  
 }
 
 public function total_subscriber()
 {    
      
   $data=$this->subscriber->get_total_subscriber();
      echo json_encode($data);
	
	}
	
}
	
?>
