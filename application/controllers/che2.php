<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Checkout_model','checkout');
		
		
    }

	public function index()
	{
		 
		$this->load->view('checkout1');	  
	}
	
	
	public function booking()
	{
		$TourId=$this->input->post("TourId");
		if($TourId == '')
		{
			 redirect('home');
		}else{
			
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 $data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");
		
		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		
        $this->load->view('checkout1',$data);	
		}	  
		
	}
	
	
	
	public function payment()
	{ 
			
		$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
		// Tour Details.........
	    
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

         $data['SingleTotal']=0;
		 $data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		
		// Passenger Details.........
		 
		 $data['name']=$this->input->post("name");
		 $data['lastname']=$this->input->post("lastname");
		 $data['phone']=$this->input->post("phone");
		 $data['email']=$this->input->post("email");
		
	 
		 $this->load->view('payment',$data);
			
	  }
	}
	

	
		public function payments()
	{ 
			
		$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
			
	   $number = mt_rand(100000000,999999999);
	   $order_data['booking_ref_num']='TRIPOYE' . $number;
	 
	   $order_data['customer_id']="user@gmail.com";
	   $order_data['total']=$this->input->post("DisFullTotal");
	   
	  // $order_id=$this->checkout->save_order($order_data);
	   
	   
	     $data['order_id']=5555;
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

        $data['SingleTotal']=0;
		$data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		//$insert_order_item=$this->checkout->save_order_item($data);
		
		
		
		$data['order_id']=5555;
		 $data['name']=$this->input->post("name");
		 $data['lastname']=$this->input->post("lastname");
		 $data['phone']=$this->input->post("phone");
		 $data['email']=$this->input->post("email");
		
		
		//$insert_traveller_detail=$this->checkout->save_traveller_detail($data_tervler);
			
	
	    //echo json_encode($data);
		 $this->load->view('payment',$data);
			
	  }
	}
	
	
	
	
	
function payu_config() {

    $configarray = array(
     "FriendlyName" => array("Type" => "System", "Value"=>"payu"),
     "MerchantKey" => array("FriendlyName" => "MerchantKey", "Type" => "text", "Size" => "20", ),
     "SALT" => array("FriendlyName" => "SALT", "Type" => "text", "Size" => "20",),
     "mode" => array("FriendlyName" => "mode", "Type" => "text", "Description" => "TEST or LIVE", ),
	
    );
	return $configarray;
}


	public function payu_money_handler()
	{
	
		
	    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	     $firstname=$this->input->post("name");
		 $lastname=$this->input->post("lastname");
		 $phone=$this->input->post("phone");
		 $email=$this->input->post("email");
		 
		 $pickup_point=$this->input->post("pickup_point");
	  	 $pickup_address=$this->input->post("pickup_address");
		
		    $address1="$pickup_point,$pickup_address";;
			$city = "Pune";
			$state="Maharashtra";      		     		
			$postal_code="413601";
			
			$amount="1";
	 
	 
	# Gateway Specific Variables
	$key = "QDFzay";
	$SALT = "X9QPs3la";
	$gatewaymode = "LIVE";
	$productinfo = "trip  PRODUCTs";



   $hashSequence = $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|||||||||||'.$SALT;
   
   
	

  $hash = hash('sha512', $hashSequence);
  
	
	
	# System Variables
	$companyname = 'payu';
	//$systemurl = $params['systemurl'];
    $PostURL = "https://test.payu.in/_payment";
    if($gatewaymode == 'LIVE')
      $PostURL = "https://secure.payu.in/_payment";
	# Enter your code submit to the Payu gateway...
	
	
	?>
<body onload="submitPayuForm()">
<form method="post" name="redirect" action="https://secure.payu.in/_payment"> 


<input type="hidden" name="key" value="<?php  echo $key ?>" />
<input type="hidden" name="productinfo" value="<?php echo $productinfo ?>" />
<input type="hidden" name="txnid" value="<?php echo  $txnid ?>" />
<input type="hidden" name="firstname" value="<?php echo $firstname ?>"/>
<input type="hidden" name="address1" value="<?php  echo $address1 ?>"/>
<input type="hidden" name="city" value="<?php echo $city ?>"/>
<input type="hidden" name="state" value="<?php echo $state ?>"/>

<input type="hidden" name="postal_code" value="<?php  echo $postal_code ?>"/>
<input type="hidden" name="email" value="<?php  echo $email ?>"/>
<input type="hidden" name="phone" value="<?php echo  $phone ?>"/>
<input type="hidden" name="amount" value="<?php echo $amount ?>"/>
<input type="hidden" name="hash" value="<?php echo $hash ?>"/>
<input type="hidden" name="surl" value="http://www.tripoye.com/brothehood1/checkout/invoice_success"/>
<input type="hidden" name="furl" value="http://www.tripoye.com/brothehood1/checkout/invoice_fail" />



</form>
</body>

<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var redirect = document.forms.redirect;
      redirect.submit();
    }
  </script>
  
  
  
  
  	<?php  
	
	}
	
	
		public function invoice_success()
	{
		
		
$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="X9QPs3la";
If (isset($_POST["additionalCharges"])) {
    $additionalCharges=$_POST["additionalCharges"];
    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;        
 } else {      
    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
}
$hash = hash("sha512", $retHashSeq);  
if ($hash != $posted_hash) {
    echo "Invalid Transaction. Please try again";
} else {
    echo "<h3>Your order status is ". $status .".</h3>";
    echo "<h4>Your transaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
} 

	
		
	 	
		
      
  
	}
	
	
	public function invoice_fail()
	{
		$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="X9QPs3la";


If (isset($_POST["additionalCharges"])) 
{
    $additionalCharges=$_POST["additionalCharges"];
    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;        
 } else {      
    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
}


$hash = hash("sha512", $retHashSeq);  

if ($hash != $posted_hash)
	{
    echo "Invalid  fail Transaction. Please try again";
} 
else 
{
    echo "<h3>Your  fail order status is ". $status .".</h3>";
    echo "<h4>Your  failtransaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
} 
		
      
  
	}
	
	
	 
}
?>
