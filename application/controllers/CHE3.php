<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Checkout_model','checkout');
		
		
    }

	public function index()
	{
		 
		$this->load->view('checkout1');	  
	}
	
	
	public function booking()
	{
		$TourId=$this->input->post("TourId");
		if($TourId == '')
		{
			 redirect('home');
		}else{
			
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 $data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");
		
		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		
        $this->load->view('checkout1',$data);	
		}	  
		
	}
	
	
	
	public function payment()
	{ 
			
		$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
		// Tour Details.........
	    
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

         $data['SingleTotal']=0;
		 $data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		
		// Passenger Details.........
		 
		 $data['name']=$this->input->post("name");
		 $data['lastname']=$this->input->post("lastname");
		 $data['phone']=$this->input->post("phone");
		 $data['email']=$this->input->post("email");
		
	 
		 $this->load->view('payment',$data);
			
	  }
	}
	

	
		public function payments()
	{ 
			
		$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
			
	   $number = mt_rand(100000000,999999999);
	   $order_data['booking_ref_num']='TRIPOYE' . $number;
	 
	   $order_data['customer_id']="user@gmail.com";
	   $order_data['total']=$this->input->post("GstFullTotal");
	   
	  // $order_id=$this->checkout->save_order($order_data);
	   
	   
	     $data['order_id']=5555;
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

        $data['SingleTotal']=0;
		$data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		$data['GstFullTotal']=$this->input->post("GstFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		//$insert_order_item=$this->checkout->save_order_item($data);
		
		
		
		 $data['order_id']=5555;
		 $data['name']=$this->input->post("name");
		 $data['lastname']=$this->input->post("lastname");
		 $data['phone']=$this->input->post("phone");
		 $data['email']=$this->input->post("email");
		
		
		//$insert_traveller_detail=$this->checkout->save_traveller_detail($data_tervler);
			
	
	    //echo json_encode($data);
		 $this->load->view('payment',$data);
			
	  }
	}
	
	
	
	
	
function payu_config() {

    $configarray = array(
     "FriendlyName" => array("Type" => "System", "Value"=>"payu"),
     "MerchantKey" => array("FriendlyName" => "MerchantKey", "Type" => "text", "Size" => "20", ),
     "SALT" => array("FriendlyName" => "SALT", "Type" => "text", "Size" => "20",),
     "mode" => array("FriendlyName" => "mode", "Type" => "text", "Description" => "TEST or LIVE", ),
	
    );
	return $configarray;
}


	public function payu_money_handler()
	{
	
/* 	$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
			
	   $number = mt_rand(100000000,999999999);
	   $order_data['booking_ref_num']='TRIPOYE' . $number;
	 
	   $order_data['customer_id']="user@gmail.com";
	   $order_data['total']=$this->input->post("GstFullTotal");
	   
	  $order_id=$this->checkout->save_order($order_data);
	   
	   
	     $data['order_id']=$order_id;
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

        $data['SingleTotal']=0;
		$data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		$data['GstFullTotal']=$this->input->post("GstFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		$insert_order_item=$this->checkout->save_order_item($data);
		
		
		
		 $data_tervler['order_id']=$order_id;
		 $data_tervler['name']=$this->input->post("name");
		 $data_tervler['lastname']=$this->input->post("lastname");
		 $data_tervler['phone']=$this->input->post("phone");
		 $data_tervler['email']=$this->input->post("email");
		
		
		$insert_traveller_detail=$this->checkout->save_traveller_detail($data_tervler);
	
	 */
		
	     $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	     $firstname=$this->input->post("name");
		 $lastname=$this->input->post("lastname");
		 $phone=$this->input->post("phone");
		 $email=$this->input->post("email");
				
		  $amount="1";

		  $order_id="185";
		  //$postal_code=$order_id;
			
	 
	 
	# Gateway Specific Variables
	$key = "gtKFFx";
	$SALT = "eCwWELxi";
	$gatewaymode = "LIVE";
	$productinfo = $order_id;



   $hashSequence = $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|||||||||||'.$SALT;
   
   
	

  $hash = hash('sha512', $hashSequence);
  
	
	
	# System Variables
	$companyname = 'payu';
	//$systemurl = $params['systemurl'];
    $PostURL = "https://test.payu.in/_payment";
    if($gatewaymode == 'LIVE')
      $PostURL = "https://secure.payu.in/_payment";
	# Enter your code submit to the Payu gateway...
	
	
	?>
<body onload="submitPayuForm()">
<form method="post" name="redirect" action="https://test.payu.in/_payment"> 


<input type="hidden" name="key" value="<?php  echo $key ?>" />
<input type="hidden" name="productinfo" value="<?php echo $productinfo ?>" />
<input type="hidden" name="txnid" value="<?php echo  $txnid ?>" />

<input type="hidden" name="firstname" value="<?php echo $firstname ?>"/>




<input type="hidden" name="email" value="<?php  echo $email ?>"/>
<input type="hidden" name="phone" value="<?php echo  $phone ?>"/>

<input type="hidden" name="amount" value="<?php echo $amount ?>"/>
<input type="hidden" name="hash" value="<?php echo $hash ?>"/>

<input type="hidden" name="surl" value="http://localhost/brothehood1/checkout/invoice_success"/>
<input type="hidden" name="furl" value="http://localhost/brothehood1/checkout/invoice_fail" />



</form>
</body>

<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var redirect = document.forms.redirect;
      redirect.submit();
    }
  </script>
  
  
  
  
  	<?php  
	
	//}
	
	}
	
		public function invoice_success()
	{
		
		
         $status=$_POST["status"];
        $firstname=$_POST["firstname"];
        $amount=$_POST["amount"];
        $txnid=$_POST["txnid"];
        $posted_hash=$_POST["hash"];
        $key=$_POST["key"];
         $productinfo=$_POST["productinfo"];
        $email=$_POST["email"];
       // $postal_code=$_POST["postal_code"];
        $salt="eCwWELxi";
		
		
If (isset($_POST["additionalCharges"])) {
    $additionalCharges=$_POST["additionalCharges"];
    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;        
 } else {      
    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
}
$hash = hash("sha512", $retHashSeq);  
if ($hash != $posted_hash) {
    echo "Invalid Transaction. Please try again";
} else {
    echo "<h3>Your order status is ". $status .".</h3>";
    echo "<h4>Your  transaction id for this transaction is----------- ".$txnid."  </br>
	
	
	".$productinfo." </br>".$amount." </br> ".$email."</br>
	
	. You may try making the payment by clicking the link below.</h4>";
} 

	
		
	 	
		
      
  
	}
	
	
	public function invoice_fail()
	{
		$status=$_POST["status"];
        $firstname=$_POST["firstname"];
        $amount=$_POST["amount"];
        $txnid=$_POST["txnid"];
		
        $posted_hash=$_POST["hash"];
        $key=$_POST["key"];
		
        $order_id=$_POST["productinfo"];
        $productinfo="productinfo";
        $email=$_POST["email"];
		
     
		
        $PG_TYPE=$_POST["PG_TYPE"];		
        $bank_ref_num=$_POST["bank_ref_num"];         
        $payment_mode=$_POST["mode"];
      
	  
	
	    $data['payment_status']=$status;
	    $data['payment_mode']=$payment_mode;
	    $data['payment_key']=$key;
	    $data['payment_txnid']=$txnid;
	    $data['payment_transaction_fee']=$amount;
	    $data['payment_amount']=$amount;
	
	   // $data['card_name']=$card_name;
	    $data['payment_addedon']=date('Y-m-d');
	    $data['payment_productinfo']="productinfo";
	    $data['payment_bank_ref_no']=$bank_ref_num;
	    $data['payment_PG_TYPE']=$PG_TYPE;
	    $data['payment_email']=$email;
		
	    $data['payment_hash']=$posted_hash;
		
		
		
	
		
		
		
        $salt="eCwWELxi";


If (isset($_POST["additionalCharges"])) 
{
    $additionalCharges=$_POST["additionalCharges"];
    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;        
 } else {      
 $additionalCharges="sssss";
    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
}


$hash = hash("sha512", $retHashSeq);  

if ($hash != $posted_hash)
	{
		 $addressinfo=$this->checkout->update_online_order($order_id,$data);
    echo "Invalid  fail Transaction. Please try again";
} 
else 
{
	
	
	
   $addressinfo=$this->checkout->update_online_order($order_id,$data);
   
    echo "<h3>Your  fail order status is ". $status .".</h3>";
     echo "<h4>Your  transaction id for this transaction is ".$txnid."  </br>
	
	
	".$firstname." </br>".$amount." </br> ".$email."</br>
	
	
	".$PG_TYPE." </br>".$bank_ref_num." </br>  </br>".$mode."</br>
	
	".$productinfo." </br>
	
	
	. You may try making the payment by clicking the link below.</h4>";
} 
		
      
  
	}
	
	
	 
}
?>
