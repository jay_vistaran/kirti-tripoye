<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Checkout_model','checkout');
		
		
    }

	public function index()
	{
		 
		$this->load->view('checkout1');	  
	}
	
	
	public function booking()
	{
		$TourId=$this->input->post("TourId");
		if($TourId == '')
		{
			 redirect('home');
		}else{
			
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 $data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");
		
		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		
        $this->load->view('checkout1',$data);	
		}	  
		
	}
	
	
	
	public function payment()
	{ 
			
		$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
		// Tour Details.........
	    
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

         $data['SingleTotal']=0;
		 $data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		
		// Passenger Details.........
		 
		 $data['name']=$this->input->post("name");
		 $data['lastname']=$this->input->post("lastname");
		 $data['phone']=$this->input->post("phone");
		 $data['email']=$this->input->post("email");
		
	 
		 $this->load->view('payment',$data);
			
	  }
	}
	

	
		public function payments()
	{ 
			
		$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
			
	   $number = mt_rand(100000000,999999999);
	   $order_data['booking_ref_num']='TRIPOYE' . $number;
	 
	   $order_data['customer_id']="user@gmail.com";
	   $order_data['total']=$this->input->post("DisFullTotal");
	   
	  // $order_id=$this->checkout->save_order($order_data);
	   
	   
	     $data['order_id']=5555;
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

        $data['SingleTotal']=0;
		$data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		//$insert_order_item=$this->checkout->save_order_item($data);
		
		
		
		$data['order_id']=5555;
		 $data['name']=$this->input->post("name");
		 $data['lastname']=$this->input->post("lastname");
		 $data['phone']=$this->input->post("phone");
		 $data['email']=$this->input->post("email");
		
		
		//$insert_traveller_detail=$this->checkout->save_traveller_detail($data_tervler);
			
	
	    //echo json_encode($data);
		 $this->load->view('payment',$data);
			
	  }
	}
	
	
	
	
	
function payu_config() {

    $configarray = array(
     "FriendlyName" => array("Type" => "System", "Value"=>"payu"),
     "MerchantKey" => array("FriendlyName" => "MerchantKey", "Type" => "text", "Size" => "20", ),
     "SALT" => array("FriendlyName" => "SALT", "Type" => "text", "Size" => "20",),
     "mode" => array("FriendlyName" => "mode", "Type" => "text", "Description" => "TEST or LIVE", ),
	
    );
	return $configarray;
}


	public function payu_money_handler()
	{
	
		

    $MERCHANT_KEY = "gtKFFx";
	$SALT = "eCwWELxi";
	$gatewaymode = "LIVE";
	$productinfo = "tripoye PRODUCTs";

$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
$email = $_POST['email'];
$mobile = $_POST['phone'];
$firstName = $_POST['name'];
$lastName = $_POST['lastname'];
$totalCost = $_POST['DisFullTotal'];
$hash         = '';
//Below is the required format need to hash it and send it across payumoney page. UDF means User Define Fields.
//$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
$hash_string = $MERCHANT_KEY."|".$txnid."|".$totalCost."|"."productinfo|".$firstName."|".$email."|||||||||||".$SALT;
$hash = strtolower(hash('sha512', $hash_string));
$action = PAYU_BASE_URL . '/_payment'; 

?>
<form action="https://test.payu.in/_payment" method="post" name="payuForm" id="payuForm" style="display: none">
    <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
    <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
    <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
    <input name="amount" type="number" value="<?php echo $totalCost; ?>" />
    <input type="text" name="firstname" id="firstname" value="<?php echo $firstName; ?>" />
    <input type="email" name="email" id="email" value="<?php echo $email; ?>" />
    <input type="text" name="phone" value="<?php echo $mobile; ?>" />
    <textarea name="productinfo"><?php echo "productinfo"; ?></textarea>
    <input type="text" name="surl" value="http://localhost/brothehood1/checkout/invoice_success"/>
    <input type="text" name="furl" value="http://localhost/brothehood1/checkout/invoice_fail"/>
    <input type="text" name="service_provider" value="payu_paisa"/>
    <input type="text" name="lastname" id="lastname" value="<?php echo $lastName ?>" />
</form>
<script type="text/javascript">
    var payuForm = document.forms.payuForm;
    payuForm.submit();
</script>
<?php
	
	}
	
	
		public function invoice_success()
	{
		$status = $_POST["status"];
$firstname = $_POST["firstname"];
$amount = $_POST["amount"];
$txnid = $_POST["txnid"];
$posted_hash = $_POST["hash"];
$key = $_POST["key"];
$productinfo = $_POST["productinfo"];
$email = $_POST["email"];
$salt = "GQs7yium";

If (isset($_POST["additionalCharges"])) {
    $additionalCharges = $_POST["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
} else {

    $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
}
$hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
    echo "Invalid Transaction. Please try again";
} else {

    echo "<h3>Thank You. Your order status is " . $status . ".</h3>";
    echo "<h4>Your Transaction ID for this transaction is " . $txnid . ".</h4>";
    echo "<h4>We have received a payment of Rs. " . $amount . ". Your order will soon be shipped.</h4>";
}
		
      
  
	}
	
	
	public function invoice_fail()
	{
		$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="fGxoywOg8S";
If (isset($_POST["additionalCharges"])) {
    $additionalCharges=$_POST["additionalCharges"];
    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;        
 } else {      
    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
}
$hash = hash("sha512", $retHashSeq);  
if ($hash != $posted_hash) {
    echo "Invalid Transaction. Please try again";
} else {
    echo "<h3>Your order status is ". $status .".</h3>";
    echo "<h4>Your transaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
} 

	
		
	 	
		
      
  
	}
	
	
	 
}
?>
