<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Checkout_model','checkout');
		$this->load->model('Users_model','users_model');
		
		
    }

	public function index()
	{
		  $TourId=$this->input->post("TourId");
		if($TourId == '')
		{
			 redirect('home');
		}else{
		$this->load->view('checkout1');	  
		}
	}
	
	
	public function booking()
	{
		$TourId=$this->input->post("TourId");
		
				
		 if($TourId == '')
		{
			 redirect('home');
			 
		}else{
			
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 $data['TourName']=$this->input->post("TourName");
		 
		 $data['transfercatname']=$this->input->post("transfercatname");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		
		
		$TourTime=$this->input->post("TourTime");
		
		 if($TourTime == 'Select Time')
		{
			$data['TourTime']="---";
			 
		}
		else
		{
			$data['TourTime']=$this->input->post("TourTime");
		}
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");
		
		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		 $User_id=$this->session->userdata('user_id');
		 $data['profile'] = $this->users_model->get_user_profile($User_id);
		
        $this->load->view('checkout1',$data);	 
		} 

		
		
		
	}
	
	
	
	
	//....#####......ALL Process after Chaeckout,,,,,include applying  coupon.........########.......................
	
	
	public function get_coupon_info($name)
	{
		 
		$data=$this->checkout->get_coupon_info($name);
		echo json_encode($data);
		
	}
	
	
	public function apply_coupon()
	{
		
        
		$coupon_name = $this->input->post("coupon_names");
		$sub_total = $this->input->post("final_totals");
		$TourDate = $this->input->post("TourDates");
		$Tour_discount = $this->input->post("tour_discount");
		
         $flag="0";
		 $flag1="false";
		 $flag1Dis="falseDis";
		 $data = $this->checkout->validate($coupon_name);
		
		 if($data != null)
		{
			foreach($data as $result)
			{
				$coupon_id = $result->id;
				$coupon_name = $result->couponcode;
				$discount_type = $result->coupontype;
				$amount = $result->discountvalue;
				$use_limit = $result->coupon_uses;
				$max_limit = $result->maxnumofuses;
				
				$bookingfrom = $result->bookingfrom;
				$bookingto = $result->bookingto;			
				
				$travellingfrom = $result->travellingfrom;
				$travellingto = $result->travellingto;
			
			} $today = date('Y-m-d');
			 
			 
			 if($Tour_discount <= $amount)			 
			 {
				 
				 
if($today >= $bookingfrom && $today <= $bookingto  && $TourDate >= $travellingfrom  && $TourDate <= $travellingto  && $use_limit < $max_limit )
			{       
		            
				$total_price= $sub_total;
				
				if($discount_type == "A")
				{
					$data_use_limit['coupon_uses']=$use_limit+1;
					$use_limit_inc=$this->checkout->update($coupon_id,$data_use_limit);
					
					
					$total_prices = $total_price - $amount;
			
				    $cart_subtotal=$total_prices;
					
					
					echo json_encode($cart_subtotal);
				}
				else
				{
					$data_use_limit['coupon_uses']=$use_limit+1;
					$use_limit_inc=$this->checkout->update($coupon_id,$data_use_limit);
					
					
			    	$total_prices = $total_price - ($amount/100 * $total_price);
					
					$cart_subtotal=$total_prices;
					
					echo json_encode($cart_subtotal);
					
				}	
			}
			else
			{
							echo json_encode($flag1);
			}

			
			
			
			 }
			else
			{
							echo json_encode($flag1Dis);
			}
			
			
			
		}
		else
		{		
			echo json_encode($flag);
		}
		
	}
	
	
	
	
	
	
	
	
	/* public function payment()
	{ 
			
		$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
		// Tour Details.........
	    
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

         $data['SingleTotal']=0;
		 $data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		
		// Passenger Details.........
		 
		 $data['name']=$this->input->post("name");
		 $data['lastname']=$this->input->post("lastname");
		 $data['phone']=$this->input->post("phone");
		 $data['email']=$this->input->post("email");
		
	 
		 $this->load->view('payment',$data);
			
	  }
	} */
	

	
	
	
  function payu_config()
  {

    $configarray = array(
     "FriendlyName" => array("Type" => "System", "Value"=>"payu"),
     "MerchantKey" => array("FriendlyName" => "MerchantKey", "Type" => "text", "Size" => "20", ),
     "SALT" => array("FriendlyName" => "SALT", "Type" => "text", "Size" => "20",),
     "mode" => array("FriendlyName" => "mode", "Type" => "text", "Description" => "TEST or LIVE", ),
	
    );
	return $configarray;
  }

  
  

	public function payu_money_handler()
	{
		
	$TourId=$this->input->post("TourId");
	   
		if($TourId == '')
		{
			 redirect('home');
			 
		}
		else
		{
			
			
	   
	   $number = mt_rand(1,10);
		$dd= date("d");	
		$mm= date("m");
		$yy= date("Y");
	   $order_data['booking_ref_num']='TRIPOYE' .$dd .$mm .$yy.$number;
	   
	  
	 
	   $order_data['customer_id']=$this->input->post("email");
	   $order_data['total']=$this->input->post("GstFullTotal");
	  
	   $order_data['payment_amount']=$this->input->post("GstFullTotal");
	   
	    $order_data['coupon_applied']=$this->input->post("coupon_applied");
	    $order_data['coupon_code']=$this->input->post("coupon_code");
	    $order_data['coupon_discount']=$this->input->post("coupon_discount");
	    $order_data['discount_total']=$this->input->post("discount_total");
	   
	   
	   
	   
	   
	   $order_data['date_added']= date('Y-m-d H:i:s');
	   $order_data['order_status']=1;
	   
	   
	  $order_id=$this->checkout->save_order($order_data);
	   
	   
	     $data['order_id']=$order_id;
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 
		 $data['ComboId']=0;
		 $data['type']=0;
		 $data['AgeType']=0; 
		 $data['DiscountType']=0; 
		 $data['cart_user_id']="user@gmail.com";
		 $data['SessionId']=1;
		 
		$data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");	

        $data['SingleTotal']=0;
		$data['SingleDisTotal']=0; 		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		$data['GstFullTotal']=$this->input->post("GstFullTotal");
		$data['GstAmount']=$this->input->post("GstAmount");

		
		$data['TransferId']=$this->input->post("TransferId");
		$data['TransferType']=$this->input->post("TransferType");
		
		
		$pickup_point=$this->input->post("pickup_point");
		$pickup_address=$this->input->post("pickup_address");
		
		
		$data['pickup_point'] = "$pickup_point,$pickup_address";
		
		$insert_order_item=$this->checkout->save_order_item($data);
		
		
		
		 $data_tervler['order_id']=$order_id;
		 $data_tervler['name']=$this->input->post("name");
		 $data_tervler['lastname']=$this->input->post("lastname");
		 $data_tervler['phone']=$this->input->post("phone");
		 $data_tervler['email']=$this->input->post("email");
		
		
		$insert_traveller_detail=$this->checkout->save_traveller_detail($data_tervler);
		
		 $email=$this->input->post("email");
		  
		 $Exiting_users=$this->checkout->exiting_users_check($email);
		 if($Exiting_users == [])
		 {
			 
		 $data_users['user_name']=$this->input->post("name");
		 $data_users['user_surname']=$this->input->post("lastname");
		 $data_users['user_mobile']=$this->input->post("phone");
		 $data_users['user_email']=$this->input->post("email");
		 $data_users['user_status']=1;
		 
		 $data_users['date_created']=date('Y-m-d H:i:s');
		 $insert_users=$this->checkout->save_users($data_users);
			 
		 }
		 else
		 {
			 
		 }
		 
		
		
		
		
	     $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	     $firstname=$this->input->post("name");
		 $lastname=$this->input->post("lastname");
		 $phone=$this->input->post("phone");
		 $email=$this->input->post("email");
		 
		 $pickup_point=$this->input->post("pickup_point");
	  	 $pickup_address=$this->input->post("pickup_address");
		
		    $address1="$pickup_point,$pickup_address";;
			$order_id = $order_id;
			$state="Maharashtra";      		     		
			$postal_code="444";
			
			$amount=$this->input->post("GstFullTotal");
		       //$amount="1";
	 
	 
	# Gateway Specific Variables
	//$key = "gtKFFx";
	$key = "QDFzay";   // Server-->
	
	//$SALT = "eCwWELxi";
	$SALT = "X9QPs3la";   //Server-->
	
	$gatewaymode = "LIVE";
	$productinfo = "Trip  PRODUCTs";



   $hashSequence = $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|||||||||||'.$SALT;
   
   
	

  $hash = hash('sha512', $hashSequence);
  
	
	
	# System Variables
	$companyname = 'payu';
	//$systemurl = $params['systemurl'];
    $PostURL = "https://test.payu.in/_payment";
    if($gatewaymode == 'LIVE')
      $PostURL = "https://secure.payu.in/_payment";
	# Enter your code submit to the Payu gateway...
	
	
	?>
<body onload="submitPayuForm()">
<form method="post" name="redirect" action="https://secure.payu.in/_payment"> 


<input type="hidden" name="key" value="<?php  echo $key ?>" />
<input type="hidden" name="productinfo" value="<?php echo $productinfo ?>" />
<input type="hidden" name="txnid" value="<?php echo  $txnid ?>" />
<input type="hidden" name="firstname" value="<?php echo $firstname ?>"/>
<input type="hidden" name="address1" value="<?php  echo $address1 ?>"/>
<input type="hidden" name="city" value="<?php echo $order_id ?>"/>
<input type="hidden" name="state" value="<?php echo $state ?>"/>


<input type="hidden" name="email" value="<?php  echo $email ?>"/>
<input type="hidden" name="phone" value="<?php echo  $phone ?>"/>
<input type="hidden" name="amount" value="<?php echo $amount ?>"/>
<input type="hidden" name="hash" value="<?php echo $hash ?>"/>


<input type="hidden" name="surl" value="https://www.tripoye.com/checkout/invoice_success"/>
<input type="hidden" name="furl" value="https://www.tripoye.com/checkout/invoice_fail" />

<!--
<input type="hidden" name="surl" value="http://localhost/brothehood1/checkout/invoice_success"/>
<input type="hidden" name="furl" value="http://localhost/brothehood1/checkout/invoice_fail" />
-->

</form>
</body>

<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var redirect = document.forms.redirect;
      redirect.submit();
    }
  </script>
  
  
  
  
  	<?php  
	
	}
	
	}
	
		public function invoice_success()
	{
		
		$amount=$_POST["amount"];
		if($amount =='')
		{
			redirect('home');
			
		}else{
		
			$status=$_POST["status"];
			$firstname=$_POST["firstname"];
			$amount=$_POST["amount"];
			$txnid=$_POST["txnid"];
			$posted_hash=$_POST["hash"];
			$key=$_POST["key"];
			$productinfo=$_POST["productinfo"];
			$email=$_POST["email"];
			
			$salt="X9QPs3la";  ///Server--
			//$salt="eCwWELxi";
			
			
			
			$order_id=$_POST["city"]; //.....Order Id 
			$state=$_POST["state"];
			
			
			
			
        $PG_TYPE=$_POST["PG_TYPE"];		
        $bank_ref_num=$_POST["bank_ref_num"];         
        $payment_mode=$_POST["mode"];
      
	  
	
	    $data['payment_status']=$status;
	    $data['payment_unmappedstatus']="captured";
		
	    $data['payment_mode']=$payment_mode;
	    $data['payment_key']=$key;
	    $data['payment_txnid']=$txnid;
	    $data['payment_transaction_fee']=$amount;
	    $data['payment_amount']=$amount;
	
	   // $data['card_name']=$card_name;
	      
	    $data['payment_addedon']= date('Y-m-d H:i:s');
	    $data['payment_productinfo']="productinfo";
	    $data['payment_bank_ref_no']=$bank_ref_num;
	    $data['payment_PG_TYPE']=$PG_TYPE;
	    $data['payment_email']=$email;
		
		 //$data['customer_id']=$email;
		
	    $data['payment_hash']=$posted_hash;
	    $data['payment_cardCategory']="domestic";
		$data['payment_payment_source']="payu";
		$data['payment_Error_Message']="No Error";

                $data['payment_discount']="0.00";
		$data['payment_additional_charges']="0.00";	



		$dd= date("d");	
		$mm= date("m");
		$yy= date("Y");
		
		$Current_today_orderId=$this->checkout->get_count_Todayorder();
		
		foreach($Current_today_orderId as $row)
		{
			$Current_orderId=$row->counts;
			
		}
	       $data['booking_ref_num']='TRIPOYE' .$dd .$mm .$yy.$Current_orderId;		
			
			$booking_ref_nums='TRIPOYE' .$dd .$mm .$yy.$Current_orderId;	
			
			 $addressinfo=$this->checkout->update_online_order($order_id,$data);
			
			$tourInfo=$this->checkout->get_order_tour_details($order_id);
	          
			  foreach($tourInfo as $row)
			  {    
			          
				 $data['TourName']= $row->TourName;				 
				 $data['TourDate']= $row->TourDate;				 
				 
				  
				 $data['TourOptName']= $row->TourOptName;				 
				 $data['TransferName']= $row->TransferName;	
				 
				 $data['AdultQuantity']= $row->AdultQuantity;
				 $data['ChildQuantity']= $row->ChildQuantity;
				 $data['InfantQuantity']= $row->InfantQuantity;
				 
				 
				  $data['FullTotal']= $row->FullTotal;
				  $data['DisFullTotal']= $row->DisFullTotal;
				 
				   $FullTotal= $row->FullTotal;
				   $DisFullTotal= $row->DisFullTotal;
				 
				 $savedTotal= $FullTotal -  $DisFullTotal;
				 
				 $data['savedTotal']=$savedTotal;
				 
				  $data['GstFullTotal']= $row->GstFullTotal;
				 
				  
			         $TourId= $row->TourId;
				  
			  }
			   $tourInfo=$this->checkout->get_tour_details($TourId);
			   
			                  $data['firstname']= $firstname;
			                  $data['tourInfo']=$tourInfo;
							  
				 foreach($tourInfo as $row)
				  {
					  $data['duration']= $row->duration;
					  $data['impinformation']= $row->impinformation;
				  }
			
			
			  If (isset($_POST["additionalCharges"])) 
			 {
			    $additionalCharges=$_POST["additionalCharges"];
			    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;        
			 } 
			 
			 else 
				 
			 {      
			    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
			 }
			  
			  
			   $hash = hash("sha512", $retHashSeq); 
			
						 
				if ($hash != $posted_hash)
					{
						
				        $ci = get_instance();
					    $ci->load->library('email');
					    $config['protocol'] = "smtp";
					    $config['smtp_host'] = "mail.tripoye.com";
					    $config['smtp_port'] = "25";
					    $config['smtp_user'] = "infotest@tripoye.com";
					    $config['smtp_pass'] = "tripoye@123";
					    $config['charset'] = "utf-8";
					    $config['mailtype'] = "html";
					    $config['newline'] = "\r\n";
					    $config['crlf'] = "\r\n";
					
					    $ci->email->initialize($config);
					
					   
					    $ci->email->from('info@tripoye.com');
					
					    
					    $ci->email->to($email);
					   
					    $ci->email->subject('Booking Acknowledgement -'.$booking_ref_nums.' ');
					    
				
				        $body = $this->load->view('booking_acknowledgment',$data,TRUE);

                        $ci->email->message($body); 
				

				//$ci->email->attach( '/test/myfile.pdf');
				$ci->email->send();
			
			
		             //$this->load->view('transcation_fail');	
		           	
			
	
	                   $this->load->view('confirmation',$data);
	
					
				   } 
				   else 
				   {
					            $ci = get_instance();
						    $ci->load->library('email');
						    $config['protocol'] = "smtp";
						    $config['smtp_host'] = "mail.tripoye.com";
						    $config['smtp_port'] = "25";
						    $config['smtp_user'] = "infotest@tripoye.com";
						    $config['smtp_pass'] = "tripoye@123";
						    $config['charset'] = "utf-8";
						    $config['mailtype'] = "html";
						    $config['newline'] = "\r\n";
						    $config['crlf'] = "\r\n";
						
						    $ci->email->initialize($config);
						
						   
						    $ci->email->from('info@tripoye.com');
						
						    
						    $ci->email->to($email);
						   
						   $ci->email->subject('Booking Acknowledgement -'.$booking_ref_nums.' ');
				
							$body = $this->load->view('booking_acknowledgment',$data,TRUE);
			
			                $ci->email->message($body); 
							
			
							//$ci->email->attach( '/test/myfile.pdf');
							$ci->email->send();
						
						
					               //$this->load->view('transcation_fail');	
						
						
				
				                      $this->load->view('confirmation',$data);
	
				   } 	
      
  
	  }
	  
	}
	  
	  
	  
	  
	
	
	public function invoice_fail()
	{
		
		$amount=$_POST["amount"];
		if($amount =='')
		{
			redirect('home');
			
		}else{
			
			$status=$_POST["status"];
			$firstname=$_POST["firstname"];
			$amount=$_POST["amount"];
			$txnid=$_POST["txnid"];
			$posted_hash=$_POST["hash"];
			$key=$_POST["key"];
			$productinfo=$_POST["productinfo"];
			$email=$_POST["email"];
			
			$salt="X9QPs3la";  ///Server--
			//$salt="eCwWELxi";


			$order_id=$_POST["city"];
			$state=$_POST["state"];
		
		
		
		         $PG_TYPE=$_POST["PG_TYPE"];		
                 $bank_ref_num=$_POST["bank_ref_num"];         
                 $payment_mode=$_POST["mode"];
             
	  
	
	    $data['payment_status']=$status;
		
	    $data['payment_mode']=$payment_mode;
	    $data['payment_key']=$key;
	    $data['payment_txnid']=$txnid;
	    $data['payment_transaction_fee']=$amount;
	    $data['payment_amount']=$amount;
	
	   // $data['card_name']=$card_name;
	    
	     $data['payment_addedon']= date('Y-m-d H:i:s');
		 
	    $data['payment_productinfo']="productinfo";
	    $data['payment_bank_ref_no']=$bank_ref_num;
	    $data['payment_PG_TYPE']=$PG_TYPE;
	    $data['payment_email']=$email;
		
		// $data['customer_id']=$email;
		
	    $data['payment_hash']=$posted_hash;
		
		$data['payment_cardCategory']="domestic";
		$data['payment_payment_source']="payu";
		$data['payment_Error_Message']="No Error";
		
		$data['payment_discount']="0.00";
		$data['payment_additional_charges']="0.00";
		
		$dd= date("d");	
		$mm= date("m");
		$yy= date("Y");
		
		$Current_today_orderId=$this->checkout->get_count_Todayorder();
		
	
		foreach($Current_today_orderId as $row)
		{
			$Current_orderId=$row->counts;
			
		}
	   $data['booking_ref_num']='TRIPOYE' .$dd .$mm .$yy.$Current_orderId;	
			
			
			
			
			 $addressinfo=$this->checkout->update_online_order($order_id,$data);
			
			 $tourInfo=$this->checkout->get_order_tour_details($order_id);
	          
			  foreach($tourInfo as $row)
			  {
				 $data['TourName']= $row->TourName;				 
				 $data['TourDate']= $row->TourDate;				 
				 
				  
				 $data['TourOptName']= $row->TourOptName;				 
				 $data['TransferName']= $row->TransferName;	
				 
				 $data['AdultQuantity']= $row->AdultQuantity;
				 $data['ChildQuantity']= $row->ChildQuantity;
				 $data['InfantQuantity']= $row->InfantQuantity;
				 
				 $data['DisFullTotal']= $row->DisFullTotal;
				 
				 $data['GstFullTotal']= $row->GstFullTotal;
				 
				  
			         $TourId= $row->TourId;
				  
			  }
			   $tourInfo=$this->checkout->get_tour_details($TourId);
			   
				 foreach($tourInfo as $row)
				  {
					  $data['duration']= $row->duration;
					  $data['impinformation']= $row->impinformation;
				  }
			
			
			
			
				If (isset($_POST["additionalCharges"])) 
				{
					$additionalCharges=$_POST["additionalCharges"];
					$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;        
				 } else {      
					$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
				}


			$hash = hash("sha512", $retHashSeq);  

			if ($hash != $posted_hash)
				{
				$this->load->view('transcation_fail');
				 } 
			else 
			{	
   
	         $this->load->view('transcation_fail');
    
            } 
		
		}
  
	}
	
	
	 
}
?>
