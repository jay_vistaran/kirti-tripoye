<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Section_manager extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Section_manager_model','section_manager');
    }
	
	
   //.......SECTION Search Bar........
   
	public function get_all_country()
	{
		 $data=$this->section_manager->get_all_country();
	      echo json_encode($data);		
	}	
	
		public function get_city_by_selected_country()
	{
		  $id=$this->input->post("id");
		  $data=$this->section_manager->get_city_by_selected_country($id);
	      echo json_encode($data);		
	}
	
	
		
	
	
    //.......SECTION TRENDING DESTINATIONS........	
	
	public function get_all_treading_destinations_city()
	{
		 $Trending_str=$this->section_manager->get_all_treading_destinations_city();
		 
		     $items = array(); 		
		  if(!empty($Trending_str))
		{	
             $cityid=explode(",",$Trending_str);
         foreach ($cityid as $key => $value)
          {
              $id= $value;
              $datas=$this->section_manager->get_city_by_id($id);
              $items[] = $datas;
          } 		  
	         
echo json_encode($items); 			  
		  }
	else
	{
			
		    
	}		 			 
	}
	
	
	
		 //.......  HOT SELLING SECTION ........		
	
	public function get_all_hot_selling()
	{
		$Hot_str=$this->section_manager->get_all_hot_selling();	
         $items = array(); 		
		  if(!empty($Hot_str))
		{		    
			 $items1 = array();	 
             $tourid=explode(",",$Hot_str);
			 
         foreach ($tourid as $key => $value)
          {
               $id= $value;	
               $tourorder_array[] =$id;	
              $tourorder=$this->section_manager->get_tourdetails_by_id($id);
			  
              $items[] =  $tourorder;			   
			  
		  }	
	         echo json_encode($items); 			  
		  }
	else
	{
			
		    
	}		 
			 
	}
	
	
	
    //....... EXCLUSIVE COMBO DEALS SECTION ........		
	
	public function get_all_exclusive_combo()
	{
		
		 $Exits_Combo_str=$this->section_manager->get_all_exclusive_combo();	
          $items = array(); 		 
		    if(!empty($Exits_Combo_str))
		{
		     
			 $items1 = array(); 
	 
			 $tourorder_array = array(); 
             $comboid=explode(",",$Exits_Combo_str);
			 
         foreach ($comboid as $key => $value)
          {
               $id= $value;			   
			   $tourorder=$this->section_manager->get_tourorder_by_combo_by_id($id);
			   $tours_ids=explode(",",$tourorder);
			   
		 	   
          foreach ($tours_ids as $key => $value)
          {
			  $tours_id= $value;			  
			  $tourorder_array[] =$tours_id;
			  
		  }	 	   
		       
               $tour_otheroption_idlist=$this->section_manager->get_tour_otheroption_base_pack_by_tour_id($tours_ids);
			    
			   $dd=array_shift($tour_otheroption_idlist);
			
		  foreach ($dd as $key => $tour_otheroption_base_pack)
            {
			  $tour_otheroption_ids= $tour_otheroption_base_pack;			  
	    	}	
			
			
			$tour_otheroption_id=explode(",",$tour_otheroption_ids);

              $array=$this->section_manager->get_all_tour_combo_by_id($id,$tourorder_array,$tour_otheroption_id);
			  
              $items[] =  $array;

          }  
                echo json_encode($items); 			  
		  }
	else
	{
			
		    
	}
	         		 
	  }
	
	

	
	
}
?>