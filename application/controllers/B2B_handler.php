<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class B2B_handler extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('B2b_manager_model','b2b_manager');
		
		
    }

	public function index()
	{
		 
		$citylist=$this->b2b_manager->get_all_cities();
		$data['city']=$citylist;
		
       $this->load->view('b2b',$data);	  
	}
	
	
	public function add_booking()
	{
		 $data['TourId']=$this->input->post("TourId");
		 $data['TourOptId']=$this->input->post("TourOptId");
		 $data['TourName']=$this->input->post("TourName");
		 
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		
		$data['AdultPrice']=$this->input->post("AdultPrice");
		$data['ChildPrice']=$this->input->post("ChildPrice");
		
		$data['Discount']=$this->input->post("Discount");
		
		$data['DisAdultPrice']=$this->input->post("DisAdultPrice");
		$data['DisChildPrice']=$this->input->post("DisChildPrice");
		
		
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		$data['InfantQuantity']=$this->input->post("InfantQuantity");
		
		
		
		$data['FullTotal']=$this->input->post("FullTotal");
		$data['DisFullTotal']=$this->input->post("DisFullTotal");
		
		
		echo json_encode($data);
      // $this->load->view('b2b',$data);	  
	}
	
	
	public function add_enquery()
	{
		
		$data['TransferName']=$this->input->post("TransferName");
		$data['TourOptName']=$this->input->post("TourOptName");
		$data['TourDate']=$this->input->post("TourDate");
		$data['TourTime']=$this->input->post("TourTime");
		$data['FinalTotal']=$this->input->post("FinalTotal");
		$data['FinalDisTotal']=$this->input->post("FinalDisTotal");
		$data['AdultQuantity']=$this->input->post("AdultQuantity");
		$data['ChildQuantity']=$this->input->post("ChildQuantity");
		
		echo json_encode($data);
      // $this->load->view('b2b',$data);	  
	}
	
	
	
		function sendMail()
	
    {
		
		
		                $name=$this->input->post('name');
				$email_id=$this->input->post('email_id');
				$phone_no=$this->input->post('phone_no');
				
				$adult=$this->input->post('adult');
				$child=$this->input->post('child');
				$infant=$this->input->post('infant');
				
				$query=$this->input->post('query');
				
				$cityname=$this->input->post('cityname');
				
				
				
    $ci = get_instance();
    $ci->load->library('email');
    $config['protocol'] = "smtp";
    $config['smtp_host'] = "mail.tripoye.com";
    $config['smtp_port'] = "25";
    $config['smtp_user'] = "infotest@tripoye.com";
    $config['smtp_pass'] = "tripoye@123";
    $config['charset'] = "utf-8";
    $config['mailtype'] = "html";
    $config['newline'] = "\r\n";
    $config['crlf'] = "\r\n";

    $ci->email->initialize($config);

   
    $ci->email->from('b2b@tripoye.com');

    
    $ci->email->to('b2b@tripoye.com');
   
    $ci->email->subject('New B2B Enquiry ('.$cityname.')');
    $ci->email->message('Dear Employee,<br> We have recieved an enquiry with below mentioned requirements.<br><br>
				
				
				Travel Agent Name: '.$name.'<br>
				
				
				Email ID: .'.$email_id.'<br>
				Mobile NO: '.$phone_no.'<br>
				
				CityName: '.$cityname.'<br>
				
				Travel Date: "----"<br>
				
				Adults: '.$adult.'<br>
				
				Childrens: '.$child.'<br>
				
				Infant: '.$infant.'<br>
				
				
			
				Message: '.$query.'<br> ');

				//$ci->email->attach( '/test/myfile.pdf');
				$ci->email->send();
			
			
		  $this->load->view('thankyou');	
			
			}
	

}
?>
