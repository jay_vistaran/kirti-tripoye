<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section_manager_model extends CI_Model {

	var $table = 'newsletter';
	
		public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	}
 
 
    public function saveNewsletter($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
		
	public function get_city_by_selected_country($id)
	{
		$this->db->from('city');
		$this->db->where('countryid',$id);
		$query = $this->db->get();
		return $query->result();	
	}
	
	
	
	
	
	public function get_all_country()
	{
		$this->db->select('c.countryname,c.id');
           
		$this->db->from('country as c');	
 	
		$q=$this->db->get();
		return $q->result();
	
	}

	
	public function get_all_city()
	{
		$this->db->select('c.*');
           
		$this->db->from('city as c');	
 	
		$q=$this->db->get();
		return $q->result();
	
	}
	
	public function get_all_treading_destinations_city()
	{
		     $this->db->select('s.trendingorder');
            $this->db->from('section_manager as s');	
            $this->db->where('s.section_name',"TRENDING_DESTINATIONS");					
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->trendingorder;
		
	}
	
	public function get_city_by_id($id)
	{
		$this->db->from('city');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();	
	}
	
	///////////......HOT SELLIING  PACKAGE  ............
	
	public function get_all_hot_selling()
	{
	
		    $this->db->select('s.trendingorder');
            $this->db->from('section_manager as s');	
            $this->db->where('s.section_name',"HOT_SELLING_PACKAGES");			
             $query = $this->db->get();
            $ret = $query->row();
            return $ret->trendingorder;	
	}
      
	
	public function get_tourdetails_by_id($id)
	{  

	
	$Sql="SELECT DISTINCT   t.id,t.tourname,tgi.gallery_img_url,tgi.gallery_img,tp.sellAP as tprice,tp.discount as tdiscount,tas.sellAP,tas.discount,tc.tourcatname
	
          FROM tour as t
    
		 INNER JOIN tour_galleryrimage as tgi
         ON tgi.TourId = t.id
		
		
		INNER JOIN tour_transferoption as tfo
        ON tfo.TourId = t.id
		
		INNER JOIN tour_price as tp
        ON tp.tourTransOptionId = tfo.id
		
		INNER JOIN tour_otheroption as top
        ON top.TourId = t.id
		
		
		INNER JOIN tour_add_services as tas
        ON tas.tourOtherOptionId = top.id
		
		INNER JOIN tour_category as tc
        ON tc.id = t.tourcatid
		
		
		
		where t.id =".$id." AND t.islive = 1 AND tgi.galleryorder=1  AND tfo.isdefault=1  AND top.base_pack=1 AND tp.seasonId=1 AND tp.subTPid=0   AND tas.seasonId=1 ";
		//tas.seasonId =1 new addded on date 14-1-2018 ..prbolem this tour Dudhsagar Waterfall tour with Spice Plantation.
	      $query = $this->db->query($Sql);

          return $query->result();
	
	}
	
	
	
	///////////......EXCLUSIVE Combo PACKAGE ............
	
	public function get_all_exclusive_combo()
	{	
		    $this->db->select('s.trendingorder');
            $this->db->from('section_manager as s');	
            $this->db->where('s.section_name',"EXCLUSIVE_COMBO_DEALS");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->trendingorder;	
			
	}
	
	
  public function get_tourorder_by_combo_by_id($id)
	{	
		    $this->db->select('c.tourorder');
            $this->db->from('tour_combo_t as c');	
            $this->db->where('comboid',$id);			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->tourorder;	
	}
	
	
	
	
	public function get_tour_otheroption_base_pack_by_tour_id($Tourid)
	{	
		  $SQL = "SELECT GROUP_CONCAT(id) as tour_otheroption_base_pack  FROM tour_otheroption where TourId IN (".implode(',',$Tourid).")  AND base_pack=1";
			   
          $query = $this->db->query($SQL);
          return $query->result();
			  
	}
	
	
	
	
	public function get_tour_add_services_by_tour_id($Tourid)
	{
	
		  $SQL = "SELECT  sum(buyAP) as combobuyAP,sum(buyCP) as combobuyCP ,sum(sellAP) as combosellAP,sum(sellCP) as combosellCP   FROM tour_add_services where tourOtherOptionId in (".implode(',', $Tourid).")";
			   
            $query = $this->db->query($SQL);

            return $query->result();
	}
	
	
	
	public function get_all_tour_combo_by_id($id,$tourorder,$tourOtherOptionId)
	{  

	$Sql="SELECT DISTINCT tas.sellAP,tas.buyCP, c.comboid,c.comboname,c.comboimageurl,cp.comboDiscount,ct.tourorder,t.id,t.tourname
	
    FROM tour_combo as c 
	
    INNER JOIN tour_combo_price as cp
        ON cp.ComboId = c.comboid
		
    INNER JOIN tour_combo_t as ct
        ON ct.comboid = c.comboid	
		
    INNER JOIN tour as t
        ON t.id  IN (".implode(',',$tourorder).")		
		
	INNER JOIN tour_otheroption as top
        ON top.TourId = t.id		
				
    INNER JOIN tour_add_services as tas
        ON tas.tourOtherOptionId  = top.id		
		
		
		
		where c.comboid =".$id."  AND top.base_pack=1   AND  tas.tourOtherOptionId in (".implode(',', $tourOtherOptionId).")";
		
	      $query = $this->db->query($Sql);

            return $query->result();	
	
	
	}
	
	
	
	
	
	///.........TOUR RATING HOME PAGE
	
	public function get_users_by_rating_tours($tourid)
	{ 
	    $this->db->select('u.*');
		$this->db->from('tour as t');
		
		$this->db->join('tour_rating as r','r.TourId = t.id');	
        $this->db->join('users as u','u.user_id = r.UserId');
		
		$this->db->where('r.TourId',$tourid);
		$this->db->where('r.approve',"1");
		
		$query = $this->db->get();
		return $query->num_rows();	
		
	}
	
	
	public function get_count_by_rating_tours($tourid)
	{ 
	    $this->db->select_sum('r.tour_rating');
		$this->db->from('tour as t');
		
		$this->db->join('tour_rating as r','r.TourId = t.id');	
        $this->db->join('users as u','u.user_id = r.UserId');
		
		$this->db->where('r.TourId',$tourid);
		$this->db->where('r.approve',"1");
		
		$query = $this->db->get();
		return $query->row_array();	
		
	}
	
}
	
	      
	
	

	


