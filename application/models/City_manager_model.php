<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City_manager_model extends CI_Model {

	
	
		public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	}
 
 
		
	public function get_exits_id_by_city($id)
	{
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"COLLECTION_BY_CITY");			
            $query = $this->db->get();
		    return $query->result();	
		
	}
	
	
	
	public function get_city_by_selected_cityid($id,$city)
	{
		$this->db->from('city');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();	
		
	}
	
	
	
		public function get_tourcatorder_by_city($id)
	{
	
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"COLLECTION_BY_CITY");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->tour_order;
   } 
	
	
	
	
	
 public function get_tourcatdetails_by_id($id)
	{  

	$Sql="SELECT DISTINCT tc.id,tc.tourcatname,tc.doublewidth_url
	
FROM tour_category as tc    	
		
		where tc.id =".$id."  ";
		
	      $query = $this->db->query($Sql);

            return $query->result();
	
	} 
	
	
	
	///////////......HOT SELLING PACKAGE  ............
	
	
	public function get_Selling_exits_id_by_city($id)
	{
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"HOT_SELLING_BY_CITY");			
            $query = $this->db->get();
		    return $query->result();	
		
	}
	
	
		public function get_tourorder_by_city($id)
	{
	
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"HOT_SELLING_BY_CITY");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->tour_order;
   } 

   	public function get_hot_selling_title_by_city($id)
	{
	
		    $this->db->select('s.title');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"HOT_SELLING_BY_CITY");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->title;
   } 
   
   
   
   public function get_tourdetails_by_id($id)
	{  

	$Sql="SELECT DISTINCT   t.id,t.tourname,tgi.gallery_img_url,tgi.gallery_img,tp.sellAP as tprice,tp.discount as tdiscount,tas.sellAP,tas.discount,tc.tourcatname
	
          FROM tour as t
    
		 INNER JOIN tour_galleryrimage as tgi
         ON tgi.TourId = t.id
		
		
		INNER JOIN tour_transferoption as tfo
        ON tfo.TourId = t.id
		
		INNER JOIN tour_price as tp
        ON tp.tourTransOptionId = tfo.id
		
		INNER JOIN tour_otheroption as top
        ON top.TourId = t.id
		
		
		INNER JOIN tour_add_services as tas
        ON tas.tourOtherOptionId = top.id
		
		INNER JOIN tour_category as tc
        ON tc.id = t.tourcatid
		
		where t.id =".$id." AND t.islive = 1 AND tgi.galleryorder=1  AND tfo.isdefault=1   AND tp.seasonId=1  AND top.base_pack=1   AND tp.subTPid=0 AND tas.seasonId=1 ";
		//tas.seasonId =1 new addded on date 14-1-2018 ..prbolem this tour Dudhsagar Waterfall tour with Spice Plantation.
		
	      $query = $this->db->query($Sql);

            return $query->result();
	
	}
	
	
	
	///////////......EXCLUSIVE Combo PACKAGE  ............
	
	public function get_Combo_exits_id_by_city($id)
	{
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"EXCLUSIVE_COMBO_BY_CITY");			
            $query = $this->db->get();
		    return $query->result();	
		
	}
	
		public function get_combo_title_by_city($id)
	{
	
		    $this->db->select('s.title');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"EXCLUSIVE_COMBO_BY_CITY");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->title;
	}
	
	
	
	public function get_all_exclusive_combo($id)
	{
	
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"EXCLUSIVE_COMBO_BY_CITY");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->tour_order;
	}
	
	
  public function get_tourorder_by_combo_by_id($id)
	{
	
		    $this->db->select('c.tourorder');
            $this->db->from('tour_combo_t as c');	
            $this->db->where('comboid',$id);			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->tourorder;	
	}
	
	
	
	
	public function get_tour_otheroption_base_pack_by_tour_id($Tourid)
	{
	
		  $SQL = "SELECT GROUP_CONCAT(id) as tour_otheroption_base_pack  FROM tour_otheroption where TourId IN (".implode(',',$Tourid).")  AND base_pack=1";
			   
           $query = $this->db->query($SQL);
            return $query->result();		
			  
	}
	
	
	
	
	
	
	
	
	public function get_all_tour_combo_by_id($id,$tourorder,$tourOtherOptionId)
	{  

	$Sql="SELECT DISTINCT tas.sellAP,tas.buyCP, c.comboid,c.comboname,c.comboimageurl,cp.comboDiscount,ct.tourorder,t.id,t.tourname
	
FROM tour_combo as c 

    INNER JOIN tour_combo_price as cp
        ON cp.ComboId = c.comboid
		
    INNER JOIN tour_combo_t as ct
        ON ct.comboid = c.comboid	
		
    INNER JOIN tour as t
        ON t.id  IN (".implode(',',$tourorder).")		
		
	INNER JOIN tour_otheroption as top
        ON top.TourId = t.id		
		
		
   INNER JOIN tour_add_services as tas
        ON tas.tourOtherOptionId  = top.id		
		
		
		
		where c.comboid =".$id."  AND top.base_pack=1   AND  tas.tourOtherOptionId in (".implode(',', $tourOtherOptionId).")";
		
	      $query = $this->db->query($Sql);

            return $query->result();
	

	}
	
	
	
	//.........CREATED SECTION....
	
	
		public function get_Created_Sections_exits_id_by_city($id)
	{
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"CREATED_SECTION_BY_CITY");			
            $query = $this->db->get();
		    return $query->result();	
		
	}
	
	public function get_tourcat_tour_exits_by_id($id)
	{
	
		    $this->db->select('c.tour_order');
            $this->db->from('created_section_tour as c');	
			$this->db->where('c.tour_cat_id',$id);			
            $this->db->where('c.section_name',"CREATED_SECTION_TOUR_BY_CITY");			
            $query = $this->db->get();
		    return $query->result();	
			
   } 
	
	
	
	
		public function get_Created_tourcatorder_by_city($id)
	{
	
		    $this->db->select('s.tour_order');
            $this->db->from('section_manager_city as s');	
			$this->db->where('s.city_id',$id);			
            $this->db->where('s.section_name',"CREATED_SECTION_BY_CITY");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->tour_order;
   } 
	
		public function get_tourcat_tour_by_id($id)
	{
	
		    $this->db->select('c.tour_order');
            $this->db->from('created_section_tour as c');	
			$this->db->where('c.tour_cat_id',$id);			
            $this->db->where('c.section_name',"CREATED_SECTION_TOUR_BY_CITY");			
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->tour_order;
			
   } 
   
	
		public function get_Created_all_tour_by_id($tourcatid,$tourorder)
	{  

	
	
	//$Sql="SELECT DISTINCT  tc.tourcatname,tc.id,
        //  tgi.gallery_img_url,tgi.gallery_img,t.tourname,tp.sellAP as tprice,tp.discount,tas.sellAP
	$Sql="SELECT DISTINCT   tc.tourcatname,t.id,t.tourname,tgi.gallery_img_url,tgi.gallery_img,tp.sellAP as tprice,tp.discount as tdiscount,tas.sellAP,tas.discount
	
	
FROM tour_category  as tc

INNER JOIN tour as t
        ON t.tourcatid = tc.id	
		

INNER JOIN tour_galleryrimage as tgi
        ON tgi.TourId = t.id	
		
		

		
   INNER JOIN tour_transferoption as tfo
        ON tfo.TourId = t.id
		
				INNER JOIN tour_price as tp
        ON tp.tourTransOptionId = tfo.id
		
		INNER JOIN tour_otheroption as top
        ON top.TourId = t.id
		
		
		INNER JOIN tour_add_services as tas
        ON tas.tourOtherOptionId = top.id
		 
		
		
		
where tc.id =".$tourcatid."  AND  tgi.galleryorder=1 AND  t.id in (".implode(',', $tourorder).") AND t.islive = 1  AND tfo.isdefault=1  AND tp.seasonId=1  AND top.base_pack=1 AND tp.subTPid=0  AND tas.seasonId=1 ";
		//tas.seasonId =1 new addded on date 14-1-2018 ..prbolem this tour Dudhsagar Waterfall tour with Spice Plantation.
		
	      $query = $this->db->query($Sql);

            return $query->result();
	

	}
	
	
	
	///////  Section Collection Tour category details page(category page)
	
	
	
	public function get_CityId_Cityname($cityname)
	{
	
		    $this->db->select('c.id');
            $this->db->from('city as c');	
			$this->db->where('c.cityname',$cityname);			
           		
            $query = $this->db->get();
            $ret = $query->row();
            return $ret->id;
	}
	
	
	
		public function get_CategoryName_by_tourCatTd($tourCatTd)
	{
	
		    $this->db->select('tc.*');
            $this->db->from('tour_category as tc');	
			$this->db->where('tc.id',$tourCatTd);			
           		
            $query = $this->db->get();
           return $query->result();
	}
		
		
		
		public function get_tour_by_tourCatTd($city_Id,$tourCatTd)
	{
	
		  	$Sql="SELECT DISTINCT   tc.tourcatname,t.id,t.tourname,tgi.gallery_img_url,tgi.gallery_img,tp.sellAP as tprice,tp.discount as tdiscount,tas.sellAP,tas.discount
	
	
FROM tour_category  as tc

INNER JOIN tour as t
        ON t.tourcatid = tc.id	
		

INNER JOIN tour_galleryrimage as tgi
        ON tgi.TourId = t.id	
		
		

INNER JOIN tour_transferoption as tfo
        ON tfo.TourId = t.id
		
INNER JOIN tour_price as tp
        ON tp.tourTransOptionId = tfo.id
		
INNER JOIN tour_otheroption as top
        ON top.TourId = t.id
		
		
INNER JOIN tour_add_services as tas
        ON tas.tourOtherOptionId = top.id
		 
		
		
     where tc.id =".$tourCatTd." AND t.islive = 1  AND tc.cityid=".$city_Id." AND tp.seasonId=1  AND t.cityid=".$city_Id."  AND tfo.isdefault=1  AND top.base_pack=1 AND tp.subTPid=0  AND tgi.galleryorder=1 AND tas.seasonId=1 ";
		//tas.seasonId =1 new addded on date 14-1-2018 ..prbolem this tour Dudhsagar Waterfall tour with Spice Plantation.
		
	      $query = $this->db->query($Sql);

            return $query->result();
			
   }
	
	
	///.........TOUR RATING CITY PAGE
	
	public function get_users_by_rating_tours($tourid)
	{ 
	    $this->db->select('u.*');
		$this->db->from('tour as t');
		
		$this->db->join('tour_rating as r','r.TourId = t.id');	
        $this->db->join('users as u','u.user_id = r.UserId');
		
		$this->db->where('r.TourId',$tourid);
		$this->db->where('r.approve',"1");
		
		$query = $this->db->get();
		return $query->num_rows();	
		
	}
	
	
	public function get_count_by_rating_tours($tourid)
	{ 
	    $this->db->select_sum('r.tour_rating');
		$this->db->from('tour as t');
		
		$this->db->join('tour_rating as r','r.TourId = t.id');	
        $this->db->join('users as u','u.user_id = r.UserId');
		
		$this->db->where('r.TourId',$tourid);
		$this->db->where('r.approve',"1");
		
		$query = $this->db->get();
		return $query->row_array();	
		
	}
	
	
	
	
}
	
	      
	
	

	


