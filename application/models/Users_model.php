<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	
		var $table = 'users';
		
		public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tableName = 'outh_users';
		$this->primaryKey = 'id';
	}
 
 
 public function save($data)
	{
		$data['user_password'] = $this->hash_password($data['user_password']);
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
 
 
 
		
 public function resolve_user_login($data)
{
		$user_email = $data['user_email'];
		$user_password = $data['user_password'];
		
		$this->db->select('user_password');
		$this->db->from('users');
		$this->db->where('user_email', $user_email);
		$hash = $this->db->get()->row('user_password');
		
        return $this->verify_password_hash($user_password, $hash);
      // return $hash;
}



   public function get_user_id_from_username($user_email)
     {
		$this->db->select('user_id');
		$this->db->from('users');
		$this->db->where('user_email', $user_email);
		return $this->db->get()->row('user_id');
     }
	 
	 
      public function get_user($user_id)
      {
		  
		$this->db->from('users');
		$this->db->where('user_id', $user_id);
		return $this->db->get()->result();
      }

	  
	  ///////////USER BOOKING PROFILE............
	  
	  
	  public function get_exits_user_id_from_username($data)
    {  
        $this->db->select('user_id');
		$this->db->from('users');
        $this->db->where('user_email', $data['user_email']);
      
        return $this->db->get()->row('user_id');
    }
	  
	  
	   public function get_user_profile($user_id)
      {
		
		$this->db->from('users');
		$this->db->where('user_id', $user_id);
		return $this->db->get()->result_array();
      }
	  
	  public function update_profile($where,$data)
	{
		$this->db->where('user_id',$where);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}
	  
	  
	  
	  
	  
	    public function get_user_all_booking($user_email)
      {
				$this->db->select('ot.*,o.payment_status,t.duration');
				$this->db->from('order_tripoye as o');
				$this->db->join('order_item as ot','ot.order_id = o.order_id');
				$this->db->join('tour as t','t.id = ot.TourId');
				
				$this->db->where('o.customer_id', $user_email);
				$q=$this->db->get();
		        return $q->result();
      }
	  
	  
	     public function get_order_details_by_id($order_id)
      {
				$this->db->select('ot.*,o.payment_status,t.duration');
				$this->db->from('order_tripoye as o');
				$this->db->join('order_item as ot','ot.order_id = o.order_id');
				$this->db->join('tour as t','t.id = ot.TourId');
				
				$this->db->where('o.order_id', $order_id);
				$q=$this->db->get();
		        return $q->result();
      }
	  
	  
	  
	  
	  
	    public function get_tour_discount_notification()
      {  
	         
				$this->db->select('t.tourname,tp.discount,tc.tourcatname,t.id');
				
				$this->db->from('tour as t');
				
				$this->db->join('tour_price as tp','tp.TourId = t.id');
				$this->db->join('tour_category as tc','tc.id = t.tourcatid');
				
				$this->db->where('tp.discount != ',0);
				
				$this->db->order_by('rand()');
                $this->db->limit(6);
				
				$q=$this->db->get();
		        return $q->result();
      }
	  
	  
	  
	  
	  
	  
	  
	  ////////// SIGN UP WITH GOOGLE
	  
	public function checkUser($data = array())
		
	{
		$this->db->select($this->primaryKey);
		$this->db->from($this->tableName);
		$this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
		$query = $this->db->get();
		$check = $query->num_rows();
		
		if($check > 0)
		{
			$result = $query->row_array();
			$data['modified'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName,$data,array('id'=>$result['id']));
			$userID = $result['id'];
		}
		else
		{
			$data['created'] = date("Y-m-d H:i:s");
			$data['modified'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName,$data);
			$userID = $this->db->insert_id();
		}

		return $userID?$userID:false;
		
    }
	  

     private function hash_password($user_password)
		{
		     return password_hash($user_password, PASSWORD_BCRYPT);
		}


       private function verify_password_hash($user_password, $hash)
        {
          return password_verify($user_password, $hash);
		  
        }
	
	
	
	
	
	
}
	
	      
	
	

	


