<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detailspage_manager_model extends CI_Model {

	var $users ='users';
	var $tour_rating ='tour_rating';
	
		public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	}
 
 
		
	public function get_tour_by_selected_tours($tourid)
	{ 
	    $this->db->select('t.*,bimg.banner_img_url,bimg.banner_img,gimg.gallery_img,gimg.gallery_img_url,c.cityname,tc.tourcatname,tc.id,t.id as tour_id');
		$this->db->from('tour as t');
		$this->db->join('tour_bannerimage as bimg','bimg.TourId = t.id');
		$this->db->join('tour_galleryrimage as gimg','gimg.TourId = t.id');
		$this->db->join('city as c','c.id = t.cityid');
		$this->db->join('tour_category as tc','tc.id = t.tourcatid');
		
		
		$this->db->where('t.id',$tourid);
		$this->db->where('bimg.bannerorder',"1");
		$query = $this->db->get();
		return $query->result();	
		
	}
	
	
	 public function get_tourdetails_by_id($id)
	{  

	$Sql="SELECT DISTINCT   t.id,t.tourname,tgi.gallery_img_url,tgi.gallery_img,tp.sellAP as tprice,tp.discount as tdiscount,tas.sellAP,tas.discount
	
          FROM tour as t
    
		 INNER JOIN tour_galleryrimage as tgi
         ON tgi.TourId = t.id
		
		
		INNER JOIN tour_transferoption as tfo
        ON tfo.TourId = t.id
		
				INNER JOIN tour_price as tp
        ON tp.tourTransOptionId = tfo.id
		
		INNER JOIN tour_otheroption as top
        ON top.TourId = t.id
		
		
		INNER JOIN tour_add_services as tas
        ON tas.tourOtherOptionId = top.id
		
		
		where t.id =".$id." AND tgi.galleryorder=1  AND tfo.isdefault=1  AND top.base_pack=1 AND tp.subTPid=0 ";
		
	      $query = $this->db->query($Sql);

            return $query->result();
	
	}
	
	
	
	
		public function get_tour_transfer_category_by_selected_tours($TourId)
	{ 
	   
		
		  /* $SQL = "SELECT b . * 
             FROM `tour_transferoption` a
             LEFT JOIN transfer_category b ON a.`transfercatId` = b.id
             WHERE a.`TourId` = '" . $tourid . "'
             ORDER BY a.`tforder` ASC"; */
		
		
		 $SQL = "SELECT a . * , c.transfercatname, c.transferoption,c.description,b.transfercatId,b.id as tour_transferoption_id,b.transfercatId as TransferType
                                FROM `tour_price` a
                                LEFT JOIN tour_transferoption b ON a.`tourTransOptionId` = b.id
                                LEFT JOIN transfer_category c ON b.`transfercatId` = c.id
								
                                WHERE a.subTPid=0 and a.seasonId = 1 and  a.`TourId` = '" . $TourId . "'
                                ORDER BY a.`id` ASC  ";
             
		  $query = $this->db->query($SQL);

         return $query->result();
		
	}
	
	
	public function get_tour_Ticket_transfer_category_by_selected_tours($tourid)
	{ 
	   
		
		  $SQL = "SELECT b . * 
             FROM `tour_transferoption` a
             LEFT JOIN transfer_category b ON a.`transfercatId` = b.id
             WHERE a.`TourId` = '" . $tourid . "' AND   b.`transfercatname` = 3 
             ORDER BY a.`tforder` ASC";
		
		  $query = $this->db->query($SQL);

         return $query->result();
		
	}
	
	
		public function get_tour_Option_by_selected_tours($TourId)
	{ 
		 /*  $SQL = "SELECT a . * 
             FROM `tour_otheroption` a
             
             WHERE a.`TourId` = '" . $tourid . "'  "; */
             
			 
			 $SQL = "SELECT a . * , b.*
                                FROM `tour_add_services` a
                                LEFT JOIN `tour_otheroption` b ON a.`tourOtherOptionId` = b.id
                                WHERE a .`seasonId` = 1 and a .`TourId` = '" . $TourId . "' AND a.status=1
                                ORDER BY a.`id` ASC";
		
		  $query = $this->db->query($SQL);

         return $query->result();
		
	}
	
	
	
	public function get_tour_Transfer_Prices_by_selected_tours($TourId)
	{ 
		 $SQL = "SELECT a . * , c.transfercatname, c.transferoption,b.transfercatId 
                                FROM `tour_price` a
                                LEFT JOIN tour_transferoption b ON a.`tourTransOptionId` = b.id
                                LEFT JOIN transfer_category c ON b.`transfercatId` = c.id
								
                                WHERE a.subTPid=0 and a.seasonId = 1 and  a.`TourId` = '" . $TourId . "'
                                ORDER BY a.`id` ASC  ";
             
		
		  $query = $this->db->query($SQL);

         return $query->result();
		
	}
	
	
	
	/////////// TOUR RATING .................
	
	public function get_tourRating_by_selected_tours($tourid)
	{ 
	    $this->db->select('t.*,r.*,u.*');
		$this->db->from('tour as t');
		
		$this->db->join('tour_rating as r','r.TourId = t.id');	
        $this->db->join('users as u','u.user_id = r.UserId');
		
		$this->db->where('r.TourId',$tourid);
		$this->db->where('r.approve',"1");
		
		$query = $this->db->get();
		return $query->result();	
		
	}
	
	
	public function get_users_by_rating_tours($tourid)
	{ 
	    $this->db->select('u.*');
		$this->db->from('tour as t');
		
		$this->db->join('tour_rating as r','r.TourId = t.id');	
        $this->db->join('users as u','u.user_id = r.UserId');
		
		$this->db->where('r.TourId',$tourid);
		$this->db->where('r.approve',"1");
		
		$query = $this->db->get();
		return $query->num_rows();	
		
	}
	
	
	public function get_count_by_rating_tours($tourid)
	{ 
	    $this->db->select_sum('r.tour_rating');
		$this->db->from('tour as t');
		
		$this->db->join('tour_rating as r','r.TourId = t.id');	
        $this->db->join('users as u','u.user_id = r.UserId');
		
		$this->db->where('r.TourId',$tourid);
		$this->db->where('r.approve',"1");
		
		$query = $this->db->get();
		return $query->row_array();	
		
	}
	
	
	public function exiting_users_check($user_email)
	{
		$this->db->select('u.user_id');           
		$this->db->from('users as u');	
		$this->db->where('u.user_email',$user_email);
		$q=$this->db->get();
		return $q->row();
	
	}
	
	
		public function save_users($data)
	{		
		$this->db->insert($this->users, $data);
		$insert_id = $this->db->insert_id();

        return  $insert_id;
	}
	
	
		public function save_rating($data)
	{		
		$this->db->insert($this->tour_rating, $data);
		$insert_id = $this->db->insert_id();

        return  $insert_id;
	}
	
	
	
}
	
	      
	
	

	


