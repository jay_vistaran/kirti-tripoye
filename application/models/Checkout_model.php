<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout_model extends CI_Model {

	var $order_tripoye ='order_tripoye';
	var $order_item ='order_item';
	var $order_traveller_detail ='order_traveller_detail';
	var $users ='users';
	var $tour_coupon ='tour_coupon';
	
		public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	}
 
 
		
   public function get_all_cities()
	{
		$this->db->select('c.*');           
		$this->db->from('city as c');	
		$q=$this->db->get();
		return $q->result();
	
	}
	
	
	public function save_order($data)
	{		
		$this->db->insert($this->order_tripoye, $data);
		$insert_id = $this->db->insert_id();

        return  $insert_id;
	}
	
	
	public function save_order_item($data)
	{		
		$this->db->insert($this->order_item, $data);
		return $this->db->insert_id();
	}
	
	public function save_traveller_detail($data)
	{		
		$this->db->insert($this->order_traveller_detail, $data);
		return $this->db->insert_id();
	}
	
	public function save_users($data)
	{		
		$this->db->insert($this->users, $data);
		$insert_id = $this->db->insert_id();

        return  $insert_id;
	}
	
	
	
	public function update_online_order($order_id,$data)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update($this->order_tripoye, $data);
		return $this->db->affected_rows();
		
		
	}
	
	
	
	
	
	
	
	 public function get_order_tour_details($order_id)
	{
		$this->db->select('o.*');           
		$this->db->from('order_item as o');	
		$this->db->where('order_id',$order_id);
		$q=$this->db->get();
		return $q->result();
	
	} 
	
	
	
	public function get_count_Todayorder()
	{
		 $today =date('Y-m-d');
		$SQL = "SELECT COUNT(*) as counts FROM `order_tripoye` WHERE date_added >=('$today 00:00:00') AND date_added <=('$today 23:59:59')";
		$q=$this->db->query($SQL);
		return $q->result();
	
	}
	
	
	public function get_tour_details($TourId)
	{
		$this->db->select('t.*');           
		$this->db->from('tour as t');	
		$this->db->where('id',$TourId);
		$q=$this->db->get();
		return $q->result();
	
	}
	
	public function exiting_users_check($user_email)
	{
		$this->db->select('u.user_email');           
		$this->db->from('users as u');	
		$this->db->where('u.user_email',$user_email);
		$q=$this->db->get();
		return $q->result();
	
	}
	
	
	
	
	
	//////...........COUPON  APPLY..................
	
	
	
	
	public function get_coupon_info($couponcode)
	{
		$this->db->from($this->tour_coupon);
		$this->db->where('couponcode',$couponcode);
		$query = $this->db->get();

		return $query->row();
	}
	
	
		public function validate($couponcode)	
	{
		$this->db->from('tour_coupon');
		$this->db->where('couponcode',$couponcode);
		$query = $this->db->get();

		return $query->result();	
		
	}
		
	public function update($where,$data)
	{
		$this->db->where('id',$where);
		$this->db->update($this->tour_coupon, $data);
		return $this->db->affected_rows();
	}
	
	
	
	
	
}
	
	      
	
	

	


