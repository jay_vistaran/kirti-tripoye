<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title> Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	
	
</head>

<body class="">

	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_wrapper_padding">

							<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Terms of Use</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><span>Terms of Use</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
			
					<div class="row">
					
						
						<div class="col-sm-12 col-md-12">

							<div class="static-wrapper">
								
							<div class="section-title">
							
								<h3 class="contact_office1">Terms of Use</h3>
								<span class="underline_text"></span>

								<p>Tripoye ("we", “us”, “our” and “Tripoye”) provides the tripoye website that includes its mobile properties (collectively, the "Website"), sub-sites, and other related services to the user of this Website and/or contributing content on this Website (“user” or “you”) subject to your compliance with all the terms, conditions, and notices contained or referenced herein (collectively, the "Terms"). You agree to be bound by these Terms, by accessing and using this Website. Also, when using any particular services or photos, text, location data, images,  and other materials that are uploaded on this Website, users will be subject to any rules that are posted applicable to such services or materials that may contain terms and conditions along with those in these Terms. All such rules or guidelines are incorporated by reference into these Terms.</p>
							
						</div>
					            <p><b>1. Eligibility</b></p>
									
								<p>By using the Services, you warrant that i) all information that you supply are complete, accurate, true, and up-to-date; ii) you are equal to or older than 13 years of age. You shall not access our Services if you are previously banned by us sans special cases that we have approved.</p>
									
								
								<p><b>2. User Account</b></p>
									
								<p>It is imperative for you to create an account and furnish certain information about yourself to use some features in the Services. You are completely responsible all and any use of your account, that includes maintaining the secrecy of your password. You agree to not disclose your password to any third party nor access the account of another member at any time. We reserve the right to close your account at any time without providing any reason.</p>
								
								
								
								<p><b>3. Content</b></p>
									
								<p>The Content and information you post on the Services are owned by you. By posting any content on or via the Services, you grant us non-exclusive, worldwide, transferable, sub-licensable, royalty-free rights to use your Content for any purpose. We may modify, publicly display, translate, use, distribute, reproduce in any media formats. We own the Tripoye Content and Services, which are protected by trade dress, copyright, trademark, patent, or other proprietary rights and laws. Tripoye thus provides you limited license to display and reproduce the Tripoye Content solely for your personal use to view the Website or use the Services. You shall not reproduce, create, distribute, modify related works or publicly display or in any way exploit any Content of Tripoye except when approved by us.</p>
								
								
								
								
								<p><b>4. Use of Services</b></p>
									
								<p>The user must take full responsibility of checking all the information on the website and also ensure that all the information submitted to tripOye or any third party is correct and appropriate. You shall receive the confirmation of all the purchase made on the website via email. Any damage or losses caused by not doing so will not be tripOye’s or any related third party’s responsibility.</p>
								
								
								<p><b>5. Payments & Cancellations</b></p>
									
								<p>TripOye accepts creditcard, net banking and debit card payments. We also accept the following credit cards- Visa, Mastercard and American Express. Full payment by either bank transfer or credit/debit card is madatory to make a reservation. In the event where you choose to pay via banktransfer, you are requested to complete the payment within 24 hours of your booking attempt. Moreover, you must also send a copy of your bank transfer receipt to us so that we can verify the payment. Failing to do this may also result in cancellation of your booking and any loss or damage occurring due to this will be solely your responsibility.</p>
								
								<p>No refunds will be available once a tour or service has commenced, or in respect of any services utilized such as  package, accommodation, meals etc. In view of a booking cancellation, you will be notified via email or any other communication channels that TripOye uses as a means.</p>
								
								
								<p>No exceptions are made to the cancellation policy of each tour or service for any reason, including neither personal or medical, weather or other forces of nature, terrorism, civil unrest, strikes, international or internal (domestic) flight cancellations, nor any other reason beyond our control. We therefore strongly recommend that you purchase a travel insurance to protect yourself from unexpected circumstances that may cause you to cancel your booking.</p>
								
								
								<p><b>6. Restrictions</b></p>
									
								<p>We reserve the right at all times to investigate and take appropriate action at our sole discretion against anyone who violates this provision. Prohibited Content or Activity are as follows :</p>
								
								<p>i) violates or infringes other users’ rights, privacy or intellectual property</p>
								
								<p>ii) incites racism, sexual discrimination, or any type of offensive or malicious act against any group or individual</p>
								<p>iii) creates false, illusive, misleading information or activities</p>
								
								<p>iv) generates promotion or commercial information without TripOye’s prior express written consent</p>
								
								
								<p>v) provides or provokes illegal or unlawful activities</p>
								
								<p>vi) uses other users’ or TripOye Content or modifies the Content for commercial purpose</p>
								
								<p>vii) deliberately writes biased comments for the benefit of another party</p>
								
								<p>viii) solicits personal information from other users</p>
								
								
								
								
								
								
								<p><b>7. Copyright Policy</b></p>
									
								<p>We respect the intellectual property rights of others and expect users of the Services to do the same. If you believe your work has been copied in a way that constitutes copyright infringement, please provide i) identification of the copyrighted work; ii) the location in TripOye where you believe the infringement has happened, and please submit the notification to: info@tripoye.com</p>
								
								
								
								<p><b>8. Privacy Policy</b></p>
									
								<p>Any information that you provide in order to use the Services is subject to our Privacy Policy. By agreeing to these Terms, you are also consenting to our use of your personal information in accordance with our Privacy Policy.</p>
								
								
								
								
								<p><b>9. Warranty Disclaimer</b></p>
									
								<p>ALL MATERIALS AND SERVICES ON THIS WEBSITE ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THE WARRANTY OF NON-INFRINGEMENT. WITHOUT LIMITING THE FOREGOING, WE MAKE NO WARRANTY THAT (I) THE SERVICES AND MATERIALS WILL MEET YOUR REQUIREMENTS, (II) THE SERVICES AND MATERIALS WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, (III) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICES OR MATERIALS WILL BE EFFECTIVE, ACCURATE OR RELIABLE, OR (IV) THE QUALITY OF ANY PRODUCTS, SERVICES, OR INFORMATION PURCHASED OR OBTAINED BY YOU FROM THE WEBSITE FROM US OR OUR AFFILIATES WILL MEET YOUR EXPECTATIONS OR BE FREE FROM MISTAKES, ERRORS OR DEFECTS. THIS WEBSITE COULD INCLUDE TECHNICAL OR OTHER MISTAKES, INACCURACIES OR TYPOGRAPHICAL ERRORS. WE MAY CHANGE, ADD OR REMOVE MATERIALS AND SERVICES ON THE WEBSITE, INCLUDING THE PRICES AND DESCRIPTIONS OF ANY PRODUCTS LISTED HEREIN, AND ITS STRUCTURE AND FUNCTIONS AT ANY TIME AT OUR SOLE DISCRETION, WITHOUT SPECIFICALLY INFORMING OF ANY SUCH CHANGE. THE MATERIALS OR SERVICES ON THIS WEBSITE MAY BE OUT OF DATE, AND WE MAKE NO COMMITMENT TO UPDATE SUCH MATERIALS OR SERVICES. THE USE OF THE SERVICES OR THE DOWNLOADING OR OTHER ACQUISITION OF ANY MATERIALS THROUGH THIS WEBSITE IS DONE AT YOUR OWN DISCRETION AND RISK AND WITH YOUR AGREEMENT THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULT FROM SUCH ACTIVITIES. WE MAKE NO WARRANTY REGARDING ANY TRANSACTIONS EXECUTED THROUGH, OR IN CONNECTION WITH THIS WEBSITE, AND YOU UNDERSTAND AND AGREE THAT SUCH TRANSACTIONS ARE CONDUCTED ENTIRELY AT YOUR OWN RISK. ANY WARRANTY THAT IS PROVIDED IN CONNECTION WITH ANY PRODUCTS, SERVICES MATERIALS, OR INFORMATION AVAILABLE ON OR THROUGH THIS WEBSITE FROM A THIRD PARTY IS PROVIDED SOLELY BY SUCH THIRD PARTY, AND NOT BY US OR ANY OTHER OF OUR AFFILIATES. Content available through this Website often represents the opinions and judgments of an information provider, Website user, or other person or entity not connected with us. We do not endorse, nor are we responsible for the accuracy or reliability of, any opinion, advice, or statement made by anyone other than an authorized TripOye spokesperson speaking in his/her official capacity. Please refer to the specific editorial policies posted on various sections of this Website for further information, which policies are incorporated by reference into these Terms. You understand and agree that temporary interruptions of the Services available through this Website may occur as normal events. You further understand and agree that we have no control over third party networks you may access in the course of the use of this Website, and therefore, delays and disruption of other network transmissions are completely beyond our control. You understand and agree that the Services available on this Website are provided "AS IS" and that we assume no responsibility for the timeliness, deletion, mis-delivery or failure to store any user communications or personalization settings.</p>
								
								
								
								
								<p><b>10. Limitation of Liability</b></p>
									
								<p>IN NO EVENT SHALL WE OR OUR AFFILIATES BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY SPECIAL, PUNITIVE, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING, BUT NOT LIMITED TO, THOSE RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, AND ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OF THIS WEBSITE OR OF ANY WEBSITE REFERENCED OR LINKED TO FROM THIS WEBSITE. FURTHER, WE SHALL NOT BE LIABLE IN ANY WAY FOR THIRD PARTY GOODS AND SERVICES OFFERED THROUGH THIS WEBSITE OR FOR ASSISTANCE IN CONDUCTING COMMERCIAL TRANSACTIONS THROUGH THIS WEBSITE, INCLUDING WITHOUT LIMITATION THE PROCESSING OF ORDERS.</p>
								
								
								
								<p><b>11. Indemnification</b></p>
									
								<p>Upon a request by us, you agree to defend, indemnify, and hold us and our Affiliates harmless from all liabilities, claims, and expenses, including attorney’s fees that arise from i) your use or misuse of this Website; ii) your violation of any term of these Terms. We reserve the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will cooperate with us in asserting any available defenses.</p>
								
								
								
								<p><b>12. Termination of Use</b></p>
									
								<p>You agree that we may, in our sole discretion, terminate or suspend your access to all or part of the Website with or without notice and for any reason, including, without limitation, breach of these Terms. Any suspected fraudulent, abusive or illegal activity may be grounds for terminating your relationship and may be referred to appropriate law enforcement authorities. Upon termination or suspension, regardless of the reasons therefore, your right to use the Services available on this Website immediately ceases, and you acknowledge and agree that we may immediately deactivate or delete your account and all related information and files in your account and/or bar any further access to such files or this Website. We shall not be liable to you or any third party for any claims or damages arising out of any termination or suspension or any other actions taken by us in connection with such termination or suspension. You may terminate the Terms by closing your account or discontinuing your use of the Services, alerting TripOye with a notice of termination. If you close your account, we may continue to display your Content in which it is considered a part of collective contribution.</p>
								
								
								<p><b>13. Contact Information</b></p>
									
								<p>Should you notice that any user is violating these Terms or have questions, please contact us at info@tripoye.com.</p>
								
								<p>We expressly reserve the right to change these Terms from time to time. You acknowledge and agree that it is your responsibility to review this Website and these Terms from time to time and to familiarize yourself with any modifications. Your continued use of this Website after such modifications will constitute acknowledgement of the modified Terms and agreement to abide and be bound by the modified Terms.</p>
								
							</div>
						
						</div>

					</div>

				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->
	
	<?php include ('footer.php')?>

	</div>  <!-- end Container Wrapper -->
 

 
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>

</body>


</html>