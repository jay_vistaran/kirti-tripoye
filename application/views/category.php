<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>
<body class="">

	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

			<!-- start Header -->
		<?php include ('header.php')?>
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
		
		 <?php 
						foreach ($TourcategoryName_array as $row) {
					 foreach($row as $second ){
									$categoryid=$second->id;
									$tourcatname=$second->tourcatname;
									$doublewidth_url=$second->doublewidth_url;
							    }
				             }
							 
									 $cityName=$city_array;
									
							   
				             ?>
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo $doublewidth_url?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title"><?php echo $tourcatname?> Tours & Activities</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								
								<li><a href="<?php echo base_url('tours')?>/<?php echo $cityName ?>"><?php echo $cityName?></a></li>
								
								<li><?php echo $tourcatname?></li>
							
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper" id="content_city">
			
				<div class="container">
				
					<div class="row">
			
			
					<section class="hotel_selling">

				<div class="container">
				
					<div class="row">
						
						<div class="col-sm-8 col-md-9 ">
							
							<div class="section-title">
							
								<h3>Bestseller - <?php echo $cityName; ?></h3>
								<span class="underline_text"></span>
								
							</div>
							
						</div>
					
					</div>
					<div id="carousel1" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false">
							
									<div class="carousel-inner">

						<div class="item active">
							<div class="carousel-col">
							    <div class=" package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight">
									
									
									
									
									
									
									<?php 		
									$i=0;
									foreach ($Tourcategory_array as $row) {
								  foreach($row as $second ){
		
								$id= $second->id;
		
								$tourname= $second->tourname;
								$gallery_img_url= $second->gallery_img_url;
								$gallery_img= $second->gallery_img;
								
								         $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);
										 $tourcatname = str_replace("&", "", $url_tourcatname);
								
										 
										 $url_string1 = preg_replace("/[\s_]/", "-", $second->tourname);
										 $url_string = str_replace("&", "", $url_string1);
									 	 $tour_id=$second->id;
										 $tours="tours";
										
										
				             ?>	
		
										<div class="carousel-col col-md-3 col-lg-3 col-sm-12 col-xs-12 mb-30" id="package-grid-item">
										<a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityName)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
											<div class="image">
												<img src="<?php echo $second->gallery_img_url;?>/<?php echo $second->gallery_img;?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->discount;?>% off</a>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->tdiscount;?>% off</a>
													<?php
													}
													
										}		
													?>
												
													
													<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
													<!--<i class="fa fa-heart-o"></i>--->
													</span>

													</div>
												</div>
											</div>
											 <div class="content clearfix">
											<h5 class="city_name_head"><?php echo $second->tourname;?></h5>
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>	<?php 
											
										foreach(array_slice($TourcategoryTour_rating , $i, 1) as $rating)
											{									
											 $rating;
											
											}
											if($rating == 0)
											{?>
										      Newly Arrived
											<?php
											}
											else
											{
												 echo $rating;
											}
											
											
											?> </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											
											<?php 
											
										foreach(array_slice($TourcategoryTour_rating , $i, 1) as $rating)
											{									
											 $rating;
											
											}
											if($rating == 0)
											{
											}
											else
											{
												?>
										   	 <p class="rating_star">(<?php echo  $rating ;?> ratings)</p>
											<?php
											}
											
											
											?>
											

											</div>
											</div>
											<div class="product-detail-right">
											
											<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 ?>
														<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></span></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														<div class="product-price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}
													
											 }
											          ?>
											
											
											
											</div>
											</div>
									
										</a>
										</div>
										
										
									
										
										
										<?php 
										$i++;
								  }
								  
                             }			
               
                ?>		
										
										
										
										
										
										
									</div>
								</div>
							</div>
						</div>
												<div class="item">
												<div class="carousel-col">
								<div class="GridLex-gap-30-wrappper package-grid-item-wrapper">
								<div class="GridLex-grid-noGutter-equalHeight">
			
							<div class="GridLex-col-3_sm-6_xs-12 mb-30">
								<div class="package-grid-item"> 
									<a href="detail-page.html">
										<div class="image">
											<img src="<?php echo base_url('assets/images/tour-package/02.jpg')?>" alt="Tour Package" />
											<div class="absolute-in-image">
												<div class="duration"><span>10% off</span>
												<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
												<!--<i class="fa fa-heart-o"></i>-->
												</span>

												</div>
											</div>
										</div>
										<div class="content clearfix">
											<h5 >Adriatic Coastal Explorer</h5>
											<div class="rating-wrapper">
												<div class="raty-wrapper">
													<div class="star-rating-read-only" data-rating-score="3.5"></div> <span> / 7 review</span>
												</div>
											</div>
											<div class="absolute-in-content">
												<div class="price1"><strike>£ 1245</strike></div>
												
												<div class="price">£ 1422</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							
							
							<div class="GridLex-col-3_sm-6_xs-12 mb-30">
								<div class="package-grid-item"> 
									<a href="detail-page.html">
										<div class="image">
											<img src="<?php echo base_url('assets/images/tour-package/02.jpg')?>" alt="Tour Package" />
											<div class="absolute-in-image">
												<div class="duration"><span>10% off</span>
												<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
												<!--<i class="fa fa-heart-o"></i>-->
												</span>

												</div>
											</div>
										</div>
										<div class="content clearfix">
											<h5 10% off>Honeymoon Time in Maldives</h5>
											<div class="rating-wrapper">
												<div class="raty-wrapper">
													<div class="star-rating-read-only" data-rating-score="3.5"></div> <span> / 7 review</span>
												</div>
											</div>
											<div class="absolute-in-content">
												<div class="price1"><strike>£ 1245</strike></div>
												
												<div class="price">£ 1422</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							
							<div class="GridLex-col-3_sm-6_xs-12 mb-30">
								<div class="package-grid-item"> 
									<a href="detail-page.html">
										<div class="image">
											<img src="<?php echo base_url('assets/images/tour-package/02.jpg')?>" alt="Tour Package" />
											<div class="absolute-in-image">
												<div class="duration"><span>10% off</span>
												<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
												<!--<i class="fa fa-heart-o"></i>-->
												</span>

												</div>
											</div>
										</div>
										<div class="content clearfix">
											<h5 >Scandinavia Attractions</h5>
											<div class="rating-wrapper">
												<div class="raty-wrapper">
													<div class="star-rating-read-only" data-rating-score="3.5"></div> <span> / 7 review</span>
												</div>
											</div>
											<div class="absolute-in-content">
												<div class="price1"><strike>£ 1245</strike></div>
												
												<div class="price">£ 1422</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							
							<div class="GridLex-col-3_sm-6_xs-12 mb-30">
								<div class="package-grid-item"> 
									<a href="detail-page.html">
										<div class="image">
											<img src="<?php echo base_url('assets/images/tour-package/02.jpg')?>" alt="Tour Package" />
											<div class="absolute-in-image">
												<div class="duration"><span>10% off</span>
												<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
												<!--<i class="fa fa-heart-o"></i>-->
												</span>

												</div>
											</div>
										</div>
										<div class="content clearfix">
											<h5 10% off>Adriatic Coastal Explorer</h5>
											<div class="rating-wrapper">
												<div class="raty-wrapper">
													<div class="star-rating-read-only" data-rating-score="3.5"></div> <span> / 7 review</span>
												</div>
											</div>
											<div class="absolute-in-content">
												<div class="price1"><strike>£ 1245</strike></div>
												
												<div class="price">£ 1422</div>
											</div>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
			</section>
	<!--	<section class="read_more_head">

					<div class=" col-md-12 col-sm-12 col-lg-12 ">
					<div class="read_more">
				<span class="teaser">text goes here this is the  complete text being shownthis is the complete complete text being shownthis is the complete text being text being complete text being shownthis is the complete text being text being is the complete text being text being  text being text being text being   </span>
				<span class="complete"> this is the  complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shown</span>
				<span class="more">more...</span>

			</div>
			</div>
			  
			
			</section>-->
						
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

		
			<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>


<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>

<script>
			
function login()
{
    
    var url;
      
  var id1=document.getElementById("login_username").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
 }
 else{
	
     url = "<?php echo site_url('authenticate/login')?>";
  
   
       var login_username=document.getElementById("login_username").value;
       var login_password=document.getElementById("login_password").value;
      
$.ajax({
        url : url,
        type: "POST",
       //data:('#login-form').serialize(),
		
        data:{user_email:login_username,user_password:login_password},
        dataType: "JSON",
     
        success: function(data)
        {
			
			if(data == true)
			{
				
				 window.location="<?php echo base_url('user-profile')?>";
			}else
			{
				// message="Please Enter Correct Username And Password,,,....";
			   ///document.getElementById("msg66a").innerHTML = message;
			   $("#msg5a").hide();
			   $("#msg66a").show();
			}
			  
     
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
  }
}          
           
     

 </script>	
 
 
 
 <script>
			
function sign_up()
{
    
    var url;
      
  var id1=document.getElementById("register_email").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
	  $("#msg6Sign_up").hide();	
 }
 else{

     url = "<?php echo site_url('authenticate/sign_up')?>";
  
   
       var register_email=document.getElementById("register_email").value;
	   
       var register_username=document.getElementById("register_username").value;
	   
       var register_password=document.getElementById("register_password").value;
      
$.ajax({
        url : url,
        type: "POST",
       //data:('#login-form').serialize(),
		
        data:{user_email:register_email,user_name:register_username,user_password:register_password},
        dataType: "JSON",
     
        success: function(data)
        {
			
			if(data == true)
			{
		         $("#msg6Sign_up").show();				
				// window.location="<?php echo base_url('user-profile')?>";
			}else
			{
				 //message="Please Enter Correct Username And Password,,,....";
			   //document.getElementById("msg66a").innerHTML = message;
			   $("#msg5a").hide();
			   $("#msg66aR").show();
			    $("#msg6Sign_up").hide();	
			}
			  
     
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
	   $("#msg6Sign_up").hide();	
  }
}          
           
     
$('#register_password, #register_password_confirm').on('keyup', function () 
{
  if ($('#register_password').val() == $('#register_password_confirm').val())
	  {
        $('#message').html('Password Matching').css('color', 'green');
     }
     else 
        $('#message').html('Password Not Matching').css('color', 'red');

});
 </script>
</body>

</html>