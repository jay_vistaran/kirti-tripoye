<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">


	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

						<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Careers</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><span>Careers</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
						
			</div>
			
			
			
			
			<div class="content-wrapper pt-0 pb-0">
		
		
			
			
				<div class="featured-wrap">
					  <div class="container">
					  <div class=" col-md-12 col-sm-12 col-lg-12 ">
					  <div class="section-title">
						 <h4 class="carrer_head">Together at Tripoye</h4>
							<span class="underline_text"></span>
					</div>
 
					<p class="para_carrer">We are a rapidly growing company and we would love to add more amazing talent to our bandwagon. We are looking to expand our dynamic team with passionate people who love their work and aims to bring about a positive change with their contribution.
					Are you game for it? If you think you have what it takes to be game changer, send us your applications today!</p>
			</div>
    <div class="heading-title">Current <span>Openings</span></div>
	<br>
    <ul class="row">
  
      <div class="col-md-6 col-sm-6">
        <div class="listWrpService">
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <div class="listImg"><img src="<?php echo base_url('assets/images/feature02.png')?>" alt=""></div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <h3><a href="#">Technical Database Engineer</a></h3>
              <p>Tripoye Tourism Pvt.Ltd.</p>
              <ul class="featureInfo">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i> Pune, Maharashtra</li>
                <li><i class="fa fa-calendar" aria-hidden="true"></i> Dec 30, 2015 - Feb 20, 2016 </li>
              </ul>
              <div class="click-btn"><a href="mailto:info@tripoye.com" >Apply Now</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="listWrpService">
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <div class="listImg"><img src="<?php echo base_url('assets/images/feature03.png')?>" alt=""></div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <h3><a href="#">Senior UI &amp; UX Designer</a></h3>
              <p>Tripoye Tourism Pvt.Ltd.</p>
              <ul class="featureInfo">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i> Pune, Maharashtra</li>
                <li><i class="fa fa-calendar" aria-hidden="true"></i> Dec 30, 2015 - Feb 20, 2016 </li>
              </ul>
              <div class="click-btn"><a href="mailto:info@tripoye.com" >Apply Now</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="listWrpService">
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <div class="listImg"><img src="<?php echo base_url('assets/images/feature02.png')?>" alt=""></div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <h3><a href="#">Marketing Graphic Design / 2D Artist</a></h3>
              <p>Tripoye Tourism Pvt.Ltd.</p>
              <ul class="featureInfo">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i> Pune, Maharashtra</li>
                <li><i class="fa fa-calendar" aria-hidden="true"></i> Dec 30, 2015 - Feb 20, 2016 </li>
              </ul>
              <div class="click-btn"><a href="mailto:info@tripoye.com" >Apply Now</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="listWrpService">
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <div class="listImg"><img src="<?php echo base_url('assets/images/feature04.png')?>" alt=""></div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <h3><a href="#">Business Development Excutive</a></h3>
              <p>Tripoye Tourism Pvt.Ltd.</p>
              <ul class="featureInfo">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i> Pune, Maharashtra</li>
                <li><i class="fa fa-calendar" aria-hidden="true"></i> Dec 30, 2015 - Feb 20, 2016 </li>
              </ul>
              <div class="click-btn"><a href="mailto:info@tripoye.com" >Apply Now</a></div>
            </div>
          </div>
        </div>
      </div>
    
  
  
    </ul>
    <div class="read-btn"><a href="#">View All Featured Jobs</a></div>
  </div>
</div>
				
					
					
						
			</div>
				
				
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->
				<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
</body>


</html>