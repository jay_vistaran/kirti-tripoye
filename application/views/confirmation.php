<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_wrapper_padding">
		<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
			
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title"></h1>
							
							<ol class="breadcrumb-list">
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><a href="#"></a></li>
								<li><span</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			
			<div class="content-wrapper">
			
				<div class="container">
			
					<div class="row">
					
						<div class="col-sm-8 col-md-9">
	
							<div class="confirmation-wrapper">
							
								<div class="payment-success">
								
									<div class="icon">
										<i class="pe-7s-check text-success"></i>
									</div>
								
									<div class="content_trip">
										
										<h2 class="heading uppercase mt-0 text-success">Thank you, your booking is complete!</h2>
										<p>Your Order ID is <span class="text-primary font700 order_id"><strong><?php echo $booking_ref_num; ?></strong></span></p>
									
									</div>
									
								</div>
									<h5 class="confirmation_head">Dear <?php echo $firstname; ?> </h5>							
									<p class="confirm_para">Your Booking is under process, as soon as we confirm from our end, </p>
									<p class="confirm_para">you will get an email confirmation along with attached booking Voucher/Ticket within 1 Business Day of purchase.</p>
									<br>	
							
							
								<div class="confirmation-content">
								
									<div class="section-title text-left">
										<h4>Booking Information</h4>
									</div>
									<br>
								
									<ul class="book-sum-list">
										<li><span class="font600">Tour Name: </span><?php echo $TourName; ?></li>
										<li><span class="font600">Travel Date: </span><?php echo $TourDate; ?></li>
										<li><span class="font600">Duration: </span><?php echo $duration; ?></li>
										<li><span class="font600">No of Adults: </span> <?php echo $AdultQuantity; ?></li>
										<li><span class="font600">No of Child: </span><?php echo $ChildQuantity; ?></li>
										<li><span class="font600">No of Infant: </span><?php echo $InfantQuantity; ?></li>
										<li><span class="font600">Transfer Type: </span><?php echo $TransferName; ?></li>
										<li><span class="font600">Tour Option: </span><?php echo $TourOptName; ?></li>
										<li><span class="font600 order_id">Total Paymemt Paid(Inclusive GST): </span><b>Rs.<?php echo $GstFullTotal; ?></b></li>
										<li><span class="font600">You Saved: </span>Rs.<?php echo $savedTotal; ?> </li>

									</ul>
									
								</div>
								
							<div class="confirmation-content">
							<p class="confirm_para">If you have an urgent concern regarding your enquiry, please refer to your reference no. and contact us directly for faster correspondence.</p>
							<p>Customer Support Email: <span class="text-primary font700">info@tripoye.com</span> and Contact No. : <span class="text-primary font700">+918308625635</span></p>
								
								</div>
								
								<div class="confirmation-content">
								
						 <?php 
						foreach ($tourInfo as $row) {							
                          
						  $inclusion=$row->inclusion;
						  
						  
	
				                      }?>
									
							<?php
			                             if($inclusion == '')
			                                     {
				  
							  }else
							  {?>
			 
			                                          <div class="section-title text-left">
										<h4>INCLUSIONS</h4>
									</div>
	
							<p class="inclusion_demo">
							 <?php					
				
							  echo $inclusion;
							?>
				
					
							</p>
			
								  <?php
								  }
							    ?>
									
									</div>
							</div>
							
						</div>

						<div class="col-sm-4 col-md-3 mt-50-xs">

							<aside class="sidebar with-filter">
							
								<div class="sidebar-inner">
								
									<div class="sidebar-module">
										<h4 class="heading mt-0">Need Booking Help?</h4>
										<div class="sidebar-module-inner">
											<p class="mb-10">Don't Worry ,If you have any concerns you can connect us anytime through below mentioned communication.</p>
											<ul class="help-list">
												<li><span class="font600">Hotline</span>: +918308625635 </li>
												<li><span class="font600">Email</span>: info@tripoye.com</li>
												<li><span class="font600">Whatsapp us</span>: +918308625635</li>

											</ul>
										</div>
									</div>
									
									
									<div class="sidebar-module">
										<h4 class="heading mt-0">Why booking with us?</h4>
										<div class="sidebar-module-inner">
											<ul class="featured-list-sm">
												<li>
													<span class="icon"><i class="fa fa-thumbs-up"></i></span>
													<h6 class="heading mt-0">No Booking Charges</h6>
													We don't charge you an extra fee for booking a hotel room with us
												</li>
												
												<li>
													<span class="icon"><i class="fa fa-calendar"></i></span>
													<h6 class="heading mt-0">Flexible Booking</h6>
													You can book up to a whole year in advance or right up until the moment of your stay
												</li>
											</ul>
										</div>
									</div>
									
								</div>
								
							</aside>

						</div>

					</div>
				
				</div>
					
			</div>

		</div>
		<!-- end Main Wrapper -->
		<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/toggle.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrapDatepickr-1.0.0.min.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
</body>
</html>