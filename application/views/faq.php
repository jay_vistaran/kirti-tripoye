<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title> Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	
	
</head>

<body class="">
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_wrapper_padding">
	
		<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Frequently asked questions</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><span>FAQ</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
			
					<div class="row">
					
					
						
						<div class="col-sm-12 col-md-12">

							<div class="faq-wrapper">
								
								<div id="faq-section-0" class="faq-section">
									<b>FAQ’s</b>


									<p>We understand that you may have a few questions for us. Here are the top most frequent questions answered by our support team for your convenience. We have covered almost all the doubts but still if your question remains unanswered, please contact us at info@tripoye.com  for further enquiries.</p>
							
									<div class="panel-group bootstarp-accordion" id="faq-accordion" role="tablist" aria-multiselectable="true">
										<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-1" aria-expanded="true" aria-controls="faq-accordion-one-1"> 1.	Why to book tours online?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-1" class="panel-collapse collapse in" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
														<p>With TripOye booking tours online have not one, two but four main benefits.</p> 
														<p>•	You save your precious time by booking your nearby or international excursions/tickets  with just a few clicks..</p> 
														<p>	•	With our exclusive online bookings, you may opt for VIP passes to places or skip-the-line tickets and enjoy many perks.</p> 
														<p>•	It's convenient, hassle-free and best for the pocket too..</p> 

													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->

										<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-2" aria-expanded="false" aria-controls="faq-accordion-one-2">2.	Booking tickets online? Is it safe?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-2" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
													
														<p>With TripOye your bookings, whether for an activity or an entire tour is 100% safe. Once you make the payment, it becomes our job to endure you enjoy the best time of your life with us.</p>

													
														
														
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->

										<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-3" aria-expanded="false" aria-controls="faq-accordion-one-3">3.	Is there a facility to pay in cash?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-3" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
													
														<p>We have the highest regard for our customer’s safety and that is why you will never see any of TripOye staff taking cash payments from you. We accept all leading credit and debit cards but unfortunately, we do NOT accept cash payments. If you’ll try our 100% secure gateway you’ll know how safe and easy to use it is.</p>

													
														
													
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->

										<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-4" aria-expanded="false" aria-controls="faq-accordion-one-4">4.	Is it possible to book for a family or a company outing?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-4" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
														<p>We would love to host your family or your organization, and we can also give you a good deal for the same. With TripOye, you can avail a customized tour itinerary too. Contact us today, for a delightful group tour (Mail us at info@tripoye.com / Call us at +918308625635).</p>

														
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->
										
										<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-5" aria-expanded="false" aria-controls="faq-accordion-one-5">5.	I have received an email with a voucher. What is that about?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-5" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
													
														<p>As soon as you make a tour payment with us, you will receive an acknowledgement email from our side & once our team confirms from our side we will send you email with the tour voucher/ticket. This will act as a confirmation from our side that your  tour is booked. This voucher has to be printed and carried along at the time of the start of trip to show the tour operator.</p>

														
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->
										
										<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-6" aria-expanded="false" aria-controls="faq-accordion-one-6">6.	I haven’t received my voucher yet?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-6" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
													
														<p>If you haven’t received your voucher immediately after payment, please wait for 24 hours. If still the voucher is not delivered, please contact our customer care and we’ll help you resolve the issue.</p>

														
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->
										
										<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-7" aria-expanded="false" aria-controls="faq-accordion-one-7">7.	Is it mandatory to print the voucher?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-7" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
													
														<p>Your voucher has the tour itinerary and activity details. You must either print the voucher or show it on the mobile. Having a printout is always a better option.</p>

														
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->
										
											<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-8" aria-expanded="false" aria-controls="faq-accordion-one-8">8.	I wish to cancel/change my booking. What should I do?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-8" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
													
														<p>As soon as you book the tour, your reservations are done and activity advances are already made. This makes it difficult for us to allow withdrawal or cancellations. But we always want to help our customers and in case of a genuine reason of cancel, we might look into refunding some charges wherever applicable. You can get in touch with our customer service team for more details.</p>
														
														<p>Also, we advise all our guests to reach at least 15-30 minutes prior at the starting point. If you’re too late, the trip might start without you. In such a scenario, you will not be eligible for any return or refund.</p>

														
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->
										
											
											<div class="panel">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#faq-accordion-one-9" aria-expanded="false" aria-controls="faq-accordion-one-9">9.	What if an activity or tour I have booked prior gets cancelled?</a>
												</h4>
											</div>
											<div id="faq-accordion-one-9" class="panel-collapse collapse" role="tabpanel">
												<div class="panel-body">
													<div class="faq-thread">
													
													
														<p>If very rare cases when an activity booked is unavailable or cancelled, we propose our guests the next best available date. If that doesn’t suit the guest, we refund the full purchase amount.</p>

														
													</div>
												</div>
											</div>
										</div>
										<!-- end of panel -->
										
										

									</div>
									<!-- end of #accordion -->

								</div>
								
							
								
								

								<div class="call-to-action mt-50">
									
									Can't find a question you wanted? <a href="#" class="btn btn-primary btn-sm btn-inverse">Contact Us</a> for your privilege question.
								
								</div>
								
							</div>
						
						</div>

					</div>

				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

							<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->


<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>

<script>
!function ($) {

  $(function(){

    var $window = $(window)
    var $body   = $(document.body)

    var navHeight = $('.navbar').outerHeight(true) + 50

    $body.scrollspy({
      target: '.scrollspy-sidebar',
      offset: navHeight
    })

    $window.on('load', function () {
      $body.scrollspy('refresh')
    })

    $('.scrollspy-container [href=#]').click(function (e) {
      e.preventDefault()
    })

    // back to top
    setTimeout(function () {
      var $sideBar = $('.scrollspy-sidebar')

      $sideBar.affix({
        offset: {
          top: function () {
            var offsetTop      = $sideBar.offset().top
            var sideBarMargin  = parseInt($sideBar.children(0).css('margin-top'), 10)
            var navOuterHeight = $('.scrollspy-nav').height()

            return (this.top = offsetTop - navOuterHeight - sideBarMargin)
          }
        , bottom: function () {
            return (this.bottom = $('.scrollspy-footer').outerHeight(true))
          }
        }
      })
    }, 100)
		
  })

}(window.jQuery)
</script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
</body>


</html>