<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

		<!-- start Header -->
	<header id="header">
	  
	<!-- start Navbar (Header) -->
	<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
	
	
	<div class="navbar-top" >
	
	<div class="container">
	
	<div class="flex-row flex-align-middle">
	<div class="flex-shrink flex-columns" id="test">
	<a class='navbar-logo' href='<?php echo site_url('home')?>'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>' id="logo1" class='img-responsive'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>' id="logo2" class='img-responsive' style="display:none">
	</a>	
	</div>
	<div class="flex-columns">
	<div class="pull-right">
	
	
	
	
	<div id="navbar" class="collapse navbar-collapse navbar-arrow">
	<ul class="nav navbar-nav" id="responsive-menu">
	<li class="top_nav "><a href="#">USD</a>
	<ul class="dropdown_top">
	<li><a href="#"><i class="ion-social-usd"></i> Dollar</a></li>
	<li><a href="#"><i class="ion-social-euro"></i> Europe</a></li>
	<li><a href="#"><i class="ion-social-yen"></i> Yen</a></li>
	</ul>
	</li>
	
	<li class="top_nav"><a href="#">Download App</a></li>
	<li class="top_nav"><a href="#">Help</a></li>
	<li class="top_nav"><a data-toggle="modal" href="#loginModal">Sign Up</a></li>
	<li class="top_nav"><a href="#">Login</a></li>
	</ul>
	
	</div><!--/.nav-collapse -->
	
	
	</div>
	</div>
	</div>

	</div>
	
	</div>
	
	<div id="slicknav-mobile"></div>


	
	</nav>
	<!-- end Navbar (Header) -->

	</header>
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
			
					<section class="booking_modal_section">
		<div class="tr-register">
			<div class="tr-regi-form">
				<h4 id="booking_modal_h4">Booking by <span>Email</span></h4>
				<p>It's free and always will be.</p>
					<form class="col s12" action="<?php echo base_url('thankyou')?>" method="post">
				
				
					<div class="row">
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
							  <div class="form-group">
							  <input type="hidden" class="form-control"  value="<?php echo $cityname; ?>" name="cityname" >
							  
							<label class="modal_newlabel">Name</label>
							<input type="text" class="form-control" name="name" required>
						  </div>
						  </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						<label  class="modal_newlabel">Email_Id</label>
						<input type="email" class="form-control" name="email_id" required>
					  </div>
					  </div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						<label  class="modal_newlabel">Contact No</label>
						<input type="number" class="form-control" name="phone_no" required>
					  </div>
					  </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
							  <div class="form-group">
							<label  class="modal_newlabel">Adult</label>
								<div class="count-input space-bottom">
                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                <input class="quantity" type="text" name="adult" value="0"/>
                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                            </div>
						  </div>
						  </div>
					</div>
				<div class="row">
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						<label  class="modal_newlabel">Child</label>
						<div class="count-input space-bottom">
                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                <input class="quantity" type="text" name="child" value="0"/>
                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                            </div>
					  </div>
					  </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
					<div class="form-group">
						<label  class="modal_newlabel">Infant</label>
					<div class="count-input space-bottom">
                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                <input class="quantity" type="text" name="infant" value="0"/>
                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                            </div>
					  </div>
					  </div>
				</div>
					<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						 <label  class="modal_newlabel">Your Query</label>
						<textarea class="form-control materialize-textarea" rows="4" name="query" cols="50">
						
						</textarea>
						</div>
					</div>

					</div>
					
					
					<div class="row">
						<div class="input-field col-md-12 col-sm-12">
							<i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style=""><input type="submit" value="submit" class="btn  btn-block submit_btn_new"></i> </div>
					</div>
				</form>
			</div>
		</div>
	</section>
 
			

		</div>
		<!-- end Main Wrapper -->
				<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>

<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
<script>
    $(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below 1
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });

</script>

</body>


</html>