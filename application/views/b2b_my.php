<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/toggle.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrapDatepickr-1.0.0.min.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

		<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Result list</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><a href="#">Desinations</a></li>
								<li><a href="#">City</a></li>
								<li><span>Result list</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
				
					<div class="row">
					
						
						<div class="col-sm-12 col-md-12">
							
							<div class="section-title">
							
								<h3 class="packages_name">Tripoye Cities </h3>
								<span class="underline_text"></span>
								<p>Of distrusts immediate enjoyment curiosity do. Marianne numerous saw thoughts the humoured.</p>
	
							</div>
							
							<div class="GridLex-gap-20-wrappper package-grid-item-wrapper on-page-result-page alt-smaller">
						
								<div class="GridLex-grid-noGutter-equalHeight">
								
													<?php 
				foreach ($city as $row) {
			?>
								
									<div class="b2b-col collections col-md-3 col-sm-6 col-xs-12 mb-30">
									<a href="#">
									<div class="blurt"></div>
										<img src="<?php echo $row->city_img_url;?>" alt="Tour Package">
										<div class="b2b">
										
										
										
             <!--<h3  onclick="open_model(this.id)" id="<?php echo $row->cityname;?>"><?php echo $row->cityname;?></h3>-->
              <a href="<?php echo site_url('booking-modal')?>"><h3  id="<?php echo $row->cityname;?>"><?php echo $row->cityname;?></h3></a>
                                        </div>
									</a>
									</div>
									
									
								<?php 									
                             }	               
                              ?>
								
	
									
								</div>
							
							</div>
							
							
						</div>
						
					</div>
	
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

	
	<footer class="footer scrollspy-footer"> <!-- add scrollspy-footer to stop sidebar scrollspy -->
			
			<div class="container">
			
				<div class="main-footer">
				
					<div class="row">
				
						<div class="col-xs-12 col-sm-4 col-md-4">
						
							<div class="footer-logo">
								<img src="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>" alt="Logo" />
							</div>
							
							<p class="footer-address">Above Keyour Electronics,Office No:2 Karvenagar,Pune-58,India. 
							<br/><br/> <i class="fa fa-phone"></i> +91-9689044624 
							<br/><br/> <i class="fa fa-envelope-o"></i> <a href="mailto:b2b@tripoye.com"> b2b@tripoye.com</a></p>
							
							<div class="footer-social">
							
								<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
							
							</div>
							
							<p class="copy-right">&#169; Copyright 2018 Tripoye. All Rights Reserved</p>
							
						</div>
						
						<div class="col-xs-12 col-sm-8 col-md-8">

							<div class="row gap-10">
							
								<div class="col-xs-12 col-sm-4 col-md-4  mt-30-xs">
								
									<h5 class="footer-title">About Tripoye</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('about')?>">Who we are</a></li>
										<li><a href="<?php echo site_url('career')?>">Careers</a></li>
										<li><a href="<?php echo site_url('contact')?>">Contact Us</a></li>
										<li><a href="<?php echo site_url('b2b')?>">Tripoye B2B</a></li>
							
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Customer Information</h5>
									
									<ul class="footer-menu">
										<li><a href="<?php echo site_url('faq')?>">FAQ</a></li>
										<li><a href="<?php echo site_url('termscondition')?>">Terms & Conditions</a></li>
										<li><a href="<?php echo site_url('privacypolicy')?>">Privacy Policy</a></li>
										<li><a href="#">Blog</a></li>
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Destinations</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('lonavala')?>">Lonavala Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('goa')?>">Goa Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('pune')?>">Pune Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('mumbai')?>">Mumbai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('dubai')?>">Dubai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('singapore')?>">Singapore Tours</a></li>
										<li><a href="#">Kuala Lumpur Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('hong-kong')?>">Homg Kong Tours</a></li>
										
									</ul>
									
								</div>
								
							</div>

						</div>
						
					</div>

				</div>
				
			</div>
			
		</footer>

	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/toggle.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrapDatepickr-1.0.0.min.js')?>"></script>


<script>
function  submit_enquery()
{
	$('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
	
	  url = "<?php echo site_url('b2B_handler/add_enquery')?>";
	
	 $.ajax({
        url : url,
        type: "POST",
        data: $('#Enquery_Form').serialize(),
        dataType: "JSON",
		
        success: function(data)
        {
             console.log(data);
            
            $('#btnSave').text('saved...'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>




<script>
function open_model(cityname)
{  
    $("#myModal2").modal("show");	
	$('#cityname').html(cityname);
}
</script>












<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
	<script>
		$(document).ready(function() {
			$("#calendar").bootstrapDatepickr();
	
		});
		
		
		
	
	</script>
	
	
	<script>
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>

</body>


</html>