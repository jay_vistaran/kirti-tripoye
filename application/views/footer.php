
	<footer class="footer scrollspy-footer"> <!-- add scrollspy-footer to stop sidebar scrollspy -->
			
			<div class="container">
			
				<div class="main-footer">
				
					<div class="row">
				
						<div class="col-xs-12 col-sm-4 col-md-4">
						
							<div class="footer-logo">
								<img src="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>" alt="Logo" />
							</div>
							
							<p class="footer-address">Above Keyour Electronics,Office No:2 Karvenagar,Pune-58,India. 
							<br/><br/> <i class="fa fa-phone"></i> +91-9689044624 
							<br/><br/> <i class="fa fa-envelope-o"></i> <a href="mailto:info@tripoye.com">info@tripoye.com</a></p>
							
							<div class="footer-social">
							
								<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
							
							</div>
							
							<p class="copy-right">&#169; Copyright 2018 Tripoye. All Rights Reserved</p>
							
						</div>
						
						<div class="col-xs-12 col-sm-8 col-md-8">

							<div class="row gap-10">
							
								<div class="col-xs-12 col-sm-4 col-md-4  mt-30-xs">
								
									<h5 class="footer-title">About Tripoye</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('about')?>">Who we are</a></li>
										<li><a href="<?php echo site_url('careers')?>">Careers</a></li>
										<li><a href="<?php echo site_url('contact-us')?>">Contact Us</a></li>
										<li><a href="<?php echo site_url('b2b-destinations')?>">Tripoye B2B</a></li>
							
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Customer Information</h5>
									
									<ul class="footer-menu">
										<li><a href="<?php echo site_url('frequently-asked-questions')?>">FAQ</a></li>
										<li><a href="<?php echo site_url('terms-of-use')?>">Terms & Conditions</a></li>
										<li><a href="<?php echo site_url('privacy-policy')?>">Privacy Policy</a></li>
										<li><a href="#">Blog</a></li>
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Destinations</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('lonavala')?>">Lonavala Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('goa')?>">Goa Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('pune')?>">Pune Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('mumbai')?>">Mumbai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('dubai')?>">Dubai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('singapore')?>">Singapore Tours</a></li>
										<li><a href="#">Kuala Lumpur Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('hong-kong')?>">Hong Kong Tours</a></li>
										
									</ul>
									
								</div>
								
							</div>

						</div>
						
					</div>

				</div>
				
			</div>
			
		</footer>
		
		
		 <script>
			
function login()
{
    
    var url;
      
  var id1=document.getElementById("login_username").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
 }
 else{
	
     url = "<?php echo site_url('authenticate/login')?>";
  
   
       var login_username=document.getElementById("login_username").value;
       var login_password=document.getElementById("login_password").value;
      
$.ajax({
        url : url,
        type: "POST",
       //data:('#login-form').serialize(),
		
        data:{user_email:login_username,user_password:login_password},
        dataType: "JSON",
     
        success: function(data)
        {
			
			if(data == true)
			{
				
				 window.location="<?php echo base_url('user-profile')?>";
			}else
			{
				// message="Please Enter Correct Username And Password,,,....";
			   ///document.getElementById("msg66a").innerHTML = message;
			   $("#msg5a").hide();
			   $("#msg66a").show();
			}
			  
     
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
  }
}          
           
     

 </script>	
 
 
 
 <script>
			
function sign_up()
{
    
    var url;
      
  var id1=document.getElementById("register_email").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
	  $("#msg6Sign_up").hide();	
 }
 else{

     url = "<?php echo site_url('authenticate/sign_up')?>";
  
   
       var register_email=document.getElementById("register_email").value;
	   
       var register_username=document.getElementById("register_username").value;
	   
       var register_password=document.getElementById("register_password").value;
      
$.ajax({
        url : url,
        type: "POST",
       //data:('#login-form').serialize(),
		
        data:{user_email:register_email,user_name:register_username,user_password:register_password},
        dataType: "JSON",
     
        success: function(data)
        {
			
			if(data == true)
			{
		         $("#msg6Sign_up").show();				
				// window.location="<?php echo base_url('user-profile')?>";
			}else
			{
				 //message="Please Enter Correct Username And Password,,,....";
			   //document.getElementById("msg66a").innerHTML = message;
			   $("#msg5a").hide();
			   $("#msg66aR").show();
			    $("#msg6Sign_up").hide();	
			}
			  
     
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
	   $("#msg6Sign_up").hide();	
  }
}          
           
     
$('#register_password, #register_password_confirm').on('keyup', function () 
{
  if ($('#register_password').val() == $('#register_password_confirm').val())
	  {
        $('#message').html('Password Matching').css('color', 'green');
     }
     else 
        $('#message').html('Password Not Matching').css('color', 'red');

});
 </script>