<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/datepicker/bootstrapDatepickr-1.0.0.min.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	<div class="modal fade modal-login modal-border-transparent" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	
	<button type="button" class="btn btn-close close" data-dismiss="modal" aria-label="Close">
	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	</button>
	
	<div class="clear"></div>
	
	<!-- Begin # DIV Form -->
	<div id="modal-login-form-wrapper">
	
	<!-- Begin # Login Form -->
	<form id="login-form">
	
	<div class="modal-body pb-5">
	
	<h4 class="text-center heading mt-10 mb-20">Sign-in</h4>
	
	<button class="btn btn-facebook btn-block">Sign-in with Facebook</button>
	
	<div class="modal-seperator">
	<span>or</span>
	</div>
	
	<div class="form-group"> 
	<input id="login_username" class="form-control" placeholder="username" type="text"> 
	</div>
	<div class="form-group"> 
	<input id="login_password" class="form-control" placeholder="password" type="password"> 
	</div>
	
	<div class="form-group">
	<div class="row gap-5">
	<div class="col-xs-6 col-sm-6 col-md-6">
	<div class="checkbox-block fa-checkbox"> 
	<input id="remember_me_checkbox" name="remember_me_checkbox" class="checkbox" value="First Choice" type="checkbox"> 
	<label class="" for="remember_me_checkbox">remember</label>
	</div>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-6 text-right"> 
	<button id="login_lost_btn" type="button" class="btn btn-link">forgot pass?</button>
	</div>
	</div>
	</div>
	
	</div>
	
	<div class="modal-footer">
	
	<div class="row gap-10">
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-block">Sign-in</button>
	</div>
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-block btn-inverse" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	</div>
	<div class="text-left">
	No account? 
	<button id="login_register_btn" type="button" class="btn btn-link">Register</button>
	</div>
	
	</div>
	</form>
	<!-- End # Login Form -->
	
	<!-- Begin | Lost Password Form -->
	<form id="lost-form" style="display:none;">
	<div class="modal-body pb-5">
	
	<h3 class="text-center heading mt-10 mb-20">Forgot password</h3>
	<div class="form-group mb-10"> 
	<input id="lost_email" class="form-control" type="text" placeholder="Enter Your Email">
	</div>
	
	<div class="text-center">
	<button id="lost_login_btn" type="button" class="btn btn-link">Sign-in</button> or 
	<button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
	</div>
	
	</div>
	
	<div class="modal-footer mt-10">
	
	<div class="row gap-10">
	<div class="col-xs-6 col-sm-6">
	<button type="submit" class="btn btn-primary btn-block">Submit</button>
	</div>
	<div class="col-xs-6 col-sm-6">
	<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	</div>
	
	</div>
	
	</form>
	<!-- End | Lost Password Form -->
	
	<!-- Begin | Register Form -->
	<form id="register-form" style="display:none;">
	
	<div class="modal-body pb-5">
	
	<h3 class="text-center heading mt-10 mb-20">Register</h3>
	
	<button class="btn btn-facebook btn-block">Register with Facebook</button>
	
	<div class="modal-seperator">
	<span>or</span>
	</div>
	
	<div class="form-group"> 
	<input id="register_username" class="form-control" type="text" placeholder="Username"> 
	</div>
	
	<div class="form-group"> 
	<input id="register_email" class="form-control" type="email" placeholder="Email">
	</div>
	
	<div class="form-group"> 
	<input id="register_password" class="form-control" type="password" placeholder="Password">
	</div>
	
	<div class="form-group"> 
	<input id="register_password_confirm" class="form-control" type="password" placeholder="Confirm Password">
	</div>

	</div>
	
	<div class="modal-footer mt-10">
	
	<div class="row gap-10">
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-block">Register</button>
	</div>
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	</div>
	
	<div class="text-left">
	Already have account? <button id="register_login_btn" type="button" class="btn btn-link">Sign-in</button>
	</div>
	
	</div>
	
	</form>
	<!-- End | Register Form -->
	
	</div>
	<!-- End # DIV Form -->
	
	</div>
	</div>
	</div>
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

	<!-- start Header -->
	<header id="header">
	  
	<!-- start Navbar (Header) -->
	<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
	<div onclick="window.location.href='cart.html'" class="cart_div hidden-xs">
	<div class="cart_icon">
	<i class="fa fa-cart-arrow-down"></i>
	</div>
	<div class="cart_value">
	2
	</div>
	</div>
	
	<div class="navbar-top" >
	
	<div class="container">
	
	<div class="flex-row flex-align-middle">
	<div class="flex-shrink flex-columns" id="test">
	<a class='navbar-logo' href='index.html'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>' id="logo1" class='img-responsive'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>' id="logo2" class='img-responsive' style="display:none">
	</a>	
	</div>
	<div class="flex-columns">
	<div class="pull-right">
	
	
	
	
	<div id="navbar" class="collapse navbar-collapse navbar-arrow">
	<ul class="nav navbar-nav" id="responsive-menu">
	<li class="top_nav "><a href="#">USD</a>
	<ul class="dropdown_top">
	<li><a href="#"><i class="ion-social-usd"></i> Dollar</a></li>
	<li><a href="#"><i class="ion-social-euro"></i> Europe</a></li>
	<li><a href="#"><i class="ion-social-yen"></i> Yen</a></li>
	</ul>
	</li>
	
	<li class="top_nav"><a href="#">Download App</a></li>
	<li class="top_nav"><a href="#">Help</a></li>
	<li class="top_nav"><a data-toggle="modal" href="#loginModal">Sign Up</a></li>
	<li class="top_nav"><a href="#">Login</a></li>
	</ul>
	
	</div><!--/.nav-collapse -->
	
	
	</div>
	</div>
	</div>

	</div>
	
	</div>
	
	<div id="slicknav-mobile"></div>


	
	</nav>
	<!-- end Navbar (Header) -->

	</header>
	
	<div class="clear"></div>
	
	<!-- start Main Wrapper -->
	<div class="main-wrapper scrollspy-container">
	 
				       <?php 
						foreach ($tourInfo as $row) {
                          $tourid=$row->id;
			              $tourname=$row->tourname;
						  
			              $banner_img=$row->banner_img;
	                      $banner_img_url=$row->banner_img_url;
						  
						  $tourdescription=$row->tourdescription;
						  $inclusion=$row->inclusion;
						  
						  $duration=$row->duration;
						  $tlocation=$row->tlocation;
						  $tourlanguage=$row->tourlanguage;
	
				         }?>
	<!-- start end Page title -->

	<div class="page-title1 " style="background-image:url('<?php echo $banner_img_url?>/<?php echo $banner_img?>');">
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
	<div class="col-md-offset-1 col-md-8 col-lg-8 col-sm-12 col-xs-12 ">
	<div class="product_image_text ">
	<div class="tag_div">
	<div class="tag">Attractions</div>
	</div>
	<div class="title"><?php echo $tourname; ?></div>
	<div class="subtitle">
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star-o"></i>
	<span>(1 Reviews)</span>
	</div>
	</div>
	</div>
	</div>

	
	</div>
	<!-- end Page title -->
	
	
	<div class="content-wrapper">
	
	<div class="container">
	
	
	

	<div class="row">
	
	
	<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12" role="main">

	<div class="detail-content-wrapper">

	
	<div class="top_icons col-sm-12 col-xs-12">
	
	
	<div class="icon_block col-sm-2 col-xs-4">
	
	<center><img src="<?php echo base_url('assets/images/product_details/icon_1.png')?>" /></center>
	<div class="text"><?php echo $duration?></div>
	</div>
	
	<div class="icon_block col-sm-2 col-xs-4">
	<center><img src="<?php echo base_url('assets/images/product_details/icon_3.png')?>" /></center>
	<div class="text">Availability Daily</div>
	</div>
	
	
	
	
			<?php 
		foreach ($tour_prices as $row) {
									foreach($row as $second ){
									
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<div class="icon_block col-sm-2 col-xs-4">
	<center><img src="<?php echo base_url('assets/images/product_details/icon_5.png')?>" /></center>
	<div class="text">Upto <?php echo $dis=$second->discount;?>% Offers</div>
	</div>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<div class="icon_block col-sm-2 col-xs-4">
	<center><img src="<?php echo base_url('assets/images/product_details/icon_5.png')?>" /></center>
	<div class="text">Upto <?php echo $dis=$second->tdiscount;;?>% Offers</div>
	</div>
													<?php
													}
													
										}		
									}
									
			}
													?>
													
													
	
	
	
	
	<div class="icon_block col-sm-2 col-xs-4">
	<center><img src="<?php echo base_url('assets/images/product_details/icon_6.png')?>" /></center>
	<div class="text"><?php echo $tlocation?></div>
	</div>
	
	
	
	<div class="icon_block col-sm-2 col-xs-4">
	<center><img src="<?php echo base_url('assets/images/product_details/translate.png')?>"/>
	</center>
	<div class="text"><?php echo $tourlanguage?></div>
	</div>	
	</div>
	
	
	<div class="reviews zero_padding col-sm-12 col-xs-12" id="reviews">
	<div class="main_heading">Reviews</div>
	<div class="r_slider_container col-sm-12 col-xs-12">
	<div class="r_slider_nav_btns" id="sliderDown" data-toggle="tooltip" title="Previous">
	<img src="<?php echo base_url('assets/images/product_details/back.png')?>" />
	</div>
	<div id="review_slideshow" class="col-sm-12 col-xs-12">
	<div class="current review col-sm-12 col-xs-12">
	<div class="review_image col-lg-1 col-sm-2 col-xs-3">
	<div>
	<img src="<?php echo base_url('assets/images/product_details/user_image.png')?>" />
	</div>
	</div>
	<div class="review_content col-lg-11 col-sm-10 col-xs-9">
	<div class="content_header">
	<div class="name_date">
	<div class="name">
	Marie Argeris
	</div>
	<div class="date">
	December 15, 2016 at 4:38 pm
	</div>
	</div>
	<div class="review_text">
	<p>
	Statement buttons cover-up tweaks patch pockets perennial lapel collar flap chest pockets topline 
	stitching cropped jacket. Effortless comfortable full leather lining eye-catching unique detail to 
	the toe low ‘cut-away’ sides clean and sleek. Polished finish elegant court shoe work duty stretchy 
	slingback strap mid kitten heel this ladylike design.
	</p>
	</div>
	</div>
	<div class="stars zero_padding col-sm-12 col-xs-12">
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star-o"></i>
	</div>
	</div>
	</div>
	<div class="col-sm-12 col-xs-12">
	<div class="review col-sm-12 col-xs-12">
	<div class="review_image col-lg-1 col-sm-2 col-xs-3">
	<div>
	<img src="<?php echo base_url('assets/images/product_details/user_image.png')?>" />
	</div>
	</div>
	<div class="review_content col-lg-11 col-sm-10 col-xs-9">
	<div class="content_header">
	<div class="name_date">
	<div class="name">
	Adenekan
	</div>
	<div class="date">
	June 1, 2017 at 1:00 pm
	</div>
	</div>
	<div class="review_text">
	<p>
	Amazing experience! The Dubai Aquarium and the Burj Khalifa are situated in Downtown 
	Dubai and hence are a perfect combo experience. We visited the Burj Khalifa in the 
	morning to escape the evening rush. The views from the top are obviously stunning. 
	The Dubai Aquarium was amazing as well. Best aquarium I have been to. The Aquarium 
	ticket is valid for the entire day so you can choose when to go, morning or evening.
	</p>
	</div>
	</div>
	<div class="stars zero_padding col-sm-12 col-xs-12">
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star-o"></i>
	</div>
	</div>
	</div>
	</div>
	<div>
	<div class="review col-sm-12 col-xs-12">
	<div class="review_image col-lg-1 col-sm-2 col-xs-3">
	<div>
	<img src="<?php echo base_url('assets/images/product_details/user_image.png')?>" />
	</div>
	</div>
	<div class="review_content col-lg-11 col-sm-10 col-xs-9">
	<div class="content_header">
	<div class="name_date">
	<div class="name">
	Manoj
	</div>
	<div class="date">
	January 26, 2017 at 10:00 am
	</div>
	</div>
	<div class="review_text">
	<p>
	Overall experience was good but it would have been better if there was an option to 
	visit Burj Khalifa and Aquarium on different days. Also, you should also provide Combo 
	package which include "Explorer pass" for Dubai aquarium. The Combo (Burj + Aquarium) 
	offers only researcher pass to Aquarium which will force the user to miss "Glass boat 
	experience" in Aquarium.
	</p>
	</div>
	</div>
	<div class="stars zero_padding col-sm-12 col-xs-12">
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star"></i>
	<i class="fa fa-star-o"></i>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="r_slider_nav_btns" id="sliderUp" data-toggle="tooltip" title="Next">
	<img src="<?php echo base_url('assets/images/product_details/next.png')?>" />
	</div>
	</div>
	</div>
	
	
	<div class="inclusion zero_padding col-sm-12 col-xs-12" id="inclusions">
	<div class="main_heading">Inclusions</div>
	
	<p class="inclusion_demo">
     <?php
	

	  echo $inclusion;
    ?>

	
	</p>
	
	
	</div>
	<div class="overview zero_padding col-sm-12 col-xs-12" id="overview">
	<div class="main_heading">Overview</div>
	<div class="content">
	<p>
	<?php echo $tourdescription?>.
	</p>
	</div>
	</div>
	
	<div class="slider zero_padding  mb-30 col-sm-12 col-xs-12">
	<div class="main_heading">Gallery</div>

	<div class="slick-gallery-slideshow">
	<div class="slider gallery-slideshow">
	 <?php 
						foreach ($tourInfo as $row) {
                           
	                      $gallery_img_url=$row->gallery_img_url;
						  $gallery_img=$row->gallery_img;
			             ?>
	 <div><div class="image"  id="slider_image"><img src="<?php echo $gallery_img_url; ?>/<?php echo $gallery_img; ?>" alt="Image" /></div></div>
						 
					<?php	 }?> 
	
				         
	
	
	
	
	</div>
	
	<div class="slider gallery-nav">
	
	<?php 
						foreach ($tourInfo as $row) {
                           
	                      $gallery_img_url=$row->gallery_img_url;
						  $gallery_img=$row->gallery_img;
			             ?>
	 <div><div class="image" id="slider_image_bottom"><img src="<?php echo $gallery_img_url?>/<?php echo $gallery_img?>" alt="Image" /></div></div>
						 
					<?php	 }?> 
	
	
	

	
	</div>
	</div>
	</div>
	
	
	
	<div class="things_to_remember zero_padding col-sm-12 col-xs-12" id="things_to_remember">
	<div class="main_heading">Things to Remember</div>
	<ul class="content">
	<li>Prebooked entrance ticket to the Louvre</li>
	<li> Booking fee</li>
	</ul>
	</div>
	<div class="additional_info zero_padding col-sm-12 col-xs-12">
	<div class="main_heading">Additional Information</div>
	<ul class="content">
	<li>  Prebooked entrance ticket to the Louvre</li>
	<li>  Booking fee</li>
	</ul>
	</div>
	<!---<div class="happening_place zero_padding col-sm-12 col-xs-12" id="happening_place">
	<div class="main_heading">Where</div>
	<div class="content">
	<div id="product_details_map"></div>
	</div>
	</div>--->
	<div class="give_reviews zero_padding col-sm-12 col-xs-12" id="give_review" >
	<div class="main_heading">Write a Review</div>
	<div class="content">
	<form class="form-vertical">
	<div class="form-group col-sm-12 col-xs-12">
	<label for="comment">Comment</label>
	<textarea class="form-control" rows="5" id="comment" style="resize:none;border:1px solid #eeeddd;"></textarea>
	</div>
	<div class="form-group col-sm-4 col-xs-12">
	<label for="fname">Name*</label>
	<input type="text" class="form-control" required style="border:1px solid #eeeddd;" id="fname">
	</div>
	<div class="form-group col-sm-4 col-xs-12">
	<label for="email">Email address*</label>
	<input type="email" class="form-control" required style="border:1px solid #eeeddd;" id="email">
	</div>
	<div class="form-group col-sm-4 col-xs-12">
	<label for="contact">Contact:</label>
	<input type="text" class="form-control" style="border:1px solid #eeeddd;" id="contact">
	</div>
	<div class="gv_stars zero_padding col-sm-12 col-xs-12">
	<div class="col-sm-4 col-xs-12">
	<label>Accomodation</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	<div class="col-sm-4 col-xs-12">
	<label>Destination</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	<div class=" col-sm-4 col-xs-12">
	<label>Meals</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	<div class="col-sm-4 col-xs-12">
	<label>Transport</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	<div class=" col-sm-4 col-xs-12">
	<label>Overall</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	</div>
	<div class="form-group submit_btn col-sm-12 col-xs-12">
	<button type="submit" class="btn btn-default">Submit</button>
	</div>
	</form>
	</div>
	</div>
	</div>
	
	</div>

	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
	

	
	
	  <!-- Modal -->
  <div class="modal fade" id="costumModal30" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" >
         
        
        </div>
        <div class="modal-body" id="new_modal_body">
		<div id="myCarousel" class="carousel slide" data-ride="carousel"  data-interval="false">
								 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
					 
					 <!-- First Model-->
					<?php 
					
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					  //  Mutliple Tour Transfer 
					  
					?>
					        <div class="item active">
						 <h4 class="modal-title" id="myModalLabel">What Are You Looking For?</h4>
						 
						 
						 
						  <?php 
						foreach ($tour_transfer as $row) {
                          $transfercatname=$row->transfercatname;
						  
                          $transferoption=$row->transferoption;
						  
			              $description=$row->description;
						  
						   $sellAP=$row->sellAP;
						   $sellCP=$row->sellCP;
						   $discount=$row->discount;
						  
			              $id=$row->id;
						  
						?>
						<div class="col-lg-12 col-md-12 col-sm-12 hr_bottom">
							
							
						 <?php 
						   if(is_array($tour_transfer_Ticket) && count($tour_transfer_Ticket) > 0)
                  {
						foreach ($tour_transfer_Ticket as $rows) {
							
							  $Ticket_transfercatname=$rows->transfercatname;
							  $Ticket_transferoption=$rows->transferoption;
						      $Ticket_id=$rows->id;
						
						      if ($Ticket_transfercatname == '')
						        {
							    ?>
					                 	                     <div class="col-sm-9 modal_detail">
											                 <h5> <?php echo $transferoption; ?> Transfer</h5>
															 
											                 <p><?php echo $description; ?>.</p>
											                  </div>
															  
										<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>" id="<?php echo $id; ?>">
									
															  
															<div class="col-sm-3">
															<a  href="#myCarousel" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" data-slide="next" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													        </div>
							   <?php
								}
								else
								{	
							         if($transfercatname == $Ticket_transfercatname)
										  {?>
								
								
															 <div class="col-sm-9 modal_detail">
											                 <h5> <?php echo $transferoption; ?> Transfer</h5>
															 
											                 <p><?php echo $description; ?>.</p>
											                  </div>
									<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>" id="<?php echo $id; ?>">
															 
															<div class="col-sm-3">
															<a  href="#myCarousel" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" data-slide="next" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													        </div>
										
										<?php
										 }
										else
										 {?> 
									                 <div class="col-sm-9 modal_detail">
													   <h5> <?php if($Ticket_transfercatname == 3) {echo $Ticket_transferoption;} ?>  +   <?php echo $transferoption; ?> Transfer</h5>
													   
							<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>" id="<?php echo $id; ?>">
													   
							
													   
													   <p><?php echo $description; ?>.</p>
													  </div>
													  
													  
													   <div class="col-sm-3">
														<a  href="#myCarousel" data-slide="next" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													    </div>
										
										<?php	
										}			
											
											
								}
						}
				
				}	
				else{
					?>                   	 <div class="col-sm-9 modal_detail">
											                 <h5> <?php echo $transferoption; ?> Transfer</h5>
															 
											                 <p><?php echo $description; ?>.</p>
											                  </div>
															  
																	<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>" id="<?php echo $id; ?>">
															  
															<div class="col-sm-3">
															<a  href="#myCarousel" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" data-slide="next" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													        </div>
						  <?php
						  }
						  ?>
						
							
							
						
							</div>
						
							
							<?php 
							}?> 
							
							</div>
							<?php
						// End Mutliple Tour Transfer 
				  }
				  else
				  {
					   if(is_array($tour_transfer_Ticket) && count($tour_transfer_Ticket) > 0)
                  {
					  //   only Ticket Tour Transfer 
					  ?>
					   
					   
					  <div class="item active">
					  
						<div class="modal-header" >

					<div class="calendar_icon">
					<?php
				foreach ($tour_transfer as $row) {
                          $transferoption=$row->transferoption;
						  $sellAP=$row->sellAP;
			  	} ?>
						<i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?>Transfer</span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						<div class="modal_date_picker">
 <h4 class="modal-title" id="myModalLabel">What Are You Looking For?safasdfasdf</h4>
							  <div class="col-md-9 date_modal">
						<div class="form-group">
						<label class="side_label">Select Date</label>
						<div class="input-group">
							<!--<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
							<input type="text" id="calendar" onclick="show_nextbtn()" placeholder="Select your Date" class="form-control" data-initialized="true" autofocus="autofocus">-->
						<input type='text' class="form-control" id="datetimepicker" name="when" placeholder="When" required autofocus/>
						
						
						</div>
						</div>
								
							</div>
							<div class="col-md-2" id="next_btn" style="display:none">
							<a href="#myCarousel" data-slide="next" id="butdate" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Next<div class="ripple-container"></div></a>
							</div>
						</div>
						</div>
					   
						<?php
						 //  End Ticket Tour Transfer 
				  }else
				  {
					   //  No Ticket Tour Transfer ?>
					 <div class="item active">
						<div class="modal-header" >
						<div class="row">
						<div id="search_modal">
						<div class="calendar_icon">
						<?php
					foreach ($tour_transfer as $row) {
							  $transferoption=$row->transferoption;
							  $sellAP=$row->sellAP;
							   $sellCP=$row->sellCP;
							  $discount=$row->discount;
							 
					} ?>
					  <input type="hidden"  id="discount_price_transfer" value="<?php echo $discount; ?>">
					  <input type="hidden"  id="sellAP_price_transfer" value="<?php echo $sellAP; ?>">
					  <input type="hidden"  id="sellCP_price_transfer" value="<?php echo $sellAP; ?>">					  
					  <i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?>Transfer  </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						</div>
						</div>
						</div>
						<div class="modal_date_picker">
					 <h4 class="modal-title" id="myModalLabel123">What Are You Looking For?</h4>	
							  <div class="col-md-8 date_modal">
						<div class="form-group">
					
						<div class="input-group">
							<input type="text" id="datetimepicker" onclick="show_nextbtn()" placeholder="Select Your Date" class="form-control" >
						</div>
						</div>
								
							</div>
							<div class="col-md-2" id="next_btn" style="display:block">
							<a href="#myCarousel" data-slide="next" id="butdate" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs ">Next<div class="ripple-container"></div></a>
							</div>
						</div>
						</div>
						   <?php						
									  //  End Ticket Tour Transfer 
								  }					 	  
						
							  }
								?>
					
                 <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					 // Mutliple Tour Transfer  selected on date page
					  ?>
					     <div class="item ">
						 				
						<div class="modal-header" >
						<div class="row">
						<div id="search_modal">
						<div class="calendar_icon">
						<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer1"> </span><span>   Transfer</span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						</div>
						</div>
						<div class="modal_date_picker">
						<div class="col-md-6 date_modal">
						<div class="form-group">
						<label class="side_label">Select Date</label>
						<div class="input-group">
						<!--<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
						<input type="text" id="calendar" onclick="show_nextbtn()" placeholder="Select Your Date" class="form-control" data-initialized="true" autofocus="autofocus">-->
												<input type='text' class="form-control" id="datetimepicker" name="when" placeholder="When" data-initialized="true"/>

						</div>
						</div>
								
							</div>
							<div class="col-md-2" id="next_btn" style="display:block">
							<a href="#myCarousel" data-slide="next" id="butdate" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30 ">Next<div class="ripple-container"></div></a>
							</div>
						</div>
						</div>
						<?php
						// End Mutliple Tour Transfer 
				  }else{
					?>
						
						
						
						
				  <?php
				  }
				  ?>
						
					 
					 <!-- Second Model-->	
					
						
						<?php 
				if(is_array($tour_option) && count($tour_option) > 1)
                  {
					  //Multiple Tour Option
					?>
					
				<div class="item">
			

					<div class="modal-header" >
							
						<div class="row">
						<div id="search_modal">
						
								 <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {?>
			  	<div class="col-lg-12 col-md-12 col-sm-12 calendar_icon" style="border-bottom:1px solid #ccc">
								
			<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer4"> Transfer  </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						<?php
				  }
				  else
				  {
					
           foreach ($tour_transfer as $row) {
                          $transferoption=$row->transferoption;
			  	} ?>
					<div class="col-lg-12 col-md-12 col-sm-12 calendar_icon" style="border-bottom:1px solid #ccc">
			<i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?> Transfer </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
					
				  <?php
				  }
				  ?>
				  
				
						<div class="col-lg-12 col-md-12 col-sm-12  calendar_icon" style="border-bottom:1px solid #ccc">
						<i class="fa fa-calendar" aria-hidden="true"></i> <span id="selected_date1"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						
					</div>
					</div>
					</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
						<h4 id="myModalLabel123">Which Tour Option You Want To Pick?</h4>
					  <?php	foreach ($tour_option as $rows) {
							
							  $optionName=$rows->optionName;
							  $description=$rows->description;
							  $option_id=$rows->id;
							  $departuretimess=$rows->departuretime;
							  $sellAP=$rows->sellAP;
							  $sellCP=$rows->sellCP;
							  $discount=$rows->discount;
							  
							  
						?>
						<div class="col-lg-12 col-md-12 col-sm-12  hr_bottom">
						<div id="search_modal">
							<div class="col-sm-9 modal_detail">
							<h5 ><?php echo $optionName; ?></h5>
							<p><?php echo $description; ?>.</p>
							</div>
							
					 <input type="hidden" value="<?php echo $optionName; 

			      if( strpos($departuretimess, ',') !== false )
                                 {
								
                                 }else{
									 echo $departuretimess;
								 }
						  ?>/<?php echo $sellAP;?>/<?php echo $sellCP;?>/<?php echo $discount;?>" id="<?php echo $option_id; ?>">
						   

						  
						  <?php 
						$departuretimess=$rows->departuretime;
						$time=explode(',', $departuretimess);						
						
							
 						if( strpos($departuretimess, ',') !== false )
                              {?>
						  <div class="col-sm-3"  >
							<span style="display:none" id="T<?php echo $option_id; ?>"><a  href="#myCarousel"   onClick="select_option(this.id)" id="<?php echo $option_id; ?>"  data-slide="next"  class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30 mb-10">Choose</a></span>
							<div class="clearfix"></div>
							
							
						  <div class="form-group">
								<select placeholder="Transportation" class="form-control" onChange="select_time(this,this.id);" id="<?php echo $option_id; ?>">
									<option>Select Time</option>						   
						   
									<?php foreach($time as $my_time){?>
						  
										<option value="<?php echo $my_time;  ?>"> 
											<?php echo $my_time;  ?>
										</option>
						
						
									  <?php
									   }
									   ?>
						   
									</select>
							<?php
                             
                                 }else{ ?>
								 <div class="col-sm-3" >
							<a  href="#myCarousel"  onClick="select_option(this.id)" id="<?php echo $option_id; ?>"  data-slide="next"  class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30 mb-10">Choose</a>
							<div class="clearfix"></div>
							
							
						  <div class="form-group">
									<p class="one-time"> <?php echo $departuretimess;?></p>
									<?php }
						  ?>
						    
							
			
						   
						   
						  </div>

						
						  	
						 </div>
						 </div>
						 </div>
						<?php
					}
						?>
									
		                </div>
					
						</div>
						
						<?php 
						
						  // End multi tour Option -->
				  }else{
					 	//one tour Option -->
					  ?>
					  
					  			<div class="item ">

						<div class="modal-header" >
							
						<div class="row">
						
							  <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					  ?>
					  <div class="col-lg-12 col-md-12 col-sm-12 calendar_icon" >
								
			
			<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer3">  Transfer </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						<?php
				  }else
				  {?>
			  <div id="search_modal">
			  <div class=" calendar_icon" >
			  <?php
					  foreach ($tour_transfer as $row) {
                          $transferoption=$row->transferoption;
						  $sellAP=$row->sellAP;
						  $sellCP=$row->sellCP;
						  $discount=$row->discount;
			  	} ?>
			<i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?> Transfer </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						</div>
						<?php
				  }
				  
				?>
						
						<div id="search_modal">
						<div class=" calendar_icon">
					
						<i class="fa fa-calendar" aria-hidden="true"></i> <span id="selected_date2"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						
						
						<div class="col-lg-12 col-md-12 col-sm-12 ">
						<div class="row">
						<div id="search_modal">
							<?php	foreach ($tour_option as $rows) {
							
							  $optionName=$rows->optionName;
							  $departuretime=$rows->departuretime;
							  $option_id=$rows->id;
							   $option_sellAP=$rows->sellAP;							   
							   $option_sellCP=$rows->sellCP;
							   $option_discount=$rows->discount;
							  
						}
						?>
						
						<div class="calendar_icon">
						<i class="fa fa-list-ul" aria-hidden="true"></i> <span> <?php echo $optionName;?>  <?php echo $departuretime;?> </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						</div>
						</div>
						</div>
					</div>
					</div>
				
					<h4 id="myModalLabel123">How Many Guests Joining?</h4>
   
      
	  
<div id="search_final">
  
  		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
    <h4 class="panel-title">
        
		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         <span class="adult_label_parent" id="show_adult"><span>Adult <span id="present_adult">1</span></span></span>
		     <span class="adult_label_parent" id="show_child">Child <span id="present_child">1</span></span>
        </a>
		
		
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
		<div id="search_back_drop">
			<div class="adult_dropdown">
		 <div class=" adult_plus">
					 <div class="form-group">
					 <div class="col-md-4">
					  <label class="side_label1">Adult<br><span class="age_span">Ages 12-99</span> </label>
					  </div>
					 
					 <?php 
					   $dis_AP = ($sellAP * $discount)/100;
					   $sellAP_price = $sellAP -  $dis_AP;
					   
					    $dis_TourAP = ($option_sellAP * $option_discount)/100;
					    $Tour_sellAP = $option_sellAP -  $dis_TourAP;
					   
					    
					    $dis_finalSellAP =  $sellAP_price +  $Tour_sellAP;
					    $finalSellAP = $sellAP + $option_sellAP;
					   ?>
			<input type="hidden"  value="<?php echo round($dis_finalSellAP);?> "id="hiddenfinalsellAP_price_transfer">
					   
					   
					    <input type="hidden" value="<?php echo $finalSellAP; ?>"  id="NoDis_hiddenfinalsellAP_price_transfer">
					  <div class="col-md-4">
					   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>
				     <?php 
					   echo round($dis_finalSellAP);
					   
					   ?>
					   
					   
					   <br><span class="age_span_strike" style="text-decoration: line-through;"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $finalSellAP = $sellAP + $option_sellAP;?></span></label>
					  </div>
					  
					  
					  <div class="col-md-4">
					  <div class="input-group" id="inputgroup">					  
						<span class="input-group-btn">
						<button type="button" class="btn btn-number" disabled="disabled"  onclick="decrement_adult()" data-type="minus" data-field="quant[1]"  id="btn_plus">
						<span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						
						<input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="1000" id="quant_adult">
											
						<span class="input-group-btn">
						<button type="button" class="btn btn-number"  onclick="increment_adult()" data-type="plus" data-field="quant[1]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
			</div>
			
			
			
					<div class="clearfix"></div>
					<div class="adult_plus">
					 <div class="form-group">
					 
					 <div class="col-md-4">
					   <label class="side_label1">Child<br><span class="age_span">Ages 2-12</span> </label>
					   </div>
					   <?php  $dis_CP = ($sellCP * $discount)/100;
					    $sellCP_price = $sellCP -  $dis_CP;
					   
                        $dis_TourCP = ($option_sellCP * $option_discount)/100;
					    $Tour_sellCP = $option_sellCP -  $dis_TourCP;					   					    
					     $dis_finalSellCP =  $sellCP_price +  $Tour_sellCP;

                         $finalSellCP = $sellCP + $option_sellCP;
						 ?>
						 
					<input type="hidden"  value="<?php  echo round($dis_finalSellCP);?> 					    
					    "id="hiddenfinalsellCP_price_transfer">
					   
					    <input type="hidden" value="<?php echo $finalSellCP; ?>"  id="NoDis_hiddenfinalsellCP_price_transfer">
					   
					   <div class="col-md-4">
					   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>
					   <?php 
					   
					   echo round($dis_finalSellCP);			   
					   
					   ?>
					   <br><span class="age_span_strike" ><i class="fa fa-inr" aria-hidden="true"></i><?php echo $finalSellCP = $sellCP + $option_sellCP;?></span></label>
					  </div>
					  
					  
					  
					  
					  <div class="col-md-4">

					   <div class="input-group" id="inputgroup">
						<span class="input-group-btn">
						<button type="button" class="btn  btn-number" onclick="decrement_child()"  data-type="minus" data-field="quant[2]" id="btn_plus">
					   <span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						<input type="text" name="quant[2]" class="form-control input-number" value="1" min="0" max="1000" id="quant_child">
						
						<span class="input-group-btn">
						
						<button type="button" class="btn btn-number" onclick="increment_child()" data-type="plus" data-field="quant[2]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
						</div>
											<div class="clearfix"></div>

				<div class=" adult_plus">
					 <div class="form-group">
						 <div class="col-md-4">
							<label class="side_label1" >Infont<br><span class="age_span">Under 2</span></label>
						</div>
					<div class="col-md-4">
				   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>00<br></label>
				  </div>
				    <div class="col-md-4">
				   <div class="input-group" id="inputgroup">
					<span class="input-group-btn">
					<button type="button" class="btn  btn-number"  data-type="minus" data-field="quant[3]" id="btn_plus">
				   <span class="glyphicon glyphicon-minus"></span>
					</button>
					</span>
					<input type="text" name="quant[2]" class="form-control input-number" value="1" min="0" max="1000" id="quant">
					<span class="input-group-btn">
					<button type="button" class="btn btn-number" data-type="plus" data-field="quant[3]" id="btn_plus">
					<span class="glyphicon glyphicon-plus"></span>
					</button>
					</span>
					</div>  
					</div>  
					</div>
					</div>
										<div class="clearfix"></div>

					</div>
				</div>
			</div>
      </div>
    </div>
   </div>
   </div>
				
						<div class="col-lg-12 col-md-12 col-sm-12 modal_detail1" style="margin-top:280px">
							<div class="col-sm-9 ">
							
							<h6 ><span><i class="fa fa-inr" aria-hidden="true" id="nodis_finalprice"></i> <span id="echo_nodis_price"><?php                         
							 echo $finalSell = $finalSellAP + $finalSellCP;	?></span></span></h6>
							 
							 <input type="hidden" value=" <?php echo $finalSell; ?>" id="hidden_nodis_finalprice">
							
							<h5 class=""><span><i class="fa fa-inr" aria-hidden="true" id="finalPrice"></i> <span id="echo_price"><?php                           
							 $dis_finalSell = $dis_finalSellAP + $dis_finalSellCP;					
							echo round($dis_finalSell);
							?></span></span></h5>							
							</div>
							
							
						
							<input type="hidden" id="hiddenfinalPrice" value="<?php echo round($dis_finalSell); ?>">
						
							
							
							<div class="col-sm-3">
							<a title="" class="redbtn mt-10" >Checkout</a>
							</div>
									
		                </div> 
				</div>
				
					  
				  <?php }
				  // End one tour Option -->
						?>
						
						
						
						
						
						<!--Thrid Model-->
    
				<div class="item">
				<form id="checkout_form">
						<div class="modal-header" >
							
						<div class="row">
						<div id="search_modal">
						
									  <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					  ?>
					  <div class="col-lg-12 col-md-12 col-sm-12 calendar_icon" >
								
			
			<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer5"> Transfer </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						
						<input type="hidden" name="TransferName" id="selected_transfer6">
						
						<?php
				  }else
				  {?>
			  <div class="col-lg-12 col-md-12 col-sm-12 calendar_icon" >
			  <?php
					  foreach ($tour_transfer as $row) {
                          $transferoption=$row->transferoption;
			  	} ?>
			<i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?> Transfer </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						
						<input type="hidden" name="TransferName" value="<?php echo $transferoption; ?>">
						<?php
				  }
						
							 
				if(is_array($tour_option) && count($tour_option) > 1)
                  {
					  ?>
					  <div class="col-lg-12 col-md-12 col-sm-12 calendar_icon">
						
	<i class="fa fa-list-ul" aria-hidden="true"></i> <span id="selected_option"></span><span id="selected_time"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						
						</div>
						
						<input type="hidden" name="TourOptName" id="selected_option1">
						
					 
						<?php
				  }else{
					 	foreach ($tour_option as $rows) {
							
							  $optionName=$rows->optionName;
							  
						}?>
						 <div class="col-lg-12 col-md-12 col-sm-12 calendar_icon" >
					<i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo  $optionName?></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev" ></a>
						</div>
						
						<input type="hidden" name="TourOptName" value="<?php echo $optionName; ?>">
				<?php		
				  }
					?>
						
					
						<div class="col-lg-12 col-md-12 col-sm-12  calendar_icon" >
				<i class="fa fa-calendar" aria-hidden="true"></i> <span id="selected_date2"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						
						<input type="hidden" name="TourDate" id="selected_date3">
						
						
					</div>
					</div>
					</div>
						<!-- Multi tour Option -->
					<h4 id="myModalLabel123">How Many Guests Joining?last</h4>
  
   
<div id="search_final">
  
  		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        
		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         <span class="adult_label_parent" id="show_adult"><span>Adult <span id="present_adult">1</span></span></span>
		     <span class="adult_label_parent" id="show_child">Child <span id="present_child">1</span></span>
        </a>
		
		      <input type="hidden"  name="AdultQuantity" value="1" id="present_adult1">	
		      <input type="hidden"  name="ChildQuantity"  value="1" id="present_child1">	
		
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
		<div id="search_back_drop">
			<div class="adult_dropdown">
			
			
			 <div class=" adult_plus">
					 <div class="form-group">
					 <div class="col-md-4">
					  <label class="side_label1">Adult<br><span class="age_span">Ages 12-99</span> </label>
					  </div>
					  
					   <input type="hidden"  id="discount_price_transfer">
					   
					   <input type="hidden"  id="sellAP_price_transfer">					  
					  
					      <div class="col-md-4">
					   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true" id="dis_finalsellAP_price_transfer"></i><br><span class="age_span_strike" ><i class="fa fa-inr" aria-hidden="true" ></i><strike id="finalsellAP_price_transfer"></strike></span></label>
					  </div>
					   <input type="hidden"  id="hiddenfinalsellAP_price_transfer">
					   <input type="hidden"  id="NoDis_hiddenfinalsellAP_price_transfer">
					  
					  
					  <div class="col-md-4">
					  <div class="input-group" id="inputgroup">
					  
						<span class="input-group-btn">
						<button type="button" class="btn btn-number" disabled="disabled"  onclick="decrement_adult()" data-type="minus" data-field="quant[1]"  id="btn_plus">
						<span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						
						<input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="1000" id="quant_adult">
											
						<span class="input-group-btn">
						<button type="button" class="btn btn-number"  onclick="increment_adult()" data-type="plus" data-field="quant[1]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
			</div>
			
			
			
					<div class="clearfix"></div>
					<div class="adult_plus">
					 <div class="form-group">
					 <div class="col-md-4">
					   <label class="side_label1">Child<br><span class="age_span">Ages 2-12</span> </label>
					   </div>
					   
					     <input type="hidden"  id="sellCP_price_transfer">
						 
					     <div class="col-md-4">
					   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true" id="dis_finalsellCP_price_transfer"></i><br><span class="age_span_strike" ><i class="fa fa-inr" aria-hidden="true" ></i> <strike id="finalsellCP_price_transfer"></strike></span></label>
					    </div>
					  
					   <input type="hidden"  id="hiddenfinalsellCP_price_transfer">
					  <input type="hidden"  id="NoDis_hiddenfinalsellCP_price_transfer">
					  <div class="col-md-4">

					   <div class="input-group" id="inputgroup">
						<span class="input-group-btn">
						<button type="button" class="btn  btn-number" onclick="decrement_child()"  data-type="minus" data-field="quant[2]" id="btn_plus">
					   <span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						<input type="text" name="quant[2]" class="form-control input-number" value="1" min="0" max="1000" id="quant_child">
						
						<span class="input-group-btn">
						
						<button type="button" class="btn btn-number" onclick="increment_child()" data-type="plus" data-field="quant[2]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
						</div>
											<div class="clearfix"></div>

				<div class=" adult_plus">
					 <div class="form-group">
						 <div class="col-md-4">
							<label class="side_label1" >Infont<br><span class="age_span">Under 2</span></label>
						</div>
						
						<div class="col-md-4">
				   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>00<br></label>
				  </div>
				   		<div class="col-md-4">
						
				   <div class="input-group" id="inputgroup">
				   
					<span class="input-group-btn">
					<button type="button" class="btn  btn-number"  data-type="minus" data-field="quant[3]" id="btn_plus">
				   <span class="glyphicon glyphicon-minus"></span>
					</button>
					</span>
					
					<input type="text" name="quant[2]" class="form-control input-number" value="1" min="0" max="1000" id="quantminus">
					
					
					<span class="input-group-btn">					
					<button type="button" class="btn btn-number" data-type="plus" data-field="quant[3]" id="btn_plus">
					<span class="glyphicon glyphicon-plus"></span>					
					</button>
					</span>
					
					
					</div>  
					</div>  
					</div>
					</div>
										<div class="clearfix"></div>

					</div>
				</div>
			</div>
      </div>
    </div>
   </div>
   </div>
				
						<div class="col-lg-12 col-md-12 col-sm-12 modal_detail1" style="margin-top:280px">
							<div class="col-sm-9 ">
							
							<input type="hidden" name="FinalTotal" id="hidden_nodis_finalprice">
							
							<h6 ><span><i class="fa fa-inr" aria-hidden="true" ></i><strike id="nodis_finalprice"></strike> </span></h6>
							<h5 class=""><span id="finalPrice"><i class="fa fa-inr" aria-hidden="true"></i> </span></h5>
							<input type="hidden" name="FinalDisTotal" id="hiddenfinalPrice">
							
							
							</div>
							<div class="col-sm-3">
							
					<!--<button class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30" onclick="checkout()" id="btnSave">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i> Checkout
					</button>-->
							
							
														<a title="" class="redbtn mt-30" >Checkout</a>

							</div>									
		                </div> 
						
						</form>
				</div>
				
				
				
				
				
				
				
				
			
				</div>		   
		  </div>
        </div>
       
      </div>
    </div>
  </div>	
	
	
 


	</form>
		</div>
		<?php 
									foreach ($tour_prices as $row) {
									foreach($row as $second ){
								  
   
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 
													$apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
					<div class="sidebar_detail" id="top">
				<div class="price_box_detail">
				<div class="old_rate1">
				From INR
				</div>
				<div class="rate">
				<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new">  Per Person</span>
				</div>
				<div class="price_gurantee_section">
				Tripoye Price Guarantee
				</div>
				<button type="button" class="btn btn-demo btn-block" onClick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
				</div>
				
	       </div>
		   
		   	<div class="sidebar_detail_bottom" id="bottom" data-spy="affix" data-offset-top="80" data-offset-bottom="395" style="display:none">
					
					<div class="price_box_detail">
					<div class="name">
					<label><?php echo $tourname?></label>
					<div class="reviews">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star-o"></i>
					</div>
					</div>
					
					
					<div class="old_rate1">
					From INR
					</div>
					<div class="rate">
					<div><i class="fa fa-inr" aria-hidden="true"></i></div><?php echo round($final_price);?> <span class="per_person_new">  Per Person</span>
					</div>
					<div class="price_gurantee_section">
					Tripoye Price Guarantee
					</div>
					<button type="button" class="btn btn-demo btn-block"  onClick="show_book()"  data-toggle="modal" data-target="#costumModal30">Book Now</button>
					</div>					
			</div>	
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
				<div class="sidebar_detail" id="top1">
				<div class="price_box_detail">
				<div class="old_rate1">
				From INR
				</div>
				<div class="rate">
				<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
				</div>
				<div class="price_gurantee_section">
				Tripoye Price Guarantee
				</div>
				<button type="button" class="btn btn-demo btn-block" onClick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
				</div>
				
	          </div>
			  
			  
			  	<div class="sidebar_detail_bottom" id="bottom1" data-spy="affix" data-offset-top="80" data-offset-bottom="395" style="display:none">
					
					<div class="price_box_detail">
					<div class="name">
					<label><?php echo $tourname?></label>
					<div class="reviews">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star-o"></i>
					</div>
					</div>
					
					
					<div class="old_rate1">
					From INR
					</div>
					<div class="rate">
					<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
					</div>
					<div class="price_gurantee_section">
					Tripoye Price Guarantee
					</div>
					<button type="button" class="btn btn-demo btn-block" onClick="show_book()"  data-toggle="modal" data-target="#costumModal30">Book Now</button>
					</div>					
			</div>	
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
										<div class="sidebar_detail" id="top2">
				<div class="price_box_detail">
				<div class="old_rate1">
				From INR
				</div>
			<div class="rate">
				<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
				</div>
				<div class="price_gurantee_section">
				Tripoye Price Guarantee
				</div>
				<button type="button" class="btn btn-demo btn-block" onClick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
				</div>
				
	         </div>
			 
			 
			 	<div class="sidebar_detail_bottom" id="bottom2" data-spy="affix" data-offset-top="80" data-offset-bottom="395" style="display:none">
					
					<div class="price_box_detail">
					<div class="name">
					<label><?php echo $tourname?></label>
					<div class="reviews">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star-o"></i>
					</div>
					</div>
					
					
					<div class="old_rate1">
					From INR
					</div>
					<div class="rate">
					<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
					</div>
					<div class="price_gurantee_section">
					Tripoye Price Guarantee
					</div>
					<button type="button" class="btn btn-demo btn-block" onClick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
					</div>					
			</div>	
													
													<?php
													}
													
											 }
											         
										 }
										 }			
               
                                       ?>
									   
					
					
					
	</div>
	</div>
	
	

	
	
	</div>
	</div>
	</div>
	</div>
	<!-- end Main Wrapper -->

	<footer class="footer scrollspy-footer"> <!-- add scrollspy-footer to stop sidebar scrollspy -->
	
	<div class="container">
	
	<div class="main-footer">
	
	<div class="row">
	
	<div class="col-xs-12 col-sm-5 col-md-3">
	
	<div class="footer-logo">
	<img src="<?php echo base_url('assets/images/logo-white.png')?>" alt="Logo" />
	</div>
	
	<p class="footer-address">324 Yarang Road, T.Chabangtigo, Muanng Pattani 9400 <br/> <i class="fa fa-phone"></i> +66 28 878 5452 <br/> <i class="fa fa-phone"></i> +66 2 547 2223 <br/> <i class="fa fa-envelope-o"></i> <a href="#">support@tourpacker.com</a></p>
	
	<div class="footer-social">
	
	<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
	<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
	<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
	<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
	
	</div>
	
	<p class="copy-right">&#169; Copyright 2016 Tour Packer. All Rights Reserved</p>
	
	</div>
	
	<div class="col-xs-12 col-sm-7 col-md-9">

	<div class="row gap-10">
	
	<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-3 mt-30-xs">
	
	<h5 class="footer-title">About Tour Packer</h5>
	
	<ul class="footer-menu">
	
	<li><a href="static-page.html">Who we are</a></li>
	<li><a href="static-page.html">Careers</a></li>
	<li><a href="static-page.html">Company history</a></li>
	<li><a href="static-page.html">Legal</a></li>
	<li><a href="static-page.html">Partners</a></li>
	<li><a href="static-page.html">Privacy notice</a></li>
	
	</ul>
	
	</div>
	
	<div class="col-xs-12 col-sm-4 col-md-3 mt-30-xs">

	<h5 class="footer-title">Customer service</h5>
	
	<ul class="footer-menu">
	
	<li><a href="static-page.html">Payment</a></li>
	<li><a href="static-page.html">Feedback</a></li>
	<li><a href="static-page.html">Contact us</a></li>
	<li><a href="static-page.html">Travel advisories</a></li>
	<li><a href="static-page.html">FAQ</a></li>
	<li><a href="static-page.html">Site map</a></li>
	
	</ul>
	
	</div>
	
	<div class="col-xs-12 col-sm-4 col-md-3 mt-30-xs">

	<h5 class="footer-title">Others</h5>
	
	<ul class="footer-menu">
	
	<li><a href="static-page.html">Destinations</a></li>
	<li><a href="static-page.html">Blog</a></li>
	<li><a href="static-page.html">Pre Departure Planning</a></li>
	<li><a href="static-page.html">Visas</a></li>
	<li><a href="static-page.html">Insurance</a></li>
	<li><a href="static-page.html">Travel Guide</a></li>
	
	</ul>
	
	</div>
	
	</div>

	</div>
	
	</div>

	</div>
	
	</div>
	
	</footer>

	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
	 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->
<script>
function select_date(date)
{
	console.log(date);
}
function  checkout()
{
	$('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
	
	  url = "<?php echo site_url('b2B_handler/add_booking')?>";
	
	 $.ajax({
        url : url,
        type: "POST",
        data: $('#checkout_form').serialize(),
        dataType: "JSON",
		
        success: function(data)
        {
             console.log(data);
            
            $('#btnSave').text('saved...'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>

	<script>
	function select_transfer(clicked_transfer_id)
{
     var str = document.getElementById(clicked_transfer_id).value; 
	 var trnsfersellap = str.split("/");
	 trnsfer = trnsfersellap[0];
     sellAP = trnsfersellap[1];
	 sellCP = trnsfersellap[2];
	 selldiscount = trnsfersellap[3];
	  
	 
	   $('#selected_transfer1').html(trnsfer);	
	   $('#selected_transfer2').html(trnsfer);	 
	   $('#selected_transfer3').html(trnsfer);
       $('#selected_transfer4').html(trnsfer);   
       $('#selected_transfer5').html(trnsfer);
       $('#selected_transfer6').val(trnsfer);
	   
      
	   $('#sellAP_price_transfer').val(sellAP);
	   
	   $('#sellCP_price_transfer').val(sellCP);
	   
	   $('#discount_price_transfer').val(selldiscount);
	   
}


function select_option(clicked_option_id)
{     
    
       var str = document.getElementById(clicked_option_id).value; 
	   
	   var optionPrice = str.split("/");
		
	  option = optionPrice[0];
      tour_sellAP = optionPrice[1];
	  tour_sellCP = optionPrice[2];
	  Touroption_discount = optionPrice[3];
	
	
	   $('#selected_option').html(option);
	   $('#selected_option1').val(option);
	   
	   
		
		
	    //Final AP  Price
		var discount_price = document.getElementById('discount_price_transfer').value;
		
	    var sellAP_price = document.getElementById('sellAP_price_transfer').value;
		
		 var dis_finaSellAP=(sellAP_price * discount_price)/100;		 
         var finalsellAP_prices = +sellAP_price - +dis_finaSellAP;
		 
		
		  var finalsellAP_price = Math.round(finalsellAP_prices);
		 
		 var dis_finalTourSellAP=(tour_sellAP * Touroption_discount)/100;	
		
	     var finalToursellAP_prices=+tour_sellAP - +dis_finalTourSellAP;
		
	     var finalToursellAP_price = Math.round(finalToursellAP_prices);
		
		
        var finalAP_prices=+sellAP_price + +tour_sellAP;	
		
		var finalAP_price = Math.round(finalAP_prices);
		
		var Dis_finalAP_price=+finalsellAP_price + +finalToursellAP_price;
		
		
		
		$('#finalsellAP_price_transfer').html(finalAP_price);
		$('#NoDis_hiddenfinalsellAP_price_transfer').val(finalAP_price);
		
	    $('#dis_finalsellAP_price_transfer').html(Dis_finalAP_price);
		$('#hiddenfinalsellAP_price_transfer').val(Dis_finalAP_price);
		
	   
	    //Final CP  Price
	   var sellCP_price = document.getElementById('sellCP_price_transfer').value;
	   
	  
		 var dis_finaSellCP=(sellCP_price * discount_price)/100;		 
         var finalsellCP_prices = +sellCP_price - +dis_finaSellCP;		 
		 
		 var finalsellCP_price = Math.round(finalsellCP_prices);
		 
		 
		 
		 var dis_finalTourSellCP=(tour_sellCP * Touroption_discount)/100;	
		
	     var finalToursellCP_prices=+tour_sellCP - +dis_finalTourSellCP;
		
	    var finalToursellCP_price = Math.round(finalToursellCP_prices);
		
		
		
        var finalCP_prices=+sellCP_price + +tour_sellCP;	
		
		var finalCP_price = Math.round(finalCP_prices);		
		
		
		var Dis_finalCP_price=+finalsellCP_price + +finalToursellCP_price;
		
		
			
		  
	    $('#finalsellCP_price_transfer').html(finalCP_price);
	    $('#NoDis_hiddenfinalsellCP_price_transfer').val(finalCP_price);
		
	    $('#dis_finalsellCP_price_transfer').html(Dis_finalCP_price);
	    $('#hiddenfinalsellCP_price_transfer').val(Dis_finalCP_price);
	   
	   
	   //Final Price
	   var finalPrices = +Dis_finalAP_price + +Dis_finalCP_price;
	   
	    var finalPrice = Math.round(finalPrices);
		
	    $('#hiddenfinalPrice').val(finalPrice);
		$('#finalPrice').html(finalPrice);
		
		var nodis_prices = +finalAP_price + +finalCP_price;
		var nodis_price = Math.round(nodis_prices);
		$('#nodis_finalprice').html(nodis_price);
	   $('#hidden_nodis_finalprice').val(nodis_price);
	   
	   
	 	 
}


function select_time(element,tour_id)
 { 
 var T_tour_id="T"+tour_id;
 
 document.getElementById(T_tour_id).style.display = 'block';
	 
    var time = element.options[element.selectedIndex].value;
    $('#selected_time').html(time);	
 }


window.onload = function(){
	   $('#datetimepicker').datetimepicker({
    inline: true,
    sideBySide: true,
    keepOpen: true,
    format: 'YYYY-MM-DD',
});
    document.getElementById('butdate').onclick = function(){
      var a = document.getElementById('datetimepicker').value; 
       $('#selected_date1').html(a);	 
	   $('#selected_date2').html(a);	 
	   $('#selected_date3').val(a);	 
	   
	   
	
    }
}; 


function increment_adult()
{
	 var a = document.getElementById('quant_adult').value;

	 document.getElementById("show_adult").style.visibility = "visible";
	 var b= +a + 1;
	
	   $('#present_adult').html(b);
       $('#present_adult1').val(b);	
	 
		 
	     var APfinalPrice = document.getElementById('hiddenfinalsellAP_price_transfer').value;
	 
	      var Incfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var IncfinalPrices = +finalPrice + +APfinalPrice;
		  
		  
		  $('#hiddenfinalPrice').val(IncfinalPrices);
	      $('#finalPrice').html(IncfinalPrices);
		  $('#echo_price').hide();
		  
		  // final strike price
		  
		  var APfinalPrice = document.getElementById('NoDis_hiddenfinalsellAP_price_transfer').value;
	 
	      var Incfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		
		  var IncfinalPrices = +finalPrice + +APfinalPrice;
		   
		  
		  $('#hidden_nodis_finalprice').val(IncfinalPrices);
	      $('#nodis_finalprice').html(IncfinalPrices);
		  $('#echo_nodis_price').hide();
	
	    
		  
}


function decrement_adult()
{
	 var a = document.getElementById('quant_adult').value;
	 if(a == 1)
	 {
		 document.getElementById("show_adult").style.visibility = "hidden";
		 
	 }else{
		 var b= +a - 1;	 
		 
	 
	   $('#present_adult').html(b);	
	   $('#present_adult1').val(b);	
	   
	    var APfinalPrice = document.getElementById('hiddenfinalsellAP_price_transfer').value;
	  
	    var Decfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var DecfinalPrices = +finalPrice -  +APfinalPrice;
		  
		  $('#hiddenfinalPrice').val(DecfinalPrices);
	      $('#finalPrice').html(DecfinalPrices);
	      $('#echo_price').hide();
		  
		  	  // final strike price
		  
		   var APfinalPrice = document.getElementById('NoDis_hiddenfinalsellAP_price_transfer').value;
	 
	       var Decfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		
		  var DecfinalPrices = +finalPrice -  +APfinalPrice;
		  
		
		  $('#hidden_nodis_finalprice').val(DecfinalPrices);
	      $('#nodis_finalprice').html(DecfinalPrices);
		  $('#echo_nodis_price').hide();
	
		  
	  }
	  
	 
	 
	 
	
	
}

function increment_child()
{
	 var a = document.getElementById('quant_child').value;
	
	 document.getElementById("show_child").style.visibility = "visible";
		 var b= +a + 1;	 
	  
	    $('#present_child').html(b);
	    $('#present_child1').val(b);
		
	      var CPfinalPrice = document.getElementById('hiddenfinalsellCP_price_transfer').value;
	  
	      var Incfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var IncfinalPrices = +finalPrice + +CPfinalPrice;
		  
		  $('#hiddenfinalPrice').val(IncfinalPrices);
	      $('#finalPrice').html(IncfinalPrices);
	      $('#echo_price').hide();
		  
		  
		  // final strike price
		  
		 
		 var CPfinalPrice = document.getElementById('NoDis_hiddenfinalsellCP_price_transfer').value;
	  
	    var Incfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		  
		  var IncfinalPrices = +finalPrice + +CPfinalPrice;
		  
		
		  $('#hidden_nodis_finalprice').val(IncfinalPrices);
	      $('#nodis_finalprice').html(IncfinalPrices);
		  $('#echo_nodis_price').hide();
	
}


function decrement_child()
{
	 var a = document.getElementById('quant_child').value;
	 
	 if(a == 1)
	 {
		 document.getElementById("show_child").style.visibility = "hidden";
		 
		 var b=0;	 
		  $('#present_child1').val(b);	
		 
		 var CPfinalPrice = document.getElementById('hiddenfinalsellCP_price_transfer').value;
	  
	   // var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		  
		  $('#hiddenfinalPrice').val(DecfinalPrices);
	      $('#finalPrice').html(DecfinalPrices);
          $('#echo_price').hide();	

		 
		 // final strike price		  
		  var CPfinalPrice = document.getElementById('NoDis_hiddenfinalsellCP_price_transfer').value;
	  
	     //var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		 
		  
		
		  $('#hidden_nodis_finalprice').val(DecfinalPrices);
	      $('#nodis_finalprice').html(DecfinalPrices);
		  $('#echo_nodis_price').hide();
		 
	 }else{
		 var b= +a - 1;	 
		 document.getElementById("show_child").style.visibility = "block";
	    
	    $('#present_child').html(b);
		$('#present_child1').val(b);
		
          var CPfinalPrice = document.getElementById('hiddenfinalsellCP_price_transfer').value;
	  
	      var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		  
		  $('#hiddenfinalPrice').val(DecfinalPrices);
	      $('#finalPrice').html(DecfinalPrices);
          $('#echo_price').hide();		


// final strike price
		  
		  var CPfinalPrice = document.getElementById('NoDis_hiddenfinalsellCP_price_transfer').value;
	  
	      var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		 
		  
		
		  $('#hidden_nodis_finalprice').val(DecfinalPrices);
	      $('#nodis_finalprice').html(DecfinalPrices);
		  $('#echo_nodis_price').hide();
		  
	 }
	
	
}

	</script>
	<script>
	function show_nextbtn()
	{
		$('#next_btn').show();
	}
	</script>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/product_details.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/index.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/bootstrapDatepickr-1.0.0.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/moment.min.js')?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/wizard.js')?>"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js'></script>



<script>
jQuery(function($) {
 $(window).scroll(function() {
   if($('.navbar-sticky-function').hasClass('navbar-primary')) {
     $('#logo1').show();
     $('#logo2').hide();

	
   }
   else{
	   $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo2').show();
     $('#logo1').hide();
   }
 });
});
</script>
<script>
function show_book()
{
	//$('.hidden_price_div').show();
	$('#top').hide();
	$('#bottom').hide();
	$('#top1').hide();
	$('#bottom1').hide();
	$('#top2').hide();
	$('#bottom2').hide();
	  
}

</script>


<script>
$('#datetimepicker').datetimepicker({
    inline: true,
    sideBySide: true,
    keepOpen: true,
    format: 'YYYY-MM-DD '
});
</script>


<script>
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
<script>
$(document).scroll(function () {
    var y = $(this).scrollTop();
	//if ( $('.hidden_price_div').css('display') == 'none' ){
	if (y > 100) {
        $('.sidebar_detail_bottom').fadeIn();
		
	
    } else {
        $('.sidebar_detail_bottom').fadeOut();
    } 
	/* }
	else
	{
		
	} */
		
	
});
</script>
<script>
 $(window).scroll(function() {
	//if ( $('.hidden_price_div').css('display') == 'none' )
	//{
	 if ($(this).scrollTop()>0)
     {
        $('.sidebar_detail').fadeOut();
		

     }
    else
     {
      $('.sidebar_detail').fadeIn();
     }
	/* }
	else
	{
		
	} */
   
 }); 
</script>




<script>
$(document).ready(function() {

  $(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
      numPanelOpen = $(accordionId + ' .collapse.in').length;
    
    $(this).toggleClass("active");

    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })

  openAllPanels = function(aId) {
    console.log("setAllPanelOpen");
    $(aId + ' .panel-collapse:not(".in")').collapse('show');
  }
  closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    $(aId + ' .panel-collapse.in').collapse('hide');
  }
     
});

</script>
<!--<script type="text/javascript">


$( "#booknow" ).click(function() {
  $( "#buynow" ).toggle( "slow", function() {
    // Animation complete.
  });
});
</script>-->

<!--<script>
$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 0) {
        $('#colorful').fadeIn();
	
    } else {
        $('#colorful').fadeOut();
    }
	
});
</script>
<script>
 $(window).scroll(function() {

    if ($(this).scrollTop()>0)
     {
        $('#original').fadeOut();
     }
    else
     {
      $('#original').fadeIn();
     }
 }); 
</script>-->





</body>

</html>