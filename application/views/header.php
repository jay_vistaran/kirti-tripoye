<!-- start Header -->
	<header id="header">
	  
	<!-- start Navbar (Header) -->
	<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
	<!--<div onclick="window.location.href='cart.html'" class="cart_div hidden-xs">
	<div class="cart_icon">
	<i class="fa fa-cart-arrow-down"></i>
	</div>
	<div class="cart_value">
	2
	</div>
	</div>-->
	
	<div class="navbar-top" >
	
	<div class="container">
	
	<div class="flex-row flex-align-middle">
	<div class="flex-shrink flex-columns" id="test">
	<a class='navbar-logo' href='<?php echo site_url('home')?>'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>' id="logo1" class='img-responsive'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>' id="logo2" class='img-responsive' style="display:none">
	</a>	
	</div>
	<div class="flex-columns">
	<div class="pull-right">
	
	
	
	
	<div id="navbar" class="collapse navbar-collapse navbar-arrow">
	<ul class="nav navbar-nav" id="responsive-menu">
	<!--<li class="top_nav "><a href="#">USD</a>
	<ul class="dropdown_top">
	<li><a href="#"><i class="ion-social-usd"></i> Dollar</a></li>
	<li><a href="#"><i class="ion-social-euro"></i> Europe</a></li>
	<li><a href="#"><i class="ion-social-yen"></i> Yen</a></li>
	</ul>
	</li>-->
	
	<!--<li class="top_nav"><a href="#">Download App</a></li>-->
	<li class="top_nav"><a href="<?php echo site_url('contact-us')?>">Help</a></li>
	
	<?php 
	  $user_id=$this->session->userdata('user_id');
	  $oauth_uid=$this->session->userdata('loggedIn');
	  
	if($user_id == '' && $oauth_uid == false) 
	{
		?>
		<li class="top_nav"><a data-toggle="modal" href="#loginModal">Login</a></li>
		<?php
	}else
	{
		?>
			<li class="top_nav"><a  href="<?php echo base_url('user-profile')?>">My Profile</a>
			<ul class="dropdown_top">
			<li><a href="<?php echo base_url('authenticate/logout')?>"> Logout</a></li>
			</ul>
		
			</li>
		<?php
	}
	?>
	
	</ul>
	
	</div><!--/.nav-collapse -->
	
	
	</div>
	</div>
	</div>

	</div>
	
	</div>
	
	<div id="slicknav-mobile"></div>


	
	</nav>
	<!-- end Navbar (Header) -->

	</header>
	
	
	
	
	
	
	<!-- BEGIN # MODAL LOGIN -->
	<div class="modal fade modal-login modal-border-transparent" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true" >
		<div class="modal-dialog">
			<div class="modal-content">
				
				<button type="button" class="btn btn-close close" data-dismiss="modal" aria-label="Close">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
				
				<div class="clear"></div>
				
				<!-- Begin # DIV Form -->
				<div id="modal-login-form-wrapper" >		
					
				<div class="alert" id="msg5a" style="display:none">
						<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
						Please enter your valid Email Id
						</div>
					
					   <div class="alert" id="msg66a" style="display:none">
						<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
						Please enter correct User name and Password
						</div>
					
					
					     <div class="alert" id="msg66aR" style="display:none">
						<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
						User Already Exits......!
						</div>
						
						
					     <div class="alert" id="msg6Sign_up" style="display:none">
						<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
					    Please Sign-in   Your Account........!
						</div>
						
							 
					<form id="login-form">
					
						<div class="modal-body pb-5">					
							<h4 class="text-center heading mt-10 mb-20">Sign-in</h4>				
                   
							<?php  $loginURL; ?>
							<a href="https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fbrothehood1%2Fauthenticate%2Fsign_up_google%2F&client_id=320138513990-nushvvi9onpmhkbl92474jug5r9jkg7b.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&access_type=online&approval_prompt=auto ">
							
							<img src="<?php echo base_url('assets/images/glogin.png'); ?>" class="google_signup" /></a>
							
							<!--<button class="btn btn-facebook btn-block">Sign-in with Facebook</button>-->
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="login_username" class="form-control" placeholder="username" name="user_email" type="text"> 
							</div>
							
							<div class="form-group"> 
								<input id="login_password" class="form-control" placeholder="password" name="user_password" type="password"> 
							</div>
			
							<div class="form-group">
								<div class="row gap-5">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="checkbox-block fa-checkbox"> 
											<input id="remember_me_checkbox" name="remember_me_checkbox" class="checkbox" value="First Choice" type="checkbox"> 
											<label class="" for="remember_me_checkbox">remember</label>
										</div>
									</div>
									
									<div class="col-xs-6 col-sm-6 col-md-6 text-right"> 
										<button id="login_lost_btn" type="button" class="btn btn-link">forgot pass?</button>
									</div>
								</div>
							</div>
						
						</div>
						
							 <div id="infoMessage"><?php echo $this->session->flashdata('err_message');?></div>
					<!-- Begin # Login Form -->
					
						
						
						
						<div class="modal-footer">						
							<div class="row gap-10">
							
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="button" onclick="login();" class="btn btn-primary btn-block">Sign-in</button>
								</div>
								
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="button" class="btn btn-primary btn-block btn-inverse" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
								
						</div>
							
							<div class="text-left">
								No account? 
								<button id="login_register_btn" type="button" class="btn btn-link">Register</button>
							</div>	
							
						</div>
						
						
					</form>
					
					<!-- End # Login Form -->
					
					
					
					
								
					<!-- Begin | Lost Password Form -->
					<form id="lost-form" style="display:none;">
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Forgot password</h3>
							<div class="form-group mb-10"> 
								<input id="lost_email" class="form-control" type="text" placeholder="Enter Your Email">
							</div>
							
							<div class="text-center">
								<button id="lost_login_btn" type="button" class="btn btn-link">Sign-in</button> or 
								<button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
							</div>
							
						</div>
						
						<div class="modal-footer mt-10">
							
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-block">Submit</button>
								</div>
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
						</div>
						
					</form>
					<!-- End | Lost Password Form -->
								
					<!-- Begin | Register Form -->
					<form id="register-form" style="display:none;" >
					
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Register</h3>
							
							<!--<button class="btn btn-facebook btn-block">Register with Facebook</button>-->
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="register_username" class="form-control" type="text"  name="user_name" placeholder="Username" required> 
							</div>
							
							<div class="form-group"> 
								<input id="register_email" class="form-control" type="email" name="user_email"  placeholder="Email" required>
							</div>
							
							<div class="form-group"> 
								<input id="register_password" class="form-control" type="password" name="user_password" placeholder="Password" required>
							</div>
							
							<div class="form-group"> 
								<input id="register_password_confirm" class="form-control" type="password" name="" placeholder="Confirm Password">
							</div>
                                     <span id='message'></span>
						</div>
							
						<div class="modal-footer mt-10">
						
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="button" onclick="sign_up();" class="btn btn-primary btn-block">Register</button>
								</div>
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
							<div class="text-left">
									Already have account? <button id="register_login_btn" type="button" class="btn btn-link">Sign-in</button>
							</div>
							
						</div>
							
					</form>
					<!-- End | Register Form -->
								
				</div>
				<!-- End # DIV Form -->
								
			</div>
		</div>
	</div>
	<!-- END # MODAL LOGIN -->
	
	
	
	 
	