<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrap-material-datetimepicker.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrapDatepickr-1.0.0.min.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">
	<!-- start Header -->
	<header id="header">
	  
	<!-- start Navbar (Header) -->
	<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
	
	
	<div class="navbar-top" >
	
	<div class="container">
	
	<div class="flex-row flex-align-middle">
	<div class="flex-shrink flex-columns" id="test">
	<a class='navbar-logo' href='<?php echo site_url('home')?>'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>' id="logo2" class='img-responsive' >
	</a>	
	</div>
	
	</div>

	</div>
	
	</div>
	
	<div id="slicknav-mobile"></div>


	
	</nav>
	<!-- end Navbar (Header) -->

	</header>
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			                                 <?php
												$DisFullTotal;
												
												$tax_total=($DisFullTotal * 5)/100;
												$final_total= $DisFullTotal + $tax_total;
												?>
			
			<div class="content-wrapper">
			
				<div class="container">
				
					<div class="row">
					
						
						<div class="col-sm-12 col-md-12">
						
			<div class="row stepsecond mb20">
						<div class="col-xs-8 col-xs-offset-2">
						<div class="col-xs-4 pr0 pl0 pos_rel">
                        <div class="leftdiv"></div>
                        <div class="line"></div>
                        <div class="stepsdot"></div>
                        <div class="stepone orange">1</div>
                        <div class="title">Add to Cart</div>
                    </div>

                    <div class="col-xs-4 pl0 pr0 pos_rel">
                        <div class="line"></div>
                        <div class="stepsdot"></div>
                        <div class="stepone yellow ">2</div>
                        <div class="title">Payment</div>
                    </div>
                    <div class="col-xs-4 pl0 pr0 pos_rel">
                        <div class="rightdiv"></div>
                        <div class="line"></div>
                        <div class="stepone gray">3</div>
                        <div class="title">Print Voucher</div>
                    </div>
                </div>
            </div>
				
						</div>
				
				<div class="col-sm-12 col-md-9 col-lg-9" role="main">
				<div id="passangerDiv" class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
				<h3 class="passenger_head"><i class="fa fa-files-o" aria-hidden="true"></i> Payment Method</h3>
				<div class="tab_back">
				<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 tab_payment">
				<ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#home" data-toggle="tab">Credit Card</a></li>
                <li><a href="#profile" data-toggle="tab">Debit Card</a></li>
                <li><a href="#messages" data-toggle="tab">Net Banking</a></li>
                <li><a href="#settings" data-toggle="tab">UPI</a></li>
                <li><a href="#mobile" data-toggle="tab">Mobile Wallet</a></li>
                <li><a href="#upi" data-toggle="tab">Cash Card</a></li>
				</ul>
				</div>
				    <div class="col-xs-12 col-md-9 col-sm-9 col-lg-9 tab_padding">
					<!-- Tab panes -->
					<div class="tab-content">
					
	<form  id="checkoutform" action="<?php echo base_url('checkout/payu_money_handler')?>" method="post" onSubmit="return clearField()";>
					
					<!-- Passenger Details-->
				<input type="hidden"  name="name" value="<?php echo $name;?>">  
			
				<input type="hidden"  name="lastname" value="<?php echo $lastname;?>">  
				
				<input type="hidden"  name="phone" value="<?php echo $phone;?>"> 
				<input type="hidden"  name="email" value="<?php echo $email;?>"> 
					
					
					<!-- Tour Details-->
					
				<input type="hidden"  name="TourId" value="<?php echo $TourId;?>">  
			
				<input type="hidden"  name="TourOptId" value="<?php echo $TourOptId;?>">  
				
				<input type="hidden"  name="AdultPrice" value="<?php echo $AdultPrice;?>"> 
				<input type="hidden"  name="DisAdultPrice" value="<?php echo $DisAdultPrice;?>"> 
				
				<input type="hidden"  name="ChildPrice" value="<?php echo $ChildPrice;?>"> 
				<input type="hidden"  name="DisChildPrice" value="<?php echo $DisChildPrice;?>"> 
				
				<input type="hidden"  name="Discount" value="<?php echo $Discount;?>"> 
				
				
				<input type="hidden"  name="TourName" value="<?php echo $TourName;?>">  
				
				<input type="hidden"  name="TourOptName" value="<?php echo $TourOptName;?>">  
				<input type="hidden"  name="TransferName" value="<?php echo $TransferName;?>"> 
				
				
				<input type="hidden"  name="TourDate" value="<?php echo $TourDate;?>">  
				<input type="hidden"  name="TourTime" value="<?php echo $TourTime;?>">  
				
				<input type="hidden"  name="AdultQuantity" value="<?php echo $AdultQuantity;?>">  
				<input type="hidden"  name="ChildQuantity" value="<?php echo $ChildQuantity;?>">  
				<input type="hidden"  name="InfantQuantity" value="<?php echo $InfantQuantity;?>"> 

				
				<input type="hidden"  name="FullTotal" value="<?php echo $FullTotal;?>">  
				<input type="hidden"  name="DisFullTotal" value="<?php echo $DisFullTotal;?>">  
				
				<input type="hidden"  name="TransferId" value="<?php echo $TransferId;?>">  
				<input type="hidden"  name="TransferType" value="<?php echo $TransferType;?>"> 

				
				<input type="hidden"  name="GstFullTotal" value="<?php echo $final_total;?>">  
				<input type="hidden"  name="pickup_point" value="<?php echo $pickup_point;?>">  
					
					
					
					
						<div class="tab-pane active" id="home">
							<div class="tab_card">
                        <div class="row">
							<p class="pay_card">Pay with Credit Card</p>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  b-select-wrap">

						<div class="form-group">
						<label >Credit Card Number</label>
						<input type="text"  placeholder="" class="form-control" data-initialized="true" autofocus="autofocus">
						</div>
						</div>
						
						 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  b-select-wrap">
						<div class="form-group">
						<label >Cardholder's Name</label>
						<input type="text"  placeholder="" class="form-control" data-initialized="true" autofocus="autofocus">
						</div>
						</div>
                           
                            
                           <div class="clearfix"></div>
                           

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-20 b-select-wrap">
							<label >Expiry Date</label>
                                <select class="dropselect" id="txtPreName" >
                                    <option value="ind">Month</option>
                                    <option value="ind">1</option>
                                    <option value="ind">2</option>
                                    <option value="ind">3</option>
                                    <option value="ind">4</option>
                                    <option value="ind">5</option>
                                    <option value="ind">6</option>
                                    <option value="ind">7</option>
                                    <option value="ind">8</option>
                                    <option value="ind">9</option>
                                    <option value="ind">10</option>
                                    <option value="ind">11</option>
                                    <option value="ind">12</option>
                                </select>
                            </div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-20 mt-25">
								<select class="dropselect" id="txtPreName" >
                                    <option value="ind">year</option>
                                    <option value="ind">2017</option>
                                    <option value="ind">2018</option>
                                    <option value="ind">2019</option>
                                    <option value="ind">2020</option>
                                    <option value="ind">2021</option>
                                    <option value="ind">2022</option>
                                    <option value="ind">2023</option>
                                    <option value="ind">2024</option>
                                    <option value="ind">2025</option>
                                    <option value="ind">2026</option>
                                    <option value="ind">2027</option>
                                    <option value="ind">2028</option>
                                    <option value="ind">2029</option>
                                    <option value="ind">2030</option>
                                </select> 
							</div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-20 ">
							<label >CVV <i class="fa fa-credit-card" aria-hidden="true"></i></label>
							<input type="text" class="form-control" value="" placeholder="Email_Id">  
							</div>
						<div class="col-xs-12">
						<div class="col-md-2 col-xs-offset-3 col-md-offset-0"></div>
						<div class="col-xs-offset-3 col-md-offset-0"> 
							<div class="checkbox-block">
										<input id="accept_booking111" name="accept_booking111" type="checkbox" class="checkbox" value="paymentsCre11ditCard">
										<label class="" for="accept_booking">Save your card for faster payment. CVV will not be saved.</label>
									</div>
						</div> 
						</div>
						<div class="col-xs-offset-2 col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<div class="wraplist Finalprice">
                                                <div class="lefttitle mt-15"><i class="fa fa-inr"></i> <?php echo $final_total;?> </div>
                                                
                                            </div>
					</div>
						</div> 
						<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<button class="button primary rounded"   type="submit">Pay Now</button> 
					</div>
						</div> 
					
                        </div>
                    </div>
						</div>
						
			</form>
						
						<div class="tab-pane" id="profile">
								<div class="tab_card">
                        <div class="row">
							<p class="pay_card">Pay with Debit Card</p>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  b-select-wrap">

						<div class="form-group">
						<label >Debit Card Number</label>
						<input type="text"  placeholder="" class="form-control" data-initialized="true" autofocus="autofocus">
						</div>
						</div>
						
						 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  b-select-wrap">
						<div class="form-group">
						<label >Cardholder's Name</label>
						<input type="text"  placeholder="" class="form-control" data-initialized="true" autofocus="autofocus">
						</div>
						</div>
                           
                            
                           <div class="clearfix"></div>
                           

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-20 b-select-wrap">
							<label >Expiry Date</label>
                                <select class="dropselect" id="txtPreName" >
                                    <option value="ind">Month</option>
                                    <option value="ind">1</option>
                                    <option value="ind">2</option>
                                    <option value="ind">3</option>
                                    <option value="ind">4</option>
                                    <option value="ind">5</option>
                                    <option value="ind">6</option>
                                    <option value="ind">7</option>
                                    <option value="ind">8</option>
                                    <option value="ind">9</option>
                                    <option value="ind">10</option>
                                    <option value="ind">11</option>
                                    <option value="ind">12</option>
                                </select>
                            </div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-20 mt-25">
								<select class="dropselect" id="txtPreName" >
                                    <option value="ind">year</option>
                                    <option value="ind">2017</option>
                                    <option value="ind">2018</option>
                                    <option value="ind">2019</option>
                                    <option value="ind">2020</option>
                                    <option value="ind">2021</option>
                                    <option value="ind">2022</option>
                                    <option value="ind">2023</option>
                                    <option value="ind">2024</option>
                                    <option value="ind">2025</option>
                                    <option value="ind">2026</option>
                                    <option value="ind">2027</option>
                                    <option value="ind">2028</option>
                                    <option value="ind">2029</option>
                                    <option value="ind">2030</option>
                                </select> 
							</div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-20 ">
							<label >CVV <i class="fa fa-credit-card" aria-hidden="true"></i></label>
							<input type="text" class="form-control" value="" placeholder="Email_Id">  
							</div>
						<div class="col-xs-12">
						<div class="col-md-2 col-xs-offset-3 col-md-offset-0"></div>
						<div class="col-xs-offset-3 col-md-offset-0"> 
					<div class="checkbox-block">
										<input id="accept_booking" name="accept_booking" type="checkbox" class="checkbox" value="paymentsCreditCard">
										<label class="" for="accept_booking">Save your card for faster payment. CVV will not be saved.</label>
									</div>
						</div> 
						</div>
						<div class="col-xs-offset-2 col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
						<div class="wraplist Finalprice">
						<div class="lefttitle mt-15"><i class="fa fa-inr"></i> <?php echo $final_total;?>  </div>
							
                       </div>
					</div>
						</div> 
						<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
						<button class="button primary rounded"   type="submit">Pay Now</button> 
						</div>
						</div> 
					
                        </div>
                    </div>
					
						</div>
						
						
						
						<div class="tab-pane" id="messages">
								<div class="tab_card">
                        <div class="row">
							<p class="pay_card">Pay with Net Banking</p>
                      
                          
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="1" name="payments" type="radio" class="radio" value="paymentsCredi3123tCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="2" name="paymgments" type="radio" class="radio" value="paymentsCredi323tCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="3" name="payhments" type="radio" class="radio" value="payment231sCredfitCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="4" name="payments" type="radio" class="radio" value="paymentsdgsCreditCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="5" name="payments" type="radio" class="radio" value="paymentsCryhtgrfeditCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="6" name="payments" type="radio" class="radio" value="paymentsCreqwqditCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="7" name="payments" type="radio" class="radio" value="paymentsCreditrytCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="8" name="payments" type="radio" class="radio" value="paymentsCreduiitCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="9" name="payments" type="radio" class="radio" value="paymentsCreditpolCard">
						<label class="" for="payments1"><span>Hdfc Net banking</span>
						<img src="<?php echo base_url('assets/images/bank/hdfc.png')?>" alt="Image"></label>
						</div>
					</div>
						</div>
						</div>
					
						</div>
						<div class="tab-pane" id="settings">
									<div class="tab_card">
                        <div class="row">
							<p class="pay_card">Pay with UPI</p>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  b-select-wrap">

						<div class="form-group">
						<label >Virtual Payment Address</label>
						<input type="text"  placeholder="" class="form-control" data-initialized="true" autofocus="autofocus">
						</div>
						</div>
					<div style="col-lg-12 col-md-12 col-sm-12 col-xs-12  b-select-wrap mt-25">
					<div class="list_view_updated">
						1. Enter your vpa and click on pay now. <br>
						2. You will receive a payment request from Yatra in your UPI App. <br>
						3. Enter mpin on your UPI app to authorize payment. <br>
					 </div>
					 </div>
					
						<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<div class="wraplist Finalprice">
                                                <div class="lefttitle mt-15"><i class="fa fa-inr"></i> <?php echo $final_total;?> </div>
                                                
                                            </div>
					</div>
						</div> 
						<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<button class="button primary rounded"   type="submit">Pay Now</button> 
					</div>
						</div> 
						<div class="clearfix"></div>
							<div class="col-xs-12 mt-30">
						<div class=""> 
						<p class="gray-light fs-xs block ">
						<input type="checkbox" class="form-control" />By clicking on Pay Now, you are agreeing to Terms & Conditions</p>
						</div> 
						</div>
					
                        </div>
                    </div>
						
						
						
						
						</div>
						<div class="tab-pane" id="mobile">
								<div class="tab_card">
                        <div class="row">
							<p class="pay_card">Pay with Mobile Wallet</p>
                      
                          <div class="alert alert-warning">
						<strong> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> </strong> As per government norms, monthly wallet transaction amount limit for non-KYC customers is Rs 20,000.
							</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="pfdshkfyments1" name="payments" type="radio" class="radio" value="paymentsCredi3123tCard">
						<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="payments11" name="paymgments" type="radio" class="radio" value="paymentsCredi323tCard">
					<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="payments111" name="payhments" type="radio" class="radio" value="payment231sCredfitCard">
						<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="payments1121" name="payments" type="radio" class="radio" value="paymentsdgsCreditCard">
					<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div>
					<div class="col-sm-4">
							<div class="radio-block">
						<input id="payments1234234" name="payments" type="radio" class="radio" value="paymentsCryhtgrfeditCard">
						<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="payments154654" name="payments" type="radio" class="radio" value="paymentsCreqwqditCard">
						<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="payments167" name="payments" type="radio" class="radio" value="paymentsCreditrytCard">
						<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="payments167" name="payments" type="radio" class="radio" value="paymentsCreduiitCard">
						<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div><div class="col-sm-4">
							<div class="radio-block">
						<input id="payments198" name="payments" type="radio" class="radio" value="paymentsCreditpolCard">
						<label class="" for="payments1">
						<img src="<?php echo base_url('assets/images/bank/mobi.png')?>" alt="Image"></label>
						</div>
					</div>
					
						<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<div class="wraplist Finalprice">
                                                <div class="lefttitle mt-15"><i class="fa fa-inr"></i> <?php echo $final_total;?>  </div>
                                                
                                            </div>
					</div>
						</div> 
						<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<button class="button primary rounded"   type="submit">Pay Now</button> 
					</div>
						</div> 
						<div class="clearfix"></div>
							<div class="col-xs-12 mt-30">
						<div class=""> 
						<p class="gray-light fs-xs block ">
						<input type="checkbox" class="form-control" />By clicking on Pay Now, you are agreeing to Terms & Conditions</p>
						</div> 
						</div>
						</div>
						</div>
						
						
						</div>
						<div class="tab-pane" id="upi">
								<div class="tab_card">
                        <div class="row">
							<p class="pay_card">Pay with Mobile Wallet</p>
                      <div class="cpmt_itzTxt">
							Dear Customer,<br>
							You can use a registered ITZ Cash Card for any booking on Yatra.com.
						</div>
						<div class="cpmt_itzTxt mt-20">
							If your card is registered with ITZ, please click the 'Pay Now' button to make a payment. If your card is not registered with ITZ, then please select any other payment option to proceed.
						</div>
						
							<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<div class="wraplist Finalprice">
                                                <div class="lefttitle mt-15"><i class="fa fa-inr"></i> <?php echo $final_total;?>  </div>
                                                
                                            </div>
					</div>
						</div> 
						<div class="col-md-4"> 
						<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue mt-30" >
					<button class="button primary rounded"   type="submit">Pay Now</button> 
					</div>
						</div> 
						<div class="clearfix"></div>
							<div class="col-xs-12 mt-30">
						<div class=""> 
						<p class="gray-light fs-xs block ">
						<input type="checkbox" class="form-control" />By clicking on Pay Now, you are agreeing to Terms & Conditions</p>
						</div> 
						</div>
						</div>
						</div>
						
						
						
						
						</div>
					</div>
				</div>
				</div>
        <div class="clearfix"></div>
					
				</div>
			</div>
				<div class="col-sm-12 col-md-3 col-lg-3 " style="position:initial">

							<div class="sidebar_div">
								
								<h4 class="tourname_sidebar">Payment Details</h4>
							
								<div class="subroomtitle1">
                                            <!--<div class="wraplist">
                                                <div class="lefttitle">Adult(x1) :  </div>
                                                <div class="rightval ellipsis"><span><i class="fa fa-inr"></i> 154564</span></div>
                                            </div>
                                            <div class="wraplist">
                                                <div class="lefttitle">Service Cost: </div>
                                                <div class="rightval"><span ><i class="fa fa-inr"></i> 1544</span></div>
                                            </div>-->
											
											<div class="wraplist">
                                                <div class="lefttitle">Total Amount: </div>
                                                <div class="rightval"><span ><i class="fa fa-inr"></i> <?php echo $DisFullTotal;?></span></div>
                                            </div>
											
                                            <div class="wraplist">
                                                <div class="lefttitle">GST (5% inclusive)</div>
												
												
                             <div class="rightval"><span><i class="fa fa-inr"></i> <?php echo $tax_total; ?></span></div>
                                            </div>
											
                                        
                                            <div class="wraplist Finalprice">
                                                <div class="lefttitle">you pay :  </div>
                                                <div class="rightval">
													
                                                    <span class="currencysymbol wraplist red"><i class="fa fa-inr"></i></span>
													<span class="wraplist red" ><?php echo $final_total;?></span>
                                                    
                                                </div>
                                            </div>
                                        </div>
								
							</div>
							
						</div>
		  
				
					</div>
					<br>
					
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>

<script>
function show_promo()
{
	$('.coopenbg').toggle();
}
</script>

</body>


</html>