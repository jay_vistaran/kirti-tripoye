<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/datepicker/bootstrapDatepickr-1.0.0.min.css')?>" rel="stylesheet">
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

						<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">User Profile</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><span>User Profile</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
				<!--DASHBOARD-->
			<div class="content-wrapper">
			
				<div class="container">
		<div class="db">
			<!--LEFT SECTION-->
			<div class="db-l">
				<div class="db-l-1">
					<ul>
						<?php
					
			   
					   if($this->session->userdata('loggedIn') == true)
					   {
						   ?>
							<li>
							<center><img src="<?php echo $picture_url=$this->session->userdata('picture_url'); ?>" alt="avtar" id="avtar_image"/>
					       </center>
							</li>
						<?php
							
					   }
					   else
					   {
						     ?>
							 
							 <li>
							<center>
							<img src="<?php echo base_url('assets/images/avater.png')?>" alt="avtar" id="avtar_image"  />
							</center>
							</li>
							
							
						<?php
					   }				   
					
					?>
						<li><span><?php echo $user_name=$this->session->userdata('user_name');?></span></li>

					</ul>
				</div>
				<div class="db-l-2">
					<ul>
						
						<li>
							<a href="<?php echo site_url('user-tour-booking')?>">
							<i class="fa fa-bus" aria-hidden="true"></i> &nbsp;Tour Bookings</a>
						</li>
						
						
						<li>
						<a href="<?php echo site_url('user-profile')?>">
						<i class="fa fa-user" aria-hidden="true"></i> &nbsp;My Profile</a>
						</li>
						
					</ul>
				</div>
			</div>
			<!--CENTER SECTION-->
			
				<?php foreach($profile as $row)
						{
							 $name = $row['user_name'];
							 $phone = $row['user_mobile'];
							 $user_email = $row['user_email'];
							 $user_surname = $row['user_surname'];
							 $user_dob = $row['user_dob'];
							 $user_city = $row['user_city'];
							 $user_address = $row['user_address'];
							
						}
							?>
			
			
			<div class="db-2">
				<div class="db-2-com db-2-main">
				
				 <div id="infoMessage"><?php echo $this->session->flashdata('err_message');?></div>
					<h4>Edit My Profile</h4>
					<div class="db-2-main-com db2-form-pay db2-form-com">
					
					
						<form class="col s12"  action="<?php echo base_url('authenticate/update_user')?>" method="post">
				
					<div class="row">
						 <div class="col-md-6 col-sm-6 col-xs-6">
							  <div class="form-group">
							  <input type="hidden" class="form-control"   name="cityname" >
							  
							<label class="modal_newlabel">User Name</label>
							<input type="text" class="form-control" name="user_name" value="<?php echo $name; ?>" required>
						  </div>
						  </div>
						   <div class="col-md-6 col-sm-6 col-xs-6 ">
							  <div class="form-group">
							  <input type="hidden" class="form-control"   name="user_surname" >
							<label class="modal_newlabel">User Surname</label>
							<input type="text" class="form-control" name="user_surname" value="<?php echo $user_surname; ?>" >
						  </div>
						  </div>
						
					</div>
				
					
					<div class="row">
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
							  <div class="form-group">
							  <input type="hidden" class="form-control"   name="user_email" >
							<label class="modal_newlabel">Email_id</label>
							<input type="email" class="form-control" name="user_email" value="<?php echo $user_email; ?>" readonly>
						  </div>
						  </div>
						  
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						<label  class="modal_newlabel">Phone</label>
						<input type="number" class="form-control" name="user_mobile"  value="<?php echo $phone; ?>" required>
					  </div>
					  </div>
					</div>
					
					
					<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12 ">
						<div class="form-group">
						<label  class="modal_newlabel">Date of Birth</label><br>
						<div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="user_dob" value="<?php echo $user_dob; ?>" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
						</div>
					</div>
					</div>
						  
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
						   <div class="form-group">
						<label  class="modal_newlabel">City</label>
						<input type="text" class="form-control" name="user_city"  value="<?php echo $user_city; ?>" required>
					  </div>
						 </div>
					</div>
					
					<div class="row">
						 <div class="col-md-12 col-sm-12 col-xs-12 ">
						<div class="form-group">
						<label for="exampleFormControlTextarea1"> Address</label>
					<input type="text" class="form-control" name="user_address" id=""  value="<?php echo $user_address; ?>" rows="3"></textarea>
						</div>
					  </div>
					</div>
				
					<div class="row">
						<div class="input-field col-md-12 col-sm-12">
							<i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style=""><input type="submit" value="submit" class="btn  btn-block submit_btn_new"></i> </div>
					</div>
					
				</form>	
				
				
				
					</div>
				</div>
			</div>
			<!--RIGHT SECTION-->
			<div class="db-3">
				<h4>Notifications</h4>
				
				<?php foreach($notification as $second)
						{
							     $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);	
								 $tourcatname = str_replace("&", "", $url_tourcatname);
								   
								 $url_string = preg_replace("/[\s_]/", "-", $second->tourname);
		                         $tour_id=$second->id;
								 $tours="tours";
							?>
				
				<ul>
					<li>
					
					
						<a href="<?php echo base_url($tours)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
						<img src="<?php echo base_url('assets/images/discount_notifiction.png')?>" alt="" />
							<h5><?php echo $discount = $second->discount; ?>% Discount Offer</h5>
							<p><?php echo $tourname = $second->tourname; ?></p>
						</a>
						
					</li>
					
					
					
				</ul>
				
						<?php 
						}
						?>				
			</div>
			
		</div>
	</div>
	<!--END DASHBOARD-->
	
				
				
				
			</div>
			</div>
			

		<!-- end Main Wrapper -->
				<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/moment.min.js')?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/bootstrapDatepickr-1.0.0.min.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
	 var e = $(this).scrollTop();
   if (e > 20)
   {
	  ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
	 $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
<script>
 $(function () {
   var bindDatePicker = function() {
		$(".date").datetimepicker({
        format:'YYYY-MM-DD',
			icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down"
			}
		}).find('input:first').on("blur",function () {
			// check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
			// update the format if it's yyyy-mm-dd
			var date = parseDate($(this).val());

			if (! isValidDate(date)) {
				//create date based on momentjs (we have that)
				date = moment().format('DD-MM-YYYY');
			}

			$(this).val(date);
		});
	}
   
   var isValidDate = function(value, format) {
		format = format || false;
		// lets parse the date to the best of our knowledge
		if (format) {
			value = parseDate(value);
		}

		var timestamp = Date.parse(value);

		return isNaN(timestamp) == false;
   }
   
   var parseDate = function(value) {
		var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
		if (m)
			value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

		return value;
   }
   
   bindDatePicker();
 });

</script>
</body>


</html>