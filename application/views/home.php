<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	
	
</head>

<body>

	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_wrapper_padding">

			<?php include ('header.php')?>


		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start hero-header with windows height -->
			<div class="hero" style="background-image:url('<?php echo base_url('assets/images/banner2.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						
							<h1 class="hero-title">Explore it Yourself</h1>
							<p class="lead">Discover the Fun-tastic Things to do at Unbelievable price</p>

						</div>
						
					</div>
					
					<div class="main-search-wrapper full-width">
					
						<div class="inner">      
		  
					<div class="searchbar_container col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="searchbar dropdown col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<input type="text" data-toggle="dropdown"  placeholder="Search for destinations, tours, etc." class="search_term  col-lg-9 col-md-9 col-sm-9 col-xs-8" />
						
						<div class="dropdown-menu col-lg-12 " id="search_menu">
		
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
		
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 bhoechie-tab-menu" id="all_countrys">
			
			 <?php 
				
					
					foreach (array_slice($country, 0, 1) as $rows)
					{
						
      
		$cityname= $rows->countryname;			
		
		 ?>
			
             <div class="list-group">
			 <a href="#" onmouseover="get_city_selected_country(<?php echo $rows->id;?>);" class="list-group-item  active text-center" id="first_active">

			 <h5 class="tab_head"><?php echo $cityname?></h5>  
			 </a>
			 
			 </div>
			 
			
			<?php      
           
	   }
	  foreach (array_slice($country, 1, 50) as $row)
	  {
			 ?>
			
             <div class="list-group">
			 <a href="#" onmouseover="get_city_selected_country(<?php echo $row->id;?>);" class="list-group-item   text-center">

			 <h5 class="tab_head"><?php echo $row->countryname?></h5>  
			 </a>
			 
			 </div>
			 
			<?php 
							
            }			
               
                ?>
			 
            </div>
			
			
            <div class="col-lg-9 col-md-9 col-sm-4 col-xs-7 bhoechie-tab">
                
                <div class="bhoechie-tab-content active" >
				
				
				<div class="row" id="all_city">				
				<?php 
				foreach ($country as $row) {
					$country_id = $row->id;
					
				foreach ($city as $rows)
				{
					$city_id = $rows->countryid;
					$img = $rows->city_img_url;
					if($country_id == $city_id)
					{
			         ?>	
 
 
					<div class="col-md-3 col-sm-6 col-lg-3 category_images">
					<a href="<?php echo site_url('tours')?>/'+cityNameRes+'" >
					<img src="<?php echo $img; ?>" class="img-responsive" alt="Tour Package" />
					<div class="caption"><p class="caption_para"><?php echo $rows->cityname; ?></p>
					</div>  </a>
					</div>
							
							
							<?php 
					}
				}
				}
			?>
					
					
					
				</div>
				
				
                </div>             
               
            </div>			
			
        </div>
 
						</div>
						<div class="search_btn col-lg-3 col-md-3 col-sm-4 col-xs-4">
						Search
						</div>
					</div>
		  </div>
							
							
							
							
						</div>
						
					</div>
				
				</div>
				
			</div>
			<!-- end hero-header with windows height -->
			
			<div class="post-hero bg-light" id="bottom_banner">
			
				<div class="container">
					
					<div class="row">
					
						<div class="col-sm-4 banner_bottom">
							<div class="featured-count clearfix">
								<div class="icon"><i class="pe-7s-map-marker"></i></div>
								<div class="content_featured">
									<h6>300+ Destinations</h6>
									<span>Tastes giving in passed direct me valley supply.</span>
								</div>
							</div>
						</div>
						
						<div class="col-sm-4 banner_bottom">
							<div class="featured-count clearfix">
								<div class="icon"> <i class="pe-7s-user"></i></div>
								<div class="content_featured">
									<h6>500+ Travel Guides</h6>
									<span>Prepared do an dissuade whatever steepest.</span>
								</div>
							</div>
						</div>
						
						<div class="col-sm-4 banner_bottom1">
							<div class="featured-count clearfix">
								<div class="icon"> <i class="pe-7s-smile"></i></div>
								<div class="content_featured">
									<h6>20000+ Happy Customers</h6>
									<span>Devonshire invitation discovered indulgence.</span>
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				
			</div>

			<section>
			
				<div class="container">
				
					<div class="row">
						
							<?php 
						$result=' ';
				foreach (array_slice($trending, 0, 1) as $row) {
					
        foreach($row as $second ){
		$cityname= $second->cityname;			
		
		 $result = $cityname;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{    ?><div class="col-sm-8 col-md-9">
							
							<div class="section-title">
							
								<h3>Trending Destinations</h3>
								<span class="underline_text"></span>
								<p>Of distrusts immediate enjoyment curiosity do. Marianne numerous saw thoughts the humoured.</p>
	
							</div>
							
						</div>
						<?php   }
	 
				?>
						
						
						
					
					</div>
					
					<div class="grid destination-grid-wrapper">
			
						<?php 
							foreach (array_slice($trending, 0, 1) as $row) {
				foreach($row as $second ){
					 
					  $cityname=$second->cityname;
					  $city_img_url=$second->city_img_url;
					  $url_string1 = preg_replace("/[\s_]/", "-", $second->cityname);
					  $url_string = str_replace("&", "", $url_string1);
							 
					  
			   ?>
				
		
						<div class="grid-item" data-colspan="10" data-rowspan="10" id="t11">
						
							
					<a href="<?php echo site_url('tours')?>/<?php echo strtolower($url_string); ?>" class="top-destination-image-bg" style="background-image:url(<?php echo $city_img_url; ?>);">
								<div class="relative">
									<h4><?php echo $cityname; ?></h4>
									<span></span>
								</div>
							</a>
							
							
							
						</div>
						
						<?php 
	    }
				}
				?>
				
				
				
					<?php 
				foreach (array_slice($trending, 1, 1) as $row) {
    foreach($row as $second ){
		 
		  $cityname=$second->cityname;
		  $city_img_url=$second->city_img_url;
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->cityname);
		  $url_string = str_replace("&", "", $url_string1);
                 
		}
   ?>
				
		
							
						<div class="grid-item" data-colspan="10" data-rowspan="4" id="t21">		
						
							
					<a href="<?php echo site_url('tours')?>/<?php echo strtolower($url_string); ?>" class="top-destination-image-bg" style="background-image:url(<?php echo $city_img_url; ?>);">
								<div class="relative">
									<h4><?php echo $cityname; ?></h4>
									<span></span>
								</div>
							</a>
							
							
							
						</div>
						
						<?php 
	    
				}
				?>
				
					<?php 
				foreach (array_slice($trending, 2, 1) as $row) {
    foreach($row as $second ){
		 
		  $cityname=$second->cityname;
		  $city_img_url=$second->city_img_url;
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->cityname);
		  $url_string = str_replace("&", "", $url_string1);
                 
		}
   ?>
				
		
							
					<div class="grid-item" data-colspan="5" data-rowspan="6" id="t3">	
						
							
					<a href="<?php echo site_url('tours')?>/<?php echo strtolower($url_string); ?>" class="top-destination-image-bg" style="background-image:url(<?php echo $city_img_url; ?>);">
								<div class="relative">
									<h4><?php echo $cityname; ?></h4>
									<span></span>
								</div>
							</a>
							
							
							
						</div>
						
						<?php 
	    
				}
				?>
						
						
					
			<?php 
				foreach (array_slice($trending, 3, 1) as $row) {
    foreach($row as $second ){
		 
		  $cityname=$second->cityname;
		  $city_img_url=$second->city_img_url;
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->cityname);
		  $url_string = str_replace("&", "", $url_string1);
                 
		}
   ?>
				
		
							
					<div class="grid-item" data-colspan="5" data-rowspan="6" id="t4">
						
							
					<a href="<?php echo site_url('tours')?>/<?php echo strtolower($url_string); ?>" class="top-destination-image-bg" style="background-image:url(<?php echo $city_img_url; ?>);">
								<div class="relative">
									<h4><?php echo $cityname; ?></h4>
									<span></span>
								</div>
							</a>
							
							
							
						</div>
						
						<?php 
	    
				}
				?>
                        
						
						
						<?php 
				foreach (array_slice($trending, 4, 1) as $row) {
    foreach($row as $second ){
		 
		  $cityname=$second->cityname;
		  $city_img_url=$second->city_img_url;
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->cityname);
		  $url_string = str_replace("&", "", $url_string1);
                 
		}
   ?>
				
		
							
						<div class="grid-item" data-colspan="10" data-rowspan="5" id="t5">
							
					<a href="<?php echo site_url('tours')?>/<?php echo strtolower($url_string); ?>" class="top-destination-image-bg" style="background-image:url(<?php echo $city_img_url; ?>);">
								<div class="relative">
									<h4><?php echo $cityname; ?></h4>
									<span></span>
								</div>
							</a>
							
							
							
						</div>
						
						<?php 
	    
				}
				?>
						
						
						
									<?php 
				foreach (array_slice($trending, 5, 1) as $row) {
    foreach($row as $second ){
		 
		  $cityname=$second->cityname;
		  $city_img_url=$second->city_img_url;
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->cityname);
		  $url_string = str_replace("&", "", $url_string1);
                 
		}
   ?>
				
		
							
							<div class="grid-item" data-colspan="5" data-rowspan="5" id="t6">
							
					<a href="<?php echo site_url('tours')?>/<?php echo strtolower($url_string); ?>" class="top-destination-image-bg" style="background-image:url(<?php echo $city_img_url; ?>);">
								<div class="relative">
									<h4><?php echo $cityname; ?></h4>
									<span></span>
								</div>
							</a>
							
							
							
						</div>
						
						<?php 
	    
				}
				?>
					
					
						
						
						
						
									<?php 
				foreach (array_slice($trending, 6, 1) as $row) {
    foreach($row as $second ){
		 
		  $cityname=$second->cityname;
		  $city_img_url=$second->city_img_url;
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->cityname);
		  $url_string = str_replace("&", "", $url_string1);
                 
		}
   ?>
				
		
							
						<div class="grid-item" data-colspan="5" data-rowspan="5" id="t7">
							
					<a href="<?php echo site_url('tours')?>/<?php echo strtolower($url_string); ?>" class="top-destination-image-bg" style="background-image:url(<?php echo $city_img_url; ?>);">
								<div class="relative">
									<h4><?php echo $cityname; ?></h4>
									<span></span>
								</div>
							</a>
							
							
							
						</div>
						
						<?php 
	    
				}
				?>
					
						
						
						
						
					</div>
					
				</div>
				
			</section>
			
				<section class="hotel_selling">

				<div class="container">
				
					<div class="row">
						
							<?php 
						$result=' ';
				foreach (array_slice($hotselling, 0, 1) as $row) {
					
        foreach($row as $second ){
		$tourname= $second->tourname;			
		
		 $result = $tourname;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{    ?><div class="col-sm-8 col-md-9 ">
							
							<div class="section-title">
							
								<h3 class="packages_name">Hot Selling Packages</h3>
								<span class="underline_text"></span>
								<p>Of distrusts immediate enjoyment curiosity do. Marianne numerous saw thoughts the humoured.</p>
								
							</div>
													
						</div>
						<?php   }
	 
				?>
				
					
						
						
						<?php 
						$result=' ';
				foreach (array_slice($hotselling, 8, 9) as $row) {
					
        foreach($row as $second ){
		$tourname= $second->tourname;			
		
		 $result = $tourname;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{    ?><div class="col-sm-4 col-md-3">
							<nav>
								<ul class="control-box pager">
									<li class="control_slide"><a data-slide="prev" href="#carousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
									<li class="control_slide"><a data-slide="next" href="#carousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
								</ul>
							</nav>
						</div>
						<?php   }
	 
				?>
				
													 
						
													
						
						
					
						
						
						
						
					</div>
					<div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false">
							
									<div class="carousel-inner">

																	
						<div class="item active">
							<div class="carousel-col">
							    <div class=" package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight" id="hot_sellings">
									<?php 
									 $i=0;
									foreach (array_slice($hotselling, 0, 8) as $row) {
									foreach($row as $second ){
										
								   $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);	
								   $tourcatname = str_replace("&", "", $url_tourcatname);
								   
								   $url_string = preg_replace("/[\s_]/", "-", $second->tourname);
		                           $tour_id=$second->id;
								   $tours="tours";
								   
   ?>
											
									
									<div class="carousel-col col-md-3 col-sm-6 col-xs-12 mb-30" id="package-grid-item">
										<a href="<?php echo base_url($tours)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
											<div class="image">
												<img src="<?php echo $second->gallery_img_url;?>/<?php echo $second->gallery_img;?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->discount;?>% off</a>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->tdiscount;?>% off</a>
													<?php
													}
													
										}		
													?>
												
													
													<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
													
													</span>

													</div>
												</div>
											</div>
											 <div class="content clearfix">
											<h5 class="city_name_head"><?php echo $second->tourname;?></h5>
											<div class="left_hand_sidedgsdf">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											
											
											<span>
											<?php 
											
										foreach(array_slice($hotselling_tour_rating , $i, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{?>
										      Newly Arrived
											<?php
											}
											else
											{
												echo $rating;
											}
											
											
											?>						
											
											</span>											
											
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<?php
											foreach(array_slice($hotselling_tour_rating , $i, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{?>
										     
											<?php
											}
											else
											{ ?>
										 <p class="rating_star">(<?php echo  $rating ;?> ratings)</p>
												<?php
											}
											
											
											?>

											</div>
											</div>
										
											<div class="product-detail-right">
											
											<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 ?>
														<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></span></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														<div class="product-price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}
													
											 }
											          ?>
											
											
											
											</div>
											
											</div>
									
										</a>
										</div>
										
										
										<?php 
										   }
										 $i++ ;
										 }
																			 
               
                                       ?>
										
										
									</div>
								</div>
							</div>
						</div>
						
				
 
						
										<div class="item ">
							<div class="carousel-col">
							    <div class=" package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight" id="hot_sellings">
									<?php 
									$j=8;
									foreach (array_slice($hotselling, 8, 16) as $row) {
									foreach($row as $second ){
										
								   $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);	
								   $tourcatname = str_replace("&", "", $url_tourcatname);
								   
								   $url_string = preg_replace("/[\s_]/", "-", $second->tourname);
		                           $tour_id=$second->id;
								   $tours="tours";
								   
   ?>
											
									
									<div class="carousel-col col-md-3 col-sm-6 col-xs-12 mb-30" id="package-grid-item">
										<a href="<?php echo base_url($tours)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
											<div class="image">
												<img src="<?php echo $second->gallery_img_url;?>/<?php echo $second->gallery_img;?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->discount;?>% off</a>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->tdiscount;?>% off</a>
													<?php
													}
													
										}		
													?>
												
													
													<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
													
													</span>

													</div>
												</div>
											</div>
											 <div class="content clearfix">
											<h5 class="city_name_head"><?php echo $second->tourname;?></h5>
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>	
											<?php 
											
										foreach(array_slice($hotselling_tour_rating , $j, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{?>
										      Newly Arrived
											<?php
											}
											else
											{
												echo  $rating;
											}
											
											
											?>
											
											</span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<?php
											foreach(array_slice($hotselling_tour_rating , $j, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{?>
										     
											<?php
											}
											else
											{ ?>
										 <p class="rating_star">(<?php echo  $rating ;?> ratings)</p>
												<?php
											}
											
											
											?>
											

											</div>
											</div>
										
											<div class="product-detail-right">
											
											<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 ?>
														<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></span></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
													<div class="product-price">
													<span><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo round($final_price);?></span>
													</div>
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														<div class="product-price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}
													
											 }
											          ?>
											
											
											
											</div>
											
											</div>
									
										</a>
										</div>
										
										
										<?php 
										 }
										$j++;
										}			
               
                                       ?>
										
										
									</div>
								</div>
							</div>
						</div>
						
								
								
								
							</div>
						</div>
					</div>
				
			</section>
			
			
			<section class="overflow-hidden why-us-half-image-wrapper">
			
				<div class="GridLex-grid-noGutter-equalHeight">
						
					<div class="GridLex-col-6_sm-12">
						
						<div class="why-us-half-image-content">
						
							<div class="section-title text-left">
							
								<h3>Why Booking With Us</h3>
								<span class="underline_text"></span>

								<p>There are several seasons that you must travel with us</p>
								
							</div>
							
							<div class="featured-item">
							
								<h4>Experts On Tour</h4>
								
								<div class="why_content clearfix">
								
									<div class="icon">
										<i class="pe-7s-users"></i>
									</div>
									
									<p>Blind would equal while oh mr lain led and fact none. One preferred sportsmen resolving the happiness continued. High at of in loud rich true.</p>
									
								</div>
							</div>
							
							<div class="featured-item">
							
								<h4>Quality Accommodation</h4>
								
								<div class="why_content clearfix">
								
									<div class="icon">
										<i class="pe-7s-home"></i>
									</div>
									
									<p>Admiration stimulated cultivated reasonable be projection possession of. Real no near room ye bred sake if some. Is arranging furnished knowledge.</p>
									
								</div>
							</div>
							
							
							<div class="featured-item">
							
								<h4>Comfortable Transport</h4>
								
								<div class="why_content clearfix">
								
									<div class="icon">
										<i class="pe-7s-car"></i>
									</div>
									
									<p>Effect twenty indeed beyond for not had county. The use him without greatly can private. Increasing it unpleasant no of contrasted no continuing.</p>
									
								</div>
							</div>
							
						</div>
						
					</div>
					
					<div class="GridLex-col-6_sm-12 image-bg">
						<div class="image-bg" style="background-image:url('<?php echo base_url('assets/images/image-01.jpg')?>');"></div>
					</div>
				
				</div>
				
			</section>
			
			
				<section class="hotel_selling">

				<div class="container">
				
					<div class="row">
						
						
								<?php 
						$result=' ';
				foreach (array_slice($combo, 0, 1) as $row) {
					
        foreach($row as $second ){
		
		$comboid= $second->comboid;			
		 $result = $comboid;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{   echo $result ?> <div class="col-sm-8 col-md-9 ">
							
							<div class="section-title">
							
								<h3>Exclusive Combo Deals</h3>
								<span class="underline_text"></span>
								<p>Of distrusts immediate enjoyment curiosity do. Marianne numerous saw thoughts the humoured.</p>
								
							</div>
							
						</div><?php   }
	 
				?>
						
						
						
						
						
						<?php 
						$result=' ';
				foreach (array_slice($combo, 4, 5) as $row) {
					
        foreach($row as $second ){
		
		$comboid= $second->comboid;			
		 $result = $comboid;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{   echo $result ?> <div class="col-sm-3 col-md-3 ">
					     <nav>
								<ul class="control-box pager">
									<li class="control_slide"><a data-slide="prev" href="#carousel1" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
									<li  class="control_slide"><a data-slide="next" href="#carousel1" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
								</ul>
					    </nav>					
					</div><?php   }
	 
				?>
				
				
					</div>
					<div id="carousel1" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false">
							
									<div class="carousel-inner">

						<div class="item active">
							<div class="carousel-col">
							    <div class=" package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight" id="ex_c1ww">
									
									<?php 
				foreach (array_slice($combo, 0, 4) as $row) {
					$result=' ';
    foreach($row as $second ){
		
		$comboid= $second->comboid;
		
		$name= $second->comboname;
		$comboimageurl= $second->comboimageurl;
		$comboDiscount= $second->comboDiscount;
		$tourorder= $second->tourname;
		$tournamelist= $second->tourname;
		
		$combobuyAP= $second->sellAP;
		$combobuyCP= $second->buyCP;
		
		 $result .= $tournamelist .'+';        
                     
   
// ....Display Price Not all Sum
				
						
					    	/* if(i < 2)
							   {
								   tournamelisting +=  tournamelist + '+';  							       
							   }
							     sumAP += parseInt(combobuyAP);  
							     sumCP += parseInt(combobuyCP); 
							  
							  	
						var alltournamelist = tournamelisting + '...' */
	}
	 
   ?>
							
					
								<div class="carousel-col col-md-3 col-sm-6 col-xs-12 mb-30" id="package-grid-item">
								
										<a href="#">
											<div class="image">
												<img src="<?php echo $comboimageurl ?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php
												
													if($comboDiscount == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
															<a href="#" class="tag1"><?php echo $comboDiscount ?>% off</a>
													<?php
													}
													?>
												
													
													
														<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
														<!--<i class="fa fa-heart-o"></i>-->
														</span>
													</div>
												</div>
											</div>
											
								<div class="content clearfix">
											<h5 class="city_name_head"><?php  echo $name;?></h5>
											<h4 class="combo_name"><?php  echo $result;?></h4>

											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>4.6 </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<p class="rating_star">(526 ratings)</p>

											</div>
											</div>
											<div class="right_hand_side">
											<div class="absolute-in-content1">
											<div class="price1"><strike> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyAP ?></strike></div>
											<div class="price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyCP ?></div>
											</div>
											</div>
											</div>
											
										</a>
											
									</div>
										
									<?php 
										
            }			
               
                ?>		
										
									
									</div>
								</div>
							</div>
						</div>
												<div class="item">
												<div class="carousel-col">
								<div class="package-grid-item-wrapper">
								<div class="GridLex-grid-noGutter-equalHeight">
			
						
						
						<?php 
				foreach (array_slice($combo, 4, 8) as $row) {
					$result=' ';
    foreach($row as $second ){
		
		$comboid= $second->comboid;
		
		$name= $second->comboname;
		$comboimageurl= $second->comboimageurl;
		$comboDiscount= $second->comboDiscount;
		$tourorder= $second->tourname;
		$tournamelist= $second->tourname;
		
		$combobuyAP= $second->sellAP;
		$combobuyCP= $second->buyCP;
		
		 $result .= $tournamelist .'+';        
                     
   
// ....Display Price Not all Sum
				
						
					    	/* if(i < 2)
							   {
								   tournamelisting +=  tournamelist + '+';  							       
							   }
							     sumAP += parseInt(combobuyAP);  
							     sumCP += parseInt(combobuyCP); 
							  
							  	
						var alltournamelist = tournamelisting + '...' */
	}
	 
   ?>
							
					
									<div class="carousel-col GridLex-col-3_sm-6_xs-12 mb-30" id="package-grid-item">
								
										<a href="#">
											<div class="image">
												<img src="<?php echo $comboimageurl ?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php
												
													if($comboDiscount == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
															<a href="#" class="tag1"><?php echo $comboDiscount ?>% off</a>
													<?php
													}
													?>
												
													
													
														<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
														<!--<i class="fa fa-heart-o"></i>-->
														</span>
													</div>
												</div>
											</div>
								 <div class="content clearfix">
											<h5 class="city_name_head"><?php  echo $name;?></h5>
											<h4 class="combo_name"><?php  echo $result;?></h4>

											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>4.6 </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<p class="rating_star">(526 ratings)</p>

											</div>
											</div>
											<div class="right_hand_side">
											<div class="absolute-in-content">
											<div class="price1"><strike> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyAP ?></strike></div>
											<div class="price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyCP ?></div>
											</div>
											</div>
											</div>	
											
											
										</a>
											
									</div>
									<?php 
										
            }			
               
                ?>		
										
						
							
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
			</section>
		
			
			<div class="newsletter-wrapper">
			
				<div class="container">
				
					<div class="flex-row flex-align-middle flex-gap-30">
						
						<div class="flex-column flex-sm-12">
							<div class="newsletter-text clearfix">
								<div class="icon">
									<i class="pe-7s-mail"></i>
								</div>
								<div class="content">
									<h3>Signup for Newsletter</h3>
									<p>Affronting everything discretion men now own did. Still round match we to. Frankness pronounce daughters remainder extensive has but.</p>
								</div>
							</div>
						</div>
						
						<div class="flex-columns flex-sm-12">
							<div class="newsletter-form">
								<form class="" >
								<b><p id="msg66" style="color:red;font-weight:bold;">   </p>
			                 <p id="msg5" style="color:red;font-weight:bold;">  </p>
									<div class="input-group">
									
										<input type="email" class="form-control" id="mail"  name="newsletter" placeholder="Enter Your Email" required>
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button"  onclick="create_subscriber()">Signup <i class="fa fa-long-arrow-right"></i></button>
										</span>
										
									</div>
								</form>
							</div>
						</div>
					
					</div>
					
				</div>
				
			</div>
			
			<div class="overflow-hidden">
			
				<div class="instagram-wrapper">
					<div id="instagram" class="instagram"></div>
				</div>
				
			</div>
			
		</div>
		<!-- end Main Wrapper -->

				<?php include ('footer.php')?>

		
	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/megamenu.js')?>"></script>
<script type="text/javascript">
  /* <![CDATA[ */   
  var mega_menu = '0';
  
  /* ]]> */
  </script>
  <script type="text/javascript">
  
	/* jQuery('.mega-menu-title').on('click', function() { */
		$(document).ready(function(){
		if (jQuery('.mega-menu-category').is(':visible')) {
			jQuery('.mega-menu-category').slideUp();
			alert(dsafisdf);

		} else {
			jQuery('.mega-menu-category').slideDown();
		}
	});


jQuery('.mega-menu-category .nav > li').hover(function() {
	jQuery(this).addClass("active");
	jQuery(this).find('.popup').stop(true, true).fadeIn('slow');
}, function() {
	jQuery(this).removeClass("active");
	jQuery(this).find('.popup').stop(true, true).fadeOut('slow');
});


jQuery('.mega-menu-category .nav > li.view-more').on('click', function(e) {
	if (jQuery('.mega-menu-category .nav > li.more-menu').is(':visible')) {
		jQuery('.mega-menu-category .nav > li.more-menu').stop().slideUp();
		jQuery(this).find('a').text('More category');
	} else {
		jQuery('.mega-menu-category .nav > li.more-menu').stop().slideDown();
		jQuery(this).find('a').text('Close menu');
	}
	e.preventDefault();
});

  </script> 
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>



<script>

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>
<script>
function show_search_menu()
{
 var x = document.getElementById("search_menu");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }

}
</script>


<script>


$(document).ready(function(){

	var id='1';
	$.ajax({
            url : "<?php echo site_url('section_manager/get_city_by_selected_country')?>" ,
            type: "POST",
            dataType: "JSON",
	        data: { id : id},
            success: function(data)
            {
				$("#all_city").empty();
			 for(var i=0;i<data.length;i++)     
                {
                    var a = data[i]['id'];
                    var b = data[i]['cityname'];
                    var c = data[i]['city_img_url'];					
					 cityName = b.replace(/\s/g, "-");                          		
					 var cityNameRes = cityName.toLowerCase();
					 
 $('#all_city').append('<div class="col-md-3 col-sm-6 col-lg-3 category_images"><a href="<?php echo site_url('tours')?>/'+cityNameRes+'" ><img src="'+ c +'" class="img-responsive" alt="Tour Package" /><div class="caption"><p class="caption_para">'+ b+'</p></div>  </a></div>');
                   
                 }
				
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               // alert('Error deleting data');
            }
        });
	
})
function get_city_selected_country(id)
{ 
	
	$('#first_active').removeClass( 'list-group-item  active text-center' );
	$('#first_active').addClass( 'list-group-item   text-center' );
	
	//var element = document.getElementById("first_active");
    //element.classList.add("list-group-item   text-center");
	
	$.ajax({
            url : "<?php echo site_url('section_manager/get_city_by_selected_country')?>" ,
            type: "POST",
            dataType: "JSON",
	        data: { id : id},
            success: function(data)
            {
				$("#all_city").empty();
			 for(var i=0;i<data.length;i++)     
                {
                    var a = data[i]['id'];
                    var b = data[i]['cityname'];
                    var c = data[i]['city_img_url'];	
				
					 cityName = b.replace(/\s/g, "-"); 
                 var cityNameRes = cityName.toLowerCase();					 
								
 $('#all_city').append('<div class="col-md-3 col-sm-6 col-lg-3 category_images"><a href="<?php echo site_url('tours')?>/'+cityNameRes+'" ><img src="'+ c +'" class="img-responsive" alt="Tour Package" /><div class="caption"><p class="caption_para">'+ b+'</p></div>  </a></div>');
                   
                 }
				
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               // alert('Error deleting data');
            }
        });
}

</script>
<script>
			
function create_subscriber()
{
    
    var url;
 
      
  var id1=document.getElementById("mail").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
   message="Please Enter Your Valid Email Id";
     document.getElementById("msg5").innerHTML = message;
      $("#msg66").hide();
      $("#msg5").show();
 }
 else{
   url = "<?php echo site_url('home/saveNewletter')?>";
  
   
       var id1=document.getElementById("mail").value;
      
$.ajax({
        url : url,
        type: "POST",
      // data:('#subscribe-form').serialize(),
        data:{newsletter:id1},
        dataType: "JSON",
     
        success: function(data)
        {
   message="Your Subscription is  Successfully Completed";
     document.getElementById("msg66").innerHTML = message;
      $("#msg5").hide();
      $("#msg66").show();
      // document.getElementById("subscribe-form").reset(); 
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
   message="Please Enter Your Valid Email Id";
     document.getElementById("msg5").innerHTML = message;
      $("#msg66").hide();
      $("#msg5").show();
  }
}          
           
           
 </script>						


 <script>
			
function login()
{
    
    var url;
      
  var id1=document.getElementById("login_username").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
 }
 else{
	
     url = "<?php echo site_url('authenticate/login')?>";
  
   
       var login_username=document.getElementById("login_username").value;
       var login_password=document.getElementById("login_password").value;
      
$.ajax({
        url : url,
        type: "POST",
       //data:('#login-form').serialize(),
		
        data:{user_email:login_username,user_password:login_password},
        dataType: "JSON",
     
        success: function(data)
        {
			
			if(data == true)
			{
				
				 window.location="<?php echo base_url('user-profile')?>";
			}else
			{
				// message="Please Enter Correct Username And Password,,,....";
			   ///document.getElementById("msg66a").innerHTML = message;
			   $("#msg5a").hide();
			   $("#msg66a").show();
			}
			  
     
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
  }
}          
           
     

 </script>	
 
 
 
 <script>
			
function sign_up()
{
    
    var url;
      
  var id1=document.getElementById("register_email").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
	  $("#msg6Sign_up").hide();	
 }
 else{

     url = "<?php echo site_url('authenticate/sign_up')?>";
  
   
       var register_email=document.getElementById("register_email").value;
	   
       var register_username=document.getElementById("register_username").value;
	   
       var register_password=document.getElementById("register_password").value;
      
$.ajax({
        url : url,
        type: "POST",
       //data:('#login-form').serialize(),
		
        data:{user_email:register_email,user_name:register_username,user_password:register_password},
        dataType: "JSON",
     
        success: function(data)
        {
			
			if(data == true)
			{
		         $("#msg6Sign_up").show();				
				// window.location="<?php echo base_url('user-profile')?>";
			}else
			{
				 //message="Please Enter Correct Username And Password,,,....";
			   //document.getElementById("msg66a").innerHTML = message;
			   $("#msg5a").hide();
			   $("#msg66aR").show();
			    $("#msg6Sign_up").hide();	
			}
			  
     
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
     // message="Please Enter Your Valid Email Id";
     // document.getElementById("msg5a").innerHTML = message;
      $("#msg66a").hide();
      $("#msg5a").show();
	   $("#msg6Sign_up").hide();	
  }
}          
           
     
$('#register_password, #register_password_confirm').on('keyup', function () 
{
  if ($('#register_password').val() == $('#register_password_confirm').val())
	  {
        $('#message').html('Password Matching').css('color', 'green');
     }
     else 
        $('#message').html('Password Not Matching').css('color', 'red');

});
 </script>
 
 
 
</body>
</html>