<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_wrapper_padding">
			<!-- start Header -->
		<!-- start Header -->
	<header id="header">
	  
	<!-- start Navbar (Header) -->
	<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
	
	
	<div class="navbar-top" >
	
	<div class="container">
	
	<div class="flex-row flex-align-middle">
	<div class="flex-shrink flex-columns" id="test">
	<a class='navbar-logo' href='<?php echo site_url('home')?>'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>' id="logo2" class='img-responsive' >
	</a>	
	</div>
	
	</div>

	</div>
	
	</div>
	
	<div id="slicknav-mobile"></div>


	
	</nav>
	<!-- end Navbar (Header) -->

	</header>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
			
	
			
			<div class="content-wrapper">
			
				<div class="container">
					<div class="row">
					<div class="col-md-12 thanks_top" >
					<center>
					<svg class="checkmark" xmlns="" viewBox="0 0 52 52">
				<circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
				<path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
				</svg>
					</center>
				
					<div class="thanks">
				
				<h3>Thank You!</h3>
			  <h5>Your Enquiry is received and we will contact you soon.</h5>
			  <center>
				 <p class="lead">
				<a class="btn btn-primary " href="<?php echo site_url('home')?>" role="button">Back to home</a>
			  </p>				
					</center>
 
				</div>
						
				</div>

					</div>
				
				</div>
					
			</div>

		</div>
		<!-- end Main Wrapper -->
		<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/toggle.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrapDatepickr-1.0.0.min.js')?>"></script>

</body>
</html>