<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title> Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	
	
</head>

<body class="">

	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_wrapper_padding">

							<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Privacy Policy</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><span>Privacy Policy</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
			
					<div class="row">
					
						
						<div class="col-sm-12 col-md-12">

							<div class="static-wrapper">
								
							<div class="section-title">
							
								<h3 class="contact_office1">Privacy Policy</h3>
								<span class="underline_text"></span>

								<p>The below mentioned Privacy policy describes how TripOye collects, shares, and uses your information, data on the website.</p>
							
						   </div>
					          <p><b>Information collection & Usage</b></p>
									
								<p>We keep a track of your site activities that include bookmarking, postings, comments, and connections. We do collect your personal information, wherever required such as full name, age, gender, email address, payment details but ensure they are never disclosed to untrusted sources. Your personal information is secure with TripOye. To obtain statistical information, our servers automatically record IP addresses, browser type, number of clicks, search terms etc. for our analytical purposes. In case you opt to connect your account with Facebook, twitter or any social media, we will receive your basic information and shall only be used to understand your preference.</p>
									
							
							
							    <p><b>Content</b></p>
									
								<p>We have designed our services to enable our users to share the valuable information with the world. You are welcome to participate in the site by expressing your views through comments, likes, shares, write tips, upload pictures. Such information will be displayed to the public so that they also benefit from it.</p>
								
								
								<p><b>Web Beacons and Cookies Policy</b></p>
									
								<p>TripOye uses cookies to make your site experience even better. Cookies are small pieces of data installed on your browser to understand which areas of our website is popular and where do you like spending most of your time. It helps us to customize internet settings according to your tastes and preferences. Cookies are used to show relevant advertisements (including the third parties) as well. Settings can be made by the user to accept or decline these cookies. However, a few sections of our website may not function properly if you decline cookies. Pixel tags or Web beacons, as they are popularly known are small transparent graphic used in the combination with cookies so as to monitor some of your activities on our website and personalize your experience with us.</p>
								
								
								<p><b>Account deactivation</b></p>
									
								<p>In case if you deactivate your account, the comments, reviews, tips, and images you uploaded will no longer be available to other users, except for the spot information contribution that you’ve made to the information. You may reactivate your account within a year but it will automatically be deleted after a year of no reactivation. The account can be deleted immediately also at your personal request.</p>
								
								<p><b>User Account and information Management</b></p>
									
								<p>We might share your public or non-personal content to a broader audience including media platforms and other websites. We may also use third party service providers to perform certain functions on our behalf, including technical services and customer support. We might also share your information if we believe that disclosure is justified to either establish our or a third party’s legal rights or in order to defend against legal claims, and take action against alleged illegal activities.</p>

                                  
								<p><b>Security</b></p>
									
								<p>We take all the required steps to safeguard personal information of our customers from loss or misuse. We exercise sensibly and commercially accepted means to safeguard your information, but we cannot promise absolute security or liability.</p>
								
								
								
								<p><b>Contact information</b></p>
									
								<p>In case of any questions, comments or concerns about TripOye’s Privacy Policy or the practices of the website, kindly contact us at info@tripoye.com.</p>
								
								<p>This Privacy Policy is effective as of January 1, 2018. We explicitly reserve the right to change this Policy from time to time. You must acknowledge and agree that it is your accountability to review the site and its privacy policy from time to time and to educate yourself with any modifications. </p>
									
								
								
							</div>
						
						</div>

					</div>

				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->
	
	<?php include ('footer.php')?>

	</div>  <!-- end Container Wrapper -->
 

 
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>

</body>

</html>