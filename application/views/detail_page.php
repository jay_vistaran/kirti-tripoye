<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/datepicker/bootstrapDatepickr-1.0.0.min.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body class=""  onload="changeText()">

<script>
function changeText()
{ 
   
$('#costumModal30').on('hidden.bs.modal', function (e) 
{      
    location.reload();
})
     
}
document.onclick=function()
    {
 changeText();
 };
</script>


	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

	<?php include ('header.php')?>
	
	<div class="clear"></div>
	
	<!-- start Main Wrapper -->
	<div class="main-wrapper scrollspy-container">
	 
				       <?php 
						foreach ($tourInfo as $row) {
							
                          $cityname=$row->cityname;
                           $tourcatid=$row->id;
						   
			              $tourname=$row->tourname;
			              $tour_id=$row->tour_id;
						  
						  
			              $tourcatname=$row->tourcatname;
			              $tourcat_id=$row->id;
						  
						  $cutoffday=$row->cutoffday;
						  
			              $banner_img=$row->banner_img;
	                      $banner_img_url=$row->banner_img_url;
						  
						  $tourdescription=$row->tourdescription;
						  $inclusion=$row->inclusion;
						  
						  $duration=$row->duration;
						  $tlocation=$row->tlocation;
						  $tourlanguage=$row->tourlanguage;
						  $impinformation=$row->impinformation;
						  $itinerarydescription=$row->itinerarydescription;
						  
						   $departurepoint=$row->departurepoint;
						  $reportingpoint=$row->reportingpoint;
						  
						  
	
				         }?>
	<!-- start end Page title -->

	<div class="page-title1 " style="background-image:url('<?php echo $banner_img_url?>/<?php echo $banner_img?>');">
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
	<div class="col-md-offset-1 col-md-8 col-lg-8 col-sm-12 col-xs-12 ">
	<div class="product_image_text ">
	<div class="tag_div">
	<div class="tag">Attractions</div>
	</div>
	
	<div class="title" id="title_page"><?php echo $tourname; ?></div>
	<div class="clearfix"></div>
	<ol class="breadcrumb-list" id="breadcrumb_list_id">
        <li><a href="<?php echo base_url('home'); ?>">Home</a></li>
        <li><a href="<?php echo base_url('tours'); ?>/<?php echo strtolower($cityname); ?>"><?php echo strtolower($cityname); ?></a></li>
      
	 <?php
	  
	 
	      $url_string = str_replace("&", "", $tourcatname);
		  $url_string1 = preg_replace("/[\s_]/", "-", $url_string);
		
		  $tours="tours";
	 ?>
	  
	  
		  <li>
		  <a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityname)?>/<?php echo $tourcat_id ?>-<?php echo strtolower($url_string1);?>">  <?php echo $tourcatname; ?>   </a>
		  
		  </li>
		    <li><?php echo $tourname; ?></li>
       
    </ol>
		<div class="clearfix"></div>
   
	<div class="subtitle">
	
	<?php 
	
	                   $TotalRatingUsers;
if($TotalRatingUsers == 0)
{
	$TotalRatingUsers = 0;
}
else
{
	$TotalRatingUsers =$TotalRatingUsers;
}
	
	                   $result='';
						foreach (array_slice($TotalRatinCount, 0, 1) as $row) 
						{
                           $tour_rating = $row['tour_rating'];	
						   $result=$tour_rating;
						}
						
						  if($result == null)
							  {
								$TourReview =0;
							  }
							  else
							  {   
						       $TourReviews= $tour_rating /  $TotalRatingUsers; 
						       $TourReview = round($TourReviews);
						       }
	 
				
						
						
	?>
	
	
	  <?php 
	   if ($TourReview == '5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	   }elseif($TourReview == '3.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	  }elseif($TourReview == '3')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
		   
	   }elseif($TourReview == '2.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
	    }elseif($TourReview == '2')
		{
			?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		 }elseif($TourReview == '1.5')
		 {
			 ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }elseif($TourReview == '1')
		 {
			  ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }else{
			 
			  ?>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }
		   ?>
	<span>(<?php echo  $TourReview;?> Reviews)</span>
	
	
	
	
	</div>
	
	
	</div>
	</div>
	</div>

	
	</div>
	<!-- end Page title -->
	
	
	<div class="content-wrapper">
	
	<div class="container">
	
	
	

	<div class="row">
	
	
	<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12" role="main">

	<div class="detail-content-wrapper">

	
	<div class="top_icons col-sm-12 col-xs-12">
	
	
	<div class="icon_block col-sm-2 col-xs-4">
	
	<center><i class="fa fa-clock-o" aria-hidden="true"></i></center>
	<div class="text"><?php echo $duration?></div>
	</div>
	
	<div class="icon_block col-sm-2 col-xs-4">
	<center><i class="fa fa-list" aria-hidden="true"></i></center>
	<div class="text">Availability Daily</div>
	</div>
	
	
	
	
			<?php 
		          foreach ($tour_prices as $row) {
									foreach($row as $second )
									        {									
											 $tour_dis=$second->tdiscount;
											  $servicedis=$second->discount;
									        }
											if($tour_dis == 0)
										{
												if($servicedis == 0)
												{
													?>
														<div class="icon_block col-sm-2 col-xs-4">
														<center><i class="fa fa-tags" aria-hidden="true"></i></center>
														<div class="text">Best Price Guarantee</div>
														</div>
													<?php
													
												}else{
													?>													  
														    <div class="icon_block col-sm-2 col-xs-4">
															<center><i class="fa fa-gift" aria-hidden="true"></i></center>
															<div class="text">Upto <?php echo $Sdis=$second->discount;;?>% Offers</div>
															</div>
													<?php
												}
													
														 									
															
										}else{
                                                       ?>													  
														    <div class="icon_block col-sm-2 col-xs-4">
												<center><i class="fa fa-gift" aria-hidden="true"></i></center>
															<div class="text">Upto <?php echo $dis=$second->tdiscount;;?>% Offers</div>
															</div>
													<?php
													}
													
												
									      }			
													?>
													
													
	
	
	
	
	<div class="icon_block col-sm-2 col-xs-4">
	<center><i class="fa fa-map-marker" aria-hidden="true"></i></center>
	<div class="text"><?php echo $tlocation?></div>
	</div>
	
	
	
	<div class="icon_block col-sm-2 col-xs-4">
	<center><i class="fa fa-language" aria-hidden="true"></i>
	</center>
	<div class="text"><?php echo $tourlanguage?></div>
	</div>	
	</div>
	
				<?php 
						$result=' ';
				foreach (array_slice($tourRating, 0, 1) as $row)
				    {
						 $user_name=$row->user_name;	
						
						 $result = $user_name;       
          
	                }
		  if($result == ' ')
		  {
			
		  }
		  else
		  {  
		  ?>
		  
		  <div class="reviews zero_padding col-sm-12 col-xs-12" id="reviews">
	      <div class="main_heading">Reviews</div>
	      <div class="r_slider_container col-sm-12 col-xs-12">
	
	
			   <?php 
						$result=' ';
				foreach (array_slice($tourRating, 1, 2) as $row) 
				{
		                 $user_name=$row->user_name;							
						 $result = $user_name;     
         
	             }
		  if($result == ' ')
		  {
			
		  }
		  else
		  {   
		  ?>
		               <div class="r_slider_nav_btns" id="sliderDown" data-toggle="tooltip" title="">
						<img src="<?php echo base_url('assets/images/product_details/back.png')?>" />
						</div>
						<?php  
						}
	 
				     ?>
	
	
	
	
	
	
	
	<div id="review_slideshow" class="col-sm-12 col-xs-12">
	
	<?php 
	        $i=1;
						foreach ($tourRating as $row) {
							
                          $user_name=$row->user_name;
						  
                          $user_surname=$row->user_surname;
						   $tour_comment=$row->tour_comment;
						  
						  $created=$row->created;
						  $tour_rating=$row->tour_rating;
	
	
	?>
	<div class="current review col-sm-12 col-xs-12">
	<div class="review_image col-lg-1 col-sm-2 col-xs-3">
	<div>
	<img src="<?php echo base_url('assets/images/product_details/user_image.png')?>" />
	</div>
	</div>
	<div class="review_content col-lg-11 col-sm-10 col-xs-9">
	<div class="content_header">
	<div class="name_date">
	<div class="name">
	<?php echo $user_name; ?>     <?php echo $user_surname;  ?>
	</div>
	<div class="date">
	<?php    echo $created;  ?>
	</div>
	</div>
	<div class="review_text">
	<p>
	<?php    echo $tour_comment;  ?>
	</p>
	</div>
	</div>
	
	<div class="stars zero_padding col-sm-12 col-xs-12">
	

	   <?php 
	   if ($tour_rating == '5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($tour_rating == '4.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($tour_rating == '4')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	   }elseif($tour_rating == '3.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	  }elseif($tour_rating == '3')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
		   
	   }elseif($tour_rating == '2.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
	    }elseif($tour_rating == '2')
		{
			?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		 }elseif($tour_rating == '1.5')
		 {
			 ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }
		 else
		 {
			  ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }
		   ?>
	

	
	
	</div>
	
	
	</div>
	</div>
	
	
	
	<?php 
      $i++;			
			}
	?>
	
	</div>
	
	
	
	 <?php 
						$result=' ';
				foreach (array_slice($tourRating, 1, 2) as $row) 
				{
		                 $user_name=$row->user_name;							
						 $result = $user_name;     
         
	             }
		  if($result == ' ')
		  {
			
		  }
		  else
		  {   
		  ?>
		               <div class="r_slider_nav_btns" id="sliderUp" data-toggle="tooltip" title="">
	                   <img src="<?php echo base_url('assets/images/product_details/next.png')?>" />
	                   </div>
						<?php  
						}
	 
				     ?>
	
	
	
	
	
	
	
	
	</div>
	</div>
	
	
		<?php  
		}
	 
		?>
	
	
	
	
	
	
	
	
	
	
	
	
	 <?php
	  if($inclusion == '')
	  {
		  
	  }else
	  {?>
			 <div class="inclusion zero_padding col-sm-12 col-xs-12" id="inclusions">
			 <div class="main_heading">Inclusions</div>
	
			<p class="inclusion_demo">
			 <?php
			

			  echo $inclusion;
			?>

	
			</p>
			</div>
	  <?php
	  }
    ?>
	
	
	
	
	 <?php
	  if($tourdescription == '')
	  {
		  
	  }else
	  {?>
			<div class="overview zero_padding col-sm-12 col-xs-12" id="overview">
			<div class="main_heading">Overview</div>
			<div class="content">
			<p>
			<?php echo $tourdescription?>.
			</p>
			</div>
			</div>
	  <?php
	  }
    ?>
	
	
		 <?php
	  if($departurepoint == '')
	  {
		  
	  }else
	  {?>
			 <div class="inclusion zero_padding col-sm-12 col-xs-12" id="inclusions">
			 <div class="main_heading">Departure Details</div>
	
	                   <h5 class="pickup" style=" margin-left: 30px; color:black">Departure Point</h5>
						<p class="gather_point" style=" margin-left: 30px;"> 
						<?php		

			         echo $departurepoint; 
			         ?></p>
					 
					  <h5 class="pickup" style=" margin-left: 30px;color:black">Reporting Point</h5>
						<p class="gather_point" style=" margin-left: 30px;"> <?php		

			         echo $reportingpoint;
			         ?></p>
			
			</div>
	  <?php
	  }
    ?>
	
	
	
	
	<div class="slider zero_padding  mb-30 col-sm-12 col-xs-12">
	<div class="main_heading">Gallery</div>

	<div class="slick-gallery-slideshow">
	<div class="slider gallery-slideshow">
	 <?php 
						foreach ($tourInfo as $row) {
                           
	                      $gallery_img_url=$row->gallery_img_url;
						  $gallery_img=$row->gallery_img;
			             ?>
	 <div><div class="image"  id="slider_image"><img src="<?php echo $gallery_img_url; ?>/<?php echo $gallery_img; ?>" alt="Image" /></div></div>
						 
					<?php	 }?> 
	
				         
	
	
	
	
	</div>
	
	<div class="slider gallery-nav">
	
	<?php 
						foreach ($tourInfo as $row) {
                           
	                      $gallery_img_url=$row->gallery_img_url;
						  $gallery_img=$row->gallery_img;
			             ?>
	 <div><div class="image" id="slider_image_bottom"><img src="<?php echo $gallery_img_url?>/<?php echo $gallery_img?>" alt="Image" /></div></div>
						 
					<?php	 }?> 
	
	
	

	
	</div>
	</div>
	</div>
	
	
	
	
	
	 <?php
	

	  if($impinformation == '')
	  {
		  
	  }else
	  {?>
		  <div class="things_to_remember zero_padding col-sm-12 col-xs-12" id="things_to_remember">
		  <div class="main_heading">Things to Remember</div>
	
	<ul class="content">
	<li><?php echo $impinformation?></li>

	</ul>
		  </div>
	  <?php
	  }
    ?>
	
	
	
	
	
	 <?php
	

	  if($itinerarydescription == '')
	  {
		  
	  }else
	  {?>
			<div class="additional_info zero_padding col-sm-12 col-xs-12">
			<div class="main_heading">Additional Information</div>
			<ul class="content">
			<li>  <?php echo $itinerarydescription?></li>
			
			</ul>
			</div>
	  <?php
	  }
    ?>
	
	
	
	
	
	
	
	<!---<div class="happening_place zero_padding col-sm-12 col-xs-12" id="happening_place">
	<div class="main_heading">Where</div>
	<div class="content">
	<div id="product_details_map"></div>
	</div>
	</div>--->
	<div class="give_reviews zero_padding col-sm-12 col-xs-12" id="give_review" >
	<div class="main_heading">Write a Review</div>
	<div class="content">
	                          <div class="alert alert-info " id="msg66" style="display:none;">						
							   Your Reviews is  Successfully Completed.
							  </div>
	                           <div class="alert alert-info " id="msg5" style="display:none;">						
							   Please Enter Your Valid Email ID.
							  </div>
	
	<form class="form-vertical" id="rating_form">
	
	<input type="hidden" value="<?php echo $tour_id; ?>" name="TourId">
	
	
	<div class="form-group col-sm-12 col-xs-12">
	<label for="comment">Comment</label>
	<textarea class="form-control" rows="5" name="tour_comment" id="comment" style="resize:none;border:1px solid #eeeddd;"></textarea>
	</div>
	<div class="form-group col-sm-4 col-xs-12">
	<label for="fname">Name*</label>
	<input type="text" class="form-control" name="user_name"  required style="border:1px solid #eeeddd;" id="fname">
	</div>
	
	                        
  
	<div class="form-group col-sm-4 col-xs-12">
	<label for="email">Email address*</label>
	<input type="email" class="form-control"   name="user_email" required style="border:1px solid #eeeddd;" id="email">
	</div>
	
	
	<div class="form-group col-sm-4 col-xs-12">
	<label for="contact">Contact:</label>
	<input type="text" class="form-control" name="user_mobile" style="border:1px solid #eeeddd;" id="contact">
	</div>
	<div class="gv_stars zero_padding col-sm-12 col-xs-12">
	
	
	<div class="col-sm-4 col-xs-12">
	<label>Review</label>
	
	<input type="hidden"  id="selected_rating" name="tour_rating">
	
	<div class="rating">
    
	
	<a class="star_a" href="#5" data-toggle="tooltip"  onclick="change(this.id);"  id="5" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" onclick="change(this.id);"  id="4" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" onclick="change(this.id);"  id="3" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" onclick="change(this.id);"  id="2"  title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" onclick="change(this.id);"  id="1" title="Give 1 star">★</a>
	
 
	</div>
	</div>
	
	
	
	
	
	
	<!--<div class="col-sm-4 col-xs-12">
	<label>Destination</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	
	<div class=" col-sm-4 col-xs-12">
	<label>Meals</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	
	<div class="col-sm-4 col-xs-12">
	<label>Transport</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>
	
	<div class=" col-sm-4 col-xs-12">
	<label>Overall</label>
	<div class="rating ">
	<a class="star_a" href="#5" data-toggle="tooltip" title="Give 5 stars">★</a>
	<a class="star_a" href="#4" data-toggle="tooltip" title="Give 4 stars">★</a>
	<a class="star_a" href="#3" data-toggle="tooltip" title="Give 3 stars">★</a>
	<a class="star_a" href="#2" data-toggle="tooltip" title="Give 2 stars">★</a>
	<a class="star_a" href="#1" data-toggle="tooltip" title="Give 1 star">★</a>
	</div>
	</div>-->
	
	</div>
	
	<div class="form-group submit_btn col-sm-12 col-xs-12">
	<button type="button"  onclick="save_rating();" class="btn btn-default">Submit</button>
	</div>
	
	
	</form>
	
	</div>
	
	
	
	</div>
	</div>
	
	</div>

	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
	

	
	
	  <!-- Modal -->
  <div class="modal fade" id="costumModal30" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" id="new_modalheader">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <h3 class="tour_head"><?php echo $tourname?></h3>
        </div>
        <div class="modal-body" id="new_modal_body">
		<div id="myCarousel" class="carousel slide" data-ride="carousel"  data-interval="false">
								

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
					
						<input type="hidden" id="cutoff" value="<?php echo $cutoffday; ?>">
					 <!-- First Model-->
					<?php 
					if(is_array($tour_transfer) && count($tour_transfer) > 1)
					{
					  //  Mutliple Tour Transfer 
					?>
					    
						<div class="item active">
						 <h4 class="modal-title" id="myModalLabel">What Are You Looking For?</h4>
						  <?php 
						foreach ($tour_transfer as $row) {
                          $transfercatname=$row->transfercatname;
                          $transferoption=$row->transferoption;
			              $description=$row->description;
					      $sellAP=$row->sellAP;
						  $sellCP=$row->sellCP;
						  $discount=$row->discount;
					      $id=$row->id;
						  
					      $transfer_id=$row->tour_transferoption_id;
					      $TransferType=$row->TransferType;
						?>
						<div class="col-lg-12 col-md-12 col-sm-12 hr_bottom">
						 <?php 
						   if(is_array($tour_transfer_Ticket) && count($tour_transfer_Ticket) > 0)
							{
							foreach ($tour_transfer_Ticket as $rows) {
							  $Ticket_transfercatname=$rows->transfercatname;
							  $Ticket_transferoption=$rows->transferoption;
						      $Ticket_id=$rows->id;
						      if ($Ticket_transfercatname == '')
						        {
							    ?>
					                 	                     <div class="col-sm-9 col-xs-12 modal_detail">
											                 <h5> <?php echo $transferoption; ?> </h5>
															 
											                 <p><?php echo $description; ?>.</p>
											                  </div>
															  
																<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>/<?php  echo $transfer_id; ?>/<?php  echo $TransferType; ?>/<?php  echo $transfercatname; ?>" id="<?php echo $id; ?>">
									
															  
															<div class="col-sm-3 col-xs-12">
															<a  href="#myCarousel" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" data-slide="next" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													        </div>
							   <?php
								}
								else
								{	
							         if($transfercatname == $Ticket_transfercatname)
										  {?>
								
								
															 <div class="col-sm-9  col-xs-12 modal_detail">
											                 <h5> <?php echo $transferoption; ?> </h5>
															 
											                 <p><?php echo $description; ?>.</p>
											                  </div>
															  
					<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>/<?php  echo $transfer_id; ?>/<?php  echo $TransferType; ?>/<?php  echo $transfercatname; ?>" id="<?php echo $id; ?>">
															 
															<div class="col-sm-3 col-xs-12">
															<a  href="#myCarousel" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" data-slide="next" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													        </div>
										
										<?php
										 }
										else
										 {?> 
									                 <div class="col-sm-9 col-xs-12 modal_detail">
													   <h5> <?php if($Ticket_transfercatname == 3) {echo $Ticket_transferoption;} ?>  +   <?php echo $transferoption; ?> </h5>
													   
		<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>/<?php  echo $transfer_id; ?>/<?php  echo $TransferType;?>/<?php  echo $transfercatname; ?>" id="<?php echo $id; ?>">
													   
							
													   
													   <p><?php echo $description; ?>.</p>
													  </div>
													  
													  
													   <div class="col-sm-3 col-xs-12">
														<a  href="#myCarousel" data-slide="next" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													    </div>
										
										<?php	
										}			
											
											
								}
						}
				
				}	
				else{
					?>                   	              <div class="col-sm-9 col-xs-12 modal_detail">
											                 <h5> <?php echo $transferoption; ?> </h5>
															 
											                 <p><?php echo $description; ?>.</p>
											               </div>
															  
				<input type="hidden" value="<?php echo $transferoption;?>/<?php  echo $sellAP; ?>/<?php  echo $sellCP; ?>/<?php  echo $discount; ?>/<?php  echo $transfer_id; ?>/<?php  echo $TransferType; ?>/<?php  echo $transfercatname; ?>" id="<?php echo $id;?>">
															  
															<div class="col-sm-3 col-xs-12">
															<a  href="#myCarousel" onClick="select_transfer(this.id)" id="<?php echo $id; ?>" data-slide="next" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30">Choose</a>
													        </div>
						  <?php
						  }
						  ?>
						
							
							
						
							</div>
						
							
							<?php 
							}?> 
							
							</div>
							<?php
						// End Mutliple Tour Transfer 
				  }
				  else
				  {
					   if(is_array($tour_transfer_Ticket) && count($tour_transfer_Ticket) > 0)
                  {
					  //   only Ticket Tour Transfer 
					  ?>
					   
					   
					  <div class="item active">
					  
						<div class="modal-header" >
					<div id="search_modal">
						<div class="calendar_icon">
							<?php
						foreach ($tour_transfer as $row) {
							      $transfercatname=$row->transfercatname;
								  $transferoption=$row->transferoption;
								  $sellAP=$row->sellAP;
						} ?>
						<i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?> </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						</div>
						
						</div>
						
						       <div class="alert alert-info " id="cutoff_msg" style="display:none;">						
							   This Tour is not Available on Selected Date,Please try another Date.
							  </div>
						
					<div class="modal_date_picker">
					 <h4 class="modal-title" id="myModalLabel123">When do you want to go?</h4>	
					 
						<div class="col-md-8 col-xs-12 date_modal">
						<div class="form-group">
					
						<div class="input-group">
							<input type="text" id="datetimepicker" placeholder="Select Your Date" class="form-control" >
						</div>
						</div>
								
						</div>
							
							<div class="col-md-2" id="next_btn" style="display:block">
							<a href="#" data-slide="next" id="butdate" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs ">Next<div class="ripple-container"></div></a>
							</div>
							
						</div>
						
						    
						
						</div>
					   
						<?php
						 //  End Ticket Tour Transfer 
				  }else
				  {
					   //  No Ticket Tour Transfer ?>
					 <div class="item active">
						<div class="modal-header" >
						<div class="row">
						<div id="search_modal">
						<div class="calendar_icon">
						<?php
					foreach ($tour_transfer as $row) {
							  $transferoption=$row->transferoption;
							  $sellAP=$row->sellAP;
							   $sellCP=$row->sellCP;
							  $discount=$row->discount;
							 
					} ?>
					  <input type="hidden"  id="discount_price_transfer" value="<?php echo $discount; ?>">
					  <input type="hidden"  id="sellAP_price_transfer" value="<?php echo $sellAP; ?>">
					  <input type="hidden"  id="sellCP_price_transfer" value="<?php echo $sellAP; ?>">					  
					  <i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?>  </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						</div>
						</div>
						</div>
						    
							  
							 <div class="alert alert-info " id="cutoff_msg" style="display:none;">						
							   This Tour is not Available on Selected Date,Please try another Date.
							  </div>
							  
							  
						<div class="modal_date_picker">
					 <h4 class="modal-title" id="myModalLabel123">When do you want to go?</h4>	
					 
						<div class="col-md-8 col-xs-12 date_modal">
						<div class="form-group">
					
						<div class="input-group">
							<input type="text" id="datetimepicker" placeholder="Select Your Date" class="form-control" >
						</div>
						</div>
								
						</div>
							  
							
							<div class="col-md-2" id="next_btn" style="display:block">
							<a href="#" data-slide="next" id="butdate" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs ">Next<div class="ripple-container"></div></a>
							</div>
							
						</div>
						
						
						  
						</div>
						
						   
						   <?php						
									  //  End Ticket Tour Transfer 
								  }					 	  
						
							  }
								?>
					
                 <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					 // Mutliple Tour Transfer  selected on date page
					  ?>
					     <div class="item ">
						 				
						<div class="modal-header" >
						<div class="row">
						<div id="search_modal">
						<div class="calendar_icon">
						<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer1"> </span><span>   </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						</div>
						</div>
						
						      <div class="alert alert-info " id="cutoff_msg" style="display:none;">						
							   This Tour is not Available on Selected Date,Please try another Date.
							  </div>
						
						<div class="modal_date_picker">
					 <h4 class="modal-title" id="myModalLabel123">When do you want to go?</h4>	

						<div class="col-md-8  col-xs-12 date_modal" id="new_date">
						<div class="form-group">
						<label class="side_label"></label>
						<div class="input-group">
						
							<input type='text' class="form-control" id="datetimepicker" name="when" placeholder="Select Your Date" data-initialized="true"/>

						</div>
						</div>
								
							</div>
							
							<div class="col-md-2" id="next_btn" style="display:block">
							<a href="#" data-slide="next" id="butdate" class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-10 ">Next<div class="ripple-container"></div></a>
							</div>
							
							
						</div>
						</div>
						<?php
						// End Mutliple Tour Transfer 
				  }else{
					?>
						
						
						
						
				  <?php
				  }
				  ?>
						
					 
					 <!-- Second Model-->	
					
						
						<?php 
				if(is_array($tour_option) && count($tour_option) > 1)
                  {
					  //Multiple Tour Option Selection
					?>
					
				<div class="item">
			

					<div class="modal-header" >
							
						<div class="row">
						<div id="search_modal">
						
								 <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {?>
			  	        <div class="col-lg-7 col-md-7 col-sm-12 calendar_icon" style="border-bottom:1px solid #ccc">
								
		             	<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer4">   </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						
						</div>
						<?php
				  }
				  else
				  {
					
           foreach ($tour_transfer as $row) {
			   
                          $transferoption=$row->transferoption;
			  	} ?>
					    <div class="col-lg-7 col-md-7 col-sm-12 calendar_icon" style="border-bottom:1px solid #ccc">
			            <i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?>  </span>&nbsp;
						<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
					
				  <?php
				  }
				  ?>
				  
				
						<div class="col-lg-5 col-md-5 col-sm-5 calendar_icon" style="border-bottom:1px solid #ccc">
						<i class="fa fa-calendar" aria-hidden="true"></i> <span id="selected_date1"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						
					</div>
					</div>
					</div>
					
					
						<div class="col-lg-12 col-md-12 col-sm-12 option_head_modal">
						
						<h4 id="myModalLabel321">Which Tour Option You Want To Pick ?</h4>
					  <?php	foreach ($tour_option as $rows) {
							
							  $optionName=$rows->optionName;
							  $description=$rows->description;
							  $option_id=$rows->id;
							  $departuretimess=$rows->departuretime;
							  $sellAP=$rows->sellAP;
							  $sellCP=$rows->sellCP;
							  $discount=$rows->discount;
							  
							  
						?>
						<div class="col-lg-12 col-md-12 col-sm-12  hr_bottom">
						<div id="search_modal1">
							<div class="col-sm-9 col-xs-12 modal_detail">
							<h5 ><?php echo $optionName; ?></h5>
							<p><?php echo $description; ?>.</p>
							</div>
							
				 <input type="hidden" value="<?php echo $optionName;?>/<?php 

			      if( strpos($departuretimess, ',') !== false )
                                 {
								
                                 }else{
									 echo $departuretimess;
								 }
						  ?>/<?php echo $sellAP;?>/<?php echo $sellCP;?>/<?php echo $discount;?>" id="<?php echo $option_id; ?>">
						   

						  
						  <?php 
						$departuretimess=$rows->departuretime;
						$time=explode(',', $departuretimess);						
						
							
 						if( strpos($departuretimess, ',') !== false )
                              {?>
						  <div class="col-sm-3 col-xs-12">
							<span style="display:none" id="T<?php echo $option_id; ?>"><a  href="#myCarousel"   onClick="select_option(this.id)" id="<?php echo $option_id; ?>"  data-slide="next"  class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30 mb-0">Choose</a></span>
							<div class="clearfix"></div>
							
							
						  <div class="form-group" id="select_time">
								<select placeholder="Transportation" class="form-control mt-20 select_option" onChange="select_time(this,this.id);" id="<?php echo $option_id; ?>">
									<option  value="" selected disabled>Select Time</option>						   
						   
									<?php foreach($time as $my_time){?>
						  
										<option value="<?php echo $my_time;  ?>"> 
											<?php echo $my_time;  ?>
										</option>
						
						
									  <?php
									   }
									   ?>
						   
									</select>
									
							<?php
                             
                                 }else{ ?>
								 
							<div class="col-sm-3 col-xs-12">
							<a  href="#myCarousel"  onClick="select_option(this.id)" id="<?php echo $option_id; ?>"  data-slide="next"  class="btn btn-primary btn-inverse btn-sm pull-right pull-left-xs mt-30 mb-10">Choose</a>
							<div class="clearfix"></div>
							
							
						  <div class="form-group">
									<p class="one-time"> <?php echo $departuretimess;?></p>
									<?php }
						  ?>
						    
						  </div>
						 </div>
					 </div>
					 
					 
				 </div>
						<?php
					}
						?>
									
		                </div>
					
						</div>
						
						<?php 
						
						  // End multi tour Option -->
				  }elseif(is_array($tour_transfer) && count($tour_transfer) < 2){
					 	
						//one Transfer AND One tour Option ONLY -->
					  ?>
					  					  
						<div class="item ">
                   <form id="checkout_form1" action="<?php echo site_url('checkout/booking')?>" method="post">
						<div class="modal-header" >
							
						<div class="row">
						
							  <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					  ?>
					    <input type="hidden" name="TourId"  value="<?php echo $tour_id; ?>">
						<input type="hidden" name="TourName"  value="<?php echo $tourname; ?>">
						<input type="hidden" name="TransferName" id="selected_transfer7">
						
						<input type="hidden" name="TransferId" id="transfer_id2">
						<input type="hidden" name="TransferType" id="TransferType2">
						
						<input type="hidden" name="transfercatname" id="transfercatname2" >
						
					  	<div id="search_modal">
					  <div class="col-lg-7 col-md-7 col-sm-12 calendar_icon" >
 
					<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer3">   </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						<?php
				  }else
				  {  
					  foreach ($tour_transfer as $row) {
                          $transferoption=$row->transferoption;
						  $sellAP=$row->sellAP;
						  $sellCP=$row->sellCP;
						  $discount=$row->discount; 
						  
						  $transfer_id=$row->tour_transferoption_id;
						  $TransferType=$row->TransferType;
						   $transfercatname=$row->transfercatname;
			  	    } ?>
					    <input type="hidden" name="TourId"  value="<?php echo $tour_id; ?>">
						<input type="hidden" name="TourName"  value="<?php echo $tourname; ?>">
						<input type="hidden" name="TransferName" value="<?php echo $transferoption; ?>">
						
						<input type="hidden" name="TransferId" value="<?php echo $transfer_id; ?>">
						<input type="hidden" name="TransferType" value="<?php echo $TransferType; ?>">
						
						<input type="hidden" name="transfercatname" value="<?php echo $transfercatname; ?>">
					
			           <div id="search_modal">
			          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 calendar_icon" >
			
			         <i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?>  </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						</div>
						<?php
				  }
				  
				?>
						
						<div id="search_modal">
						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 calendar_icon">
					
						<i class="fa fa-calendar" aria-hidden="true"></i> <span id="selected_date2"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						
						<input type="hidden" name="TourDate" id="selected_date4">
						
						
						
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 calendar_icon" id="search_modal_single">
							<?php	foreach ($tour_option as $rows) {
							
							  $optionName=$rows->optionName;
							  $departuretime=$rows->departuretime;
							  $option_id=$rows->id;
							   $option_sellAP=$rows->sellAP;							   
							   $option_sellCP=$rows->sellCP;
							   $option_discount=$rows->discount;
							  
						}
						?>
						
						<div class="col-md-9 col-lg-9 col-sm-12 ">
						<i class="fa fa-list-ul" aria-hidden="true"></i>
						<span class="how_many_issue"><?php echo $optionName;?></span>
						<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						
						<?php 
						$departuretimess=$rows->departuretime;
						$time=explode(',', $departuretimess);						
						if( strpos($departuretimess, ',') !== false )
                              {?>
						 <div class="col-md-3 col-lg-3 col-sm-12 select_time_issue" >
						 <div class="form-group ">
						<select placeholder="Transportation" class="form-control mt-10 select_option" name="TourTime"  id="">
								<option  disabled="disabled">Select Time</option>						   
						   
									<?php foreach($time as $my_time){?>
						  
										<option value="<?php echo $my_time;  ?>"> 
											<?php echo $my_time;  ?>
										</option>
						
						
									  <?php
									   }
									   ?>
						   
									</select>
									</div>
									</div>
									
							   <?php
                             
                                 }
								 else
								 { ?>
							  <div class="col-md- col-lg-1 col-sm-12 selected_time_dept " >
						 <div class="form-group ">
								 <span  class=" " id="selected_time"><?php echo $departuretime; ?></span>
								</div>
									</div> 
				             <?php		
								?>
								
								<input type="hidden"   name="TourTime"   value="<?php echo $departuretime;?> ">
								
								<?php
								
								 }?>
								 
                     
						
						<input type="hidden" name="TourOptId" value="<?php echo $option_id;?> ">
						<input type="hidden" name="TourOptName" value="<?php echo $optionName;?> ">
					
						
						</div>
						
					</div>
					</div>
				
					<h4 id="myModalLabel123">How Many Guests Joining?</h4>
   
			<div id="search_final">
			  
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
				  <div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
        
				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				
				 <span class="adult_label_parent" id="show_adult"><span>Adult <span id="present_adult">1</span></span></span>
				 
				 <span class="adult_label_parent"  id="show_child">Child <span id="present_child">1</span></span>
				 
					   <input type="hidden"  name="AdultQuantity" value="1" id="present_adult2">	
					   <input type="hidden"  name="ChildQuantity"  value="0" id="present_child2">	
					   <input type="hidden"  name="InfantQuantity"  value="0" >	
				 
				</a>
		
		
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
		<div id="search_back_drop">
			<div class="adult_dropdown">
		 <div class=" adult_plus">
					 <div class="form-group">
					 <div class="col-md-4 col-sm-12 col-xs-12">
					  <label class="side_label1">Adult<span class="age_span">Ages 12-99</span> </label>
					  </div>
					 
					 <?php 
					    $dis_AP = ($sellAP * $discount)/100;
					    $sellAP_price = $sellAP -  $dis_AP;
					   
					    $dis_TourAP = ($option_sellAP * $option_discount)/100;
					    $Tour_sellAP = $option_sellAP -  $dis_TourAP;
					   
					    
					    $dis_finalSellAP =  $sellAP_price +  $Tour_sellAP;
					    $finalSellAP = $sellAP + $option_sellAP;
					   ?>
					   
					  
					   
					   <!-- Adult Price-->
					    <input type="hidden" value="<?php echo $finalSellAP; ?>"  id="NoDis_hiddenfinalsellAP_price_transfer">
						
						 <!-- DisAdult Price-->
			          <input type="hidden"  value="<?php echo round($dis_finalSellAP);?> "id="hiddenfinalsellAP_price_transfer">
						
						
					 <input type="hidden"  name="Discount"  value="<?php echo $discount; ?>">	
					 <input type="hidden"  name="AdultPrice"  value="<?php echo $finalSellAP; ?>">	
					 <input type="hidden"  name="DisAdultPrice"  value="<?php echo round($dis_finalSellAP);?>">	
						
						
						
					  <div class="col-md-4 col-sm-12 col-xs-6">
					   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>
				     <?php 
					   echo round($dis_finalSellAP);
					   
					   ?>
					   
					   
					 <!--  <br><span class="age_span_strike" style="text-decoration: line-through;"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $finalSellAP = $sellAP + $option_sellAP;?></span></label>-->
					   
					   
					   
					   <?php 
					   if($finalSellAP == $dis_finalSellAP)
					   {
						   
					   }else{
						   ?>
						  <br>
						    <span class="age_span_strike" style="text-decoration: line-through;"><i class="fa fa-inr" aria-hidden="true"></i>
					   
					        <?php							  
								    echo $finalSellAP = $sellAP + $option_sellAP;					 
						    
							   }
							   
							  ?></i></span></label>
					   
					   
					   
					   
					   
					   
					  </div>
					  
					  
					  <div class="col-md-4 col-sm-12 col-xs-6">
					  <div class="input-group" id="inputgroup">					  
						<span class="input-group-btn">
						<button type="button" class="btn btn-number" disabled="disabled"  onclick="decrement_adult()" data-type="minus" data-field="quant[1]"  id="btn_plus">
						<span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						
						<input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="1000" id="quant_adult">
											
						<span class="input-group-btn">
						<button type="button" class="btn btn-number"  onclick="increment_adult()" data-type="plus" data-field="quant[1]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
			</div>
			
			
			
					<div class="clearfix"></div>
					<div class="adult_plus">
					 <div class="form-group">
					 
					 <div class="col-md-4 col-sm-12 col-xs-12">
					   <label class="side_label1">Child<span class="age_span">Ages 2-12</span> </label>
					   </div>
					   <?php  $dis_CP = ($sellCP * $discount)/100;
					    $sellCP_price = $sellCP -  $dis_CP;
					   
                        $dis_TourCP = ($option_sellCP * $option_discount)/100;
					    $Tour_sellCP = $option_sellCP -  $dis_TourCP;					   					    
					     $dis_finalSellCP =  $sellCP_price +  $Tour_sellCP;

                         $finalSellCP = $sellCP + $option_sellCP;
						 ?>
						 
						  <!--Child  Price-->						 					   
					    <input type="hidden" value="<?php echo $finalSellCP; ?>"  id="NoDis_hiddenfinalsellCP_price_transfer">
						
						 <!-- DisChild Price-->
						<input type="hidden"  value="<?php  echo round($dis_finalSellCP);?>					    
					    "id="hiddenfinalsellCP_price_transfer">
					   
					   
			    <input type="hidden"  name="ChildPrice"  value="<?php echo $finalSellCP; ?>">	
			   	<input type="hidden"  name="DisChildPrice"   value="<?php  echo round($dis_finalSellCP);?>">	
					   
					   
					   <div class="col-md-4 col-sm-12 col-xs-6">
					   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>
					   <?php 
					   
					   echo round($dis_finalSellCP);			   
					   
					   ?>
					   <!--<br><span class="age_span_strike" ><i class="fa fa-inr" aria-hidden="true"></i><?php echo $finalSellCP = $sellCP + $option_sellCP;?></span></label>-->
					   
					   
					   
					   <?php
					   if($finalSellCP == $dis_finalSellCP)
					   {
						   
					   }else{?>	 
					    <br>
					   <span class="age_span_strike" style="text-decoration: line-through;"><i class="fa fa-inr" aria-hidden="true"></i>
					   <?php echo $finalSellCP = $sellCP + $option_sellCP;
					   ?>
					   </span></label>
					   <?php
					   }
					   ?>
					   
					  </div>
					  
					  
					  
					  
					  <div class="col-md-4 col-sm-12 col-xs-6">

					   <div class="input-group" id="inputgroup">
						<span class="input-group-btn">
						<button type="button" class="btn  btn-number" onclick="decrement_child()"  data-type="minus" data-field="quant[2]" id="btn_plus">
					   <span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						<input type="text" name="quant[2]" class="form-control input-number" value="0" min="0" max="1000" id="quant_child">
						
						<span class="input-group-btn">
						
						<button type="button" class="btn btn-number" onclick="increment_child()" data-type="plus" data-field="quant[2]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
						</div>
											<div class="clearfix"></div>

				<div class=" adult_plus">
					 <div class="form-group">
						 <div class="col-md-4 col-sm-12 col-xs-12">
							<label class="side_label1" >Infant<span class="age_span">Under 2</span></label>
						</div>
					<div class="col-md-4 col-sm-12 col-xs-6">
				   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>00<br></label>
				  </div>
				    <div class="col-md-4 col-sm-12 col-xs-6">
					   <div class="input-group" id="inputgroup">
				   
					<span class="input-group-btn">
					<button type="button" class="btn"   id="btn_plus">
				   <span class="glyphicon glyphicon-minus"></span>
					</button>
					</span>
					
					<input type="text"  class="form-control" value="0" min="0" max="1000" id="quantminus">
					
					
					<span class="input-group-btn">					
					<button type="button" class="btn"  id="btn_plus">
					<span class="glyphicon glyphicon-plus"></span>					
					</button>
					</span>
					
					
					</div> 
					</div>  
					</div>
					</div>
										<div class="clearfix"></div>

					</div>
				</div>
			</div>
      </div>
    </div>
   </div>
   </div>
				
						<div class="col-lg-12 col-md-12 col-sm-12 modal_detail1">
							<div class="col-sm-9 ">
							
							<!--<h6 ><span><i class="fa fa-inr" aria-hidden="true" id="nodis_finalprice"></i> <span id="echo_nodis_price"><?php                         
							  //$finalSell = $finalSellAP + $finalSellCP;
							  $finalSell = $finalSellAP ;
                        echo round($finalSell);

							 ?></span></span></h6>-->
							 
							 
							 <h6 ><?php                         
							  //$finalSell = $finalSellAP + $finalSellCP;
							  $finalSell = $finalSellAP ;
							  if($finalSell == $dis_finalSellAP)
							  {
								  
							  }else{?>
							  
							  
								  <span><i class="fa fa-inr" aria-hidden="true" id="nodis_finalprice" style="text-decoration:line-through"></i> <span id="echo_nodis_price">
								  <?php
								    echo round($finalSell);
								 ?></span></span></h6>
							 <?php  
							 }				  
                           

							 ?>	
							 
						
							
							<h5 class=""><span class="rate_final"><i class="fa fa-inr" aria-hidden="true" id="finalPrice"></i>
							<span id="echo_price"><?php                           
							// $dis_finalSell = $dis_finalSellAP + $dis_finalSellCP;					
							 $dis_finalSell = $dis_finalSellAP ;					
							echo round($dis_finalSell);
							?>
			<!--<input type="hidden" name="FullTotal" value=" <?php echo $finalSell; ?>" >
			<input type="hidden" name="DisFullTotal" value="<?php echo round($dis_finalSell);?>">-->
							
							
							</span></span></h5>							
							</div>
							
					      <input type="hidden" value=" <?php echo $finalSell; ?>" id="hidden_nodis_finalprice">						
						  <input type="hidden" id="hiddenfinalPrice" value="<?php echo round($dis_finalSell);?>">
						
						
		    <input type="hidden" name="FullTotal"   id="hidden_nodis_finalprice1" value=" <?php echo $finalSell; ?>" >
			<input type="hidden" name="DisFullTotal"  id="hiddenfinalPrice1" value="<?php echo round($dis_finalSell);?>">
							
							
							<div class="col-sm-3 col-xs-12">
							<button  type="submit"  class="redbtn mt-30" id="btnSave">Checkout</button>
							</div>
									
		                </div> 
						</form>
				</div>
					  
					
					
					  
				  <?php }
				  else
				  {
					  //Multi Transfer Type AND One tour Option ONLY -->
					  ?>
					    <!--- ####################################################################-->
					 
					  <div class="item ">
               <form id="checkout_form1" action="<?php echo site_url('checkout/booking')?>" method="post">
						<div class="modal-header" >
							
						<div class="row">
						
							  <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					  ?>
					    <input type="hidden" name="TourId"  value="<?php echo $tour_id; ?>">
						<input type="hidden" name="TourName"  value="<?php echo $tourname; ?>">
						<input type="hidden" name="TransferName" id="selected_transfer7N">
						
						<input type="hidden" name="TransferId" id="transfer_id2N">
						<input type="hidden" name="TransferType" id="TransferType2N">
						
						<input type="hidden" name="transfercatname" id="transfercatname2N" >
						
					  	<div id="search_modal">
					  <div class="col-lg-7 col-md-7 col-sm-12 calendar_icon" >
 
					<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer3N">   </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						<?php
				  }else
				  {  
					  foreach ($tour_transfer as $row)
					  {
							  $transferoption=$row->transferoption;
							  $sellAP=$row->sellAP;
							  $sellCP=$row->sellCP;
							  $discount=$row->discount; 
							  
							  $transfer_id=$row->tour_transferoption_id;
							  $TransferType=$row->TransferType;
							  $transfercatname=$row->transfercatname;
			  	    } ?>
					   <!-- <input type="hidden" name="TourId"  value="<?php echo $tour_id; ?>">
						<input type="hidden" name="TourName"  value="<?php echo $tourname; ?>">
						<input type="hidden" name="TransferName" value="<?php echo $transferoption; ?>">
						
						<input type="hidden" name="TransferId" value="<?php echo $transfer_id; ?>">
						<input type="hidden" name="TransferType" value="<?php echo $TransferType; ?>">
						
						<input type="hidden" name="transfercatname" value="<?php echo $transfercatname; ?>">
					
			           <div id="search_modal">
			          <div class="col-lg-7 col-md-7 col-sm-12  calendar_icon" >
			
			         <i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption; ?>  </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						</div>-->
						<?php
				  }
				  
				?>
						
						<div id="search_modal">
						<div class="col-lg-5 col-md-5 col-sm-12  calendar_icon">
					
						<i class="fa fa-calendar" aria-hidden="true"></i> <span id="selected_date2N"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						</div>
						
						<input type="hidden" name="TourDate" id="selected_date4N">
						
						
						
						<div class="col-md-12 col-lg-12 col-sm-12 calendar_icon" id="search_modal_single">
							<?php	foreach ($tour_option as $rows) {
							
							  $optionName=$rows->optionName;
							  $departuretime=$rows->departuretime;
							  $option_id=$rows->id;
							  
							   $option_sellAP=$rows->sellAP;							   
							   $option_sellCP=$rows->sellCP;
							   $option_discount=$rows->discount;
							  
						}
						 
						?>
						
						<div class="col-md-9 col-lg-9 col-sm-12 ">
						<i class="fa fa-list-ul" aria-hidden="true"></i>
						<span class="how_many_issue"><?php echo $optionName;
						
						
						
						?></span>
						
						<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						
						<?php 
						$departuretimess=$rows->departuretime;
						$time=explode(',', $departuretimess);						
						if( strpos($departuretimess, ',') !== false )
                              {?>
						 <div class="col-md-1 col-lg-1 col-sm-12 ">
						 <div class="form-group ">
						<select placeholder="Transportation" class="form-control mt-10 select_option" name="TourTime"  id="">
								<option  value="" selected disabled>Select Time</option>					   
						   
									<?php foreach($time as $my_time){?>
						  
										<option value="<?php echo $my_time;  ?>"> 
											<?php echo $my_time;  ?>
										</option>
						
						
									  <?php
									   }
									   ?>
						   
									</select>
									</div>
									</div>
									
							   <?php
                             
                                 }
								 else
								 { ?>
							  <div class="col-md-1 col-lg-1 col-sm-12 selected_time_dept " >
						 <div class="form-group ">
								 <span  class=" " id="selected_time"><?php echo $departuretime; ?></span>
								</div>
									</div> 
				             <?php		
								?>
								
								<input type="hidden"   name="TourTime"   value="<?php echo $departuretime;?> ">
								
								<?php
								
								 }?>								 
                     
						
						
						<input type="hidden" name="TourOptId" value="<?php echo $option_id;?> ">
						<input type="hidden" name="TourOptName" value="<?php echo $optionName;?> ">
					
						
					
						
						</div>
						
					</div>
					</div>
				
					<h4 id="myModalLabel123">How Many Guests Joining?</h4>
   
			<div id="search_final">
			  
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
				  <div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
        
				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				
				 <span class="adult_label_parent" id="show_adult"><span>Adult <span id="present_adult">1</span></span></span>
				 
				 <span class="adult_label_parent"  id="show_child">Child <span id="present_child">1</span></span>
				 
					   <input type="hidden"  name="AdultQuantity" value="1" id="present_adult2">	
					   <input type="hidden"  name="ChildQuantity"  value="0" id="present_child2">	
					   <input type="hidden"  name="InfantQuantity"  value="0" >	
				 
				</a>
		
		
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
		<div id="search_back_drop">
			<div class="adult_dropdown">
		 <div class=" adult_plus">
					 <div class="form-group">
					 <div class="col-md-4 col-sm-12 col-xs-12">
					  <label class="side_label1">Adult<span class="age_span">Ages 12-99</span> </label>
					  </div>
					 
					 <?php 
					  //  $dis_AP = ($sellAP * $discount)/100;
					   // $sellAP_price = $sellAP -  $dis_AP;
					   
					    $dis_TourAP = ($option_sellAP * $option_discount)/100;
					    $Touroption_sellAP = $option_sellAP -  $dis_TourAP;
					   
					    
					    //$dis_finalSellAP =  $sellAP_price +  $Tour_sellAP;
					   // $finalSellAP = $sellAP + $option_sellAP;
					   ?>
					   
					  <!-- IF Multiple TanferType Than one Option SELL AP TOATL-->
					    <input type="hidden"   id="NOdis_one_tourOp_sellAP" value="<?php echo $option_sellAP; ?>">
					    <input type="hidden"   id="one_tourOp_sellAP" value="<?php echo $Touroption_sellAP; ?>">
					    <input type="hidden"   id="TnsfsellAP_one_tour">
					   
					   <!-- Adult Price-->
					    <input type="hidden"  id="NoDis_hiddenfinalsellAP_price_transfer">
						
						 <!-- DisAdult Price-->
			          <input type="hidden"  id="hiddenfinalsellAP_price_transfer">
						
						
					 <input type="hidden"  name="Discount" id="DiscountOneOp">	
			         <input type="hidden"  name="AdultPrice" id="AdultPriceOneOp" >	
					 <input type="hidden"  name="DisAdultPrice" id="DisAdultPriceOneOp" >	
						
						
						

						
					  <div class="col-md-4 col-sm-12 col-xs-6">
					   <label class="side_label_final" >
					   
					   <span><i class="fa fa-inr" aria-hidden="true" id="dis_finalpriceONRoptionSELLAP" > <span id="echo_dis_priceONETOUR">
								  <?php
								   // echo round($dis_finalSellAP);
						 ?></i></span></span>
					   
					   
					   <br>
						   
						    <span><i class="fa fa-inr" aria-hidden="true" id="Nodis_finalpriceONRoptionSELLAP" class="age_span_strike" style="text-decoration:line-through"> <span id="echo_NOdis_priceONETOUR">
								</i></span></span></label>
							  </div>
					  
					  
					  <div class="col-md-4 col-sm-12 col-xs-6">
					  <div class="input-group" id="inputgroup">					  
						<span class="input-group-btn">
						<button type="button" class="btn btn-number" disabled="disabled"  onclick="decrement_adult()" data-type="minus" data-field="quant[1]"  id="btn_plus">
						<span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						
						<input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="1000" id="quant_adult">
											
						<span class="input-group-btn">
						<button type="button" class="btn btn-number"  onclick="increment_adult()" data-type="plus" data-field="quant[1]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
			</div>
			
			
			
					<div class="clearfix"></div>
					<div class="adult_plus">
					 <div class="form-group">
					 
					 <div class="col-md-4 col-sm-12 col-xs-12">
					   <label class="side_label1">Child<span class="age_span">Ages 2-12</span> </label>
					   </div>
					   <?php 
  					    //$dis_CP = ($sellCP * $discount)/100;
					  //  $sellCP_price = $sellCP -  $dis_CP;
					   
                        $dis_TourCP = ($option_sellCP * $option_discount)/100;
					    $Touroption_sellCP = $option_sellCP -  $dis_TourCP;					   					    
					    // $dis_finalSellCP =  $sellCP_price +  $Tour_sellCP;

                         //$finalSellCP = $sellCP + $option_sellCP;
						 ?>
						 
						 
						 <!-- IF Multiple TanferType Than one Option SELL CP TOATL-->
					    <input type="hidden"   id="NOdis_one_tourOp_sellCP" value="<?php echo $option_sellCP; ?>">
					   <input type="hidden"   id="one_tourOp_sellCP" value="<?php echo $Touroption_sellCP; ?>">
					   <input type="hidden"   id="TnsfsellCP_one_tour">
						 
						 
						  <!--Child  Price-->						 					   
					    <input type="hidden"   id="NoDis_hiddenfinalsellCP_price_transfer">
						
						 <!-- DisChild Price-->
						<input type="hidden"  id="hiddenfinalsellCP_price_transfer">
					   
					   
			    <input type="hidden"  name="ChildPrice" id="ChildPriceOp"  >	
			   	<input type="hidden"  name="DisChildPrice"  id="DisChildPriceOp" >	
					   
					   
					   <div class="col-md-4 col-sm-12 col-xs-6">
					   <label class="side_label_final">
					    <span><i class="fa fa-inr" aria-hidden="true" id="dis_finalpriceONRoptionSELLCP" > 
						 </i></span>					   
					   
					   <br>
					   	
                      
					   
					   <span><i class="fa fa-inr" aria-hidden="true" id="Nodis_finalpriceONRoptionSELLCP" class="age_span_strike" style="text-decoration:line-through"> <span id="echo_NOdis_priceONETOURCP">
								  
					   
					   </i></span></span></label>
					  </div>
					  
					  
					  
					  
					  <div class="col-md-4 col-sm-12 col-xs-6">

					   <div class="input-group" id="inputgroup">
						<span class="input-group-btn">
						<button type="button" class="btn  btn-number" onclick="decrement_child()"  data-type="minus" data-field="quant[2]" id="btn_plus">
					   <span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						<input type="text" name="quant[2]" class="form-control input-number" value="0" min="0" max="1000" id="quant_child">
						
						<span class="input-group-btn">
						
						<button type="button" class="btn btn-number" onclick="increment_child()" data-type="plus" data-field="quant[2]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
						</div>
											<div class="clearfix"></div>

				<div class=" adult_plus">
					 <div class="form-group">
						 <div class="col-md-4 col-sm-12 col-xs-12">
							<label class="side_label1" >Infant<span class="age_span">Under 2</span></label>
						</div>
					<div class="col-md-4 col-sm-12 col-xs-6">
				   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>00<br></label>
				  </div>
				    <div class="col-md-4 col-sm-12 col-xs-6">
					   <div class="input-group" id="inputgroup">
				   
					<span class="input-group-btn">
					<button type="button" class="btn"   id="btn_plus">
				   <span class="glyphicon glyphicon-minus"></span>
					</button>
					</span>
					
					<input type="text"  class="form-control" value="0" min="0" max="1000" id="quantminus">
					
					
					<span class="input-group-btn">					
					<button type="button" class="btn"  id="btn_plus">
					<span class="glyphicon glyphicon-plus"></span>					
					</button>
					</span>
					
					
					</div> 
					</div>  
					</div>
					</div>
										<div class="clearfix"></div>

					</div>
				</div>
			</div>
      </div>
    </div>
   </div>
   </div>
				
						<div class="col-lg-12 col-md-12 col-sm-12 modal_detail1">
							<div class="col-sm-9 ">
							
							      <h6 >							  
								  <span><i class="fa fa-inr" aria-hidden="true" id="nodis_finalprice" style="text-decoration:line-through"></i> <span id="echo_nodis_price">
								 </span></span>
								 </h6>
								 
								 
										 
						
							
							<h5 class="">
							
							<span class="rate_final">
							<i class="fa fa-inr" aria-hidden="true" id="finalPrice"></i> 
							<span id="echo_price">
							
							
			<!--<input type="hidden" name="FullTotal" value=" <?php echo $finalSell; ?>" >
			<input type="hidden" name="DisFullTotal" value="<?php echo round($dis_finalSell);?>">-->
							
							
							</span></span></h5>							
							</div>
							
					      <input type="hidden"  id="hidden_nodis_finalprice">						
						  <input type="hidden" id="hiddenfinalPrice" >
						
						
		    <input type="hidden" name="FullTotal"   id="hidden_nodis_finalprice1"  >
			<input type="hidden" name="DisFullTotal"  id="hiddenfinalPrice1" >
							
							
							<div class="col-sm-3 col-xs-12">
							<button  type="submit"  class="redbtn mt-30" id="btnSave">Checkout</button>
							</div>
									
		                </div> 
						</form>
				</div>
				      <!---$$$$$$$$$$#########################--->
				 <?php }
				  
				  
				  // End Multi TanferType AND one tour Option -->
						?>
						
						
						
						
						
						<!--Thrid Model-->
    
			<div class="item">
				<form id="checkout_form" action="<?php echo site_url('checkout/booking')?>" method="post">
						<div class="modal-header" >
							
						<div class="row">
						<div id="search_modal">
						
						<!-- Multi tour Option -->
									  <?php 
				if(is_array($tour_transfer) && count($tour_transfer) > 1)
                  {
					  ?>
					  <div class="col-lg-7 col-md-7 col-sm-12 calendar_icon" >
								
			
			<i class="fa fa-car" aria-hidden="true"></i> <span id="selected_transfer5">  </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						
						<input type="hidden" name="TourId"  value="<?php echo $tour_id; ?>">
						<input type="hidden" name="TourName"  value="<?php echo $tourname; ?>">
						<input type="hidden" name="TransferName" id="selected_transfer6">
						
						<input type="hidden" name="TransferId" id="transfer_id1">
						<input type="hidden" name="TransferType" id="TransferType1">
						
						<input type="hidden" name="transfercatname" id="transfercatname1">
						
						
						
						
						<?php
				     }else
				      {
					  foreach ($tour_transfer as $row) {
                          $transferoption=$row->transferoption;
						  $sellAP=$row->sellAP;
						  $sellCP=$row->sellCP;
						  $discount=$row->discount; 
						  
						  $transfer_id=$row->tour_transferoption_id;
						  $TransferType=$row->TransferType;
						   $transfercatname=$row->transfercatname;
			  	    } ?> 
				
			          <div class="col-lg-7 col-md-7 col-sm-12 calendar_icon" >
			  
			          <i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo $transferoption ; ?>  </span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev"></a>
						</div>
						
						
						
					
					
					
					    <input type="hidden" name="TourId"  value="<?php echo $tour_id; ?>">
						<input type="hidden" name="TourName"  value="<?php echo $tourname; ?>">
						<input type="hidden" name="TransferName" value="<?php echo $transferoption; ?>">
						
						<input type="hidden" name="TransferId" value="<?php echo $transfer_id; ?>">
						<input type="hidden" name="TransferType" value="<?php echo $TransferType; ?>">
						
						<input type="hidden" name="transfercatname" value="<?php echo $transfercatname; ?>">
					
					

					<?php
				  }
					?>
					<div class="col-lg-5 col-md-5 col-sm-12  calendar_icon" >
				<i class="fa fa-calendar" aria-hidden="true"></i> <span id="selected_date2"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						</div>
						
						<input type="hidden" name="TourDate" id="selected_date3">					
						<?php
				 
				if(is_array($tour_option) && count($tour_option) > 1)
                  {
					  ?>
					  <div class="col-lg-12 col-md-12 col-sm-12 calendar_icon">
						
	<i class="fa fa-list-ul" aria-hidden="true"></i> <span id="selected_option"></span><span id="selected_time"></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev">Change</a>
						
						</div>
						
						<input type="hidden" name="TourOptId" id="selected_option_id">
						<input type="hidden" name="TourOptName" id="selected_option1">
					
						
						<input type="hidden"   name="TourTime"   id="selected_time1">
						
					 
						<?php
				  }else{
					 	foreach ($tour_option as $rows) {
							
							  $optionName=$rows->optionName;
							  
						}?>
						 <div class="col-lg-12 col-md-12 col-sm-12 calendar_icon" >
					<i class="fa fa-car" aria-hidden="true"></i> <span> <?php echo  $optionName?></span>&nbsp;
							<a class="change" href="#myCarousel" data-slide="prev" ></a>
						</div>
						
						<input type="hidden" name="TourOptName" value="<?php echo $optionName; ?>">
				<?php		
				  }
					?>
						
					
						
						
					</div>
					</div>
					</div>
						<!-- Multi tour Option -->
					<h4 id="how_many_guest">How Many Guests Joining?</h4>
  
   
<div id="search_final">
  
  		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        
		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		
         <span class="adult_label_parent" id="show_adult"><span>Adult <span id="present_adult">1</span></span></span>
		     <span class="adult_label_parent"   id="show_child">Child <span id="present_child">1</span></span>
        </a>
		
		       <input type="hidden"  name="AdultQuantity" value="1" id="present_adult1">	
		       <input type="hidden"  name="ChildQuantity"  value="0" id="present_child1">	
			   <input type="hidden"  name="InfantQuantity"  value="0" >	
		
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
		<div id="search_back_drop">
			<div class="adult_dropdown">
			
			
			 <div class=" adult_plus">
					 <div class="form-group">
					 <div class="col-md-4 col-sm-12 col-xs-12">
					  <label class="side_label1">Adult<span class="age_span">Ages 12-99</span> </label>
					  </div>
					  
					    <input type="hidden"  id="discount_price_transfer">
					    <input type="hidden"  name="Discount"  id="discount">	
					   
					   <input type="hidden"  id="sellAP_price_transfer">					  
					  
					   <div class="col-md-4 col-sm-12 col-xs-6">
						  
					   <label class="side_label_final" >
					   <i class="fa fa-inr" aria-hidden="true" id="dis_finalsellAP_price_transfer"></i><br>
					   
					   
					   <span class="age_span_strike" id="strike_price_AP" style="text-decoration:line-through"><i class="fa fa-inr" aria-hidden="true" ></i><strike id="finalsellAP_price_transfer"></strike></span>
					   
					   </label>
					  </div>
					  
					  <!-- Adult Price-->
					   <input type="hidden"  id="NoDis_hiddenfinalsellAP_price_transfer">
					    <!-- DisAdult Price-->
					   <input type="hidden"  id="hiddenfinalsellAP_price_transfer">
					   
					   
					    <input type="hidden"  name="AdultPrice"  id="adultprice">	
					    <input type="hidden"  name="DisAdultPrice"  id="Disadultprice">	
					   
					   
					  <div class="col-md-4 col-sm-12 col-xs-6">
					  <div class="input-group" id="inputgroup">
					  
						<span class="input-group-btn">
						<button type="button" class="btn btn-number" disabled="disabled"  onclick="decrement_adult()" data-type="minus" data-field="quant[1]"  id="btn_plus">
						<span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						
						<input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="1000" id="quant_adult">
											
						<span class="input-group-btn">
						<button type="button" class="btn btn-number"  onclick="increment_adult()" data-type="plus" data-field="quant[1]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
			</div>
			
			
			
					<div class="clearfix"></div>
					<div class="adult_plus">
					 <div class="form-group">
					 <div class="col-md-4 col-sm-12 col-xs-12">
					   <label class="side_label1">Child<span class="age_span">Ages 2-12</span> </label>
					   </div>
					   
					     <input type="hidden"  id="sellCP_price_transfer">
						 
					     <div class="col-md-4 col-sm-12 col-xs-6">
					   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true" id="dis_finalsellCP_price_transfer"></i><br>
					   
					   <span class="age_span_strike" id="strike_price_CP" style="text-decoration:line-through" ><i class="fa fa-inr" aria-hidden="true" ></i> <strike id="finalsellCP_price_transfer"></strike></span>
					   </label>
					    </div>
					   
					     <!-- Child Price-->
					  <input type="hidden"  id="NoDis_hiddenfinalsellCP_price_transfer">
					  
					   <!-- Dis Child Price-->
					   <input type="hidden"  id="hiddenfinalsellCP_price_transfer">
					  
					  
					  
					    <input type="hidden"  name="ChildPrice"  id="ChildPrice">	
					    <input type="hidden"  name="DisChildPrice"  id="DisChildPrice">	
						
						
					  <div class="col-md-4 col-sm-12 col-xs-6">

					   <div class="input-group" id="inputgroup">
						<span class="input-group-btn">
						<button type="button" class="btn  btn-number" onclick="decrement_child()"  data-type="minus" data-field="quant[2]" id="btn_plus">
					   <span class="glyphicon glyphicon-minus"></span>
						</button>
						</span>
						
						<input type="text" name="quant[2]" class="form-control input-number" value="0" min="0" max="1000" id="quant_child">
						
						<span class="input-group-btn">
						
						<button type="button" class="btn btn-number" onclick="increment_child()" data-type="plus" data-field="quant[2]" id="btn_plus">
						<span class="glyphicon glyphicon-plus"></span>
						</button>
						</span>
						
						
						</div>  
						</div>  
						</div>
						</div>
											<div class="clearfix"></div>

				<div class=" adult_plus">
					 <div class="form-group">
						 <div class="col-md-4 col-sm-12 col-xs-12">
							<label class="side_label1" >Infant<span class="age_span">Under 2</span></label>
						</div>
						
						<div class="col-md-4 col-sm-12 col-xs-6">
				   <label class="side_label_final" ><i class="fa fa-inr" aria-hidden="true"></i>00<br></label>
				  </div>
				   		<div class="col-md-4 col-sm-12 col-xs-6">
						
				   <div class="input-group" id="inputgroup">
				   
					<span class="input-group-btn">
					<button type="button" class="btn"   id="btn_plus">
				   <span class="glyphicon glyphicon-minus"></span>
					</button>
					</span>
					
					<input type="text"  class="form-control" value="0" min="0" max="1000" id="quantminus">
					
					
					<span class="input-group-btn">					
					<button type="button" class="btn"  id="btn_plus">
					<span class="glyphicon glyphicon-plus"></span>					
					</button>
					</span>
					
					
					</div>  
					</div>  
					</div>
					</div>
										<div class="clearfix"></div>

					</div>
				</div>
			</div>
      </div>
    </div>
   </div>
   </div>
				
						<div class="col-lg-12 col-md-12 col-sm-12 modal_detail1">
							<div class="col-sm-9 ">
							
							
							
							<h6 id="strike_price_hide"><span><i class="fa fa-inr" aria-hidden="true" style="text-decoration:line-through" ></i><strike id="nodis_finalprice"></strike> </span></h6>
							<h5 class=""><span class="rate_final"><i class="fa fa-inr" aria-hidden="true" id="finalPrice"></i> <span id="echo_price"> </span></span></h5>
							
							<input type="hidden" name="FullTotal" id="hidden_nodis_finalprice">
							<input type="hidden" name="DisFullTotal" id="hiddenfinalPrice">
							
							
							</div>
							<div class="col-sm-3 col-xs-12">
							<button  type="submit"  class="redbtn mt-30" id="btnSave">Checkout</button>
							</div>									
		                </div> 
						
										</form>
									</div>
								</div>		   
							</div>
						</div>
					</div>
				</div>
			  </div>	
			</form>
		</div>
		<?php 
									foreach ($tour_prices as $row) {
									foreach($row as $second ){
								  
   
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 
													$apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
					<div class="sidebar_detail" id="top">
				<div class="price_box_detail">
				<div class="old_rate1">
				From INR
				</div>
				<div class="rate">
				<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new">  Per Person</span>
				</div>
				<div class="price_gurantee_section">
				Tripoye Price Guarantee
				</div>
				<button type="button" class="btn btn-demo btn-block" onClick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
				</div>
				
	       </div>
		   
		   	<div class="sidebar_detail_bottom" id="bottom" data-spy="affix" data-offset-top="80" data-offset-bottom="395" style="display:none">
					
					<div class="price_box_detail">
					<div class="name">
					<label><?php echo $tourname?></label>
					
					
					<div class="reviews">
		  <?php 
	   if ($TourReview == '5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	   }elseif($TourReview == '3.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	  }elseif($TourReview == '3')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
		   
	   }elseif($TourReview == '2.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
	    }elseif($TourReview == '2')
		{
			?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		 }elseif($TourReview == '1.5')
		 {
			 ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }elseif($TourReview == '1')
		 {
			  ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }else{
			 
			  ?>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }
		   ?>
					</div>
					
					
					</div>
					
					
					<div class="old_rate1">
					From INR
					</div>
					<div class="rate">
					<div><i class="fa fa-inr" aria-hidden="true"></i></div><?php echo round($final_price);?> <span class="per_person_new">  Per Person</span>
					</div>
					<div class="price_gurantee_section">
					Tripoye Price Guarantee
					</div>
					<button type="button" class="btn btn-demo btn-block"  onclick="show_book()"  data-toggle="modal" data-target="#costumModal30">Book Now</button>
					</div>					
			</div>	
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
				<div class="sidebar_detail" id="top1">
				<div class="price_box_detail">
				<div class="old_rate1">
				From INR
				</div>
				<div class="rate">
				<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
				</div>
				<div class="price_gurantee_section">
				Tripoye Price Guarantee
				</div>
				<button type="button" class="btn btn-demo btn-block" onclick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
				</div>
				
	          </div>
			  
			  
			  	<div class="sidebar_detail_bottom" id="bottom1" data-spy="affix" data-offset-top="80" data-offset-bottom="395" style="display:none">
					
					<div class="price_box_detail">
					<div class="name">
					<label><?php echo $tourname?></label>
					<div class="reviews">
		  <?php 
	   if ($TourReview == '5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	   }elseif($TourReview == '3.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	  }elseif($TourReview == '3')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
		   
	   }elseif($TourReview == '2.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
	    }elseif($TourReview == '2')
		{
			?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		 }elseif($TourReview == '1.5')
		 {
			 ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }elseif($TourReview == '1')
		 {
			  ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }else{
			 
			  ?>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }
		   ?>
					</div>
					</div>
					
					
					<div class="old_rate1">
					From INR
					</div>
					<div class="rate">
					<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
					</div>
					<div class="price_gurantee_section">
					Tripoye Price Guarantee
					</div>
					<button type="button" class="btn btn-demo btn-block" onclick="show_book()"  data-toggle="modal" data-target="#costumModal30">Book Now</button>
					</div>					
			</div>	
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
										<div class="sidebar_detail" id="top2">
				<div class="price_box_detail">
				<div class="old_rate1">
				From INR
				</div>
			<div class="rate">
				<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
				</div>
				<div class="price_gurantee_section">
				Tripoye Price Guarantee
				</div>
				<button type="button" class="btn btn-demo btn-block" onclick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
				</div>
				
	         </div>
			 
			 
			 	<div class="sidebar_detail_bottom" id="bottom2" data-spy="affix" data-offset-top="80" data-offset-bottom="395" style="display:none">
					
					<div class="price_box_detail">
					<div class="name">
					<label><?php echo $tourname?></label>
					<div class="reviews">
		  <?php 
	   if ($TourReview == '5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
		   <?php
	   }
	   elseif($TourReview == '4')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	   }elseif($TourReview == '3.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
	  }elseif($TourReview == '3')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		   
		   
	   }elseif($TourReview == '2.5')
	   {
		   ?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
	    }elseif($TourReview == '2')
		{
			?>
		   	<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
			<i class="fa fa-star-o"></i>
		   <?php
		 }elseif($TourReview == '1.5')
		 {
			 ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }elseif($TourReview == '1')
		 {
			  ?>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }else{
			 
			  ?>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
				<i class="fa fa-star-o"></i>
			   <?php
		 }
		   ?>
					</div>
					</div>
					
					
					<div class="old_rate1">
					From INR
					</div>
					<div class="rate">
					<div></div><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($final_price);?> <span class="per_person_new"> <strike><?php echo $Tsum;?></strike> Per Person</span>
					</div>
					<div class="price_gurantee_section">
					Tripoye Price Guarantee
					</div>
					<button type="button" class="btn btn-demo btn-block" onclick="show_book()" data-toggle="modal" data-target="#costumModal30">Book Now</button>
					</div>					
			</div>	
												<?php
													}
													
											 }
											         
										 }
										 }			
               
                                       ?>
									   
					
					
					
	</div>
	</div>
	
	
			<div>
                            
                            <a title="" onclick="show_book()" data-toggle="modal" data-target="#costumModal30" class="tourdetbtnl1  btn_scroll " >Book Now</a>
                        </div>							
						
	
	
	</div>
	</div>
	</div>
	</div>
	<!-- end Main Wrapper -->
	
	<footer class="footer scrollspy-footer"> <!-- add scrollspy-footer to stop sidebar scrollspy -->
			
			<div class="container">
			
				<div class="main-footer">
				
					<div class="row">
				
						<div class="col-xs-12 col-sm-4 col-md-4">
						
							<div class="footer-logo">
								<img src="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>" alt="Logo" />
							</div>
							
							<p class="footer-address">Above Keyour Electronics,Office No:2 Karvenagar,Pune-58,India. 
							<br/><br/> <i class="fa fa-phone"></i> +91-9689044624 
							<br/><br/> <i class="fa fa-envelope-o"></i> <a href="mailto:info@tripoye.com">info@tripoye.com</a></p>
							
							<div class="footer-social">
							
								<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
							
							</div>
							
							<p class="copy-right">&#169; Copyright 2018 Tripoye. All Rights Reserved</p>
							
						</div>
						
						<div class="col-xs-12 col-sm-8 col-md-8">

							<div class="row gap-10">
							
								<div class="col-xs-12 col-sm-4 col-md-4  mt-30-xs">
								
									<h5 class="footer-title">About Tripoye</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('about')?>">Who we are</a></li>
										<li><a href="<?php echo site_url('career')?>">Careers</a></li>
										<li><a href="<?php echo site_url('contact')?>">Contact Us</a></li>
										<li><a href="<?php echo site_url('b2b')?>">Tripoye B2B</a></li>
							
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Customer Information</h5>
									
									<ul class="footer-menu">
										<li><a href="<?php echo site_url('faq')?>">FAQ</a></li>
										<li><a href="<?php echo site_url('termscondition')?>">Terms & Conditions</a></li>
										<li><a href="<?php echo site_url('privacypolicy')?>">Privacy Policy</a></li>
										<li><a href="#">Blog</a></li>
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Destinations</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('lonavala')?>">Lonavala Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('goa')?>">Goa Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('pune')?>">Pune Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('mumbai')?>">Mumbai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('dubai')?>">Dubai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('singapore')?>">Singapore Tours</a></li>
										<li><a href="#">Kuala Lumpur Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('hong-kong')?>">Homg Kong Tours</a></li>
										
									</ul>
									
								</div>
								
							</div>

						</div>
						
					</div>

				</div>
				
			</div>
			
		</footer>
	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
	 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->
	
	<script type="text/javascript">
  
   function change(rating)
   {
	   
    $('#selected_rating').val(rating);

    
   }

</script>
	<script>

function save_rating()
{
    
    var url;      
    var id1=document.getElementById("email").value;

  if(id1!='')
  {
   //validateForm();
     var atpos = id1.indexOf("@");
     var dotpos = id1.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=id1.length) {
  
   message="Please Enter Your Valid Email Id";
     document.getElementById("msg5").innerHTML = message;
      $("#msg66").hide();
      $("#msg5").show();
 }
 else{
   url = "<?php echo site_url('detailspage_manager/add_rating')?>";
  
   
       var id1=document.getElementById("email").value;
      
$.ajax({
        url : url,
        type: "POST",
        data:$('#rating_form').serialize(),
        //data:{newsletter:id1},
        dataType: "JSON",
     
        success: function(data)
        {
			console.log(data);
     // message="Your Reviews is  Successfully Completed";
     //document.getElementById("msg66").innerHTML = message;
      $("#msg5").hide();
      $("#msg66").show();
       $("#rating_form").trigger("reset");
    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
 }
  
  }
  else
  {
  // message="Please Enter Your Valid Email Id";
     //document.getElementById("msg5").innerHTML = message;
      $("#msg66").hide();
      $("#msg5").show();
  }
}          
           
           
 </script>
	
	
<script>
function select_date(date)
{
	console.log(date);
}
function  checkout()
{
	$('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
	
	  url = "<?php echo site_url('b2B_handler/add_booking')?>";
	
	 $.ajax({
        url : url,
        type: "POST",
        data: $('#checkout_form').serialize(),
        dataType: "JSON",
		
        success: function(data)
        {
             console.log(data);
            
            $('#btnSave').text('saved...'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>

	<script>
	function select_transfer(clicked_transfer_id)
{
     var str = document.getElementById(clicked_transfer_id).value; 
	 var trnsfersellap = str.split("/");
	 trnsfer = trnsfersellap[0];
     sellAP = trnsfersellap[1];
	 
	 sellCP = trnsfersellap[2];
	 selldiscount = trnsfersellap[3];
	 transfer_id = trnsfersellap[4];
	 TransferType = trnsfersellap[5];
	 transfercatname = trnsfersellap[6];
	 
	  
	 
	   $('#selected_transfer1').html(trnsfer);	
	   $('#selected_transfer2').html(trnsfer);	 
	   $('#selected_transfer2').html(trnsfer);	 
	   $('#selected_transfer3').html(trnsfer);
	   $('#selected_transfer3N').html(trnsfer);
       $('#selected_transfer4').html(trnsfer);   
       $('#selected_transfer5').html(trnsfer);
	   
       $('#selected_transfer6').val(trnsfer);
       $('#selected_transfer7').val(trnsfer);
       $('#selected_transfer7N').val(trnsfer);
	   
	   $('#transfer_id1').val(transfer_id);
	   $('#transfer_id2').val(transfer_id);
	   $('#transfer_id2N').val(transfer_id);
	   
	    $('#TransferType1').val(TransferType);
	   $('#TransferType2').val(TransferType);
	   $('#TransferType2N').val(TransferType);
	   
	   $('#transfercatname1').val(transfercatname);
	   $('#transfercatname2').val(transfercatname);
	   $('#transfercatname2N').val(transfercatname);
	   
       $('#discount_price_transfer').val(selldiscount);
	   $('#discount').val(selldiscount);
	  
	  ///////SELL AP
	  
	   $('#sellAP_price_transfer').val(sellAP);
	   
	   $('#TnsfsellAP_one_tour').val(sellAP);   
	  
	   
	      var dis_finaSellAP=(sellAP * selldiscount)/100;		 
          var finalsellAP_prices = +sellAP - +dis_finaSellAP;
		 
		  var one_tourOp_sellAP = document.getElementById('one_tourOp_sellAP').value; 
	      var  ONE_TOURop_SELLAP = +finalsellAP_prices + +one_tourOp_sellAP;
		
	   
	    $('#dis_finalpriceONRoptionSELLAP').html(ONE_TOURop_SELLAP);
		
	    $('#hiddenfinalsellAP_price_transfer').val(ONE_TOURop_SELLAP);
	    
	    

		
		
		 $('#DisAdultPriceOneOp').val(ONE_TOURop_SELLAP);
		 $('#AdultPriceOneOp').val(sellAP);
		 $('#DiscountOneOp').val(selldiscount);
		
		
		  var NOdis_one_tourOp_sellAP = document.getElementById('NOdis_one_tourOp_sellAP').value; 
		  var No_dis = +sellAP + +NOdis_one_tourOp_sellAP;
		
		$('#Nodis_finalpriceONRoptionSELLAP').html(No_dis);
		$('#NoDis_hiddenfinalsellAP_price_transfer').val(No_dis);
		
		
		if(No_dis == ONE_TOURop_SELLAP)
		{
			$('#Nodis_finalpriceONRoptionSELLAP').hide();
		}
		else
		{
			
		}
		
	
	    
		
		//////,,,,,SELL CP 
		
	   $('#sellCP_price_transfer').val(sellCP);
	   
	    $('#TnsfsellCP_one_tour').val(sellCP);   
	  
	   
	     var dis_finaSellCP=(sellCP * selldiscount)/100;		 
         var finalsellCP_prices = +sellCP - +dis_finaSellCP;
		 
		  var one_tourOp_sellCP = document.getElementById('one_tourOp_sellCP').value; 
	      var  ONE_TOURop_SELLCP = +finalsellCP_prices + +one_tourOp_sellCP;
		
	   
	    $('#dis_finalpriceONRoptionSELLCP').html(ONE_TOURop_SELLCP);
		
	    $('#hiddenfinalsellCP_price_transfer').val(ONE_TOURop_SELLCP);
	    
	    $('#echo_dis_priceONETOURCP').hide(); 

		
		
		 $('#DisChildPriceOp').val(ONE_TOURop_SELLCP);
		 $('#ChildPriceOp').val(sellCP);
		 //$('#DiscountOneOp').val(selldiscount);
		
		
		  var NOdis_one_tourOp_sellCP = document.getElementById('NOdis_one_tourOp_sellCP').value; 
		  var No_dis = +sellCP + +NOdis_one_tourOp_sellCP;
		
		$('#Nodis_finalpriceONRoptionSELLCP').html(No_dis);
		$('#NoDis_hiddenfinalsellCP_price_transfer').val(No_dis);
		
		if(No_dis == ONE_TOURop_SELLCP)
		{
			$('#Nodis_finalpriceONRoptionSELLCP').hide();
		}
		else
		{
			
		}
	    
		
		
		
		
		
		var final_price =+ONE_TOURop_SELLAP + +ONE_TOURop_SELLCP;
		
				
		$('#finalPrice').html(ONE_TOURop_SELLAP);
		
		$('#hiddenfinalPrice').val(ONE_TOURop_SELLAP);
		$('#hiddenfinalPrice1').val(ONE_TOURop_SELLAP);
		
	       
		
	      var NOdis_one_tourOp_sellAP = document.getElementById('NOdis_one_tourOp_sellAP').value; 
		  var No_disALL = +sellAP + +NOdis_one_tourOp_sellAP;
	  
		$('#nodis_finalprice').html(No_disALL);
		
		$('#hidden_nodis_finalprice').val(No_disALL);
		$('#hidden_nodis_finalprice1').val(No_disALL);
		
		
		
		
		
		if(No_disALL == ONE_TOURop_SELLAP)
		{
			$('#nodis_finalprice').hide();
		}
		else
		{
			
		}
	   
	   
	  
}


function select_option(clicked_option_id)
{     
    
        var str = document.getElementById(clicked_option_id).value; 
	    var optionPrice = str.split("/");
		
	    option = optionPrice[0];
	    tour_time = optionPrice[1];	
	   
      tour_sellAP = optionPrice[2];
	 
	  tour_sellCP = optionPrice[3];
	  Touroption_discount = optionPrice[4];
	
	if(tour_time == '')
	   {
		   
	   }else{
		    $('#selected_time').html(tour_time);
			$('#selected_time1').val(tour_time);
	   }
	
	   $('#selected_option').html(option);
	   $('#selected_option1').val(option); 
	   $('#selected_option_id').val(clicked_option_id);
	   
	   
		
		
	    //Final AP  Price
		 var discount_price = document.getElementById('discount_price_transfer').value;
		
	     var sellAP_price = document.getElementById('sellAP_price_transfer').value;
		
		 var dis_finaSellAP=(sellAP_price * discount_price)/100;		 
         var finalsellAP_prices = +sellAP_price - +dis_finaSellAP;
		 
		
		  var finalsellAP_price = Math.round(finalsellAP_prices);
		 
		 var dis_finalTourSellAP=(tour_sellAP * Touroption_discount)/100;	
		
	     var finalToursellAP_prices=+tour_sellAP - +dis_finalTourSellAP;
		
	     var finalToursellAP_price = Math.round(finalToursellAP_prices);
		
		
        var finalAP_prices=+sellAP_price + +tour_sellAP;	
		
		var finalAP_price = Math.round(finalAP_prices);
		
		var Dis_finalAP_price=+finalsellAP_price + +finalToursellAP_price;
		
		
		if(finalAP_price == Dis_finalAP_price)
		{
			$('#dis_finalsellAP_price_transfer').html(Dis_finalAP_price);
			$('#hiddenfinalsellAP_price_transfer').val(Dis_finalAP_price);
			$('#Disadultprice').val(Dis_finalAP_price);
			
			$('#strike_price_AP').hide();
			
		}
		else
		{
			$('#strike_price_AP').show();
			
			$('#finalsellAP_price_transfer').html(finalAP_price);
			$('#NoDis_hiddenfinalsellAP_price_transfer').val(finalAP_price);
			$('#adultprice').val(finalAP_price);		
			
			
			$('#dis_finalsellAP_price_transfer').html(Dis_finalAP_price);
			$('#hiddenfinalsellAP_price_transfer').val(Dis_finalAP_price);
			$('#Disadultprice').val(Dis_finalAP_price);
			
			
		}
		
	   
	    //Final CP  Price
	   var sellCP_price = document.getElementById('sellCP_price_transfer').value;
	   
	  
		 var dis_finaSellCP=(sellCP_price * discount_price)/100;		 
         var finalsellCP_prices = +sellCP_price - +dis_finaSellCP;		 
		 
		 var finalsellCP_price = Math.round(finalsellCP_prices);
		 
		 
		 
		 var dis_finalTourSellCP=(tour_sellCP * Touroption_discount)/100;	
		
	     var finalToursellCP_prices=+tour_sellCP - +dis_finalTourSellCP;
		
	    var finalToursellCP_price = Math.round(finalToursellCP_prices);
		
		
		
        var finalCP_prices=+sellCP_price + +tour_sellCP;	
		
		var finalCP_price = Math.round(finalCP_prices);		
		
		
		var Dis_finalCP_price=+finalsellCP_price + +finalToursellCP_price;
		
		
			if(finalCP_price == Dis_finalCP_price)
			{
				    $('#dis_finalsellCP_price_transfer').html(Dis_finalCP_price);
					$('#hiddenfinalsellCP_price_transfer').val(Dis_finalCP_price);
					$('#DisChildPrice').val(Dis_finalCP_price);
					
			       $('#strike_price_CP').hide();
			}
			else
			{
				    $('#strike_price_CP').show();
					
				    $('#finalsellCP_price_transfer').html(finalCP_price);
					$('#NoDis_hiddenfinalsellCP_price_transfer').val(finalCP_price);
					$('#ChildPrice').val(finalCP_price);
					
					$('#dis_finalsellCP_price_transfer').html(Dis_finalCP_price);
					$('#hiddenfinalsellCP_price_transfer').val(Dis_finalCP_price);
					$('#DisChildPrice').val(Dis_finalCP_price);
					
			}
		  
	
	   
	   //Final Price
	   //var finalPrices = +Dis_finalAP_price + +Dis_finalCP_price;
	   var finalPrices = +Dis_finalAP_price;
	   
	    var finalPrice = Math.round(finalPrices);
		
	    
		
		//var nodis_prices = +finalAP_price + +finalCP_price;
		var nodis_prices = +finalAP_price;
		
		var nodis_price = Math.round(nodis_prices);
		
		
		if(finalPrice == nodis_price)
		{
			    $('#hiddenfinalPrice').val(finalPrice);
				$('#hiddenfinalPrice1').val(finalPrice);
				$('#finalPrice').html(finalPrice);
				$('#finalPrice').val(finalPrice);
				
			    $('#strike_price_hide').hide();
			
		}
		else
		{
			 $('#strike_price_hide').show();
			 
			    $('#hiddenfinalPrice').val(finalPrice);
				$('#hiddenfinalPrice1').val(finalPrice);
				$('#finalPrice').html(finalPrice);
				$('#finalPrice').val(finalPrice);
				
			$('#nodis_finalprice').html(nodis_price);
			$('#nodis_finalprice').val(nodis_price);
			$('#hidden_nodis_finalprice').val(nodis_price);
		}
		
	   
	   
	 	 
}


function select_time(element,tour_id)
 { 
 var T_tour_id="T"+tour_id;
 
 document.getElementById(T_tour_id).style.display = 'block';
	 
    var time = element.options[element.selectedIndex].value;
    $('#selected_time').html(time);	
	$('#selected_time1').val(time);	
 }


window.onload = function()
{
    document.getElementById('butdate').onclick = function()
	{
		var todayDate = new Date().toISOString().slice(0,10);
		
		var cutoffs = document.getElementById('cutoff').value; 

		var cutoff = +cutoffs + 2;
		var Inc_date = incrementDate(todayDate, cutoff);
		
		
      var SelectedDate = document.getElementById('datetimepicker').value; 
	  
	  if(SelectedDate > Inc_date || cutoffs == 0)
	{
	  
	  var a = document.getElementById('butdate'); 
         a.href = "#myCarousel";
		 $('#cutoff_msg').hide();
	}
	else
	{	
		 var a = document.getElementById('butdate'); 
         a.href = "#";
		 $('#cutoff_msg').show();
		
	}
	  
       $('#selected_date1').html(SelectedDate);	 
	   $('#selected_date2').html(SelectedDate);	 
	   $('#selected_date2N').html(SelectedDate);	 
	   $('#selected_date3').val(SelectedDate);	
       $('#selected_date4').val(SelectedDate);	   
       $('#selected_date4N').val(SelectedDate);	   
    }
}; 



function incrementDate(date_str, incrementor) {
    var parts = date_str.split("-");
    var dt = new Date(
        parseInt(parts[0], 10),      // year
        parseInt(parts[1], 10) - 1,  // month (starts with 0)
        parseInt(parts[2], 10)       // date
    );
    dt.setTime(dt.getTime() + incrementor * 86400000);
    parts[0] = "" + dt.getFullYear();
    parts[1] = "" + (dt.getMonth() + 1);
    if (parts[1].length < 2) {
        parts[1] = "0" + parts[1];
    }
    parts[2] = "" + dt.getDate();
    if (parts[2].length < 2) {
        parts[2] = "0" + parts[2];
    }
    return parts.join("-");
};

function increment_adult()
{
	 var a = document.getElementById('quant_adult').value;

	 document.getElementById("show_adult").style.visibility = "visible";
	 var b= +a + 1;
	
	   $('#present_adult').html(b);
       $('#present_adult1').val(b);	
       $('#present_adult2').val(b);	
	 
		 
	     var APfinalPrice = document.getElementById('hiddenfinalsellAP_price_transfer').value;
	 
	      var Incfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var IncfinalPrices = +finalPrice + +APfinalPrice;
		  
		  
		  $('#hiddenfinalPrice').val(IncfinalPrices);
		  $('#hiddenfinalPrice1').val(IncfinalPrices);
	      $('#finalPrice').html(IncfinalPrices);
	      $('#finalPrice').val(IncfinalPrices);
		  $('#echo_price').hide();
		  
		  // final strike price
		  
		  var APfinalPrice = document.getElementById('NoDis_hiddenfinalsellAP_price_transfer').value;
	 
	      var Incfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		
		  var IncfinalPrices = +finalPrice + +APfinalPrice;
		   
		  
		  $('#hidden_nodis_finalprice').val(IncfinalPrices);
		  $('#hidden_nodis_finalprice1').val(IncfinalPrices);
	      $('#nodis_finalprice').html(IncfinalPrices);
	      $('#nodis_finalprice').val(IncfinalPrices);
		  $('#echo_nodis_price').hide();
	
	    
		  
}


function decrement_adult()
{
	
	 var a = document.getElementById('quant_adult').value;
	 
	 if(a == 1)
	 {
		 document.getElementById("show_adult").style.visibility = "hidden";
		 
	 }else{
		 var b= +a - 1;	 
		 
	 
	   $('#present_adult').html(b);	
	   $('#present_adult1').val(b);	
	   $('#present_adult2').val(b);	
	   
	    var APfinalPrice = document.getElementById('hiddenfinalsellAP_price_transfer').value;
	  
	    var Decfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var DecfinalPrices = +finalPrice -  +APfinalPrice;
		  
		  $('#hiddenfinalPrice').val(DecfinalPrices);
		  $('#hiddenfinalPrice1').val(DecfinalPrices);
		  
	      $('#finalPrice').html(DecfinalPrices);
	      $('#finalPrice').val(DecfinalPrices);
	      $('#echo_price').hide();
		  
		  	  // final strike price
		  
		   var APfinalPrice = document.getElementById('NoDis_hiddenfinalsellAP_price_transfer').value;
	 
	       var Decfinalprice = APfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		
		  var DecfinalPrices = +finalPrice -  +APfinalPrice;
		  
		
		  $('#hidden_nodis_finalprice').val(DecfinalPrices);
		  $('#hidden_nodis_finalprice1').val(DecfinalPrices);
	      $('#nodis_finalprice').html(DecfinalPrices);
	      $('#nodis_finalprice').val(DecfinalPrices);
		  $('#echo_nodis_price').hide();
	
		  
	  }
	  
	 
	 
	 
	
	
}

function increment_child()
{
	 var a = document.getElementById('quant_child').value;
	
	 document.getElementById("show_child").style.visibility = "visible";
		 var b= +a + 1;	 
	  
	    $('#present_child').html(b);
	    $('#present_child1').val(b);
	    $('#present_child2').val(b);
		
	      var CPfinalPrice = document.getElementById('hiddenfinalsellCP_price_transfer').value;
	  
	      var Incfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var IncfinalPrices = +finalPrice + +CPfinalPrice;
		  
		  $('#hiddenfinalPrice').val(IncfinalPrices);
		  $('#hiddenfinalPrice1').val(IncfinalPrices);
		  
	      $('#finalPrice').html(IncfinalPrices);
	      $('#finalPrice').val(IncfinalPrices);
	      $('#echo_price').hide();
		  
		  
		  // final strike price
		  
		 
		 var CPfinalPrice = document.getElementById('NoDis_hiddenfinalsellCP_price_transfer').value;
	  
	    var Incfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		  
		  var IncfinalPrices = +finalPrice + +CPfinalPrice;
		  
		
		  $('#hidden_nodis_finalprice').val(IncfinalPrices);
		  $('#hidden_nodis_finalprice1').val(IncfinalPrices);
		   
	      $('#nodis_finalprice').html(IncfinalPrices);
	      $('#nodis_finalprice').val(IncfinalPrices);
		  $('#echo_nodis_price').hide();
	
}


function decrement_child()
{
	 var a = document.getElementById('quant_child').value;

	if(a == 0)
	  //if(a == 1)
	 {
		 document.getElementById("show_child").style.visibility = "hidden";
		 
		 var b=0;	 
		  $('#present_child1').val(b);	
		  $('#present_child2').val(b);	
		 
		/*  var CPfinalPrice = document.getElementById('hiddenfinalsellCP_price_transfer').value;
	  
	   // var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		  
		  $('#hiddenfinalPrice').val(DecfinalPrices);
		  $('#hiddenfinalPrice1').val(DecfinalPrices);
		  
	      $('#finalPrice').html(DecfinalPrices);
	      $('#finalPrice').val(DecfinalPrices);
          $('#echo_price').hide();	

		 
		 // final strike price		  
		  var CPfinalPrice = document.getElementById('NoDis_hiddenfinalsellCP_price_transfer').value;
	  
	     //var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		 
		  
		
		  $('#hidden_nodis_finalprice').val(DecfinalPrices);
		  $('#hidden_nodis_finalprice1').val(DecfinalPrices);
		  
	      $('#nodis_finalprice').html(DecfinalPrices);
	      $('#nodis_finalprice').val(DecfinalPrices);
		  $('#echo_nodis_price').hide();  */
		 
	 }else{ 
		// var b= +a - 1;
		var b= a;
		
		if(b == 1){
			document.getElementById("show_child").style.visibility = "hidden";
		}
		 document.getElementById("show_child").style.visibility = "block";
	    
	    $('#present_child').html(b);
		$('#present_child1').val(b);
		$('#present_child2').val(b);
		
          var CPfinalPrice = document.getElementById('hiddenfinalsellCP_price_transfer').value;
	  
	      var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hiddenfinalPrice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		  
		  $('#hiddenfinalPrice').val(DecfinalPrices);
		  $('#hiddenfinalPrice1').val(DecfinalPrices);
		  
	      $('#finalPrice').html(DecfinalPrices);
	      $('#finalPrice').val(DecfinalPrices);
          $('#echo_price').hide();		


// final strike price
		  
		  var CPfinalPrice = document.getElementById('NoDis_hiddenfinalsellCP_price_transfer').value;
	  
	      var Decfinalprice = CPfinalPrice * b;

		  var finalPrice = document.getElementById('hidden_nodis_finalprice').value;
		 		  
		  var DecfinalPrices = +finalPrice  -  +CPfinalPrice;
		 
		  
		
		  $('#hidden_nodis_finalprice').val(DecfinalPrices);
	      $('#nodis_finalprice').html(DecfinalPrices);
	      $('#nodis_finalprice').val(DecfinalPrices);
		  $('#echo_nodis_price').hide();
		  
	 }
	
	
}

	</script>
	<script>
	function show_nextbtn()
	{
		$('#next_btn').show();
	}
	</script>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/product_details.js')?>"></script>
<!--<script type="text/javascript" src="<?php echo base_url('assets/js/index.js')?>"></script>-->
<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/moment.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/bootstrapDatepickr-1.0.0.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/wizard.js')?>"></script>



<script>
jQuery(function($) {
 $(window).scroll(function() {
	 var e = $(this).scrollTop();
   if (e > 20)
   {
	  ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
	 $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
<script>
function show_book()
{
	//$('.hidden_price_div').show();
	$('#top').hide();
	$('#bottom').hide();
	$('#top1').hide();
	$('#bottom1').hide();
	$('#top2').hide();
	$('#bottom2').hide();

	  
}

</script>


<script>
$('#datetimepicker').datetimepicker({
    inline: true,
    sideBySide: true,
    keepOpen: true,
    format: 'YYYY-MM-DD ',
   minDate: moment(1, 'h')	
});


</script>


<script>
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});


$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});


$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});


$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
<script>
$(document).scroll(function () {
    var y = $(this).scrollTop();
	//if ( $('.hidden_price_div').css('display') == 'none' ){
	if (y > 75) {
        $('.sidebar_detail_bottom').fadeIn();
		
	
    } else {
        $('.sidebar_detail_bottom').fadeOut();
    } 
	/* }
	else
	{
		
	} */
		
	
});
</script>

<script>
 $(window).scroll(function() {
	//if ( $('.hidden_price_div').css('display') == 'none' )
	//{
	 if ($(this).scrollTop()>0)
     {
        $('.sidebar_detail').fadeOut();
	 }
    else
     {
      $('.sidebar_detail').fadeIn();
     }
	/* }
	else
	{
		
	} */
   
 }); 
</script>




<script>
$(document).ready(function() {

document.getElementById("show_child").style.visibility = "hidden";

  $(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
      numPanelOpen = $(accordionId + ' .collapse.in').length;
    
    $(this).toggleClass("active");

    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })

  openAllPanels = function(aId) {
    console.log("setAllPanelOpen");
    $(aId + ' .panel-collapse:not(".in")').collapse('show');
  }
  closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    $(aId + ' .panel-collapse.in').collapse('hide');
  }
     
});

</script>
<!--<script type="text/javascript">


$( "#booknow" ).click(function() {
  $( "#buynow" ).toggle( "slow", function() {
    // Animation complete.
  });
});
</script>-->

<!--<script>
$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 0) {
        $('#colorful').fadeIn();
	
    } else {
        $('#colorful').fadeOut();
    }
	
});
</script>
<script>
 $(window).scroll(function() {

    if ($(this).scrollTop()>0)
     {
        $('#original').fadeOut();
     }
    else
     {
      $('#original').fadeIn();
     }
 }); 
</script>-->





</body>

</html>