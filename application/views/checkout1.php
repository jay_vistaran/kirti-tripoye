<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrap-material-datetimepicker.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrapDatepickr-1.0.0.min.css')?>" rel="stylesheet">	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

		<!-- start Header -->
	<header id="header">
	  
	<!-- start Navbar (Header) -->
	<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
	
	
	<div class="navbar-top" >
	
	<div class="container">
	
	<div class="flex-row flex-align-middle">
	<div class="flex-shrink flex-columns" id="test">
	<a class='navbar-logo' href='<?php echo site_url('home')?>'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>' id="logo2" class='img-responsive' >
	</a>	
	</div>
	
	</div>

	</div>
	
	</div>
	
	<div id="slicknav-mobile"></div>


	
	</nav>
	<!-- end Navbar (Header) -->

	</header>
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
		
			
			<div class="content-wrapper">
			
				<div class="container">
				
			<form  id="checkoutform" action="<?php echo base_url('checkout/payu_money_handler')?>" method="post" onSubmit="return clearField()";>
				
					<div class="row">						
						<div class="col-sm-12 col-md-12 mt-50">
						
			<div class="row stepsecond mb20">
						<div class="col-xs-12 ">
						<div class="col-xs-4 pr0 pl0 pos_rel">
                        <div class="line"></div>
                        <div class="stepsdot"></div>
                        <div class="stepone orange">1</div>
                        <div class="title">Add to Cart</div>
                    </div>

                    <div class="col-xs-4 pl0 pr0 pos_rel">
                        <div class="line"></div>
                        <div class="stepsdot"></div>
                        <div class="stepone yellow ">2</div>
                        <div class="title">Payment</div>
                    </div>
                    <div class="col-xs-4 pl0 pr0 pos_rel">
                        <div class="rightdiv"></div>
                        <div class="line"></div>
                        <div class="stepone gray">3</div>
                        <div class="title">Print Voucher</div>
                    </div>
                </div>
            </div>
				
						</div>
				
				
				
				<div class="col-sm-12 col-md-8 col-lg-8" role="main">
			<div id="passangerDiv" class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
						<h3 class="passenger_head"><i class="fa fa-users" aria-hidden="true"></i> Lead Passenger Details</h3>

						<div class="listwraap">
                        <div class="row">
						<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 mb-20 b-select-wrap">
						<h6 class="side_travller"> Traveller Details</h6>
						</div>

						 <?php 
						   
						   $name='';
						   $phone='';
						   $user_email='';
						   $user_surname='';
						   foreach($profile as $row)
						{
							 $name = $row['user_name'];
							 $phone = $row['user_mobile'];
							 
							 $user_email = $row['user_email'];
							 $user_surname = $row['user_surname'];
							
						}
							?>
						
						
						  
						<?php 
						if($name == '')
						{
							?>
							 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="text" class="form-control" name="name" id="hotel_checkout" placeholder="First name" required>  
							</div>
							
							<?php
						}else{
							?>
							 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="text" class="form-control" name="name" value="<?php echo $name; ?>" id="hotel_checkout" placeholder="First name" required>  
							</div>
							<?php
						}
						?>
                        
                           
						   	<?php 
						if($user_surname == '')
						{
							?>
							
							  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="text" class="form-control" name="lastname" placeholder="Last name" required>  
                            </div>
							
							
							<?php
						}else{
							?>
							
							  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="text" class="form-control" name="lastname" value="<?php echo $user_surname; ?>" placeholder="Last name" required>  
                            </div>
							
							<?php
							}
							?>
						   
						   
							
                           <div class="clearfix"></div>
						   
						   
						   
                            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 mb-20 b-select-wrap">
							<h6 class="side_travller">Contact Details</h6>
							</div>

							   	<?php 
						if($phone == '')
						{
							?>
							
							 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="number" class="form-control" name="phone" id="hotel_checkout" placeholder="Mobile Number" required>  
                            </div>
							
							
							<?php
						}else{
							?>
							
							 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="number" class="form-control" name="phone" id="hotel_checkout" placeholder="Mobile Number" value="<?php echo $phone; ?>"  required>  
                            </div>
							
							<?php
						}
						?>
							
							
							
								   	<?php 
						if($user_email == '')
						{
							?>
							
							 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="email" class="form-control" name="email" placeholder="Email_Id" required>  
							</div>
							
							
							<?php
						}else{
							?>
							
							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-20">
							<input type="email" class="form-control" name="email" value="<?php echo $user_email; ?>" placeholder="Email_Id" required>  
							</div>
							
							<?php
						}
						?>
							
							
                            
							
							
							
							
							
							<div class="col-xs-12">
							<div class="col-md-2 col-xs-offset-3 col-md-offset-0"></div>
							
							<div class="col-xs-offset-3 col-md-offset-0"> 
							<p class="gray-light fs-xs block ">Your booking details will be sent to this email address and mobile.</p>
							</div> 
							
							</div>
                        </div>
                    </div>
				</div>
				
		<div id="passangerDiv" class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
      <h3 class="passenger_head"><i class="fa fa-university" aria-hidden="true"></i> HOTEL PICKUP DETAILS  </h3>
      
  
      
      

      <div class="listwraap">
               
                        <div class="row">
      
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 mb-20 b-select-wrap">
      <h6 class="side_travller"> Hotel Name</h6>
      </div>

                           
                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12  ">
       
       <?php
       
       if($transfercatname == 3)
       {
        ?>
      
        <input type="text" class="form-control" name="pickup_point" id="hotel_checkout" placeholder="Not Requried" disabled>
        
          <?php 
        
       }
       else
       {?>
        <input type="text" class="form-control" id="hotel_checkout"  name="pickup_point" placeholder="Name">  
        
        
        <?php 
          }
       ?>
       
       </div>
                           
                           <div class="clearfix"></div>
                            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 mb-20 b-select-wrap">
       <h6 class="side_travller">Hotel Address</h6>
       </div>
       <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 mt-0 mb-20">
       
       <?php
       
       if($transfercatname == 3)
       {
        ?>
        
       <textarea class="form-control"  name="pickup_address" id="exampleFormControlTextarea1"  placeholder="Not Requried"  rows="3" disabled></textarea> 
        
          <?php 
        
       }
       else
       {?>
        <textarea class="form-control"  name="pickup_address" id="exampleFormControlTextarea1" rows="3"></textarea> 
        
        
        <?php 
          }
       ?>
       
       </div>
    
                        </div>
                    </div>
    </div>
						
				</div>
				
				<input type="hidden"  name="TourId" value="<?php echo $TourId;?>">  
			
				<input type="hidden"  name="TourOptId" value="<?php echo $TourOptId;?>">  
				
				<input type="hidden"  name="AdultPrice" value="<?php echo $AdultPrice;?>"> 
				<input type="hidden"  name="DisAdultPrice" value="<?php echo $DisAdultPrice;?>"> 
				
				<input type="hidden"  name="ChildPrice" value="<?php echo $ChildPrice;?>"> 
				<input type="hidden"  name="DisChildPrice" value="<?php echo $DisChildPrice;?>"> 
				
				<input type="hidden"  name="Discount" value="<?php echo $Discount;?>"> 
				
				
				<input type="hidden"  name="TourName" value="<?php echo $TourName;?>">  
				
				<input type="hidden"  name="TourOptName" value="<?php echo $TourOptName;?>">  
				<input type="hidden"  name="TransferName" value="<?php echo $TransferName;?>"> 
				
				
				<input type="hidden"  name="TourDate" value="<?php echo $TourDate;?>">  
				<input type="hidden"  name="TourTime" value="<?php echo $TourTime;?>">  
				
				<input type="hidden"  name="AdultQuantity" value="<?php echo $AdultQuantity;?>">  
				<input type="hidden"  name="ChildQuantity" value="<?php echo $ChildQuantity;?>">  
				<input type="hidden"  name="InfantQuantity" value="<?php echo $InfantQuantity;?>"> 

				
				<input type="hidden"  name="FullTotal" value="<?php echo $FullTotal;?>">  
				<input type="hidden"  name="DisFullTotal" value="<?php echo $DisFullTotal;?>">  
				
				<input type="hidden"  name="TransferId" value="<?php echo $TransferId;?>">  
				<input type="hidden"  name="TransferType" value="<?php echo $TransferType;?>">  
				
				
				
				
			
					
					
					<!-- Tour Details-->
					                      <?php
												$DisFullTotal;												
												$tax_total=($DisFullTotal * 5)/100;
												$final_total= $DisFullTotal + $tax_total;
												?>
				
				
				
		<input type="hidden" id="gst_cal_amount" name="GstAmount" value="<?php echo $tax_total;?>">  
		<input type="hidden" id="final_total_with_coupon" name="GstFullTotal" value="<?php echo $final_total;?>">  
		
		<input type="hidden"  name="coupon_applied" id="coupon_applied">  
		<input type="hidden"  name="coupon_code" id="coupon_codes"> 
		<input type="hidden"  name="coupon_discount"  id="coupon_discounts"> 
		<input type="hidden"  name="discount_total"  id="discount_totals"> 
				
	   		
				
				
				
				<div class="col-sm-12 col-md-4 col-lg-4 col-xs-12" >
							<div class="sidebar_div">
								
								<h4 class="tourname_sidebar"><?php echo $TourName;?></h4>
							
								<div class="subroomtitle1">
                                            <div class="wraplist">
                                                <div class="lefttitle">Option Name :  </div>
                                                <div class="rightval ellipsis"><span ><?php echo $TourOptName;?> </span></div>
                                            </div>
                                            <div class="wraplist">
                                                <div class="lefttitle">Transfer Option : </div>
                                                <div class="rightval"><span ><?php echo $TransferName;?></span></div>
                                            </div>
                                            <div class="wraplist">
                                                <div class="lefttitle">Date</div>
                                                <div class="rightval"><span ><?php echo $TourDate;?> </span></div>
                                            </div>
                                            <div class="wraplist">
                                                <div class="lefttitle">Time :  </div>
                                                <div class="rightval"><span ><?php echo $TourTime;?></span></div>
                                            </div>
                                            <div class="wraplist">
                                                <div class="lefttitle">Passenger :  </div>
                                                <div class="rightval">
												
												<?php 
												if($AdultQuantity == 0)
												{
													
												}else
												{
													?>
													<span >Adult <?php echo $AdultQuantity;?></span> 
													<?php													
												}
												?>
												
													<?php 
												if($ChildQuantity == 0)
												{
													
												}else
												{
													?>
													,Child <span ><?php echo $ChildQuantity;?></span>
													<?php													
												}
												?>
												
													<?php 
												if($InfantQuantity == 0)
												{
													
												}else
												{
													?>
													, Infant <span ><?php echo $InfantQuantity;?></span>
													<?php													
												}
												?>
												
												
												
												</div>
                                            </div>
                                            <div class="wraplist red">
                                                <div class="lefttitle">Last Date to Cancel :  </div>
                                                <div class="rightval">
                                                    <span style="color:red;"> Non-Refundable </span>
                                                </div>
                                            </div>
                                            <div class="wraplist" ></div>
											<div class="sep-v-hor"></div>
											
											
											
											
											
											
											
											
                                            <div class="wraplist Finalprice_total">
                                                <div class="lefttitle">TOTAL :  </div>
                                                <div class="rightval">
													
                                                    <span class="currencysymbol wraplist red"><i class="fa fa-inr"></i></span>
													<?php 
													
													if($FullTotal == $DisFullTotal)
													{
														
													}else
													{?>
														<strike id="fulltotal_strike"> 
													<?php	echo $FullTotal;  ?>
														</strike> 
													<?php	
													}
													
													?>
													
													<strike id="finaltotal_aftercoupon" style="display:none;"> 
													<?php											
														echo $DisFullTotal;													
													?>
													</strike> 												
													
													&nbsp;
													
													<span class="wraplist red" style="color:red;font-size:20px;" id="final_total_with_coupons">
													
													<span class="wraplist red" style="color:red;font-size:20px;" id="echo_final_total"><?php echo $DisFullTotal;?>
													
													</span>
													</span>
                                                    
                                                </div>
                                            </div>
											
											
											
											<div class="wraplist" id="coupon_amount_show_div" style="display:none;">
                                                <div class="lefttitle">Discount </div>							
												
                             <div class="rightval" id="dis_rs"style="display:none;"><span ><i class="fa fa-inr" id="coupon_amount"></i> </span></div>
                             <div class="rightval" id="dis_per" style="display:none;"><span ><i class="fa fa-inr" id="coupon_amount_per"></i> %</span></div>
                                            </div>
											
											
												<div class="">
								
							
							
								<div class="subroomtitle1">
                                            											
											
                                            <div class="wraplist">
                                                <div class="lefttitle">GST (5% Extra)</div>
												
												
                             <div class="rightval">
							 <span style="display:none;" id="gst_calRslogo">
							 <i class="fa fa-inr" id="gst_cal"></i> 
							 </span>
							 
							 
							 <span id="echo_gst_cal"><i class="fa fa-inr" ></i> <?php echo $tax_total; ?>
							 </span>
							 </div>
                                            </div>
											
                                        
                                            <div class="wraplist Finalprice">
                                                <div class="lefttitle">Final payment :  </div>
                                                <div class="rightval">
													
                                                    <span class="currencysymbol wraplist red" id="final_total_with_gst_couponStrike" style="display:none;"><i class="fa fa-inr"></i></span>
													<span class="wraplist red" id="final_total_with_gst_coupon">
													
													</span>
													
													
													<span class="currencysymbol wraplist red" id="echo_final_with_gstStrike"><i class="fa fa-inr"></i></span>
													<span class="wraplist red" id="echo_final_with_gst">
													<?php echo $final_total;?>
													</span>
                                                    
                                                </div>
                                            </div>
                                        </div>
								
							</div>
											
                                        </div>
										
										
										 
								
							</div>
							
					<br>
					<div class="center-block text-center sticky-sm-bottom hide-under-overlay continoue" >
					<button class="button primary rounded"   type="submit">Continue Payment</button> 
					</div>
						</div>		   
		  
				
					</div>					
					</form>
					
					
					
			 <div class="col-lg-4 col-md-4 col-sm-4 promo_div ">
					 <!--<div class=" sidebar_div price_titlecode br_wrapper mb15 ">
					 
				
                        <div class="usecode" onclick="show_promo()">Use Promo Code </div>
						<b><div id="msg" style="color:#993333"></div>
						
						<div id="cname" style="color:#993333"></div>
						
						</b>
						<form id="coupon-form">
                        <div class="coopenbg" style="display:none;">

						    <input type="hidden"  value="<?php echo $DisFullTotal;?>" name="final_totals" id="final_total">
							
						    <input type="hidden"  value="<?php echo $TourDate;?>" name="TourDates" id="TourDate">
							 
						    <input type="text" name="tour_discount" id="tour_discount" value="<?php echo $Discount;?>"> 	
						
                            <input placeholder="Enter Promo Code" id="coupon_name"  name="coupon_names" class="input-text col-md-12 col-sm-6 col-xs-8 Promocodetxt mr10" type="text">
							
							
							
                           <!-- <div class="promocodemsg" id="promonotvalid"><span class="ml20 mt10 entercode_shake red-color animated pull-left  shake" data-animation-type="shake" data-animation-duration="1" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">Enter Valid Promo Code</span></div>--
						   
						   
                            <div class="promocodemsg hide" id="promovalid"><span class="ml20 mt10 green-color animated pull-left  shake" data-animation-type="shake" data-animation-duration="1" data-animation-delay="0" style="animation-duration: 1s; visibility: visible;">Promo Code Apply Successfully</span></div>
                       </div>

                          
							</form>
							
							<button  style="display:none;" class="soap-popupbox Promocodebtn col-md-6 col-sm-6 col-xs-4 " id="promocodeButton" onclick="apply_coupon();">Redeem</button>
							
							 
                    </div>-->
					
					
					<div class="promo-code-wrapper">
       <div class="promo-code-header">
       <span class="undefined notranslate" onclick="show_promo()">Have a coupon code?</span>
       </div>
       <div class="promo-code-hidden-wrapper " id="coopenbg" style="display:none;">
       <div class="promo-code-form-row">
       <div class="promo-code-field-wrapper field-wrapper  mt-20">
	   <b><div id="msg" style="color:#993333"></div>
		<div id="cname" style="color:#993333"></div>
		</b>		
	   <form id="coupon-form">
				<input type="hidden"  value="<?php echo $DisFullTotal;?>" name="final_totals" id="final_total">
					<input type="hidden"  value="<?php echo $TourDate;?>" name="TourDates" id="TourDate">
					 <input type="hidden" name="tour_discount" id="tour_discount" value="<?php echo $Discount;?>"> 
					    <input type="text"  placeholder="Enter promo code" id="coupon_name"  name="coupon_names" maxlength="15" value="">
        </form>
	</div>
	
       <div class="promo-code-submit-button hero-button mt-20">
      <button type="button"  class="btn btn-primary" onclick="apply_coupon()" style="display:none;" id="promocodeButton">Apply</button>
       </div>
	   
       </div>
       </div>
       </div>
					
				
                    </div>
					
					
					
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/moment.min.js')?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrapDatepickr-1.0.0.min.js')?>"></script>




 <script>
 function apply_coupon()
	 {
		
		  var url;   
		  var  name1=document.getElementById("coupon_name").value;
		  var  total1=document.getElementById("final_total").value;
		  var  TourDate=document.getElementById("TourDate").value;
		  
		if(name1=='') 
			{
			   $('#msg').show();
				document.getElementById("msg").innerHTML = "Coupon Doesn't Exist!";
		}
		else
		{
			
			
			 url = "<?php echo site_url('checkout/apply_coupon')?>";
    $.ajax({
        url : url,
        type: "POST",
		dataType:"JSON",
		data: $('#coupon-form').serialize(),
		
       success:function(data){
		  var arr=data;
		  console.log(data);
		  
		    if(data!="false")
			{
				if(data=="0")
			{
				    document.getElementById('coupon-form').reset();
					$('#coupon-form')[0].reset();
					$('#cname').hide();
					$('#msg').show();
				    document.getElementById("msg").innerHTML = "Coupon Doesn't Exist!";
			}
			else if(data=="falseDis")
			{
				  document.getElementById('coupon-form').reset();
					$('#coupon-form')[0].reset();
					$('#cname').hide();
					$('#msg').show();
				    document.getElementById("msg").innerHTML = "This Tour discount is higher than the coupon code discount, the higher discount value has been provided to you. !";
				
			}
			else
			{
				 $("#fulltotal_strike").hide();
				 $("#echo_final_total").hide();
				 $("#finaltotal_aftercoupon").show();
				 
				 
				 $("#final_total_with_coupons").html(arr);
				 $("#discount_totals").val(arr);
				 
				 
				 //GST 
				  var final_total_with_coupons = arr;
				  												
				  var tax_total=(final_total_with_coupons * 5)/100;
				  var final_total_with_gst_coupon= final_total_with_coupons + tax_total;
				  

				   $("#gst_calRslogo").show();
				   $("#gst_cal").html(tax_total);
				   $("#gst_cal_amount").val(tax_total);
				   
				   $("#final_total_with_gst_couponStrike").show();
				   
				   var final_total_with_gst_coupon11 = Math.round(final_total_with_gst_coupon);	
				   
				   $("#final_total_with_gst_coupon").html(final_total_with_gst_coupon11);
				   
				   $("#final_total_with_coupon").val(final_total_with_gst_coupon11);
				   
				   
				   $("#echo_gst_cal").hide();
				
				  $("#echo_final_with_gstStrike").hide();
				  $("#echo_final_with_gst").hide();
				
				
				
				  
		  var  name=document.getElementById("coupon_name").value;
		  	  
		  var url;   
			
        url = "<?php echo site_url('checkout/get_coupon_info')?>/"  + name;
    
    $.ajax({
        url : url,
        type: "POST",
       
		dataType:"JSON",
		
       success:function(data)
	   {	   
	       var coupon_id=data.id;
		   var name=data.couponcode;
           var amount = data.discountvalue;
           var coupontype = data.coupontype;
		   
		     var coupon_applied = 1;
		     $("#coupon_applied").val(coupon_applied);
		     $("#coupon_codes").val(name);
		     $("#coupon_discounts").val(amount); 
		   
		   
		     $("#coupon_amount_show_div").show();
			 
			 if(coupontype == "A")
			 {
				 $('#dis_rs').show();
				  $('#dis_per').hide();
		         $("#coupon_amount").html(amount);
			 }
			 else
			 {
				   $('#dis_per').show();
				   $('#dis_rs').hide();
				 $("#coupon_amount_per").html(amount);
			 }
		   
		  // document.getElementById("cname").innerHTML = 'Your coupon is : ' + name + ' and amount : ' + amount +' ' + coupontype + '<br>successfully applied....';
		   
	       $('#msg').hide();
           $('#cname').show();
	       document.getElementById('coupon-form').reset();
	       $('#coupon-form')[0].reset();
		   
		  },
		  
                error: function (jqXHR, textStatus, errorThrown)
                {
          
           
                }
        }); 
				
            // document.getElementById("msg").innerHTML = "Coupon is successfully applied.............!";	 
			 
			}
             


			 
			}			
			else
			{
		       document.getElementById('coupon-form').reset();
				$('#coupon-form')[0].reset();	
				$('#cname').hide();
				$('#msg').show();
			 document.getElementById("msg").innerHTML = "Coupon Exist but Expired!";     	  
			}
			
			
			
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    }); 
		
			
		}       
		
	 }
	 </script>
	 
	 
<script>

function booking()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
        url = "<?php echo base_url('checkout/payment')?>";
    
    $.ajax({
        url : url,
        type: "POST",
        data: $('#checkoutform').serialize(),
        dataType: "JSON",
		
        success: function(data)
        {

           

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data........');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

</script>


<script>
function show_promo()
{
	
	$('#coopenbg').toggle();
	$('#promocodeButton').toggle();
}

</script>

<script type="text/javascript">
function clearField() {
   if(document.getElementById) {
	  
      document.checkoutform.reset();
   }
}
</script>

<script>
 $(document).ready(function(){
  
 $('#datetimepicker').datetimepicker({
    inline: true,
    sideBySide: true,
    keepOpen: true,
 autoclose: false,
    format: 'YYYY-MM-DD',
 
});
});
  
 
 </script>
</body>


</html>