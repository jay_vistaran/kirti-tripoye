<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>

    <style type="text/css">

* {
  margin: 0;
  padding: 0;
  font-size: 100%;
	font-family: -webkit-pictograph;
  line-height: 1.65; }

img {
  max-width: 100%;
  margin: 0 auto;
  display: block; }

body,
.body-wrap {
  width: 100% !important;
  height: 100%;
  background: #efefef;
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none; }

a {
  color: #ee1c25;
  text-decoration: none; }

.text-center {
  text-align: center; }

.text-right {
  text-align: right; }

.text-left {
  text-align: left; }

.button {
  display: inline-block;
  color: white;
  background: #ee1c25;
  border: solid #ee1c25;
  border-width: 10px 20px 8px;
  font-weight: bold;
  border-radius: 4px; }

h1, h2, h3, h4, h5, h6 {
  margin-bottom: 20px;
  line-height: 1.25; }

h1 {
  font-size: 32px; }

h2 {
  font-size: 28px; }

h3 {
  font-size: 24px; }

h4 {
  font-size: 20px; }

h5 {
  font-size: 16px; }

p, ul, ol {
  font-size: 16px;
  font-weight: normal;
  margin-bottom: 10px; }

.container {
  display: block !important;
  clear: both !important;
  margin: 0 auto !important;
  max-width: 780px !important; 
  border:1px groove #ccc;
  }
  .container table {
    width: 100% !important;
    border-collapse: collapse; }
  .container .masthead {
    padding: 80px 0;
    background: #ee1c25;
    color: white; }
    .container .masthead h1 {
      margin: 0 auto !important;
      max-width: 90%;
      text-transform: uppercase; }
  .container .content {
    background: white;
    padding: 30px 35px; }
    .container .content.footer {
      background: none; }
      .container .content.footer p {
        margin-bottom: 0;
        color: #888;
        text-align: center;
        font-size: 14px; }
      .container .content.footer a {
        color: #888;
        text-decoration: none;
        font-weight: bold; }
		.enquiry {
		text-align:center;
				border:2px solid grey;

		}
		.package{
		border:2px solid grey;
		}
		.package1
		{
		    border: 2px solid grey;
			text-align:center;
		}
		.package2
		{
		    border: 2px solid grey;
			text-align:center;
		}
		.enquiry_table{
		border:2px solid grey;
		}
		.enquiry1
		{
		    border: 2px solid grey;
		}
    </style>
</head>
<body>
<table class="body-wrap">
    <tr>
        <td class="container">
            <table>
                    <tr>
                        <td class="content">
                          <table >
                            <tbody>
                              <tr>
                                <td>
                                    <th > 
									  <img src="http://www.tripoye.com/brothehood1/assets/images/TripoyeLogo2-3v.png" style="float:left">
									  
									 <!--<img src="http://www.tripoye.com/brothehood1/assets/images/android-app-on-google-play.jpg" style="float:right"> -->
									 
									  </th>
                                  
                                </td>
                                
                                
                              </tr>
							
                            </tbody>
                          </table>
                        </td>
                      </tr>
                <tr>
                    <td class="content">

                        <a><h4>Note: This is not your Ticket/Voucher</h4></a>
						<h6>Dear <?php echo $firstname; ?> ,</h6>
						<p>Greetings from Tripoye.com</p>
                        <p>Thank you for placing an order with <a href="">Tripoye.com</a> Your Order Id is <?php echo $booking_ref_num; ?>  is in process as soon as we confirm from our end,you will get an email confirmation along with attached booking Voucher/Ticket within 1 Business Day of purchase. </p>
						<p>Following are your tour details:-</p>
                        <table>
                            <tr>
                                <td align="center">
                                    <!---<p>
                                        <a href="#" class="button">Share the Awesomeness</a>
                                    </p>-->
									<div style="overflow-x:auto;">
  <table class="package">
    <tr>
      <td class="package1">Tour Name</td>
            <td class="package1"><?php echo $TourName; ?></td>

    </tr>
	 <tr>
      <td class="package1">Travel Date</td>
            <td class="package1"><?php echo $TourDate; ?></td>

    </tr>
	 <tr>
      <td class="package1">Duration</td>
            <td class="package1"><?php echo $duration; ?> </td>

    </tr>
	 <tr>
      <td class="package1">No of Adults</td>
            <td class="package1"><?php echo $AdultQuantity; ?> </td>

    </tr>
	<tr>
      <td class="package1">No Of Childs</td>
            <td class="package1"><?php echo $ChildQuantity; ?> </td>

    </tr>
	<tr>
      <td class="package1">No Of Infants</td>
            <td class="package1"><?php echo $InfantQuantity; ?> </td>

    <tr>
      <td class="package1">Total Paymemt Paid(Inclusive GST)</td>
            <td class="package1"><?php echo $GstFullTotal; ?> </td>

    </tr>
	 <tr>
      <td class="package1">You Saved</td>
            <td class="package1">Rs.<?php echo $savedTotal; ?> </td>

    </tr>
      
  </table>
</div>
                                </td>
                            </tr>
							
                        </table>
						<br>
												<p>However, we request you to kindly note that the response may take a little longer, especially during busy months. Should this occur,we appreciate your patience, and we assure you that you will receive a response.</p>
						<p>If you have an urgent concern regarding your enquiry, please refer  your Order ID and contact us directly for faster correspondence.</p>

						<p>Customer Support Email:- <a href="">info@tripoye.com</a> and Contact No. : <a href="">+918308625635</a></p>
					     <p>Thanks & Regards,<br>Tripoye Tourism Pvt Ltd<br>KS Residency<br>Above Keyour Electronics,<br>Office No:2 Karvenagar,Pune-58,India.</p>
                       

                    </td>
                </tr>
				
            </table>

        </td>
    </tr>
	
   
</table>
</body>
</html>