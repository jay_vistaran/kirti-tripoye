
	<footer class="footer scrollspy-footer"> <!-- add scrollspy-footer to stop sidebar scrollspy -->
			
			<div class="container">
			
				<div class="main-footer">
				
					<div class="row">
				
						<div class="col-xs-12 col-sm-4 col-md-4">
						
							<div class="footer-logo">
								<img src="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>" alt="Logo" />
							</div>
							
							<p class="footer-address">Above Keyour Electronics,Office No:2 Karvenagar,Pune-58,India. 
							<br/><br/> <i class="fa fa-phone"></i> +91-9689044624 
							<br/><br/> <i class="fa fa-envelope-o"></i> <a href="mailto:info@tripoye.com">info@tripoye.com</a></p>
							
							<div class="footer-social">
							
								<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
							
							</div>
							
							<p class="copy-right">&#169; Copyright 2018 Tripoye. All Rights Reserved</p>
							
						</div>
						
						<div class="col-xs-12 col-sm-8 col-md-8">

							<div class="row gap-10">
							
								<div class="col-xs-12 col-sm-4 col-md-4  mt-30-xs">
								
									<h5 class="footer-title">About Tripoye</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('about')?>">Who we are</a></li>
										<li><a href="<?php echo site_url('career')?>">Careers</a></li>
										<li><a href="<?php echo site_url('contact')?>">Contact Us</a></li>
										<li><a href="<?php echo site_url('b2b')?>">Tripoye B2B</a></li>
							
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Customer Information</h5>
									
									<ul class="footer-menu">
										<li><a href="<?php echo site_url('faq')?>">FAQ</a></li>
										<li><a href="<?php echo site_url('termscondition')?>">Terms & Conditions</a></li>
										<li><a href="<?php echo site_url('privacypolicy')?>">Privacy Policy</a></li>
										<li><a href="#">Blog</a></li>
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4 mt-30-xs">

									<h5 class="footer-title">Destinations</h5>
									
									<ul class="footer-menu">
									
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('lonavala')?>">Lonavala Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('goa')?>">Goa Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('pune')?>">Pune Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('mumbai')?>">Mumbai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('dubai')?>">Dubai Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('singapore')?>">Singapore Tours</a></li>
										<li><a href="#">Kuala Lumpur Tours</a></li>
										<li><a href="<?php echo site_url('tours')?>/<?php echo ('hong-kong')?>">Homg Kong Tours</a></li>
										
									</ul>
									
								</div>
								
							</div>

						</div>
						
					</div>

				</div>
				
			</div>
			
		</footer>