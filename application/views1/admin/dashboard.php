<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin  | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\bootstrap\css\bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\dist\css\AdminLTE.min.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\dist\css\skins\_all-skins.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\plugins\iCheck\flat\blue.css')?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\plugins\morris\morris.css')?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\plugins\jvectormap\jquery-jvectormap-1.2.2.css')?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\plugins\datepicker\datepicker3.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\plugins\daterangepicker\daterangepicker.css')?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('assets\admin\plugins\bootstrap-wysihtml5\bootstrap3-wysihtml5.min.css')?>">
	<!-- Datatables -->	 
	<link href="<?php echo base_url('assets\admin\plugins\datatableme\css\dataTables.bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets\admin\plugins\datatableme\css\buttons.bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets\admin\plugins\datatableme\css\fixedHeader.bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets\admin\plugins\datatableme\css\responsive.bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets\admin\plugins\datatableme\css\scroller.bootstrap.min.css')?>" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include 'header.php'?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><div id="total_enq">  </div></h3>

              <p>Enquries</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="enquiry" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="total_subscriber"></h3>

              <p>Subscriber</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="subsciber" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
     

        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
	  
	   <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
           
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Enquiry Table </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                			            <div class="box-body table-responsive no-padding">

                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%;">
                      <thead>
                        <tr>
						   <th>Enquiry ID</th>
                           <th>Name</th>
						   <th>Mobile No.</th>
						    <th>Email_ID</th>
						
						  <th>Subject</th>
                          <th>Message</th>
						  <th>File</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                        
                        
                      </tbody>
                    </table>
            </div>
                        </div>

            <!-- /.box-body -->
          </div>
		  <div class="box-header">
              <h3 class="box-title">Subscriber Table </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                                			            <div class="box-body table-responsive no-padding">

                    <table id="datatable-buttons1" class="table table-striped table-bordered" style="width:100%;">
                      <thead>
                        <tr>
						   <th>Subscriber ID</th>
                           
						  
                          <th>Email_ID</th>
                          
                         
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                        
                        
                      </tbody>
                    </table>
            </div>
          <!-- /.box -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
      
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include 'footer.php'?>



  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>

  <!-- Datatables -->
    <script>
	
var save_method; //for save method string
var table2;

      $(document).ready(function() {
		   table2 = $('#datatable-buttons').DataTable({ 
		   
  dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('admin/Home/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
		
	});
       
      });
	        
 </script>
 <script>
 



 function delete_enquries(enquiry_id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('admin/home/delete_enquries')?>/" +enquiry_id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
             location.reload();
               
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}
 </script>
 
 <script>
	
var save_method; //for save method string
var table1;

      $(document).ready(function() {
		   table1 = $('#datatable-buttons1').DataTable({ 
		   
  dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('admin/home/ajax_list_subscriber')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
		
	});
       
      });
	        
 </script>
 <script>
 
 function reload_table()
{
    table1.ajax.reload(null,false); //reload datatable ajax 
	 table2.ajax.reload(null,false); //reload datatable ajax 
}


 function delete_subscriber(subscriber_id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('admin/home/delete_subscriber')?>/" +subscriber_id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}
 </script>
 
 <script>
$(document).ready(function()
{
 
  $.ajax({
            url : "<?php echo site_url('admin/home/total_enquries')?>",
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
           
      document.getElementById("total_enq").innerHTML = data;
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data,,,,,,,,');
            }
        });
 
 
 $.ajax({
            url : "<?php echo site_url('admin/home/total_subscriber')?>",
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
            
   
      document.getElementById("total_subscriber").innerHTML = data;
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data,,,,,,,,');
            }
        });
 
})

</script>
 
    <!-- /Datatables -->
<!-- ./wrapper -->
</body>
</html>
