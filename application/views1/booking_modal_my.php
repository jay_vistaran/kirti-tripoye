<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	<div class="modal fade modal-login modal-border-transparent" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	
	<button type="button" class="btn btn-close close" data-dismiss="modal" aria-label="Close">
	<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	</button>
	
	<div class="clear"></div>
	
	<!-- Begin # DIV Form -->
	<div id="modal-login-form-wrapper">
	
	<!-- Begin # Login Form -->
	<form id="login-form">
	
	<div class="modal-body pb-5">
	
	<h4 class="text-center heading mt-10 mb-20">Sign-in</h4>
	
	<button class="btn btn-facebook btn-block">Sign-in with Facebook</button>
	
	<div class="modal-seperator">
	<span>or</span>
	</div>
	
	<div class="form-group"> 
	<input id="login_username" class="form-control" placeholder="username" type="text"> 
	</div>
	<div class="form-group"> 
	<input id="login_password" class="form-control" placeholder="password" type="password"> 
	</div>
	
	<div class="form-group">
	<div class="row gap-5">
	<div class="col-xs-6 col-sm-6 col-md-6">
	<div class="checkbox-block fa-checkbox"> 
	<input id="remember_me_checkbox" name="remember_me_checkbox" class="checkbox" value="First Choice" type="checkbox"> 
	<label class="" for="remember_me_checkbox">remember</label>
	</div>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-6 text-right"> 
	<button id="login_lost_btn" type="button" class="btn btn-link">forgot pass?</button>
	</div>
	</div>
	</div>
	
	</div>
	
	<div class="modal-footer">
	
	<div class="row gap-10">
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-block">Sign-in</button>
	</div>
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-block btn-inverse" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	</div>
	<div class="text-left">
	No account? 
	<button id="login_register_btn" type="button" class="btn btn-link">Register</button>
	</div>
	
	</div>
	</form>
	<!-- End # Login Form -->
	
	<!-- Begin | Lost Password Form -->
	<form id="lost-form" style="display:none;">
	<div class="modal-body pb-5">
	
	<h3 class="text-center heading mt-10 mb-20">Forgot password</h3>
	<div class="form-group mb-10"> 
	<input id="lost_email" class="form-control" type="text" placeholder="Enter Your Email">
	</div>
	
	<div class="text-center">
	<button id="lost_login_btn" type="button" class="btn btn-link">Sign-in</button> or 
	<button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
	</div>
	
	</div>
	
	<div class="modal-footer mt-10">
	
	<div class="row gap-10">
	<div class="col-xs-6 col-sm-6">
	<button type="submit" class="btn btn-primary btn-block">Submit</button>
	</div>
	<div class="col-xs-6 col-sm-6">
	<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	</div>
	
	</div>
	
	</form>
	<!-- End | Lost Password Form -->
	
	<!-- Begin | Register Form -->
	<form id="register-form" style="display:none;">
	
	<div class="modal-body pb-5">
	
	<h3 class="text-center heading mt-10 mb-20">Register</h3>
	
	<button class="btn btn-facebook btn-block">Register with Facebook</button>
	
	<div class="modal-seperator">
	<span>or</span>
	</div>
	
	<div class="form-group"> 
	<input id="register_username" class="form-control" type="text" placeholder="Username"> 
	</div>
	
	<div class="form-group"> 
	<input id="register_email" class="form-control" type="email" placeholder="Email">
	</div>
	
	<div class="form-group"> 
	<input id="register_password" class="form-control" type="password" placeholder="Password">
	</div>
	
	<div class="form-group"> 
	<input id="register_password_confirm" class="form-control" type="password" placeholder="Confirm Password">
	</div>

	</div>
	
	<div class="modal-footer mt-10">
	
	<div class="row gap-10">
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-block">Register</button>
	</div>
	<div class="col-xs-6 col-sm-6 mb-10">
	<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	</div>
	
	<div class="text-left">
	Already have account? <button id="register_login_btn" type="button" class="btn btn-link">Sign-in</button>
	</div>
	
	</div>
	
	</form>
	<!-- End | Register Form -->
	
	</div>
	<!-- End # DIV Form -->
	
	</div>
	</div>
	</div>
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

		<!-- start Header -->
	<header id="header">
	  
	<!-- start Navbar (Header) -->
	<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
	<div onclick="window.location.href='cart.html'" class="cart_div hidden-xs">
	<div class="cart_icon">
	<i class="fa fa-cart-arrow-down"></i>
	</div>
	<div class="cart_value">
	2
	</div>
	</div>
	
	<div class="navbar-top" >
	
	<div class="container">
	
	<div class="flex-row flex-align-middle">
	<div class="flex-shrink flex-columns" id="test">
	<a class='navbar-logo' href='index.html'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>' id="logo1" class='img-responsive'>
	<img src='<?php echo base_url('assets/images/TripoyeLogo2-3v.png')?>' id="logo2" class='img-responsive' style="display:none">
	</a>	
	</div>
	<div class="flex-columns">
	<div class="pull-right">
	
	
	
	
	<div id="navbar" class="collapse navbar-collapse navbar-arrow">
	<ul class="nav navbar-nav" id="responsive-menu">
	<li class="top_nav "><a href="#">USD</a>
	<ul class="dropdown_top">
	<li><a href="#"><i class="ion-social-usd"></i> Dollar</a></li>
	<li><a href="#"><i class="ion-social-euro"></i> Europe</a></li>
	<li><a href="#"><i class="ion-social-yen"></i> Yen</a></li>
	</ul>
	</li>
	
	<li class="top_nav"><a href="#">Download App</a></li>
	<li class="top_nav"><a href="#">Help</a></li>
	<li class="top_nav"><a data-toggle="modal" href="#loginModal">Sign Up</a></li>
	<li class="top_nav"><a href="#">Login</a></li>
	</ul>
	
	</div><!--/.nav-collapse -->
	
	
	</div>
	</div>
	</div>

	</div>
	
	</div>
	
	<div id="slicknav-mobile"></div>


	
	</nav>
	<!-- end Navbar (Header) -->

	</header>
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
			
					<section class="booking_modal_section">
		<div class="tr-register">
			<div class="tr-regi-form">
				<h4>Booking by <span>Email</span></h4>
				<p>It's free and always will be.</p>
				<form >
					<div class="row">
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
							  <div class="form-group">
							<label class="modal_newlabel">Name</label>
							<input type="name" class="form-control" id="name">
						  </div>
						  </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						<label  class="modal_newlabel">Email_Id</label>
						<input type="name" class="form-control" id="name">
					  </div>
					  </div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						<label  class="modal_newlabel">Contact No</label>
						<input type="name" class="form-control" id="name">
					  </div>
					  </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
							  <div class="form-group">
							<label  class="modal_newlabel">Adult</label>
								<div class="count-input space-bottom">
                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                <input class="quantity" type="text" name="quantity" value="1"/>
                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                            </div>
						  </div>
						  </div>
					</div>
				<div class="row">
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						<label  class="modal_newlabel">Child</label>
						<div class="count-input space-bottom">
                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                <input class="quantity" type="text" name="quantity" value="1"/>
                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                            </div>
					  </div>
					  </div>
						 <div class="col-md-6 col-sm-12 col-xs-12 ">
					<div class="form-group">
						<label  class="modal_newlabel">Infant</label>
					<div class="count-input space-bottom">
                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                <input class="quantity" type="text" name="quantity" value="1"/>
                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                            </div>
					  </div>
					  </div>
				</div>
					<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 ">
						  <div class="form-group">
						 <label  class="modal_newlabel">Your Query</label>
						<textarea class="form-control materialize-textarea" rows="4" cols="50">
						
						</textarea>
						</div>
					</div>

					</div>
					
					
					<div class="row">
						<div class="input-field col-md-12 col-sm-12">
							<i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style=""><input type="submit" value="submit" class="btn  btn-block submit_btn_new"></i> </div>
					</div>
				</form>
			</div>
		</div>
	</section>
 
			

		</div>
		<!-- end Main Wrapper -->

		<footer class="footer">
			
			<div class="container">
			
				<div class="main-footer">
				
					<div class="row">
				
						<div class="col-xs-12 col-sm-5 col-md-3">
						
							<div class="footer-logo">
								<img src="<?php echo base_url('assets/images/logo-white.png')?>" alt="Logo" />
							</div>
							
							<p class="footer-address">324 Yarang Road, T.Chabangtigo, Muanng Pattani 9400 <br/> <i class="fa fa-phone"></i> +66 28 878 5452 <br/> <i class="fa fa-phone"></i> +66 2 547 2223 <br/> <i class="fa fa-envelope-o"></i> <a href="#">support@tourpacker.com</a></p>
							
							<div class="footer-social">
							
								<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
							
							</div>
							
							<p class="copy-right">&#169; Copyright 2016 Tour Packer. All Rights Reserved</p>
							
						</div>
						
						<div class="col-xs-12 col-sm-7 col-md-9">

							<div class="row gap-10">
							
								<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-3 mt-30-xs">
								
									<h5 class="footer-title">About Tour Packer</h5>
									
									<ul class="footer-menu">
									
										<li><a href="static-page.html">Who we are</a></li>
										<li><a href="static-page.html">Careers</a></li>
										<li><a href="static-page.html">Company history</a></li>
										<li><a href="static-page.html">Legal</a></li>
										<li><a href="static-page.html">Partners</a></li>
										<li><a href="static-page.html">Privacy notice</a></li>
										
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-3 mt-30-xs">

									<h5 class="footer-title">Customer service</h5>
									
									<ul class="footer-menu">
									
										<li><a href="static-page.html">Payment</a></li>
										<li><a href="static-page.html">Feedback</a></li>
										<li><a href="static-page.html">Contact us</a></li>
										<li><a href="static-page.html">Travel advisories</a></li>
										<li><a href="static-page.html">FAQ</a></li>
										<li><a href="static-page.html">Site map</a></li>
										
									</ul>
									
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-3 mt-30-xs">

									<h5 class="footer-title">Others</h5>
									
									<ul class="footer-menu">
									
										<li><a href="static-page.html">Destinations</a></li>
										<li><a href="static-page.html">Blog</a></li>
										<li><a href="static-page.html">Pre Departure Planning</a></li>
										<li><a href="static-page.html">Visas</a></li>
										<li><a href="static-page.html">Insurance</a></li>
										<li><a href="static-page.html">Travel Guide</a></li>
										
									</ul>
									
								</div>
								
							</div>

						</div>
						
					</div>

				</div>
				
			</div>
			
		</footer>

	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>

<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
<script>
    $(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below 1
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });

</script>

</body>


</html>