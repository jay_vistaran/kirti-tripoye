<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	<div class="modal fade modal-login modal-border-transparent" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<button type="button" class="btn btn-close close" data-dismiss="modal" aria-label="Close">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
				
				<div class="clear"></div>
				
				<!-- Begin # DIV Form -->
				<div id="modal-login-form-wrapper">
					
					<!-- Begin # Login Form -->
					<form id="login-form">
					
						<div class="modal-body pb-5">
					
							<h4 class="text-center heading mt-10 mb-20">Sign-in</h4>
						
							<button class="btn btn-facebook btn-block">Sign-in with Facebook</button>
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="login_username" class="form-control" placeholder="username" type="text"> 
							</div>
							<div class="form-group"> 
								<input id="login_password" class="form-control" placeholder="password" type="password"> 
							</div>
			
							<div class="form-group">
								<div class="row gap-5">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="checkbox-block fa-checkbox"> 
											<input id="remember_me_checkbox" name="remember_me_checkbox" class="checkbox" value="First Choice" type="checkbox"> 
											<label class="" for="remember_me_checkbox">remember</label>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 text-right"> 
										<button id="login_lost_btn" type="button" class="btn btn-link">forgot pass?</button>
									</div>
								</div>
							</div>
						
						</div>
						
						<div class="modal-footer">
						
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block">Sign-in</button>
								</div>
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block btn-inverse" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							<div class="text-left">
								No account? 
								<button id="login_register_btn" type="button" class="btn btn-link">Register</button>
							</div>
							
						</div>
					</form>
					<!-- End # Login Form -->
								
					<!-- Begin | Lost Password Form -->
					<form id="lost-form" style="display:none;">
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Forgot password</h3>
							<div class="form-group mb-10"> 
								<input id="lost_email" class="form-control" type="text" placeholder="Enter Your Email">
							</div>
							
							<div class="text-center">
								<button id="lost_login_btn" type="button" class="btn btn-link">Sign-in</button> or 
								<button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
							</div>
							
						</div>
						
						<div class="modal-footer mt-10">
							
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-block">Submit</button>
								</div>
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
						</div>
						
					</form>
					<!-- End | Lost Password Form -->
								
					<!-- Begin | Register Form -->
					<form id="register-form" style="display:none;">
					
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Register</h3>
							
							<button class="btn btn-facebook btn-block">Register with Facebook</button>
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="register_username" class="form-control" type="text" placeholder="Username"> 
							</div>
							
							<div class="form-group"> 
								<input id="register_email" class="form-control" type="email" placeholder="Email">
							</div>
							
							<div class="form-group"> 
								<input id="register_password" class="form-control" type="password" placeholder="Password">
							</div>
							
							<div class="form-group"> 
								<input id="register_password_confirm" class="form-control" type="password" placeholder="Confirm Password">
							</div>

						</div>
							
						<div class="modal-footer mt-10">
						
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block">Register</button>
								</div>
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
							<div class="text-left">
									Already have account? <button id="register_login_btn" type="button" class="btn btn-link">Sign-in</button>
							</div>
							
						</div>
							
					</form>
					<!-- End | Register Form -->
								
				</div>
				<!-- End # DIV Form -->
								
			</div>
		</div>
	</div>
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

						<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">About Us</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><span>About Us</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper pt-0 pb-0">
			
				<div class="section pt-60">
				
					<div class="container">
					<div class="section-title">
					<h3 class="packages_name">About Tripoye</h3>
					<span class="underline_text"></span>
					</div>
						<p class="font15 mb-30">At TripOye, we offer sterling tourism services to a discerning global audience. We have achieved international reckoning as a leading Destination Management Company (DMC). TripOye is based out of the Indian subcontinent and are backed by a dedicated team and sophisticated technology that enables us to synchronize tours and travels globally. </p>
						<p class="font15 mb-30">We offer various travel services to our customers, from hotel reservations, visa assistance, airline ticketing, leisure activities, and luxury travel and business trips. We offer multilingual support to our customers, in terms of assistance, tour commentary and other travel needs. We offer a wide variety of tours, ranging from luxury day-trips, adventure tours and excursions, sight-seeing tours and luxury cruises. </p>
						<p class="font15 mb-30">TripOye have outlets across the world, and are adept at serving the needs of customers in global star hotels. Our wide range of tours and excursions offer our customers to experience adventures that they never thought possible. Our vision is to not only exceed our customer base’s expectations, but to be reckoned as the best DMC in the world. We have dedicated our considerable resources and know-how to achieving this end. We welcome you to contact us via our website whenever you choose, and discover the scope and quality of our tourism services for yourself. </p>
					<div class="best_of">
					<div class="section-title">
					<h3 class="packages_name">We bring you the best of</h3>
					<span class="underline_text"></span>
					</div>
					<ul class="best_of_list">
					<li><strong>MICE Events:</strong> MICE stands for Meetings, Incentives, Conferences & Exhibitions. Today MICE is the fastest growing travel segment, aimed at the business traveller. We offer exclusive &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;MICE destination programs in different countries, ensuring that all conferencing attributes are present and available for the business traveller.</li>
					<li><strong>Groups:</strong> We organize specialized custom tours for groups. We design our tour packages keeping the group’s own itinerary in mind, so that the group’s overall travel plan flows &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smoothly. </li> 
					<li><strong>FIT:</strong> Foreign Independent Tour (FIT) refers to independent travellers who design their own itinerary and travel. We organize special custom tours for FIT travellers, making sure that our &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;travel program flows well within the FIT traveller’s own program. </li>
					<li><strong>End-to-end Solutions:</strong> End-to-end travel booking ensures quality and efficiency when it comes to business trips. End-to-end travel solutions eliminates the need for middle steps, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;leaving the entire tour planning in the hands of one tour company, thereby representing significant cost savings. </li>
					<li><strong>Online Bookings: </strong> Our time-saving, accurate online tour bookings saves time and ensures that our clients are in total control of their own travel plans. </li>
					</ul>
					<h5 class="one_liner">We directly collaborate with hotels and airlines to bring you the very best deals, time after time</h5>
					
					</div>
					</div>
					
					
						
			</div>
				
				
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->
				<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>
</body>


<!-- Mirrored from crenoveative.com/envato/tour-packer/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Nov 2017 17:19:09 GMT -->
</html>