<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	
	
</head>


<body class="">
	
	          
									 <?php 
						foreach ($cityInfo1 as $row) {
                          $cityid=$row->id;
			              $cityname=$row->cityname;
	                      $city_img_url=$row->city_img_url;
	
				             }?>
							 <input type="hidden"  id="CityId"  value="<?php echo $cityid;?>">

	<!-- BEGIN # MODAL LOGIN -->
	<div class="modal fade modal-login modal-border-transparent" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<button type="button" class="btn btn-close close" data-dismiss="modal" aria-label="Close">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
				
				<div class="clear"></div>
				
				<!-- Begin # DIV Form -->
				<div id="modal-login-form-wrapper">
					
					<!-- Begin # Login Form -->
					<form id="login-form">
					
						<div class="modal-body pb-5">
					
							<h4 class="text-center heading mt-10 mb-20">Sign-in</h4>
						
							<button class="btn btn-facebook btn-block">Sign-in with Facebook</button>
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="login_username" class="form-control" placeholder="username" type="text"> 
							</div>
							<div class="form-group"> 
								<input id="login_password" class="form-control" placeholder="password" type="password"> 
							</div>
			
							<div class="form-group">
								<div class="row gap-5">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="checkbox-block fa-checkbox"> 
											<input id="remember_me_checkbox" name="remember_me_checkbox" class="checkbox" value="First Choice" type="checkbox"> 
											<label class="" for="remember_me_checkbox">remember</label>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 text-right"> 
										<button id="login_lost_btn" type="button" class="btn btn-link">forgot pass?</button>
									</div>
								</div>
							</div>
						
						</div>
						
						<div class="modal-footer">
						
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block">Sign-in</button>
								</div>
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block btn-inverse" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							<div class="text-left">
								No account? 
								<button id="login_register_btn" type="button" class="btn btn-link">Register</button>
							</div>
							
						</div>
					</form>
					<!-- End # Login Form -->
								
					<!-- Begin | Lost Password Form -->
					<form id="lost-form" style="display:none;">
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Forgot password</h3>
							<div class="form-group mb-10"> 
								<input id="lost_email" class="form-control" type="text" placeholder="Enter Your Email">
							</div>
							
							<div class="text-center">
								<button id="lost_login_btn" type="button" class="btn btn-link">Sign-in</button> or 
								<button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
							</div>
							
						</div>
						
						<div class="modal-footer mt-10">
							
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-block">Submit</button>
								</div>
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
						</div>
						
					</form>
					<!-- End | Lost Password Form -->
								
					<!-- Begin | Register Form -->
					<form id="register-form" style="display:none;">
					
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Register</h3>
							
							<button class="btn btn-facebook btn-block">Register with Facebook</button>
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="register_username" class="form-control" type="text" placeholder="Username"> 
							</div>
							
							<div class="form-group"> 
								<input id="register_email" class="form-control" type="email" placeholder="Email">
							</div>
							
							<div class="form-group"> 
								<input id="register_password" class="form-control" type="password" placeholder="Password">
							</div>
							
							<div class="form-group"> 
								<input id="register_password_confirm" class="form-control" type="password" placeholder="Confirm Password">
							</div>

						</div>
							
						<div class="modal-footer mt-10">
						
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block">Register</button>
								</div>
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
							<div class="text-left">
									Already have account? <button id="register_login_btn" type="button" class="btn btn-link">Sign-in</button>
							</div>
							
						</div>
							
					</form>
					<!-- End | Register Form -->
								
				</div>
				<!-- End # DIV Form -->
								
			</div>
		</div>
	</div>
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_city_page">

			
	  
				<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->

			<div class="page-title" style="background-image:url('<?php echo $city_img_url; ?>');  ">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title"><?php echo $cityname;?> Tours & Activities</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="index.html">Homepage</a></li>
								<li><a href="#">Desinations</a></li>
								<li><a href="#">City</a></li>
								<li><span>Result list</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper" id="content_city">
			
				<div class="container">
				
					<div class="row">
						
					
										
					<section class="hotel_selling" id="city_section_first">

				<div class="container">
				
				<?php 
									
				foreach (array_slice($collection, 0, 1) as $row) {
    foreach($row as $second ){
		$name=$second->tourcatname;
	    }
		if($name == '')
		{
			
		}else{?>
			
				<div class="row">						
						<div class="col-sm-8 col-md-9 ">							
							<div class="section-title">
							
								<h3 class="packages_name">Tripoye Collections - <?php echo $cityname;?></h3>
								<span class="underline_text"></span>
															<p>Of distrusts immediate enjoyment curiosity do. Marianne numerous saw thoughts the humoured.</p>
	
							</div>
							
							
							
													
						</div>
						
										<?php 
						$result=' ';
				foreach (array_slice($collection, 4, 5) as $row) {
					
        foreach($row as $second ){
		$tourcatname= $second->tourcatname;			
		
		 $result = $tourcatname;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{    ?> <div class="col-sm-4 col-md-3 ">
							<nav>
								<ul class="control-box pager">
									<li class="control_slide"><a data-slide="prev" href="#carousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
									<li class="control_slide"><a data-slide="next" href="#carousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
								</ul>
							</nav>
						</div>
						<?php   }
	 
				?>
						
					    	

						
					     </div>
					<?php
		
	                 }
   
				}
				?>
					
					
					<div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false">
							
									<div class="carousel-inner">

						<div class="item active">
							<div class="carousel-col">
							    <div class="package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight" id="collections">
									
									
									 <?php 
									 $i = 0;
				foreach (array_slice($collection, 0, 4) as $row) {
    foreach($row as $second ){
		  $tourcat_id = $second->id;
		    $url_string = str_replace("&", "", $second->tourcatname);
		  $url_string1 = preg_replace("/[\s_]/", "-", $url_string);
		
		  $tours="tours";
		  
   ?>
  
										<div class="carousel-col collections col-md-3 col-sm-6 col-xs-12 mb-30">
									
			<a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityname)?>/<?php echo $tourcat_id?>-<?php echo strtolower($url_string1);?>">
										<div class="blurt"></div>
											
												<img src="<?php echo $second->doublewidth_url;?>" alt="Tour Package" />
												
												<div class="centered">
												
                                                 <h3><?php echo $second->tourcatname;?></h3>

                                                  </div>
											
											
											
										</a>
											
										</div>
										<?php 
										if($i==2) break;
										 }
            }			
               
                ?>
										
										
							
									
								
										
										
									</div>
								</div>
							</div>
						</div>
												<div class="item">
												<div class="carousel-col">
								<div class="GridLex-gap-30-wrappper package-grid-item-wrapper">
								<div class="GridLex-grid-noGutter-equalHeight">
			
			
			
												 <?php 
									 $i = 0;
				foreach (array_slice($collection, 4, 8) as $row) {
    foreach($row as $second ){
     
       $tourcat_id = $second->id;
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->tourcatname);
		 $url_string = str_replace("&", "", $url_string1);
		  $tours="tours";
   ?>
  
										<div class="carousel-col collections col-md-3 col-sm-6 col-xs-12 mb-30">
									
								<a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityname)?>/<?php echo $tourcat_id?>-<?php echo strtolower($url_string);?>">
										<div class="blurt"></div>
											
												<img src="<?php echo $second->doublewidth_url;?>" alt="Tour Package" />						
											
												<div class="centered">
												
                                                 <h3><?php echo $second->tourcatname;?></h3>

                                                </div>
											
											
											
										</a>
											
										</div>
										<?php 
										if($i==2) break;
										 }
            }			
               
                ?>
							
							
						
							
						
							
						
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
			</section>
			
			
					<section class="hotel_selling_picks"  id="city_section_second">

				<div class="container">
				
				<?php 
									
				foreach (array_slice($hotselling, 0, 1) as $row) {
    foreach($row as $second ){
		$name=$second->tourname;
	    }
		if($name == '')
		{
			
		}else{?>
		
					<div class="row">
						
						<div class="col-sm-8 col-md-9 ">
							
							<div class="section-title">
							
								<h3>Hot Selling Picks - <?php echo $cityname;?></h3>
								<span class="underline_text"></span>
								<?php 
									
				          foreach ($hotselling_title as $title) {
  
		                             }
	
	                            ?>
								
								<p><?php echo $title;;?>.</p>
				
	
							</div>
							
						</div>
						
								<?php 
						$result=' ';
				foreach (array_slice($hotselling, 4, 8) as $row) {
					
        foreach($row as $second ){
		$tourname= $second->tourname;			
		
		 $result = $tourname;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{    ?><div class="col-sm-3 col-md-3 ">
					     <nav>
								<ul class="control-box pager">
									<li class="control_slide"><a data-slide="prev" href="#hot" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
									<li  class="control_slide"><a data-slide="next" href="#hot" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
								</ul>
					     </nav>
					
					</div>
						<?php   }
	 
				?>
				
					
					</div>
					
					
					<?php
		          }
				}?>
					
					
					<div id="hot" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false">
							
									<div class="carousel-inner">

						<div class="item active">
							<div class="carousel-col">
							    <div class="package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight" id="hot_sellings">
									
									
									
										<?php 
										$i=0;
				foreach (array_slice($hotselling, 0, 4) as $row) {
    foreach($row as $second ){
		
		  $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);	
		  $tourcatname = str_replace("&", "", $url_tourcatname);
		  
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->tourname);
		  $url_string = str_replace("&", "", $url_string1);
		
		$tour_id=$second->id;
		$tours="tours";
		
		              
								   
								
		
   ?>
										
								
										
									<div class="carousel-col col-md-3 col-sm-6 col-lg-3 col-xs-12 mb-30" id="package-grid-item">
				<a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityname)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
											<div class="image">
												<img src="<?php echo $second->gallery_img_url;?>/<?php echo $second->gallery_img;?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->discount;?>% off</a>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->tdiscount;?>% off</a>
													<?php
													}
													
										}		
													?>
												
													
													<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
													<!--<i class="fa fa-heart-o"></i>-->
													</span>

													</div>
												</div>
											</div>
											 <div class="content123 clearfix">
											<h5 class="city_name_head"><?php echo $second->tourname;?></h5>
											<div class="prodict">
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>	<?php 
											
										foreach(array_slice($hotselling_tour_rating , $i, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{?>
										      Newly Arrived
											<?php
											}
											else
											{
												echo  $rating;
											}
											
											
											?> </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<?php 
											
										foreach(array_slice($hotselling_tour_rating , $i, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{
											}
											else
											{
												?>
										     	 <p class="rating_star">(<?php echo  $rating ;?> ratings)</p>
											<?php
											}
											
											
											?>
											

											</div>
											</div>
											</div>
												<div class="product-detail-right">
											
											<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 ?>
														<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></span></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														<div class="product-price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}
													
											 }
											          ?>
											
											
											
											</div>
											</div>
									
										</a>
										</div>
										
										
										<?php 
										 }
										$i++; 
                               }			
               
                ?>
										
										
							
										
									
									</div>
								</div>
							</div>
						</div>
												<div class="item">
												<div class="carousel-col">
								<div class=" package-grid-item-wrapper">
								<div class="GridLex-grid-noGutter-equalHeight">
			
			
			
			 <?php 
			 $j=4;
				foreach (array_slice($hotselling, 4, 8) as $row) {
    foreach($row as $second ){
		
		 $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);	
		  $tourcatname = str_replace("&", "", $url_tourcatname);
		  
		  $url_string1 = preg_replace("/[\s_]/", "-", $second->tourname);
		  $url_string = str_replace("&", "", $url_string1);
		
		$tour_id=$second->id;
		$tours="tours";
							
		
   ?>
										
								
										
									<div class="carousel-col col-md-3 col-sm-6 col-lg-3 col-xs-12 mb-30" id="package-grid-item">
									<a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityname)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
											<div class="image">
												<img src="<?php echo $second->gallery_img_url;?>/<?php echo $second->gallery_img;?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->discount;?>% off</a>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->tdiscount;?>% off</a>
													<?php
													}
													
										}		
													?>
												
													
													<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
													<!--<i class="fa fa-heart-o"></i>-->
													</span>

													</div>
												</div>
											</div>
											 <div class="content123 clearfix">
											<h5 class="city_name_head"><?php echo $second->tourname;?></h5>
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span><?php 
											
										foreach(array_slice($hotselling_tour_rating , $j, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{?>
										      Newly Arrived
											<?php
											}
											else
											{
												echo  $rating;
											}
											
											
											?> </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<?php 
											
										foreach(array_slice($hotselling_tour_rating , $j, 1) as $rating)
											{									
											$rating;
											}
											if($rating == 0)
											{
											}
											else
											{
												?>
										     	 <p class="rating_star">(<?php echo  $rating ;?> ratings)</p>
											<?php
											}
											
											
											?>
											

											</div>
											</div>
											<div class="product-detail-right">
											
											<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 ?>
														<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></span></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														<div class="product-price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}
													
											 }
											          ?>
											
											
											
											</div>
											</div>
									
										</a>
										</div>
										
										<?php 
										 }
										 $j++;
                                      }			
               
                                 ?>
							
							
					
					
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
			</section>
			
			
			
			<section class="hotel_selling_picks"  id="city_section">

				<div class="container">
				<?php 
									
				foreach (array_slice($combo, 0, 1) as $row) {
    foreach($row as $second ){
		$name=$second->comboname;
	    }
		if($name == '')
		{
			
		}else{?>
					<div class="row">						
						<div class="col-sm-8 col-md-9 ">							
							<div class="section-title">
							
								<h3>Exclusive Combo Deals - <?php echo $cityname;?></h3>
								<span class="underline_text"></span>
																	<?php 
									
				          foreach ($combo_title as $title) {
  
		                             }
	
	                            ?>
								
								<p><?php echo $title;;?>.</p>

							</div>
							
						</div>
						
						
									<?php 
						$result=' ';
				foreach (array_slice($combo, 4, 5) as $row) {
					
        foreach($row as $second ){
		
		$comboid= $second->comboid;			
		 $result = $comboid;       
          }
	 }
		  if($result == ' ')
		  {
			
		  }else{   echo $result ?> <div class="col-sm-3 col-md-3 ">
					      <nav>
								<ul class="control-box pager">
									<li class="control_slide"><a data-slide="prev" href="#exclusive" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
									<li  class="control_slide"><a data-slide="next" href="#exclusive" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
								</ul>
					       </nav>
					
					</div><?php   }
	 
				?>
					
					
					
					</div>
					
		  <?php
		      }
				}?>
					
					<div id="exclusive" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false">
							
									<div class="carousel-inner">

						<div class="item active">
							<div class="carousel-col">
							    <div class="package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight" id="ex_c11">
									
					
							<?php 
				foreach (array_slice($combo, 0, 4) as $row) {
					$result=' ';
    foreach($row as $second ){
		
		$comboid= $second->comboid;
		
		$name= $second->comboname;
		$comboimageurl= $second->comboimageurl;
		$comboDiscount= $second->comboDiscount;
		$tourorder= $second->tourname;
		$tournamelist= $second->tourname;
		
		$combobuyAP= $second->sellAP;
		$combobuyCP= $second->buyCP;
		
		 $result .= $tournamelist .'+';        
                     
   
// ....Display Price Not all Sum
				
						
					    	/* if(i < 2)
							   {
								   tournamelisting +=  tournamelist + '+';  							       
							   }
							     sumAP += parseInt(combobuyAP);  
							     sumCP += parseInt(combobuyCP); 
							  
							  	
						var alltournamelist = tournamelisting + '...' */
	}
	 
   ?>
							
					
								<div class="carousel-col col-md-3 col-sm-6 col-lg-3 col-xs-12 mb-30" id="package-grid-item">
								
										<a href="detail-page.html">
											<div class="image">
												<img src="<?php echo $comboimageurl ?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php
												
													if($comboDiscount == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
															<a href="#" class="tag1"><?php echo $comboDiscount ?>% off</a>
													<?php
													}
													?>
												
													
													
														<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
														<!--<i class="fa fa-heart-o"></i>-->
														</span>
													</div>
												</div>
											</div>
										
											
												 <div class="content123 clearfix">
											<h5 class="city_name_head"><?php  echo $name;?></h5>
											<h4 class="combo_name"><?php  echo $result;?></h4>
		
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>4.6 </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<p class="rating_star">(526 ratings)</p>

											</div>
											</div>
											<div class="right_hand_side">
											<div class="absolute-in-content1">
											<div class="price1"><strike><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyAP ?></strike></div>
											<div class="price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyCP ?></div>
											</div>
											</div>
											</div>
											
											
											
											
											
											
											
										</a>
											
									</div>
										
									<?php 
										
            }			
               
                ?>		
				
										
										
										
										
								
										
									</div>
								</div>
							</div>
						</div>
												<div class="item">
												<div class="carousel-col">
								<div class="package-grid-item-wrapper">
								<div class="GridLex-grid-noGutter-equalHeight">
			
						
							
						<?php 
				foreach (array_slice($combo, 4, 8) as $row) {
					$result=' ';
    foreach($row as $second ){
		
		$comboid= $second->comboid;
		
		$name= $second->comboname;
		$comboimageurl= $second->comboimageurl;
		$comboDiscount= $second->comboDiscount;
		$tourorder= $second->tourname;
		$tournamelist= $second->tourname;
		
		$combobuyAP= $second->sellAP;
		$combobuyCP= $second->buyCP;
		
		 $result .= $tournamelist .'+';        
                     
   
// ....Display Price Not all Sum
				
						
					    	/* if(i < 2)
							   {
								   tournamelisting +=  tournamelist + '+';  							       
							   }
							     sumAP += parseInt(combobuyAP);  
							     sumCP += parseInt(combobuyCP); 
							  
							  	
						var alltournamelist = tournamelisting + '...' */
	}
	 
   ?>
							
					
								<div class="carousel-col col-md-3 col-sm-6 col-lg-3 col-xs-12 mb-30" id="package-grid-item">
								
										<a href="detail-page.html">
											<div class="image">
												<img src="<?php echo $comboimageurl ?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php
												
													if($comboDiscount == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
															<a href="#" class="tag1"><?php echo $comboDiscount ?>% off</a>
													<?php
													}
													?>
												
													
													
														<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
														<!--<i class="fa fa-heart-o"></i>-->
														</span>
													</div>
												</div>
											</div>
											
											
												 <div class="content123 clearfix">
											<h5 class="city_name_head"><?php  echo $name;?></h5>
											<h4 class="combo_name"><?php  echo $result;?></h4>
		
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>4.6 </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<p class="rating_star">(526 ratings)</p>

											</div>
											</div>
											<div class="right_hand_side">
											<div class="absolute-in-content1">
											<div class="price1"><strike> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyAP ?></strike></div>
											<div class="price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $combobuyCP ?></div>
											</div>
											</div>
											</div>
										</a>
											
									</div>
										
									<?php 
										
            }			
               
                ?>		
										
							
						
					
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
			</section>
			
			
			<section class="hotel_selling" id="city_section">

			<?php 
			$i=0;	
				foreach (array_slice($create_section, 0, 3) as $row) {
					
    foreach($row as $second ){		
		
		$tourcatname= $second->tourcatname;
		$id= $second->id;
	}
	 
   ?>
			
			
			
				<div class="container">
					<div class="row">
						
						<div class="col-sm-8 col-md-9 ">
							
							<div class="section-title">
							
								<h3> <?php echo $tourcatname ?> – <?php echo $cityname;?></h3>
								<span class="underline_text"></span>
							<p>Of distrusts immediate enjoyment curiosity do. Marianne numerous saw thoughts the humoured.</p>

							</div>
							
						</div>
						
											<?php 
											
						$result=' ';
				  foreach(array_slice($row, 4, 8) as $second ){
					
       // foreach($row as $second ){
		
		$tourcatname= $second->tourcatname;			
		 $result = $tourcatname;       
         // }
	 }
		  if($result == ' ')
		  {
			
		  }else{    $result ?> <div class="col-sm-3 col-md-3 ">
					       <nav>
								<ul class="control-box pager">
									<li class="control_slide"><a data-slide="prev" href="#tour<?php echo $id;?>" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
									<li  class="control_slide"><a data-slide="next" href="#tour<?php echo $id;?>" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
								</ul>
					       </nav>					
					</div><?php   }
	 
				?>
						
					
					
					
					
					</div>
	<div id="tour<?php echo $id;?>" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false">
							
									<div class="carousel-inner">

						<div class="item active">
							<div class="carousel-col">
							    <div class="package-grid-item-wrapper">
									<div class="GridLex-grid-noGutter-equalHeight">
									
									<?php 	
                              								
								  foreach(array_slice($row, 0, 4) as $second ){
		
								$id= $second->id;
		
								$tourname= $second->tourname;
								$gallery_img_url= $second->gallery_img_url;
								$gallery_img= $second->gallery_img;
								
								         $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);
										 $tourcatname = str_replace("&", "", $url_tourcatname);
								
										 $url_string1 = preg_replace("/[\s_]/", "-", $second->tourname);
										 $url_string = str_replace("&", "", $url_string1);
									 	 $tour_id=$second->id;
										 $tours="tours";
										
				             ?>	
		
										<div class="carousel-col col-md-3 col-sm-6 col-lg-3 col-xs-12 mb-30" id="package-grid-item">
										<a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityname)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
											<div class="image">
												<img src="<?php echo $second->gallery_img_url;?>/<?php echo $second->gallery_img;?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->discount;?>% off</a>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->tdiscount;?>% off</a>
													<?php
													}
													
										}		
													?>
												
													
													<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
													<!--<i class="fa fa-heart-o"></i>-->
													</span>

													</div>
												</div>
											</div>
											 <div class="content123 clearfix">
											<h5 class="city_name_head"><?php echo $second->tourname;?></h5>
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>4.5</span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<p class="rating_star">(526 ratings)</p>

											</div>
											</div>
											<div class="product-detail-right">
											
											<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 ?>
														<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></span></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														<div class="product-price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}
													
											 }
											          ?>
											
											
											
											</div>
											</div>
									
										</a>
										</div>
										
										
									
										
										
										<?php 
				
                                        }			
               
                                ?>			
								
								
										
									
									
									</div>
								</div>
							</div>
						</div><br><br>
						
								<div class="item">
								<div class="carousel-col">
								<div class="package-grid-item-wrapper">
								<div class="GridLex-grid-noGutter-equalHeight">
			
										<?php 		
								  foreach(array_slice($row, 4, 8) as $second ){
		
								
								$id= $second->id;
		
								$tourname= $second->tourname;
								$gallery_img_url= $second->gallery_img_url;
								$gallery_img= $second->gallery_img;
								
		 
			                          $url_tourcatname = preg_replace("/[\s_]/", "-", $second->tourcatname);
										 $tourcatname = str_replace("&", "", $url_tourcatname);
								
										 $url_string1 = preg_replace("/[\s_]/", "-", $second->tourname);
										 $url_string = str_replace("&", "", $url_string1);
									 	 $tour_id=$second->id;
										 $tours="tours";
										
				             ?>	
		
										<div class="carousel-col col-md-3 col-sm-6 col-lg-3 col-xs-12 mb-30" id="package-grid-item">
										<a href="<?php echo base_url($tours)?>/<?php echo strtolower($cityname)?>/<?php echo strtolower($tourcatname)?>/<?php echo strtolower($url_string)?>-<?php echo $tour_id?>">
											<div class="image">
												<img src="<?php echo $second->gallery_img_url;?>/<?php echo $second->gallery_img;?>" alt="Tour Package" />
												
												<div class="absolute-in-image">
													<div class="duration">
													<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
												
													$dis=$second->discount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->discount;?>% off</a>
													<?php
													}
															
										}else{
												
													$dis=$second->tdiscount;
													if($dis == 0)
													{
														 ?>
														<a href="#" ></a>
													<?php
													}else
													{
                                                       ?>
													  
														<a href="#" class="tag1"><?php echo $second->tdiscount;?>% off</a>
													<?php
													}
													
										}		
													?>
												
													
													<span class="heart_icon" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
													<!--<i class="fa fa-heart-o"></i>-->
													</span>

													</div>
												</div>
											</div>
											 <div class="content123 clearfix">
											<h5 class="city_name_head"><?php echo $second->tourname;?></h5>
											<div class="left_hand_side">
											<div class="rating-wrapper">
											<div class="raty-wrapper">
											<span>4.6 </span>
											<div class="star-rating-read-only1">
											<img src="<?php echo base_url('assets/images/raty/star-on.png')?>" alt="star">
											</div> 

											</div>
											<p class="rating_star">(526 ratings)</p>

											</div>
											</div>
											<div class="product-detail-right">
											
											<?php 
											$tour_dis=$second->tdiscount;
											if($tour_dis == 0)
										{
														
																										
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$servicedis=$second->discount;
													if($servicedis == 0)
													{
														 ?>
														<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></span></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $servicedis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}									
														
											}else{
												
																									
										     $tsellAP=$second->tprice;
											 $tservice=$second->sellAP;
											
										    $Tsum = $tsellAP+$tservice;
																							
												
													$tdis=$second->tdiscount;
													if($tdis == 0)
													{
														 ?>
														<div class="product-price"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo  $Tsum;?></div>
													<?php
													}else
													{	
												    $apply_discount=($Tsum * $tdis)/100;
													$final_price=	$Tsum -  $apply_discount;
                                                       ?>					  
														
													<div class="product-cut-price">													
													<strike><i class="fa fa-inr" aria-hidden="true"></i> 
													<?php echo  $Tsum;?>
													</strike>													
													</div>
													
							<div class="product-price"><span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo round($final_price);?></span></div>
													
													<?php
													}
													
											 }
											          ?>
											
											
											
											</div>
											</div>
									
										</a>
											</div>
										
										
										
									
										
										
										<?php 
				
            }			
               
                ?>	
										</div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
						</div>
												
					</div>
					
					
					
						
										<?php 
				$i++;
            }			
               
                ?>			
					
					
				
			</section>
			
			
			
			
			
			
			
			
						<!---<section class="read_more_head">

					<div class=" col-md-12 col-sm-12 col-lg-12 ">
					<div class="read_more">
				<span class="teaser">text goes here this is the  complete text being shownthis is the complete complete text being shownthis is the complete text being text being complete text being shownthis is the complete text being text being is the complete text being text being  text being text being text being   </span>
				<span class="complete"> this is the  complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shownthis is the text being shownthis is the complete text being shownthis is the complete text being shownthis is the complete text being shown</span>
				<span class="more">more...</span>

			</div>
			</div>
			  
			
			</section>-->
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

				<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>


<script>
 $(document).ready(function() {
        $('#showmenu').click(function() {
                $('.menu').show("fast");
				
				$('#showmenu').hide("fast");
        }); 
		$('#hidemenu').click(function() {
                $('.menu').hide("fast");
				alert("sfsdf");
				
        });
    });
</script>
<script>

/* $(document).ready(function(){
    $(".show_menu_city").click(function(){
        $(".hide_para1").show();
    });
    $(".show_menu_city1").click(function(){
        $(".hide_para1").hide();
    });
}); */

</script>
<script>
$(".more").toggle(function(){
    $(this).text("less").siblings(".complete").show();    
}, function(){
    $(this).text("more").siblings(".complete").hide();    
});

</script>

</body>

</html>