<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripoye | Home</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/TripoyeLogo2-3.png')?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" media="screen">	
	<link href="<?php echo base_url('assets/css/animate.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/component.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/ionicons/css/ionicons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/simple-line-icons/css/simple-line-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/rivolicons/style.css')?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	<div class="modal fade modal-login modal-border-transparent" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<button type="button" class="btn btn-close close" data-dismiss="modal" aria-label="Close">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
				
				<div class="clear"></div>
				
				<!-- Begin # DIV Form -->
				<div id="modal-login-form-wrapper">
					
					<!-- Begin # Login Form -->
					<form id="login-form">
					
						<div class="modal-body pb-5">
					
							<h4 class="text-center heading mt-10 mb-20">Sign-in</h4>
						
							<button class="btn btn-facebook btn-block">Sign-in with Facebook</button>
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="login_username" class="form-control" placeholder="username" type="text"> 
							</div>
							<div class="form-group"> 
								<input id="login_password" class="form-control" placeholder="password" type="password"> 
							</div>
			
							<div class="form-group">
								<div class="row gap-5">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="checkbox-block fa-checkbox"> 
											<input id="remember_me_checkbox" name="remember_me_checkbox" class="checkbox" value="First Choice" type="checkbox"> 
											<label class="" for="remember_me_checkbox">remember</label>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 text-right"> 
										<button id="login_lost_btn" type="button" class="btn btn-link">forgot pass?</button>
									</div>
								</div>
							</div>
						
						</div>
						
						<div class="modal-footer">
						
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block">Sign-in</button>
								</div>
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block btn-inverse" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							<div class="text-left">
								No account? 
								<button id="login_register_btn" type="button" class="btn btn-link">Register</button>
							</div>
							
						</div>
					</form>
					<!-- End # Login Form -->
								
					<!-- Begin | Lost Password Form -->
					<form id="lost-form" style="display:none;">
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Forgot password</h3>
							<div class="form-group mb-10"> 
								<input id="lost_email" class="form-control" type="text" placeholder="Enter Your Email">
							</div>
							
							<div class="text-center">
								<button id="lost_login_btn" type="button" class="btn btn-link">Sign-in</button> or 
								<button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
							</div>
							
						</div>
						
						<div class="modal-footer mt-10">
							
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-block">Submit</button>
								</div>
								<div class="col-xs-6 col-sm-6">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
						</div>
						
					</form>
					<!-- End | Lost Password Form -->
								
					<!-- Begin | Register Form -->
					<form id="register-form" style="display:none;">
					
						<div class="modal-body pb-5">
						
							<h3 class="text-center heading mt-10 mb-20">Register</h3>
							
							<button class="btn btn-facebook btn-block">Register with Facebook</button>
							
							<div class="modal-seperator">
								<span>or</span>
							</div>
							
							<div class="form-group"> 
								<input id="register_username" class="form-control" type="text" placeholder="Username"> 
							</div>
							
							<div class="form-group"> 
								<input id="register_email" class="form-control" type="email" placeholder="Email">
							</div>
							
							<div class="form-group"> 
								<input id="register_password" class="form-control" type="password" placeholder="Password">
							</div>
							
							<div class="form-group"> 
								<input id="register_password_confirm" class="form-control" type="password" placeholder="Confirm Password">
							</div>

						</div>
							
						<div class="modal-footer mt-10">
						
							<div class="row gap-10">
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-block">Register</button>
								</div>
								<div class="col-xs-6 col-sm-6 mb-10">
									<button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
								</div>
							</div>
							
							<div class="text-left">
									Already have account? <button id="register_login_btn" type="button" class="btn btn-link">Sign-in</button>
							</div>
							
						</div>
							
					</form>
					<!-- End | Register Form -->
								
				</div>
				<!-- End # DIV Form -->
								
			</div>
		</div>
	</div>
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper" id="container_wrapper_padding">

					<?php include ('header.php')?>

		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Contact Us</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="<?php echo site_url('home')?>">Homepage</a></li>
								<li><span>Contact Us</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
	
				<div class="container">
					
					<div class="row">

							
							<div class="section-title">
							
								<h3 class="contact_office">Our Offices are worldwide</h3>
								<span class="underline_text"></span>

								<p>Expression acceptance imprudence particular had eat unsatiable.</p>
								
							
						</div>
					
					</div>
					
				</div>
				
				<div id="map_5" class="mapbox"></div>
		
				<div class="bg-light section pt-40 pb-20">
				
					<div class="container">

						<div class="map-contact">
							
							<div class="top-place-inner">
							
								<div id="map_5_list" class="list row gap-0">
									
									<div class="col-sm-4">
										<div class="maplocation map-top-destination-item" 
											data-name="Dubai, UAE"  
											data-lat=" 25.276987"
											data-cat="place1"
											data-lng="55.2708">
											
											<div class="top-place-item mb-30 maplink">
												<div class="icon"><img src="<?php echo base_url('assets/images/map-marker/01.png')?>" alt="map" /></div>
												<div class="content">
													<h5 class="heading mt-0">Headquarter @ Pune, India</h5>
													<ul class="address-list">
														<li><i class="fa fa-map-marker"></i> Above Keyour Electronics,Office No:2 Karvenagar,Pune-58,India. </li>
														<li><i class="fa fa-phone"></i> +91-9689044624 </li>
														<li><i class="fa fa-envelope"></i> info@tripoye.com</li>
													</ul>
												</div>
											</div>
											<div class="infobox">
												<div class="infobox-inner">
													<div class="image">
														<img src="<?php echo base_url('assets/images/hero-header/breadcrumb.jpg')?>" alt="Images" />
													</div>
													<div class="content">
														<h6 class="heading">Our Office At Dubai</h6>
														<ul class="address-list">
														<li><i class="fa fa-map-marker"></i> 545, Marina Rd., Mohammed Bin Rashid Boulevard, Dubai 123234,</li>
															<li><i class="fa fa-phone"></i> + 971 4 436 4784</li>
															<li><i class="fa fa-envelope"></i> dubai-support@tourpacker.com</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-sm-4">
										<div class="maplocation map-top-destination-item" 
											data-name="Koh Sire"  
											data-lat="1.290270"
											data-cat="place2"
											data-lng="103.851959">
											
											<div class="top-place-item mb-30 maplink">
												<div class="icon"><img src="<?php echo base_url('assets/images/map-marker/02.png')?>" alt="map" /></div>
												<div class="content">
													<h5 class="heading mt-0">Branch @ Singapore</h5>
													<ul class="address-list">
														<li><i class="fa fa-map-marker"></i> 6985, Pasir Panjang Road #01-01 Citilink Warehouse Complex, Singapore, 118529</li>
														<li><i class="fa fa-phone"></i> +65-6375-8560 </li>
														<li><i class="fa fa-envelope"></i> dubai-singapore@tourpacker.com</li>
													</ul>
												</div>
											</div>
											<div class="infobox">
												<div class="infobox-inner">
													<div class="image">
														<img src="<?php echo base_url('assets/images/contact/singapore.jpg')?>" alt="Images" />
													</div>
													<div class="content">
														<h6 class="heading">Our office at Singapore</h6>
														<ul class="address-list">
															<li><i class="fa fa-map-marker"></i> 6985, Pasir Panjang Road #01-01 Citilink Warehouse Complex, Singapore, 118529</li>
															<li><i class="fa fa-phone"></i> +65-6375-8560 </li>
															<li><i class="fa fa-envelope"></i> dubai-singapore@tourpacker.com</li>
														</ul>
													</div>
												</div>
											</div>
										</div>	
									</div>
									
									<div class="col-sm-4">
										<div class="maplocation map-top-destination-item" 
											data-name="Berline, Germany"  
											data-lat="52.518623"
											data-cat="place3"
											data-lng="13.376198">
											
											<div class="top-place-item mb-30 maplink">
												<div class="icon"><img src="<?php echo base_url('assets/images/map-marker/03.png')?>" alt="map" /></div>
												<div class="content">
													<h5 class="heading mt-0">Branch @ Berlin, Germany</h5>
													<ul class="address-list">
														<li><i class="fa fa-map-marker"></i> Tour Packer Company, Charlottenstraße 49, 10117 Berlin, Germany</li>
														<li><i class="fa fa-phone"></i> +49(30)26050</li>
														<li><i class="fa fa-envelope"></i> Berlin-support@tourpacker.com</li>
													</ul>
												</div>
											</div>
											<div class="infobox">
												<div class="infobox-inner">
													<div class="image">
														<img src="<?php echo base_url('assets/images/contact/berlin.jpg')?>" alt="Images" />
													</div>
													<div class="content">
														<h6 class="heading">Our office at Berlin, Germany</h6>
														<ul class="address-list">
															<li><i class="fa fa-map-marker"></i> Tour Packer Company, Charlottenstraße 49, 10117 Berlin, Germany</li>
															<li><i class="fa fa-phone"></i> +49(30)26050</li>
															<li><i class="fa fa-envelope"></i> Berlin-support@tourpacker.com</li>
														</ul>
													</div>
												</div>
											</div>
										</div>	
									</div>

								</div>
					
							</div>
							
						</div>
					
					</div>
					
				</div>
				
				<div class="section pt-60 pb-0">
				
					<div class="container">
					
						<div class="row">

								
								<div class="section-title">
								<h3>Keep in touch</h3>
								<span class="underline_text"></span>
								</div>
								
						
						</div>
			
						
							<div class="row">
							
								<div class="col-sm-4">

									<h5 class="heading mt-5">Let's Social</h5>
									<p>May indulgence difficulty ham can put especially. Bringing remember for supplied her why was confined. Middleton principle did she procuring extensive believing add. Weather adapted prepare oh is calling.</p>
									
									<div class="boxed-social mb-30-xs clearfix">
										
										<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
										<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
										<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
										<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
									
									</div>
								
								</div>
								
								<div class="col-sm-8">
								
									<div class="row">
									
									
									<form id="checkout_form" action="<?php echo site_url('home/sendMail')?>" method="post">
										<div class="col-sm-6">
										
											<div class="form-group">
												<label for="inputName">Your Name <span class="font10 text-danger">(required)</span></label>
												<input id="inputName" type="text" name="name" class="form-control" data-error="Your name is required" required>
												<div class="help-block with-errors"></div>
											</div>
											
											<div class="form-group">
												<label for="inputEmail">Your Email <span class="font10 text-danger">(required)</span></label>
												<input id="inputEmail" type="email" name="email_id"  class="form-control" data-error="Your email is required and must be a valid email address" required>
												<div class="help-block with-errors"></div>
											</div>
											
											<div class="form-group">
												<label>Subject</label>
												<input type="text"  name="subject" class="form-control" />
											</div>
										
										</div>
										
										<div class="col-sm-6">
										
											<div class="form-group">
												<label for="inputMessage">Message <span class="font10 text-danger">(required)</span></label>
												<textarea id="inputMessage" class="form-control" rows="9" name="query"  data-minlength="50" data-error="Your message is required and must not less than 50 characters" required></textarea>
												<div class="help-block with-errors"></div>
											</div>
										
										</div>
										
										<div class="col-sm-12 text-right text-left-sm">
											<button type="submit" class="btn btn-primary mt-5">Send Message</button>
										</div>
										</form>
									</div>

								</div>
							
							</div>
					
						
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

					<?php include ('footer.php')?>


	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.waypoints.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.easing.1.3.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/SmoothScroll.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.slicknav.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.placeholder.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/instagram.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/spin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.introLoader.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/select2.full.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.responsivegrid.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ion.rangeSlider.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/readmore.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/validator.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.raty.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/customs.js')?>"></script>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/MarkerClusterer.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/infobox.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.mosne.map-for-category.js')?>"></script>
<script>
$(function () {

	var mycolor = "#ff0066";
	var mycolor2 = "#966E7E";
	var mybg_color = "#F7F7F7";

	var cluster_styles = [{
			url: 'images/m3.png',
			height: 30,
			width: 30,
			opt_textSize: 14,
			anchor: [3, 0],
			textColor: '#222222'
	}, {
			url: 'images/m4.png',
			height: 40,
			width: 40,
			opt_textSize: 17,
			opt_anchor: [6, 0],
			opt_textColor: '#222222'
	}, {
			url: 'images/m5.png',
			width: 50,
			height: 50,
			opt_textSize: 21,
			opt_anchor: [8, 0],
			opt_textColor: '#222222'
	}, {
			url: 'images/m5.png',
			width: 50,
			height: 50,
			opt_textSize: 21,
			opt_anchor: [8, 0],
			opt_textColor: '#222222'
	}];

	var my_cat_style ={
		place1: { icon: 'images/map-marker/01-marker.png'},
		place2: { icon: 'images/map-marker/02-marker.png'},
		place3: { icon: 'images/map-marker/03-marker.png'},
	};
		 
	var mapOptions = {
			zoom: 2,
			center: new google.maps.LatLng(25.2048, 55.2708),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			panControl: false,
			rotateControl: false,
			streetViewControl: false,
			scrollwheel: false,
	};
	
	$("#map_5").mosne_map({
			elements: '#map_5_list .maplocation',
			cluster_styles: {
					zoomOnClick: true,
					maxZoom: 5,
					styles: cluster_styles,
			},
			cat_style: my_cat_style ,
			zoom: 2,
			clickedzoom: 2, 	
			infowindows: false,  	
			infobox: true,   			
			map_opt: mapOptions,								
		
	});

});
</script>
<script>
jQuery(function($) {
 $(window).scroll(function() {
  var e = $(this).scrollTop();
   if (e > 20)
   {
   ($('.navbar-sticky-function').hasClass('navbar-primary')) 
     $('#logo2').show();
     $('#logo1').hide();
   }
   else 
   {
  $('.navbar-sticky').hasClass('navbar-primary')
    $('#logo1').show();
     $('#logo2').hide();
   }
 });
});

</script>

</body>


</html>