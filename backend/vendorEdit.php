<?php
	require_once("header.php");
    //global $cleaned;
    $cleaned = clean($_GET);
	$SystemVendors = Users::getVendorById($cleaned['id']);
    $getSystemCountry = Users::getSystemCountryAproved();//for Country
    $getSystemCity = Users::getCityByCountryIdApproved($SystemVendors['countryid']);//for City           
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Users</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Edit User</h4>
                                <form id="jvalidateVendateEdit" role="form" class="form-horizontal" action="lib/scripts/php/all/vendors.php">
                                <div class="panel-body">   
                                <input type="hidden" class="form-control" name="vendor_id" value="<?php echo $SystemVendors['vendor_id'];?>"/> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Select Country:</label>
                                        <div class="col-md-9">          
                                            <select class="form-control select" name="countryid" onChange="js_country_city(this.value)">
                                                <option value="">Select Country</option>
                                                <?php 
                                                foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>" <?php if($SystemCountry['id']==$SystemVendors['cityid']){?> selected="selected" <?php }?>><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>   
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <label class="col-md-3 control-label">Select City:</label>
                                        <div class="col-md-9">                       
                                            <select class="form-control select" name="cityid" id="cityid" onChange="js_tour_category(this.value)">
                                            <?php 
                                            foreach($getSystemCity as $SystemCity)
                                                  { ?>  
                                            <option  <?php if($SystemCity['id']==$SystemVendors['cityid']){?> selected="selected" <?php }?>  value="<?php echo $SystemCity['id'];?>"><?php echo $SystemCity['cityname'];?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">First Name:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="vendor_name" value="<?php echo $SystemVendors['vendor_name'];?>"/>
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div>
                                    <div class="form-group">                                        
                                        <label class="col-md-3 control-label">Last name:</label>          
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="vendor_surname" value="<?php echo $SystemVendors['vendor_surname'];?>"/>                                        
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Company:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="vendor_company" value="<?php echo $SystemVendors['vendor_company'];?>"/>
                                            <span class="help-block">min size = 2, max size = 20</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail:</label>
                                        <div class="col-md-9">
                                            <input type="text" name="vendor_email" class="form-control" value="<?php echo $SystemVendors['vendor_email'];?>"/>                                        
                                            <span class="help-block">required email</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Phone:</label>
                                        <div class="col-md-9">
                                            <input type="text" name="vendor_phone" class="form-control" value="<?php echo $SystemVendors['vendor_phone'];?>"/>                                        
                                            <span class="help-block">required Phone</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Skype Id:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $SystemVendors['vendor_skypeid'];?>" name="vendor_skypeid" class="form-control"/>                                        
                                            <span class="help-block">required Skype Id</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Whatssap No:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $SystemVendors['vendor_watsappno'];?>" name="vendor_watsappno" class="form-control"/>                                        
                                            <span class="help-block">required Whatssap No</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Office Address:</label>
                                        <div class="col-md-9">
                                            <textarea type="text" name="vendor_officeaddress" class="form-control"><?php echo $SystemVendors['vendor_officeaddress'];?></textarea>                                     
                                            <span class="help-block">required Office Address</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="user_status">
                                                <option value="1" <?php if($SystemVendors['vendor_status']==1){?> selected="selected" <?php }?>>Active</option>
                                                <option value="0" <?php if($SystemVendors['vendor_status']==0){?> selected="selected" <?php }?>>Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidateVendateEdit.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>
<script>
function js_country_city(counterid) {
    selectCity = document.getElementById('cityid');
    //document.getElementById('tourcatid').innerHTML = "";
    $("#tourcatid").selectpicker('refresh');
$.ajax({
                        url: "lib/scripts/php/all/ajaxCityOnCountryId.php",
                        type: "POST",
                        data:'CountryId='+counterid,
                        success: function(data){
                        //alert(data);  
                                        console.log(data); 
                                        //alert(data);
                                        if(data)
                                        {                                  
                                            var duce = jQuery.parseJSON(data);
                                            var status = duce.status;
                                            var cityoptions =duce.cityoptions;
                                                                                    
                                   }
                                   if (status == 1 ) {
                                       selectCity.innerHTML = cityoptions;
                                       $("#cityid").selectpicker('refresh');
                                   }else {
                                       alert("Select Country");
                                       
                                   }
                                   
        
                                            
                                            } 
                                           
                               
                   });

}
</script>


                
                
                
              
                






