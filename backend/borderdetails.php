<?php

  require_once("header.php");

  if(!isset($_GET['id']) && empty($_GET['id'] ))

  {

    header("location:orders.php"); 

  }

  $getSystem_order_tripoye = Users::getSystemordersById($_GET['id']);

  $getSystem_order_item = Users::getSystemOrderItemByOrderId($_GET['id']);

  $getSystem_TravelerDetails =  Users::getSystemTravelerByOrderId($_GET['id']);

  $getSystemVendors = Users::getSystemVendors();

  $getOrderStatus = Users::getOrderStatus();

  //echo "<pre>";print_r($getSystem_order_tripoye[0]['user_email']);die;
 
?>

<div class="page-content-wrap">

    <div class="row">

        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->

            <div class="panel panel-default">

                <div class="panel-heading">                                

                    <h3 class="panel-title">Order Details</h3>  

                    <ul class="panel-controls">

                      <div class="col-md-4" >

                       <a href="orders.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-arrow-left"></span> Back</button></a>

                      </div>

                    </ul>                              

                </div>

                <div class="panel-body">

                    <ul  class="nav nav-pills">

                      <li class="active"><a  href="#1b" data-toggle="tab">Order Details</a></li>

                      <li><a href="#2b" data-toggle="tab">Tours Details</a></li>

                      <li><a href="#3b" data-toggle="tab">Tour Traveler</a></li>

                      <li><a href="#4b" data-toggle="tab">Payment Details</a></li>

                    </ul>

                    <div class="tab-content clearfix">

                      <?php /* Tab 1*/?>

                      <div class="tab-pane active" id="1b">

                        <div class="row">

                          <div class="col-md-2">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><b>Customer Name: </b></li>

                              <li class="list-group-item justify-content-between"><b>Customer Email: </b></li>

                              <li class="list-group-item justify-content-between"><b>Customer Phone: </b></li>

                              <li class="list-group-item justify-content-between"><b>Order Date: </b></li>

                              <li class="list-group-item justify-content-between"><b>Total: </b></li>

                              <li class="list-group-item justify-content-between"><b>Coupon Applied: </b></li>

                              <li class="list-group-item justify-content-between"><b>Coupon Discount: </b></li>

                              <li class="list-group-item justify-content-between"><b>Discount Total: </b></li>

                            </ul>

                          </div>

                          <div class="col-md-4">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['user_name'].' '.$getSystem_order_tripoye[0]['user_surname'];?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['user_email'];?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['user_phone']?$getSystem_order_tripoye[0]['user_phone']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['date_added']?$getSystem_order_tripoye[0]['date_added']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['total']?$getSystem_order_tripoye[0]['total']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['couponcode']?$getSystem_order_tripoye[0]['couponcode']:'No';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['coupon_discount']?$getSystem_order_tripoye[0]['coupon_discount']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['discount_total']?$getSystem_order_tripoye[0]['discount_total']:'-----';?></li>

                            </ul>

                          </div>

                          <div class="col-md-1"></div>

                          <div class="col-md-5">

                            <h5>Assign Vendors To Tours</h5><hr>

                            <form class="form-horizontal" role="form"  action="lib/scripts/php/all/updateOrderStatus.php" method="post">

                            <?php foreach ($getSystem_order_item as $key => $insertVendorsArr) {?>

                            <div class="row">

                              <div class="col-md-6">

                                <?php echo $insertVendorsArr['TourOptName'];?>

                              </div>

                              <div class="col-md-6">                                                          

                                <?php 

                                  foreach($getSystemVendors as $SystemVendor)

                                  { 

                                    if($insertVendorsArr['vendorid'] == $SystemVendor['vendor_id'])
                                      
                                      {?>  

                                <input type="text" class="form-control" name="orderstatus" value="<?php echo $SystemVendor['vendor_name'];?> <?php echo $SystemVendor['vendor_surname'];?>" required readonly>

                                <?php } }?>

                              </div>

                            </div><br>

                            <?php }?>

                            <h5>Assign Order Status</h5><hr>

                            <div class="row">

                              <div class="col-md-6">

                                <label>Order Status</label>

                              </div>

                              <div class="col-md-6">                                                          

                                <?php 

                                  foreach($getOrderStatus as $OrderStatus)

                                  { 

                                    if($getSystem_order_tripoye[0]['order_status'] == $OrderStatus['order_status_id'])

                                      {?>  

                                <input type="text" class="form-control" name="orderstatus" value="<?php echo $OrderStatus['name'];?>" required readonly>

                                <?php } }?>

                              </div>

                            </div>

                            <hr>

                          </form>

                          </div>

                        </div>

                    </div>



                      <?php /* Tab 2*/?>

                      <div class="tab-pane" id="2b">

                      <?php foreach ($getSystem_order_item as $key => $getSystem_order_items) {?>

                        <div class="row">

                          <div class="col-md-2">

                          <b>Order Item <?php echo $key+1;?></b>

                          </div>

                          <div class="col-md-3">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><b>Order Id: </b></li>

                              <li class="list-group-item justify-content-between"><b>Tour: </b></li>

                              <li class="list-group-item justify-content-between"><b>Tour Option: </b></li>

                              <li class="list-group-item justify-content-between"><b>Tour Date: </b></li>

                              <li class="list-group-item justify-content-between"><b>Tour Transfer Name: </b></li>

                              <li class="list-group-item justify-content-between"><b>Combo Name: </b></li>

                              <li class="list-group-item justify-content-between"><b>Adult Price: </b></li>

                              <li class="list-group-item justify-content-between"><b>Child Price: </b></li>

                              <li class="list-group-item justify-content-between"><b>Discount Price: </b></li>

                              <li class="list-group-item justify-content-between"><b>Discount Adult Price: </b></li>

                              <li class="list-group-item justify-content-between"><b>Discount Child Price: </b></li>

                              <li class="list-group-item justify-content-between"><b>Tour Time: </b></li>

                              <li class="list-group-item justify-content-between"><b>Adult Quantity: </b></li>

                              <li class="list-group-item justify-content-between"><b>Child Quantity: </b></li>

                              <li class="list-group-item justify-content-between"><b>Infant Quantity: </b></li>

                              <li class="list-group-item justify-content-between"><b>Single Totals: </b></li>

                              <li class="list-group-item justify-content-between"><b>Single Discount Total: </b></li>

                              <li class="list-group-item justify-content-between"><b>Full Total: </b></li>

                              <li class="list-group-item justify-content-between"><b>Discount Full Total: </b></li>

                              <li class="list-group-item justify-content-between"><b>Cart User Email: </b></li>

                              <li class="list-group-item justify-content-between"><b>Pick Up Point: </b></li>

                            </ul>

                          </div>

                          <div class="col-md-5">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['order_id']?$getSystem_order_items['order_id']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['tourname']?$getSystem_order_items['tourname']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['optionName']?$getSystem_order_items['optionName']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['TourDate']?$getSystem_order_items['TourDate']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['TransferName']?$getSystem_order_items['TransferName']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['comboname']?$getSystem_order_items['comboname']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['AdultPrice']?$getSystem_order_items['AdultPrice']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['ChildPrice']?$getSystem_order_items['ChildPrice']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['Discount']?$getSystem_order_items['Discount']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['DisAdultPrice']?$getSystem_order_items['DisAdultPrice']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['DisChildPrice']?$getSystem_order_items['DisChildPrice']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['TourTime']?$getSystem_order_items['TourTime']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['AdultQuantity']?$getSystem_order_items['AdultQuantity']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['ChildQuantity']?$getSystem_order_items['ChildQuantity']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['InfantQuantity']?$getSystem_order_items['InfantQuantity']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['SingleTotal']?$getSystem_order_items['SingleTotal']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['SingleDisTotal']?$getSystem_order_items['SingleDisTotal']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['FullTotal']?$getSystem_order_items['FullTotal']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['DisFullTotal']?$getSystem_order_items['DisFullTotal']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['cart_user_id']?$getSystem_order_items['cart_user_id']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_items['pickup_point']?$getSystem_order_items['pickup_point']:'-----';?></li>

                            </ul>

                          </div>

                        </div>

                        <hr>

                      <?php }?>

                      </div>





                      <?php /* Tab 3*/?>

                      <div class="tab-pane" id="3b">

                        <?php //foreach ($getSystem_TravelerDetails as $key => $getSystem_TravelerDetail) {?>

                        <div class="row">

                          <div class="col-md-2">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><b>Name: </b></li>

                              <li class="list-group-item justify-content-between"><b>Email: </b></li>

                              <li class="list-group-item justify-content-between"><b>Phone: </b></li>

                              <li class="list-group-item justify-content-between"><b>Remark: </b></li>

                            </ul>

                          </div>

                          <div class="col-md-4">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_TravelerDetails[0]['name']?$getSystem_TravelerDetails[0]['name']:'-----';?> <?php echo $getSystem_TravelerDetails[0]['lastname']?$getSystem_TravelerDetails[0]['lastname']:'';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_TravelerDetails[0]['email']?$getSystem_TravelerDetails[0]['email']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_TravelerDetails[0]['phone']?$getSystem_TravelerDetails[0]['phone']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_TravelerDetails[0]['remark']?$getSystem_TravelerDetails[0]['remark']:'-----';?></li>

                            </ul>

                          </div>

                        </div>

                        <hr>

                      <?php //}?>

                      </div>





                      <?php /* Tab 4*/?>

                      <div class="tab-pane" id="4b">

                        <div class="row">

                          <div class="col-md-3">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><b>Pyment ID:</b></li>

                              <li class="list-group-item justify-content-between"><b>Pyment Mode:</b></li>

                              <li class="list-group-item justify-content-between"><b>Pyment Status:</b></li>

                              <li class="list-group-item justify-content-between"><b>Pyment TXNID:</b></li>

                              <li class="list-group-item justify-content-between"><b>Pyment Transaction Fee:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Amount:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Card Category:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Discount:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Additional Charges:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Added On:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment product Info:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Email:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment PG Type:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Name On Card:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment issuing Bank:</b></li>

                              <li class="list-group-item justify-content-between"><b>Payment Card Type:</b></li>

                            </ul>

                          </div>

                          <div class="col-md-4">

                            <ul class="list-group">

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_id']?$getSystem_order_tripoye[0]['payment_id']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_mode']?$getSystem_order_tripoye[0]['payment_mode']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_status']?$getSystem_order_tripoye[0]['payment_status']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_txnid']?$getSystem_order_tripoye[0]['payment_txnid']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_transaction_fee']?$getSystem_order_tripoye[0]['payment_transaction_fee']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_amount']?$getSystem_order_tripoye[0]['payment_amount']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_cardCategory']?$getSystem_order_tripoye[0]['payment_cardCategory']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_discount']?$getSystem_order_tripoye[0]['payment_discount']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_additional_charges']?$getSystem_order_tripoye[0]['payment_additional_charges']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_addedon']?$getSystem_order_tripoye[0]['payment_addedon']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_productinfo']?$getSystem_order_tripoye[0]['payment_productinfo']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_email']?$getSystem_order_tripoye[0]['payment_email']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_PG_TYPE']?$getSystem_order_tripoye[0]['payment_PG_TYPE']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_name_on_card']?$getSystem_order_tripoye[0]['payment_name_on_card']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_issuing_bank']?$getSystem_order_tripoye[0]['payment_issuing_bank']:'-----';?></li>

                              <li class="list-group-item justify-content-between"><?php echo $getSystem_order_tripoye[0]['payment_card_type']?$getSystem_order_tripoye[0]['payment_card_type']:'-----';?></li>

                            </ul>

                          </div>

                        </div>

                      </div>

                    </div>

                </div>

            </div>

            <!-- END DEFAULT DATATABLE -->

        </div>

    </div>

</div>

<?php require_once("footer.php");

?>



<style type="text/css">

.nav-pills>li>a {

  border-radius: 4px 4px 0 0!important;

  font-weight: bold!important;

  font-size: 12px!important;

  color: #000;

}

.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a, .nav-pills>li.active>a:focus{background: #e34724;}



.tab-content{

  margin-top: 30px;

}

</style>