<?php //echo "<pre>";
//print_r($_GET);die;
	require_once("header.php");
         //global $cleaned;
        $cleaned = clean($_GET);
        //dump($cleaned);
	$getSystemCity = Users::getTourRatingByid($cleaned['id']);//for global
	$Coupon['tour_rating'] = $getSystemCity['tour_rating'];
	//echo "<pre>";
	//print_r($getSystemCity);
	//die;
        
	$getSystemCountry = Users::getSystemCountryAproved();//for global
                     
?>
  <style>
   .rating { 
                border: none;
                float: left;
            }

            .rating > input { display: none; } 
            .rating > label:before { 
                margin: 5px;
                font-size: 1.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }

            .rating > .half:before { 
                content: "\f089";
                position: absolute;
            }

            .rating > label { 
                color: #ddd; 
                float: right; 
            }

            .rating > input:checked ~ label, 
            .rating:not(:checked) > label:hover,  
            .rating:not(:checked) > label:hover ~ label { color: #FFD700;  }

            .rating > input:checked + label:hover, 
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label, 
            .rating > input:checked ~ label:hover ~ label { color: #FFED85;  }    
  </style>
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2>Tour Rating</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-12"> 
                        <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                           <!-- <a href="couponadd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Coupon</button></a> -->
                                     </div>
                                       
                                    </ul>                                
                                </div>                       

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Edit Tour Rating</h4>
                                <form id="jvalidate_city"  method="post" enctype="multipart/form-data" role="form" class="form-horizontal" action="lib/scripts/php/all/ratingtedit.php">
                                <div class="panel-body">                                    
                                                 
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate City Name, Please Try Another City name</span> <?php }?> <div class="form-group">
                                        <label class="col-md-3 control-label">Tour Name</label>  
                                        <div class="col-md-9">
                                          <?php echo $getSystemCity['tourname'];?>
                                          
                                           
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">User Name:</label>  
                                        <div class="col-md-9">
                                           <?php echo $getSystemCity['user_name']." ".$getSystemCity['user_surname'];?>
                                                                                       
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Rating:</label>  
                                        <div class="col-md-9">
                                            <fieldset id='demo3' class="rating">
                        <input class="stars" type="radio" id="star53" name="rating_tour" value="5" <?php if ($Coupon['tour_rating'] == '5') { echo "checked";} ?>/>
                        <label class = "full" for="star53" title="Awesome - 5 stars"></label>
                        <input class="stars" type="radio" id="star4half3" name="rating_tour" value="4.5" <?php if ($Coupon['tour_rating'] == '4.5') { echo "checked";} ?> />
                        <label class="half" for="star4half3" title="Pretty good - 4.5 stars"></label>
                        <input class="stars" type="radio" id="star43" name="rating_tour" value="4" <?php if ($Coupon['tour_rating'] == '4') { echo "checked";} ?> />
                        <label class = "full" for="star43" title="Pretty good - 4 stars"></label>
                        <input class="stars" type="radio" id="star3half3" name="rating_tour" value="3.5" <?php if ($Coupon['tour_rating'] == '3.5') { echo "checked";} ?> />
                        <label class="half" for="star3half3" title="Meh - 3.5 stars"></label>
                        <input class="stars" type="radio" id="star33" name="rating_tour" value="3" <?php if ($Coupon['tour_rating'] == '3') { echo "checked";} ?> />
                        <label class = "full" for="star33" title="Meh - 3 stars"></label>
                        <input class="stars" type="radio" id="star2half3" name="rating_tour" value="2.5" <?php if ($Coupon['tour_rating'] == '2.5') { echo "checked";} ?>/>
                        <label class="half" for="star2half3" title="Kinda bad - 2.5 stars"></label>
                        <input class="stars" type="radio" id="star23" name="rating_tour" value="2" <?php if ($Coupon['tour_rating'] == '2') { echo "checked";} ?>/>
                        <label class = "full" for="star23" title="Kinda bad - 2 stars"></label>
                        <input class="stars" type="radio" id="star1half3" name="rating_tour" value="1.5" <?php if ($Coupon['tour_rating'] == '1.5') { echo "checked";} ?> />
                        <label class="half" for="star1half3" title="Meh - 1.5 stars"></label>
                        <input class="stars" type="radio" id="star13" name="rating_tour" value="1" <?php if ($Coupon['tour_rating'] == '1') { echo "checked";} ?>/>
                        <label class = "full" for="star13" title="Sucks big time - 1 star"></label>
                        <input class="stars" type="radio" id="starhalf3" name="rating_tour" value="0.5" <?php if ($Coupon['tour_rating'] == '0.5') { echo "checked";} ?> />
                        <label class="half" for="starhalf3" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                                        </div>
                                    </div>
                                    
                                                           
                                     </div> 
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Comment:</label>  
                                        <div class="col-md-4">
                                           <textarea class="form-control" rows="5" name="description"><?php echo $getSystemCity['tour_comment'];?></textarea>
                                            
                                        </div>
                                    </div>
                                   
                                    
                                    
                                     
                                    
                                    <div class="btn-group " style="padding-top: 10px; margin-left:25%;">
                                        
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>   
                                    </div>                                                                                                                       
                                </div>   
                                 <input type="hidden" class="form-control" name="ratingtid" value="<?php echo $getSystemCity['id'];?>"/>                                            
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>


                
                
                
              
                






