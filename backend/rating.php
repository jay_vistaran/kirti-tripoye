<?php
require_once("header.php");
        //global $cleaned;
         
        $getTourRating = Users::getTourRating();//for global
		$getComboRating = Users::getComboRating();
      // echo "<pre>";
	  // print_r($getComboRating);
	  // die;
		
         
        $getSystemCity = Users::getSystemCityAproved();//for global
         
        $getSystemTC= Users::getTourCategoryAproved();//for global
        
        $getSystemCoupon= Users::getCoupon();//for global
?>
<style>
 .rating { 
                border: none;
                float: left;
            }

            .rating > input { display: none; } 
            .rating > label:before { 
                margin: 5px;
                font-size: 1.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }

            .rating > .half:before { 
                content: "\f089";
                position: absolute;
            }

            .rating > label { 
                color: #ddd; 
                float: right; 
            }

            .rating > input:checked ~ label { color: #FFD700;  }


               
</style>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Tour Rating</h3>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                           <!-- <a href="couponadd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Coupon</button></a> -->
                                            <a href="addtourrating.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Tour Rating</button></a>
                                     </div>
                                       
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                   
                                    
                                    
                                    <table class="table tablef datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Tour Name</th>
                                                <th>User Name</th>
                                                <th style="text-align:left;">Rating</th>
                                                <th>Comment</th>
                                                <th>Approve</th>
                                                <th>Action</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getTourRating as $Coupon){?>    
                                            <tr>
                                            <td><?php echo $i; ?></td>
                                                <td><?php echo $Coupon['tourname'];?></td>
                                                
                                                <td><span data-toggle="modal" data-target="#myModalNormT_<?php echo $Coupon['user_id']; ?>" style="color:BLUE"><?php echo $Coupon['user_name']." ".$Coupon['user_surname'];?></span></td>
                                                
                                                <td><fieldset id='demo3' class="rating">
                        <input class="stars" type="radio" id="star53" name="rating_tour_<?php echo  $i; ?>" value="5" <?php if ($Coupon['tour_rating'] == '5') { echo "checked";} ?>/>
                        <label class = "full" for="star53" title="Awesome - 5 stars"></label>
                        <input class="stars" type="radio" id="star4half3" name="rating_tour_<?php echo  $i; ?>" value="4.5" <?php if ($Coupon['tour_rating'] == '4.5') { echo "checked";} ?> />
                        <label class="half" for="star4half3" title="Pretty good - 4.5 stars"></label>
                        <input class="stars" type="radio" id="star43" name="rating_tour_<?php echo  $i; ?>" value="4" <?php if ($Coupon['tour_rating'] == '4') { echo "checked";} ?> />
                        <label class = "full" for="star43" title="Pretty good - 4 stars"></label>
                        <input class="stars" type="radio" id="star3half3" name="rating_tour_<?php echo  $i; ?>" value="3.5" <?php if ($Coupon['tour_rating'] == '3.5') { echo "checked";} ?> />
                        <label class="half" for="star3half3" title="Meh - 3.5 stars"></label>
                        <input class="stars" type="radio" id="star33" name="rating_tour_<?php echo  $i; ?>" value="3" <?php if ($Coupon['tour_rating'] == '3') { echo "checked";} ?> />
                        <label class = "full" for="star33" title="Meh - 3 stars"></label>
                        <input class="stars" type="radio" id="star2half3" name="rating_tour_<?php echo  $i; ?>" value="2.5" <?php if ($Coupon['tour_rating'] == '2.5') { echo "checked";} ?>/>
                        <label class="half" for="star2half3" title="Kinda bad - 2.5 stars"></label>
                        <input class="stars" type="radio" id="star23" name="rating_tour_<?php echo  $i; ?>" value="2" <?php if ($Coupon['tour_rating'] == '2') { echo "checked";} ?>/>
                        <label class = "full" for="star23" title="Kinda bad - 2 stars"></label>
                        <input class="stars" type="radio" id="star1half3" name="rating_tour_<?php echo  $i; ?>" value="1.5" <?php if ($Coupon['tour_rating'] == '1.5') { echo "checked";} ?> />
                        <label class="half" for="star1half3" title="Meh - 1.5 stars"></label>
                        <input class="stars" type="radio" id="star13" name="rating_tour_<?php echo  $i; ?>" value="1" <?php if ($Coupon['tour_rating'] == '1') { echo "checked";} ?>/>
                        <label class = "full" for="star13" title="Sucks big time - 1 star"></label>
                        <input class="stars" type="radio" id="starhalf3" name="rating_tour_<?php echo  $i; ?>" value="0.5" <?php if ($Coupon['tour_rating'] == '0.5') { echo "checked";} ?> />
                        <label class="half" for="starhalf3" title="Sucks big time - 0.5 stars"></label>
                    </fieldset></td>
                                                <td><?php echo $Coupon['tour_comment'];?></td>
                                                <td><button type="button" class="btn btn-warning" name="Approvet<?php echo $Coupon['id'];?>" id="approvet_<?php echo $Coupon['id'];?>" onClick="approvet('<?php echo $Coupon['id'];?>')"><?php if ($Coupon['approve'] == 1) { echo "Approve";}else {echo "Not Approve";} ?></button></td>
                                                 <td><a href="ratingtedit.php?id=<?php echo $Coupon['id'];  ?>"><button type="button" class="btn btn-primary" name="edit<?php echo $Coupon['id'];?>" id="edit_<?php echo $Coupon['id'];?>">Edit</button></a>
                                                  <button type="button" class="btn btn-danger" name="delete<?php echo $Coupon['id'];?>" id="delete_<?php echo $Coupon['id'];?>" onClick="deletemoderatingt('<?php echo $Coupon['id'];?>')">Delete</button></td>

                                               
                                                
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table>
                                    
                                    
                                </div>
                                 <div class="panel-heading">                                
                                    <h3 class="panel-title">Combo Rating</h3>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                           <!-- <a href="couponadd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Coupon</button></a> -->
                                           <a href="addcomborating.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Combo Rating</button></a>
                                     </div>
                                       
                                    </ul>                                
                                </div>
                                
                                <div class = "panel-body">
                                   
                                    
                                    
                                    <table class="table tablef datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Combo Name</th>
                                                <th>User Name</th>
                                                <th style="text-align:left;">Rating</th>
                                                <th>Comment</th>
                                                <th>Approve</th>
                                                <th>Action</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getComboRating as $Coupon){?>    
                                            <tr>
                                            <td><?php echo $i; ?></td>
                                                <td><?php echo $Coupon['comboname'];?></td>
                                                
                                                <td><span data-toggle="modal" data-target="#myModalNormC_<?php echo $Coupon['user_id']; ?>" style="color:BLUE"><?php echo $Coupon['user_name']." ".$Coupon['user_surname'];?></span></td>
                                                
                                                <td><fieldset id='demo3' class="rating">
                        <input class="stars" type="radio" id="star53" name="rating_combo_<?php echo  $i; ?>" value="5" <?php if ($Coupon['combo_rating'] == '5') { echo "checked";} ?>/>
                        <label class = "full" for="star53" title="Awesome - 5 stars"></label>
                        <input class="stars" type="radio" id="star4half3" name="rating_combo_<?php echo  $i; ?>" value="4.5" <?php if ($Coupon['combo_rating'] == '4.5') { echo "checked";} ?> />
                        <label class="half" for="star4half3" title="Pretty good - 4.5 stars"></label>
                        <input class="stars" type="radio" id="star43" name="rating_combo_<?php echo  $i; ?>" value="4" <?php if ($Coupon['combo_rating'] == '4') { echo "checked";} ?> />
                        <label class = "full" for="star43" title="Pretty good - 4 stars"></label>
                        <input class="stars" type="radio" id="star3half3" name="rating_combo_<?php echo  $i; ?>" value="3.5" <?php if ($Coupon['combo_rating'] == '3.5') { echo "checked";} ?> />
                        <label class="half" for="star3half3" title="Meh - 3.5 stars"></label>
                        <input class="stars" type="radio" id="star33" name="rating_combo_<?php echo  $i; ?>" value="3" <?php if ($Coupon['combo_rating'] == '3') { echo "checked";} ?> />
                        <label class = "full" for="star33" title="Meh - 3 stars"></label>
                        <input class="stars" type="radio" id="star2half3" name="rating_combo_<?php echo  $i; ?>" value="2.5" <?php if ($Coupon['combo_rating'] == '2.5') { echo "checked";} ?>/>
                        <label class="half" for="star2half3" title="Kinda bad - 2.5 stars"></label>
                        <input class="stars" type="radio" id="star23" name="rating_combo_<?php echo  $i; ?>" value="2" <?php if ($Coupon['combo_rating'] == '2') { echo "checked";} ?>/>
                        <label class = "full" for="star23" title="Kinda bad - 2 stars"></label>
                        <input class="stars" type="radio" id="star1half3" name="rating_combo_<?php echo  $i; ?>" value="1.5" <?php if ($Coupon['combo_rating'] == '1.5') { echo "checked";} ?> />
                        <label class="half" for="star1half3" title="Meh - 1.5 stars"></label>
                        <input class="stars" type="radio" id="star13" name="rating_combo_<?php echo  $i; ?>" value="1" <?php if ($Coupon['combo_rating'] == '1') { echo "checked";} ?>/>
                        <label class = "full" for="star13" title="Sucks big time - 1 star"></label>
                        <input class="stars" type="radio" id="starhalf3" name="rating_combo_<?php echo  $i; ?>" value="0.5" <?php if ($Coupon['combo_rating'] == '0.5') { echo "checked";} ?> />
                        <label class="half" for="starhalf3" title="Sucks big time - 0.5 stars"></label>
                    </fieldset></td>
                                                <td><?php echo $Coupon['combo_comment'];?></td>
                                               	<td><button type="button" class="btn btn-warning" name="Approvec<?php echo $Coupon['id'];?>" id="approvec_<?php echo $Coupon['id'];?>" onClick="approvec('<?php echo $Coupon['id'];?>')"><?php if ($Coupon['approve'] == 1) { echo "Approve";}else {echo "Not Approve";} ?></button></td>
                                                <td><a href="ratingcedit.php?id=<?php echo $Coupon['id'];  ?>"><button type="button" class="btn btn-primary" name="editc<?php echo $Coupon['id'];?>" id="editc_<?php echo $Coupon['id'];?>">Edit</button></a>
                                                  <button type="button" class="btn btn-danger" name="deletec<?php echo $Coupon['id'];?>" id="deletec_<?php echo $Coupon['id'];?>" onClick="deletemoderatingc('<?php echo $Coupon['id'];?>')">Delete</button></td>
                                                
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table>
                                    
                                    
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                            

                        </div>
                    </div>                                
                    <?php if (isset($getTourRating) && is_array($getTourRating) && count($getTourRating)>0) { 
		  				foreach($getTourRating as $customerKey => $customerValue) {
							$SystemUsers = Users::getUserById($customerValue['user_id']);
		 ?>		
                <div class="modal fade" id="myModalNormT_<?php echo $customerValue['user_id']; ?>" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    User Information
                    
                </h4>
                
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
               
                 <h6><li>User ID: <?php echo $customerValue['user_id'];  ?></li></h6>
                 <br/>
                <form role="form" id="changelevel" name="changelevel" method="post" action="../../controller/customer/customer.php">
                 <table class="table table-hover table-bordered" data-link="row">
          <thead>
            <tr>
              <th style="width:300px;">Username</th>
              <td><?php echo $SystemUsers['user_name']." ".$SystemUsers['user_surname'];?></td>
            </tr>
            <tr>  
              <th style="width:300px;">email</th>
              <td><?php echo $SystemUsers['user_email'];?></td>
             </tr>
             <tr>
              <th style="width:300px;">Mobile</th>
              <td><?php echo $SystemUsers['user_mobile'];?></td>
             </tr> 
             <tr>
              <th style="width:300px;">Date of Birth</th>
              <td><?php echo $SystemUsers['user_dob'];?></td>
             </tr> 
             <tr>
              <th style="width:300px;">Role</th>
              <td></td>
             </tr>
             <tr> 
              <th style="width:300px;">Status</th>
              <td><?php if($SystemUsers['user_status']==1){?>Active<?php }else { ?>Inactive<?php } ?></td>
              </tr>
             </tr>
            
                       
                        
          </thead>
          <tbody>
           </tbody>
        </table>
                 <!-- <button type="submit" class="btn btn-default">Submit</button>-->
                
                
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                           
                            Close
                </button>
                <input type="hidden" name="user_id" value="<?php echo  $customerValue['user_id']; ?>"/>
               
            </div>
            </form>
        </div>
    </div>
</div>
<?php } } ?>
<?php if (isset($getComboRating) && is_array($getComboRating) && count($getComboRating)>0) { 
		  				foreach($getComboRating as $customerKey => $customerValue) {
							$SystemUsers = Users::getUserById($customerValue['user_id']);
		 ?>		
                <div class="modal fade" id="myModalNormC_<?php echo $customerValue['user_id']; ?>" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    User Information
                    
                </h4>
                
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
               
                 <h6><li>User ID: <?php echo $customerValue['user_id'];  ?></li></h6>
                 <br/>
                <form role="form" id="changelevel" name="changelevel" method="post" action="../../controller/customer/customer.php">
                 <table class="table table-hover table-bordered" data-link="row">
          <thead>
            <tr>
              <th style="width:300px;">Username</th>
              <td><?php echo $SystemUsers['user_name']." ".$SystemUsers['user_surname'];?></td>
            </tr>
            <tr>  
              <th style="width:300px;">email</th>
              <td><?php echo $SystemUsers['user_email'];?></td>
             </tr>
             <tr>
              <th style="width:300px;">Mobile</th>
              <td><?php echo $SystemUsers['user_mobile'];?></td>
             </tr> 
             <tr>
              <th style="width:300px;">Date of Birth</th>
              <td><?php echo $SystemUsers['user_dob'];?></td>
             </tr> 
             <tr>
              <th style="width:300px;">Role</th>
              <td></td>
             </tr>
             <tr> 
              <th style="width:300px;">Status</th>
              <td><?php if($SystemUsers['user_status']==1){?>Active<?php }else { ?>Inactive<?php } ?></td>
              </tr>
             </tr>
            
                       
                        
          </thead>
          <tbody>
           </tbody>
        </table>
                 <!-- <button type="submit" class="btn btn-default">Submit</button>-->
                
                
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                           
                            Close
                </button>
                <input type="hidden" name="user_id" value="<?php echo  $customerValue['user_id']; ?>"/>
               
            </div>

            </form>
        </div>
    </div>
</div>
<?php } } ?>          

                </div>
                <!-- PAGE CONTENT WRAPPER -->  


             <script type="text/javascript">
                
                      /*  function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. Becouse if you delete Tour here then releted Tour Detail will be deleted. ");
                                if(conf)
                                {
                                       window.location.href="lib/scripts/php/all/coupondelete.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }
						*/
</script>

<?php	require_once("footer.php");
?>


<script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>

<script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/center.js'></script>   
         
<script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
 <script type="text/javascript">
                
                       /* function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. Becouse if you delete Tour here then releted Tour Detail will be deleted. ");
                                if(conf)
                                {
                                       window.location.href="lib/scripts/php/all/deletetour.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }*/
						function deletemoderatingt(id){
                    noty({
                        text: 'Are you sure Want to delete This',
                        layout: 'center',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									window.location.href="lib/scripts/php/all/ratingtdelete.php?id="+id;
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }
				function deletemoderatingc(id){
                    noty({
                        text: 'Are you sure Want to delete This',
                        layout: 'center',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									window.location.href="lib/scripts/php/all/ratingcdelete.php?id="+id;
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }
				
	function approvec(reviewid) {
		//alert(reviewid);
		 caseApprove = $("#approvec_"+reviewid).html();
		if (caseApprove == 'Approve') {
			 actionvalue	 = 0;
		}else{
			 actionvalue	 = 1;
		}
		 $.post("lib/scripts/php/all/ajaxcomboapprove.php", { reviewid: reviewid, action: actionvalue },
            function(data){
				//alert(data);
				console.log(data);
				if(data == 1 ){
					$("#approvec_"+reviewid).html('Approve');
				}else{
					$("#approvec_"+reviewid).html('Not Approve');
				}
               //button.addClass('pressed');
              // button.html("Playing!") ;
            }
        );
	}
	function approvet(reviewid) {
		//alert(reviewid);
		 caseApprove = $("#approvet_"+reviewid).html();
		if (caseApprove == 'Approve') {
			 actionvalue	 = 0;
		}else{
			 actionvalue	 = 1;
		}
		 $.post("lib/scripts/php/all/ajaxtourapprove.php", { reviewid: reviewid, action: actionvalue },
            function(data){
				//alert(data);
				console.log(data);
				if(data == 1 ){
					$("#approvet_"+reviewid).html('Approve');
				}else{
					$("#approvet_"+reviewid).html('Not Approve');
				}
               //button.addClass('pressed');
              // button.html("Playing!") ;
            }
        );
	}
				


$(".tablef").dataTable({
  "aoColumnDefs" : [
 {
   'bSortable' : false,
   'aTargets' : [ 2,3,4, 5,6 ]
 }]
});

</script>