<?php
	require_once("header.php");
        //global $cleaned;
	$getSystemUsers = Users::getSystemUsers();//for Users
        
        $getSystemCountry = Users::getSystemCountryAproved();//for Country
         
        $getSystemCity = Users::getSystemCityAproved();//for City
         
        $getSystemTC= Users::getTourCategoryAproved();//for TourCategory
          
        $getCancelPolicyAproved= Users::getPageCancelPolicyAproved();//for CancelPolicy
        
        $getPageTermConditionAproved= Users::getPageTermConditionAproved();//for CancelPolicy
                     
?>
<script type="text/javascript" 	src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".flip1").click(function() {
			$(".panel1").slideToggle("slow");
		});
                
                $(".flip2").click(function() {
			$(".panel2").slideToggle("slow");
		});
                
                $(".flip3").click(function() {
			$(".panel3").slideToggle("slow");
		});
                
                $(".flip4").click(function() {
			$(".panel4").slideToggle("slow");
		});
	});
        
</script>
<style>
.btn-primary.active {
            background-color: #e34724;
    }
.btn_red.active {
  background-color: red;
   }
   .form-inline .form-group input[type=text] {
	width:230px;
}
  
</style>
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span>Manage Tour</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">  
               
                 
                 
                <div class="row">
                 <div class="panel panel-default"> 
                <div class="panel-body">
                <div class="col-sm-8" >
                                            <div class="btn-group btn-group-justified">  
                                                <a href="createTour1.php" class="btn btn-primary btn-lg active"  style=" font-weight: bold;">Tour Detail</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Timing</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Image</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Transfer Option</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Buying Price</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Selling Price</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Discount</a>
                                            </div>                                         
                                    </div>
                       </div>
                       </div>             
                                     <div class="panel-heading">                                
                                                <div class="col-sm-4" >
                                                    <h4><strong>Add New Tour</strong></h4>
                                                 </div>
                                     </div> 
                
               
                <div class="panel panel-default">                                  
                <div class="panel-body">
                    
    <!-- first part start here -->
    <form id="jvalidate_tour" role="form" class="form-inline" action="lib/scripts/php/all/createTour1.php">
    <div class='row'>
        <div class='col-sm-4'>    
            <div class='form-group'>
                <label for="user_title">Select Country:</label>
                 <select class="select" name="countryid">

                                                 <?php 
                                                 foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>"><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>    
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_firstname">Select City:</label>
                <select class="select" name="cityid">
                                               <?php 
                                                 foreach($getSystemCity as $SystemCity)
                                                      { ?>  
                                                <option value="<?php echo $SystemCity['id'];?>"><?php echo $SystemCity['cityname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_lastname">Select Tour Category:</label>
                <select class="select" name="tourcatid">
                                        <?php 
                                                 foreach($getSystemTC as $SystemTC)
                                                      { ?>  
                                                <option value="<?php echo $SystemTC['id'];?>"><?php echo $SystemTC['tourcatname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>         
            </div>
        </div>
    </div>
    <br/> <br/> <br/> 
    <!-- first part End here --> 
    <!-- first II start -->
    
     <div class='row'>
        <div class='col-sm-4'>    
            <div class='form-group'>
                <label for="user_title">Full Day / Half Day:</label>
                 <select class="select" name="fhday">
                                                <option value="1">Full Day</option>
                                                <option value="0">Half Day</option>
                                                
                                            </select>     
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_firstname">Tour Name:</label>
                <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger">Duplicate Tour Name, Please Try Another Tour name</span> <?php }?>
                <input type="text" class="form-control mx-sm-10"   name="tourname"/>
                <span class="help-block">min size = 20, max size = 25</span>
                                                           
                                            
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_lastname">Cancel Policy:</label>
                 <select class="select" name="cancelpolicy">

                                                 <?php 
                                                 foreach($getCancelPolicyAproved as $CancelPolicyAproved)
                                                      { ?>  
                                                <option value="<?php echo $CancelPolicyAproved['id'];?>"><?php echo $CancelPolicyAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>           
            </div>
        </div>
        
    </div>
    
    <!-- first second end here -->
    <!-- Second part start -->
    
    <br/> <br/> <br/>
    <!--- second part end -->
    <!-- third part start -->
     <div class='row'>
        
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_firstname">Term & Condition:</label>
                 <select class="select" name="termcond">

                                                 <?php 
                                                 foreach($getPageTermConditionAproved as $TermConditionAproved)
                                                      { ?>  
                                                <option value="<?php echo $TermConditionAproved['id'];?>"><?php echo $TermConditionAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                              
                                            
            </div>
        </div>
        

        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_lastname">CutOff Day:</label>
                <input type="text" max="10" class="form-control numericOnly" name="cutoffday" value=""/>
                                            <span class="help-block">min size = 20, max size = 25</span>     
            </div>
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
    <br/> <br/> <br/>
    <!--- start fourth row here-->
    <div class='row'>
        
       <div class='col-sm-4'>    
            <div class='form-group'>
            <label class="checkbox-inline"><input type="checkbox" name = "islive"  value="">Is Live</label>
			<label class="checkbox-inline"><input type="checkbox" name="recommended" value="">Recommended to Bestselling</label>
			<label class="checkbox-inline"><input type="checkbox" name="mealincluded" value="">Meal Included</label>
                
            </div>
        </div>
      
        

        <div class='col-sm-4'>
            
        </div>
        <div class='col-sm-4'>    
           
        </div>
    </div>
    <br/> <br/> <br/>
    <!-- end fourth row here -->
    <div class="panel-group accordion">
                                    
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColOne">
                                               + Important Information
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColOne">
                                       <textarea class="ckeditor" rows="5" name="content1" ></textarea>
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColTwo">
                                                + Itinerary Description
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColTwo">
                                        <textarea class="ckeditor" rows="5" name="content2" ></textarea>
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColThree">
                                                + Description
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColThree">
                                          <textarea class="ckeditor" rows="5" name="content3" ></textarea>
		
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                          
                            <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColFour">
                                               + Inclusion
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColFour">
                                          <textarea class="ckeditor" rows="5" name="content4" ></textarea>
		
                                    </div>                                
                                </div>
                            </div>
                            </div>
    <!-- Thrid Part End                   -->
    <div class='row'>
        
        <div class='col-sm-1'>
           
        </div>
        
        <div class='col-sm-7'>
            <div class="btn-group">
                                       <button name="submit" value="1" class="btn btn-primary" type="submit">Save & Next</button>
                                    </div>
                                    <div class="btn-group">
                                        <button name="savexit" value="1" class="btn btn-primary" type="submit">Save & Exit</button>
                                    </div>
                                    <div class="btn-group">
                                        <a href="managetour.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div> 
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
   
                                    
    
    </form>

                    </div>    
                    </div>                            
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add New Tour</strong></h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <!--<div class="panel-body">
                                   
                                </div>-->
                                <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Select Country:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                    <select class="form-control select" name="countryid">

                                                 <?php 
                                                 foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>"><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>  
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Select Tour Category:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                    <select class="form-control select" name="tourcatid">

                                                 <?php 
                                                 foreach($getSystemTC as $SystemTC)
                                                      { ?>  
                                                <option value="<?php echo $SystemTC['id'];?>"><?php echo $SystemTC['tourcatname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>  
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            
                                             
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tour Name:</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        
                                                     <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger">Duplicate Tour Name, Please Try Another Tour name</span> <?php }?>
                                                  <input type="text" class="form-control"   name="tourname"/>
                
                                                    </div>                                            
                                                    <span class="help-block">min size = 20, max size = 25</span>
                                                </div>
                                            </div>
                                                                         
                                            
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Term & Condition:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                    <select class="form-control select" name="termcond">

                                                   <?php 
                                                 foreach($getPageTermConditionAproved as $TermConditionAproved)
                                                      { ?>  
                                                <option value="<?php echo $TermConditionAproved['id'];?>"><?php echo $TermConditionAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>  
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                           <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-9">                                                                                                                                        
                                                   
                                                    <label class="checkbox-inline"><input type="checkbox" class="icheckbox" name = "islive"  value=""/>		Is Live</label>
			<label class="checkbox-inline"><input type="checkbox" name="recommended" class="icheckbox" value=""/>		Recommended to Bestselling</label>
			<label class="checkbox-inline"><input type="checkbox" name="mealincluded" class="icheckbox" value=""/>		Meal Included</label>
                                                    <!-- <span class="help-block">Checkbox sample, easy to use</span>-->
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                            
                                                
                                                
                                            
                                                <label class="col-md-3 control-label">Select City:</label>
                                                <div class="col-md-9">                                                                                            
                                                    <select class="form-control select" name="cityid">
                                                        <?php 
                                                 foreach($getSystemCity as $SystemCity)
                                                      { ?>  
                                                <option value="<?php echo $SystemCity['id'];?>"><?php echo $SystemCity['cityname'];?></option>
                                                <?php } ?>
                                                    </select>
                                                    <!--<span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Full Day / Half Day:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                    <select class="form-control select" name="fhday">

                                                  <option value="1">Full Day</option>
                                                <option value="0">Half Day</option>
                                                
                                                
                                            </select>  
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                             
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Cancel Policy:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                    <select class="form-control select" name="cancelpolicy">

                                                  <?php 
                                                 foreach($getCancelPolicyAproved as $CancelPolicyAproved)
                                                      { ?>  
                                                <option value="<?php echo $CancelPolicyAproved['id'];?>"><?php echo $CancelPolicyAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>  
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">CutOff Day:</label>
                                                <div class="col-md-9">                                            
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        
                                                     
                                                  <input type="text" max="10" class="form-control numericOnly"   name="cutoffday" value=""/>
                
                                                    </div>                                            
                                                    <span class="help-block">min size = 20, max size = 25</span>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        
                                    </div>
                                    <br/><br/><br/>
<div class="panel-group accordion">
                                    
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColOne">
                                               + Important Information
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColOne">
                                       <textarea class="ckeditor" rows="5" name="content1" ></textarea>
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColTwo">
                                                + Itinerary Description
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColTwo">
                                        <textarea class="ckeditor" rows="5" name="content2" ></textarea>
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColThree">
                                                + Description
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColThree">
                                          <textarea class="ckeditor" rows="5" name="content3" ></textarea>
		
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                          
                            <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColFour">
                                               + Inclusion
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColFour">
                                          <textarea class="ckeditor" rows="5" name="content4" ></textarea>
		
                                    </div>                                
                                </div>
                            </div>
                            </div>
                                </div>
                                <div class="panel-footer">
                                    <div class='row'>
        
        <div class='col-sm-1'>
           
        </div>
        
        <div class='col-sm-7'>
            <div class="btn-group">
                                       <button name="submit" value="1" class="btn btn-primary" type="submit">Save & Next</button>
                                    </div>
                                    <div class="btn-group">
                                        <button name="savexit" value="1" class="btn btn-primary" type="submit">Save & Exit</button>
                                    </div>
                                    <div class="btn-group">
                                        <a href="managetour.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div> 
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  


<?php	require_once("footer.php");
?>
                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        
        
                     
        
        <script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->
        
