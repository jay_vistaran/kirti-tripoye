<?php
	require_once("header.php");
	if(!isset($_GET['id']) || $_GET['id']=='' )
            {
                    header("location:comboadd.php"); 
            }
        $comboid=$_GET['id'];
		$today = date('Y-m-d');

        //global $cleaned;
	//$getCancelPolicyAproved= Users::getPageCancelPolicyAproved();//for CancelPolicy
        $getSystemCountry = Users::getSystemCountryAproved();//for global
         
        $getSystemCity = Users::getSystemCityAproved();//for global
         
        $getSystemTC= Users::getTourCategoryAproved();//for global   
		$getSystemComboTour=Users::getSystemComboTById($comboid);
       $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid =$getSystemDefaultSeason['id'];
		}
		else{
			$sid =0;
		}
	    //echo "<pre>";
		//print_r($getSystemComboTour);die; 
		if (isset($getSystemComboTour['tourorder']) &&  $getSystemComboTour['tourorder']!= "") {
				$getSystemComboTour['tourorder'] = explode(",",$getSystemComboTour['tourorder']);
				
		}
		//echo "<pre>";
		//print_r($getSystemComboTour['tourorder']);die;
      /////////////////
                     
?>


<style>
.btn-primary.active {
            background-color: #e34724;
    }
.btn_red.active {
  background-color: red;
   }
   .form-inline .form-group #tourname {
	width:230px;
}
  
</style>
<link rel="stylesheet" href="tabletest/css/style.css">
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<style>
    .content {
	
    margin-left: -37px;
    margin-right: 30px;
    padding: 10px;
    text-align: center;
}
    /* Sortable ******************/
#sortable { 
	list-style: none; 
	text-align: left; 
}
#sortable li { 
	margin: 0 0 10px 0;
	height: 75px; 
	background: #dbd9d9;
	border: 1px solid #999999;
	border-radius: 5px;
	color: #333333;
}

#sortable li img {
	height: 65px;
	border: 5px solid #cccccc;
	display: inline-block;
	float: left;
}
#sortable li div {
	padding: 5px;
}
#sortable li h2 {    
	font-size: 16px;
    line-height: 20px;
}
</style>
  <script>
  /* $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
  */
  $(function() {
    $('#sortable').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function(event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            document.getElementById("toursortingorder").value = list_sortable;
    		// change order in the database using Ajax
            
        }
    }); // fin sortable
}); 
  </script>
  <?php
  $getSystemComboTourCount=Users::getSystemComboTById($comboid);  ?>
				
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2>Manage Combo</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">  
               
                 
                 
                <div class="row">
                 <div class="panel panel-default"> 
                <div class="panel-body">
                <div class="col-md-8" >
                


                                            <div class="btn-group btn-group-justified">  
                                              <a href="comboedit.php?id=<?php echo $comboid; ?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Combo Details</a>
                                                <a href="<?php  if (isset($getSystemComboTourCount) && is_array($getSystemComboTourCount) && count($getSystemComboTourCount)>0) {?>comboedit1.php?id=<?php echo $comboid; ?><?php }else{?>comboadd1.php?id=<?php echo $comboid; ?> <?php } ?>" class="btn btn-primary btn-lg active"  style=" font-weight: bold;">Select Tour</a>
                                                <a href="comboadd2.php?id=<?php echo $comboid; ?>&sId=<?php echo $sid; ?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Combo Pricing</a>                                        </div>                                         
                                    </div>
                       <div class="col-md-4" ></div>            
                       </div>
                       </div>             
                                    
                
               
                                            
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                           <form id="jvalidate_tour123" role="form" class="form-horizontal" method="post" action="lib/scripts/php/all/comboedit1.php" onsubmit="return checkMinimumTour()">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Select Tour</strong></h3>
                                    <!--<ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul> -->
                                </div>
                                <!--<div class="panel-body">
                                   
                                </div>-->
                                <div class="panel-body">                                                                        
                                     <input type="hidden" name="comboid" value="<?php echo $comboid; ?>"/>
                                     <input type="hidden" name="toursortingorder"  id = "toursortingorder" />
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Select Country:</label>
                                                <div class="col-md-9">                                                                                            
                                                   
                                                     
                                                    <select class="form-control select" name="countryid" id="countryid" onChange="js_country_city(this.value)">
                 	<option value="">Select Country</option>

                                                 <?php 
                                                 foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>"><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select> 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Select Tour Category:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                    <select class="form-control select" name="tourcatid" id="tourcatid" >
                                        <?php 
                                               /*  foreach($getSystemTC as $SystemTC)
                                                      { ?>  
                                                <option value="<?php echo $SystemTC['id'];?>"><?php echo $SystemTC['tourcatname'];?></option>
                                                <?php } */?>
												
                                                
                                                
                                            </select>                             
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                             
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Select Tour:</label>
                                                <div class="col-md-9"><select class="form-control select" name="tourname" id="tourname"></select> </div>
                                            </div>
                                             
                                            
                                           
                                                                         
                                            
                                             
                                           
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                            
                                                
                                                
                                            
                                                <label class="col-md-3 control-label">Select City:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                     <select class="form-control select" name="cityid" id="cityid" onChange="js_tour_category(this.value)">
              
                                                
                                                
                                                
                                            </select>  
                                                    <!--<span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <!-- <label class="col-md-3 control-label"></label>-->
                                                <div class="col-md-3">                                                                                            
                                                    
                                                   
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                   <button  name="FindTour" value="1" class="btn btn-primary btn-lg" type="button" onClick="js_tour_name()">Find Tour</button>
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                             
                                             <div class="form-group">
                                                <div class="col-md-3">                                                                                            
                                                    
                                                   
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                   <span><button type="button" class="btn btn-primary btn-lg" name="btn_js_addTour" onClick="js_addTour()">Add</button></span>
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            
                                            
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class='row'>
    
        
       <div class='col-sm-4'>    
            <h4><strong>Added Tour</strong></h4>
            <div id="message-box-tour" style="display:none" >
            <span class="alert alert-danger" style="font-size:13px"  >Please Add Minimum Two Unique Tour.</span>
            </div>
<?php if(isset($_REQUEST['msg'])){?><span class="alert alert-danger" style="font-size:13px">Please Add Minimum Two Unique Tour.</span><br/><br/><br/> <?php }?>
        </div>
       
        

        <div class='col-sm-4'>
            
        </div>
        <div class='col-sm-4'>    
           
        </div>
       
    </div>
                                        
                                    
                                    

                                
                                <table class="responstable" id="sortabletablebannerHead">
   <thead>
      <tr>
        <th>Tour Name</th>
                                                <th>ORDER</th>
                                                <th>Remove</th>
                                                
      </tr>
      </thead>
  
    <!--<tfoot>
      <tr class="ui-state-default">
        <th colspan="4">Original</th>
        <th colspan="4">table footer</th>
        <th colspan="4">table footer</th>
        <th colspan="4">table footer</th>
        <th colspan="4">table footer</th>
        <th colspan="4">Current Pos</th>
      </tr>
    </tfoot>-->
  
    <tbody id="select_tour_combo">
              <?php
                    if(isset($getSystemComboTour['tourorder']) && is_array($getSystemComboTour['tourorder']) && count($getSystemComboTour['tourorder'])>0){
						foreach( $getSystemComboTour['tourorder'] as $key => $value ) {
							
							$tourdetails =  Users::getTourByID($value);
							//echo "<pre>";print_r($tourdetails);
			?>		
            		<tr id="<?php echo $tourdetails['id']; ?>"><td><?php echo $tourdetails['tourname']; ?></td> <td><span style="background-image: url('img/drag.png');background-repeat: no-repeat;background-position: center;width: 30px;height: 30px; display: inline-block;float: left;cursor: move;"></span></td><td><a href="#" type="submit" onClick="return delete_rowbanner('<?php echo $tourdetails['id']; ?>');"><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td><input type="hidden" name="arraytourname[]" value="<?php echo $tourdetails['id']; ?>" </tr>
							
						<?php	
						}
						
					}
			//die;
			?>        
                            
    
        
                            
    </tbody>
   
</table>
</div>
</div>
                              <div class="panel-footer">
                                    <div class='row'>
        
        <div class='col-sm-1'>
           
        </div>
        
        <div class='col-sm-7'>
            <div class="btn-group">
                                       <button name="submit" value="1" class="btn btn-primary" type="submit">Save & Next</button>
                                    </div>
                                    <div class="btn-group">
                                        <button name="savexit" value="1" class="btn btn-primary" type="submit">Save & Exit</button>
                                    </div>
                                    <div class="btn-group">
                                        <a href="combo.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div> 
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  
<?php	require_once("footer.php");
?>
<script>	


	/*function delete_rowbanner(id)
                        {
							var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
								   //parent = document.getElementById("sortable");
							       //child =document.getElementById(id);parent.removeChild(child);
								   document.getElementById(id).outerHTML="";
                                                                   
                                }
                                else
                                {
                                        return false;
                                }
							
							
                        }
						*/
						
	function checkMinimumTour() {
			//alert("hello");
			var countTour = document.getElementById("select_tour_combo").childElementCount;
			if (countTour<2){
				//alert(countTour);
				document.getElementById("message-box-tour").style.display="block";
				return false;
			}
			

			
		}	
		
		 $( "#select_tour_combo" ).sortable({
	update: function( event, ui ) {
		//alert("hello");
	}
		  });				
		
</script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'></script>
<script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
 <script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
<script>

function delete_rowbanner(id) {
                    noty({
                        text: 'Are you sure want to delete',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									   document.getElementById(id).outerHTML="";
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }	
/*function delete_rowbanner(id)
                        {
							var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
								   //parent = document.getElementById("sortable");
							       //child =document.getElementById(id);parent.removeChild(child);
								   document.getElementById(id).outerHTML="";
                                                                   
                                }
                                else
                                {
                                        return false;
                                }
							
                        }*/
						
function js_country_city(counterid) {
	selectCity = document.getElementById('cityid');
	document.getElementById('tourcatid').innerHTML = "";
	$("#tourcatid").selectpicker('refresh');
$.ajax({
						url: "lib/scripts/php/all/ajaxCityOnCountryId.php",
						type: "POST",
						data:'CountryId='+counterid,
						success: function(data){
						//alert(data);	
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var status = duce.status;
											var cityoptions =duce.cityoptions;
                                    												
								   }
								   if (status == 1 ) {
									   selectCity.innerHTML = cityoptions;
									   $("#cityid").selectpicker('refresh');
								   }else {
									  noty({
                        text: 'Select City',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									 $noty.close();
                                       
                                }
                                },
                                
                                
                            ]
                    })
									   
								   }
								   
		
											
											} 
										   
						       
				   });

}

function js_tour_category(cityid) {
//alert("tourcatid");	
tourcatid = document.getElementById('tourcatid');
$.ajax({
						url: "lib/scripts/php/all/ajaxTourCategoryOnCityId.php",
						type: "POST",
						data:'cityid='+cityid,
						success: function(data){
						//alert(data);	
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var status = duce.status;
											var tourcatoptions =duce.tourcatoptions;
                                    												
								   }
								   if (status == 1 ) {
									   tourcatid.innerHTML = tourcatoptions;
									   $("#tourcatid").selectpicker('refresh');
								   }else {
									   noty({
                        text: 'Select City',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									 $noty.close();
                                       
                                }
                                },
                                
                                
                            ]
                    })
									  // alert("Select City");
									   
								   }
								   
		
											
											} 
										   
						       
				   });
	
}
function js_tour_name() {
	//alert("hello");
	tourcatid = document.getElementById('tourcatid').value;
	cityid = document.getElementById('cityid').value;
	countryid =  document.getElementById('countryid').value;
	tourname= document.getElementById('tourname');
	//alert(tourcatid);
	//alert(cityid);
	//alert(countryid);
	
$.ajax({
						url: "lib/scripts/php/all/ajaxTourNameOnCountryCityTourCategoryId.php",
						type: "POST",
						data:'cityid='+cityid+'&tourcatid='+tourcatid+'&countryid='+countryid,
						success: function(data){
						//alert(data);	
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var status = duce.status;
											var tournameoptions =duce.tournameoptions;
                                    												
								   }
								   if (status == 1 ) {
									   tourname.innerHTML = tournameoptions;
									   $("#tourname").selectpicker('refresh');
								   }else {
									   noty({
                        text: 'There is No Tour exist.',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									 $noty.close();
                                       
                                }
                                },
                                
                                
                            ]
                    })
									  // alert("There is No Tour exist");
									   tourname.innerHTML ="";
									   $("#tourname").selectpicker('refresh');
									   
								   }
								   
		
											
											} 
										   
						       
				   });
	
}

function js_addTour() {
	document.getElementById("message-box-tour").style.display="none";
	var addTourID=document.getElementById("tourname").value; 
	if ( addTourID > 0 ) {
					$.ajax({
						url: "lib/scripts/php/all/ajaxCouponAddTour.php",
						type: "POST",
						data:'TourId='+addTourID,
						success: function(data){
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			 duce = jQuery.parseJSON(data);
                                   			 tourname = duce.tourname;
                                    												
								   }
								  //alert(tourname);
		
											if (document.getElementById(addTourID)) {
												 noty({
                        text: 'This Tour already exist.',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									 $noty.close();
                                       
                                }
                                },
                                
                                
                            ]
                    })
												
												//alert("This Tour already exist");
												return false;
											}
							
		
											var table = document.getElementById("sortabletablebannerHead").getElementsByTagName('tbody')[0];;
											//var table=document.getElementById("data_tableED");
                                            var table_len=(table.rows.length);
                                             var row = table.insertRow(table_len).outerHTML= '<tr id="'+addTourID+'" style="height: 40px;">\n\<td >'+tourname+'</td>\n\<td><span style="background-image: url(\'img/drag.png\');background-repeat: no-repeat;background-position: center;width: 30px;height: 30px; display: inline-block;float: left;cursor: move;"></span></td>\n\<td><a href="#" type="submit" onClick=\'return delete_rowbanner('+addTourID+');\'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>\n\<input type="hidden" name="arraytourname[]" value=\''+addTourID+'\'</tr>';} 
										   });
	}
}						
</script>

