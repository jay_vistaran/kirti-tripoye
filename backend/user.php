<?php
	require_once("header.php");
	//for pagination
	if(isset($_GET["page"]))
	$page = (int)$_GET["page"];
	else
	$page = 1;
	$setLimit = 10;
	$pageLimit = ($page * $setLimit) - $setLimit;
	$serial = (($page-1) * $setLimit) + 1;
	//end pagination
        //global $cleaned;
	$getSystemUsers = Users::getSystemUsers($pageLimit, $setLimit);//for global
	//echo "<pre>";print_r($getSystemUsers);die;
        //$website_users=count($getSystemUsers);
                //$SQL = "SELECT * FROM `users` WHERE `user_account_disabled` = '0' ORDER BY `user_name`, `user_surname` ASC";
		//$user=mysql_query($SQL);
                //$result=mysql_fetch_array($user);
                //$website_users=mysql_num_rows($user);
         
?>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Users</h3>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                         <a href="userAdd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add New User</button></a>
                                     </div>
                                      
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <form name="edit_cat_form" id="edit_cat_form" action="" method="post">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>DOB</th>
                                                <th>Mobile</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getSystemUsers as $NewSystemUsers){?>    
                                            <tr>
                                                <td><?php echo $serial;?></td>
                                                <td><?php echo $NewSystemUsers['user_name'] . '&nbsp;' .$NewSystemUsers['user_surname'];?></td>
                                                <td><?php echo $NewSystemUsers['user_email'];?></td>
                                                <td><?php echo $NewSystemUsers['user_address'];?></td>
                                                <td><?php echo $NewSystemUsers['user_dob'];?></td>
                                                <td><?php echo $NewSystemUsers['user_mobile'];?></td>
                                                <td><a href="userEdit.php?id=<?php echo $NewSystemUsers['user_id'];?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                                                    
                                                    <a href="#" type="submit" onClick='return deletemode(<?php echo $NewSystemUsers['user_id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>
                                            </tr>
                                        <?php  $i++; $serial++;}?>    
                                         <tr><td colspan="7"><?php echo Users::displayPaginationBelow($setLimit,$page,'users');?></td></tr>
                                        </tbody>
                                    </table></form>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->  


             <script type="text/javascript">
                
                        function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This ");
                                if(conf)
                                {
                                        window.location.href="lib/scripts/php/all/users-actions.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }
</script>

<?php	require_once("footer.php");
?>