<?php

        session_start();

	require_once("lib/config.php");

	require_once("lib/scripts/php/functions.php");

        require_once("lib/scripts/php/new_functions.php");

	checkLogin();	

	//dump($_SESSION);

	//dbConnect();

        if($_REQUEST['action'] == "logout")

            {

                checkLogin();

            }

     

?>

<!DOCTYPE html>

<html lang="en">

    <head>        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- META SECTION -->

        <title>Traveloye - Admin </title>            

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <meta name="viewport" content="width=device-width, initial-scale=1" />

        

        <link rel="icon" href="favicon.ico" type="image/x-icon" />

        <!-- END META SECTION -->

        

        <!-- CSS INCLUDE -->        

        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>

        <!-- EOF CSS INCLUDE -->  

        

        

        <!-- CK EDITOR Added By VV -->

        <style>

            #cke_content{

                width: 840px;/* For the widht of FCK editor*/

            }

            #cke_content1{

                width: 840px;/* For the widht of FCK editor*/

            }

            #cke_content2{

                width: 840px;/* For the widht of FCK editor*/

            }

            #cke_content3{

                width: 840px;/* For the widht of FCK editor*/

            }

            #cke_content4{

                width: 840px;/* For the widht of FCK editor*/

            }

        </style>

            <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

	    

       <!-- CK EDITOR Added By VV -->

    </head>

    <body>

        <!-- START PAGE CONTAINER -->

        <div class="page-container">

            

            <!-- START PAGE SIDEBAR -->

            <div class="page-sidebar">

                <!-- START X-NAVIGATION -->

                <ul class="x-navigation">

                    <li class="xn-logo">

                        <a href="landing.php">Tour</a>

                        <a href="#" class="x-navigation-control"></a>                    </li>

                    <li class="xn-profile">

                        <a href="#" class="profile-mini">

                            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>                        </a>

                        <div class="profile">

                            

                            <div class="profile-data">

                                <div class="profile-data-name"><?php echo $_SESSION['Tour-Track']['user_name'];?></div>

                                <div class="profile-data-title">Web Developer/Designer</div>

                            </div>

                            

                        </div>                                                                        

                    </li>

                    <li class="xn-title">Navigation</li>

                    <li class="active">

                        <a href="landing.php"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                    </li> 

                    
                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Order</span></a>

                        <ul>

                            <li><a href="orders.php"><span class="fa fa-bars"></span>Order</a></li> 

                        </ul>

                    </li>                   

                    <li class="xn-openable">

                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">User Administrator</span></a>

                        <ul>

                            <li><a href="userAdd.php"><span class="fa fa-image"></span> Add User</a></li>

                            <li><a href="user.php"><span class="fa fa-user"></span> User List</a></li>
                    

                        </ul>

                    </li>
					
						   <li class="xn-openable">

                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text"> Section Manager</span></a>

                        <ul>

                            <li class="xn-openable">
							<a href="#"><span class="fa fa-image"></span> <span  class="xn-text">Home Page</span></a>
							<ul>
						 		<li><a href="trendingAdd.php?id=1"><span class="fa fa-user"></span> Trending destination</a></li>
							    <li><a href="hotSellingAdd.php?id=3"><span class="fa fa-user"></span> Hot Seeling Packages</a></li>
							  
								<li><a href="exclusiveComboAdd.php?id=2"><span class="fa fa-user"></span> Exclusive Combo Deals</a></li>

							
							 </ul>
							</li>
							
						<li class="xn-openable">
							<a href="#"><span class="fa fa-image"></span> <span  class="xn-text">City Page</span></a>
							<ul>
						 		<li><a href="manageCollectionByCity.php"><span class="fa fa-user"></span>Trip Oye Collections</a></li>
							    
								  <li><a href="manageHotSellingByCity.php"><span class="fa fa-user"></span>Hot Seeling Packages</a></li>
								<li><a href="manageComboByCity.php"><span class="fa fa-user"></span> Exclusive Combo Deals</a></li>
								
									<li><a href="manageCreateSectionByCity.php"><span class="fa fa-user"></span> Create Section </a></li>


							
							 </ul>
							</li>
                            

                                                   

                        </ul>

                    </li>
					
					
					

                    <li class="xn-openable">

                    

                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Global Settings</span></a>

                        <ul>

                            <li><a href="country.php"><span class="fa fa-cogs"></span> Manage Country</a></li>

                            <li><a href="city.php"><span class="fa fa-cogs"></span> Manage City</a></li>

                            <li><a href="tourcategory.php"><span class="fa fa-cogs"></span> Tour Category</a></li>

                            <li><a href="season.php"><span class="fa fa-cogs"></span> Manage Season</a></li>

                            <li><a href="transfercategory.php"><span class="fa fa-cogs"></span> Transfer Option</a></li>

                            <li><a href="#"><span class="fa fa-cogs"></span> Tour Category Sorting</a></li>

                            <li><a href="#"><span class="fa fa-cogs"></span> Manage Combos</a></li>

                            <li><a href="page.php"><span class="fa fa-cogs"></span> Manage Pages</a></li>

                            <li><a href="phone.php"><span class="fa fa-cogs"></span> Organize Phone Number</a></li>

                            

                        </ul>

                    </li>

                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Rate of Exchange </span></a>

                        <ul>

                            <li><a href="newcurrencyAdd.php"><span class="fa fa-users"></span> Add New Currency</a></li>

                            <li><a href="rateofexchange.php"><span class="fa fa-users"></span> Rate of Exchange</a></li>

                            

                        </ul>

                    </li>

                    

                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Manage Tour </span></a>

                        <ul>

                            <li><a href="createTour1.php"><span class="fa fa-users"></span> Add Tour</a></li>

                            <li><a href="managetour.php"><span class="fa fa-users"></span> Manage Tour</a></li>

                        </ul>

                    </li>



                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-archive"></span> <span class="xn-text">Manage Vendor </span></a>

                        <ul>

                            <li><a href="vendorAdd.php"><span class="fa fa-users"></span> Add Vendor</a></li>

                            <li><a href="vendor.php"><span class="fa fa-users"></span> Vendor List</a></li>

                            

                        </ul>

                    </li>

                    

                   <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-random"></span> <span class="xn-text">Manage App Banner </span></a>

                        <ul>

                           

                            <li><a href="appbanner.php"><span class="fa fa-users"></span> App Banner</a></li>

                            

                        </ul>

                    </li>

                     <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-square-o"></span> <span class="xn-text">Coupon Management </span></a>

                        <ul>

                            <li><a href="couponadd.php"><span class="fa fa-square-o"></span> Add Coupon</a></li>

                            <li><a href="coupon.php"><span class="fa fa-square-o"></span> Coupon Management</a></li>

                            

                        </ul>

                    </li>



<li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Combo </span></a>

                        <ul>

                            <li><a href="comboadd.php"><span class="fa fa-users"></span> Add Combo</a></li>

                            <li><a href="combo.php"><span class="fa fa-users"></span> Manage Combo</a></li>

                            

                        </ul>

                    </li>

					<li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Report</span></a>
                        <ul>

                            <li><a href="vreport.php"><span class="fa fa-users"></span>Booking Report</a></li>

                            

                        </ul>
                        
                    </li>

                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Rating</span></a>

                        <ul>

                            <li><a href="rating.php"><span class="fa fa-users"></span>Rating</a></li> 

                        </ul>

                    </li>    


  <li class="xn-openable">

                        

                        <a href="manageNewsLetter.php"><span class="fa fa-file-text-o"></span> <span class="xn-text">Newletter</span></a>

                      

                    </li>




                   <!--  <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Order </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                    

                    

                     <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Review </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                     <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Review </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                     <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Auto Email/SMS/PUSH </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                    

                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text"> Manage APP Banner </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Chat </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Contest </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                     <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Addon Master </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                    <li class="xn-openable">

                        

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Manage Report </span></a>

                        <ul>

                            <li><a href="ml_city.php"><span class="fa fa-users"></span> City</a></li>

                            <li><a href="ml_location.php"><span class="fa fa-users"></span> Location</a></li>

                            

                        </ul>

                    </li>

                    

                   <li class="xn-openable">

                    

                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Category</span></a>

                        <ul>

                            <li><a href="category.php"><span class="fa fa-edit"></span> Category</a></li>

                            

                            <li><a href="sub_category.php"><span class="fa fa-pencil"></span> SubCategory</a></li>

                            

                        </ul>

                    </li>

		  

					<li class="xn-openable">

                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Ad Manage</span></a>

                        <ul>

                            <li><a href="adsAdd.php"><span class="fa fa-image"></span> Add Ads</a></li>

                            <li><a href="adslist.php"><span class="fa fa-th"></span> Ads List</a></li>

                        </ul>

                    </li>

                    

                    <li class="xn-openable">

                        <a href="#"><span class="fa fa-random"></span> <span class="xn-text">Business</span></a>

                         

                        <ul>

                            <li><a href="businessAdd.php"><span class="fa fa-pencil"></span> Add Business</a></li>

                            <li><a href="business.php"><span class="fa fa-arrows-h"></span> Business List</a></li>

                            <li><a href="businessNA.php"><span class="fa fa-list-ul"></span> Pending Business List</a></li>

                        </ul>

                    </li>

                    

                    <li class="xn-openable">

                        <a href="#"><span class="fa fa-th"></span> <span class="xn-text">Issue Manage</span></a>

                         

                        <ul>

                            

                            <li><a href="issuelist.php"><span class="fa fa-list-ul"></span> Issue List</a></li>

                        </ul>

                    </li>

                    -->

                                        

                    

                    

                                        

                                       

                    

                </ul>

                <!-- END X-NAVIGATION -->

            </div>

            <!-- END PAGE SIDEBAR -->

            

            <!-- PAGE CONTENT -->

            <div class="page-content">

                

                <!-- START X-NAVIGATION VERTICAL -->

                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">

                    <!-- TOGGLE NAVIGATION -->

                    <li class="xn-icon-button">

                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>                    </li>

                    <!-- END TOGGLE NAVIGATION -->

                    

                    <!-- SIGN OUT -->

                    <li class="xn-icon-button pull-right">

                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                    </li> 

                    <!-- END SIGN OUT -->

                   

                </ul>

                <!-- END X-NAVIGATION VERTICAL -->                     



                <!-- START BREADCRUMB -->

                <ul class="breadcrumb">

                    <li><a href="#">Home</a></li>                    

                    <li class="active">Dashboard</li>

                </ul>

                <!-- END BREADCRUMB -->                       

                