<?php
	require_once("header.php");
	$getSystemUsers = Users::getSystemUsers();
	$getSystemTour = Users::getSystemTourComboAproved();
	//echo "<pre>";
	//print_r($getSystemUsers);
	//echo "<hr/>";
	//print_r($getSystemTour);
	//die;
        //global $cleaned;
        $SQL = "SELECT * FROM `users` WHERE `user_account_disabled` = '0' ORDER BY `user_name`, `user_surname` ASC";
        $getSystemUsers = MySQL::query($SQL);
	//$getSystemUsers = Users::getSystemUsers();//for global
        $getAdminRoles = Users::getAdminRoles();//for global
           
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<style>
   .rating { 
                border: none;
                float: left;
            }

            .rating > input { display: none; } 
            .rating > label:before { 
                margin: 5px;
                font-size: 1.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }

            .rating > .half:before { 
                content: "\f089";
                position: absolute;
            }

            .rating > label { 
                color: #ddd; 
                float: right; 
            }

            .rating > input:checked ~ label, 
            .rating:not(:checked) > label:hover,  
            .rating:not(:checked) > label:hover ~ label { color: #FFD700;  }

            .rating > input:checked + label:hover, 
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label, 
            .rating > input:checked ~ label:hover ~ label { color: #FFED85;  }    
  </style>
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Add Combo Rating</h4>
                                <form id="jvalidateSeason" role="form" class="form-horizontal" method="POST" action="lib/scripts/php/all/addcomborating.php">
                                <div class="panel-body" style="width: 750px;"> 
                                     <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate Season Name, Please Try Another Season name</span> <?php }?>
                                     
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Comboname :</label>
                                        <div class="col-md-9">
                                            <select class="selectpicker" data-live-search="true" name="ComboId">
                                            	<?php if(isset($getSystemTour) && is_array($getSystemTour) && count($getSystemTour) > 0 )  {
														foreach( $getSystemTour as $key => $value ) {  ?>
                                                        <option data-tokens="<?php echo  $value['comboname']; ?>" value="<?php echo  $value['comboid']; ?>"><?php echo  $value['comboname']; ?></option>
                                                       <?php 
														}
														
												}
													   ?> 
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Username:</label>  
                                        <div class="col-md-9">
                                            <select class="selectpicker" data-live-search="true" name="UserId">
                                            	<?php if(isset($getSystemUsers) && is_array($getSystemUsers) && count($getSystemUsers) > 0 )  {
														foreach( $getSystemUsers as $key => $value ) {  ?>
                                                        <option data-tokens="<?php echo  $value['user_name']." ".$value['user_surname']; ?>" value="<?php echo  $value['user_id']; ?>"><?php echo  $value['user_name']." ".$value['user_surname'];  ?></option>
                                                       <?php 
														}
														
												}
													   ?> 
                                            </select>

                                        </div>
                                    </div>
                                                
                                    
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Rating:</label>  
                                        <div class="col-md-9">
                                            <fieldset id='demo3' class="rating">
                        <input class="stars" type="radio" id="star53" name="rating_tour" value="5" <?php if ($Coupon['tour_rating'] == '5') { echo "checked";} ?>/>
                        <label class = "full" for="star53" title="Awesome - 5 stars"></label>
                        <input class="stars" type="radio" id="star4half3" name="rating_tour" value="4.5" <?php if ($Coupon['tour_rating'] == '4.5') { echo "checked";} ?> />
                        <label class="half" for="star4half3" title="Pretty good - 4.5 stars"></label>
                        <input class="stars" type="radio" id="star43" name="rating_tour" value="4" <?php if ($Coupon['tour_rating'] == '4') { echo "checked";} ?> />
                        <label class = "full" for="star43" title="Pretty good - 4 stars"></label>
                        <input class="stars" type="radio" id="star3half3" name="rating_tour" value="3.5" <?php if ($Coupon['tour_rating'] == '3.5') { echo "checked";} ?> />
                        <label class="half" for="star3half3" title="Meh - 3.5 stars"></label>
                        <input class="stars" type="radio" id="star33" name="rating_tour" value="3" <?php if ($Coupon['tour_rating'] == '3') { echo "checked";} ?> />
                        <label class = "full" for="star33" title="Meh - 3 stars"></label>
                        <input class="stars" type="radio" id="star2half3" name="rating_tour" value="2.5" <?php if ($Coupon['tour_rating'] == '2.5') { echo "checked";} ?>/>
                        <label class="half" for="star2half3" title="Kinda bad - 2.5 stars"></label>
                        <input class="stars" type="radio" id="star23" name="rating_tour" value="2" <?php if ($Coupon['tour_rating'] == '2') { echo "checked";} ?>/>
                        <label class = "full" for="star23" title="Kinda bad - 2 stars"></label>
                        <input class="stars" type="radio" id="star1half3" name="rating_tour" value="1.5" <?php if ($Coupon['tour_rating'] == '1.5') { echo "checked";} ?> />
                        <label class="half" for="star1half3" title="Meh - 1.5 stars"></label>
                        <input class="stars" type="radio" id="star13" name="rating_tour" value="1" <?php if ($Coupon['tour_rating'] == '1') { echo "checked";} ?>/>
                        <label class = "full" for="star13" title="Sucks big time - 1 star"></label>
                        <input class="stars" type="radio" id="starhalf3" name="rating_tour" value="0.5" <?php if ($Coupon['tour_rating'] == '0.5') { echo "checked";} ?> />
                        <label class="half" for="starhalf3" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                                        </div>
                                    </div>
                                    
                                                           
                                     
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Comment:</label>  
                                        <div class="col-md-6">
                                           <textarea class="form-control" rows="5" name="combo_comment"></textarea>
                                            
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Week Days :</label>
                                        <div class="col-md-9">
                                            <input type="checkbox" name="select_all" id="select_all" value="11">  All  &nbsp;
                                            <input type="checkbox" name="none" id="none" value="10">  None
                                        </div>
                                    </div>-->
                                   
<!--                                     <div class="form-group">
                                        <label class="col-md-3 control-label">&nbsp; </label>
                                        <div class="col-md-3" style="width: 555px;">-->
                                            
                                          <?php 
                                             
                                         /* $mydays = array(
                                              1 => 'Monday', 2 => 'Tuesday', 
                                              3 =>'Wednesday', 4 =>'Thursday', 
                                              5 =>'Friday', 6 =>'Saturday', 7 =>'Sunday');
                                          
                                         foreach ($mydays as $id => $wname) {
                                            ?>    
                                           
                                            <input class="wbox" type="checkbox" name="weekdays[]" value="<?php echo $id;?>"> <?php echo $wname;?>
                                           
                                            <?php echo "&nbsp;"; }
                                            */
                                            
                                            ?>
                                          
                                          
                                            
                                           
<!--      <input class="wbox" type="checkbox" name="weekdays[]" value="1"> Monday &nbsp;
                                            <input class="wbox" type="checkbox" name="weekdays[]" value="2"> Tuesday &nbsp;
                                            <input class="wbox" type="checkbox" name="weekdays[]" value="3"> Wednesday &nbsp;
                                            <input class="wbox" type="checkbox" name="weekdays[]" value="4"> Thursday &nbsp;
                                            <input class="wbox" type="checkbox" name="weekdays[]" value="5"> Friday &nbsp;
                                            <input class="wbox" type="checkbox" name="weekdays[]" value="6"> Saturday &nbsp;
                                            <input class="wbox" type="checkbox" name="weekdays[]" value="7"> Sunday &nbsp;-->
                                            
                                           
                                            
<!--                                        </div>                        
                                    </div>-->
                                     
                                     
                                   
                                    <div class="btn-group pull-right">
                                       
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");


?>

<script>
jQuery("#select_all").change(function(){  //"select all" change
    var status = this.checked; // "select all" checked status
    jQuery('.wbox').each(function(){ //iterate all listed checkbox items
        this.checked = status; //change ".checkbox" checked status
        jQuery('.none').checked=false;
    });
});

jQuery("#none").change(function(){  //"select all" change
    var status = this.checked; // "select all" checked status
    jQuery('.wbox').each(function(){ //iterate all listed checkbox items
        this.checked = false; //change ".checkbox" checked status
        jQuery("#select_all")[0].checked = false; //change "select all" checked status to false
    });
    
   
})
jQuery('.wbox').change(function(){ //".checkbox" change
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(this.checked == false){ //if this item is unchecked
        jQuery("#select_all")[0].checked = false; //change "select all" checked status to false
        jQuery("#none")[0].checked = false; //change "select all" checked status to false
    }
    
    
    //check "select all" if all checkbox items are checked
    if (jQuery('.wbox:checked').length == jQuery('.wbox').length ){
        jQuery("#select_all")[0].checked = true; //change "select all" checked status to true
    }
});

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script> 