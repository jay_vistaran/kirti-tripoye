<?php
	require_once("header.php");
        //global $cleaned;
	$getCouponById = Users::getCouponById($_GET['id']);//for Users
       // dump($getCouponById); die;
        $getSystemTour = Users::getSystemTourIsLive();//for Country
         //dump($getSystemTour);
        $getSystemCity = Users::getSystemCityAproved();//for City
         
        $getSystemTC= Users::getTourCategoryAproved();//for TourCategory
          if ( isset($getCouponById['dropdwonid']) ) {
			  
			  $getCouponById['dropdwonid'] =explode(",",$getCouponById['dropdwonid']);
			  
		  }
       //echo "<pre>";
	   //print_r($getCouponById['dropdwonid']);
	   //die;
                     
?>

<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span>Coupon Management</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                 <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                
                                   
                        
                                    <div class="panel-heading">                                
                                                <div class="col-md-4" >
                                                    <h4><strong>Edit Coupon</strong></h4>

                                                     
                                                </div>
                                     </div>
                        
                                    <div class="panel-body">
                                             
                                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                
                                <form id="jvalidate_coupon" role="form" class="form-horizontal" action="lib/scripts/php/all/couponedit.php">
                                <div class="panel-body">                                    
                                                 
                                   <div class="form-group">
                                        <label class="col-md-3 control-label">Coupon Code:</label>  
                                        <div class="col-md-9">
                                             <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger">Duplicate Coupon Code, Please Try Another Coupon Code</span> <?php }?>
                                             <input type="text" class="form-control" name="couponcode" value="<?php echo $getCouponById['couponcode'];?>"/>
                                            <input type="hidden" class="form-control" name="id" value="<?php echo $getCouponById['id'];?>"/>
                                             <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Coupon Type:</label>  
                                        <div class="col-md-9">
                                         <input type="radio" name="coupontype" value="F" <?php if($getCouponById['coupontype']=='F'){echo "checked";}?> > Flat &nbsp;
                                        <input type="radio" name="coupontype" value="P" <?php if($getCouponById['coupontype']=='P'){echo "checked";}?> > Percentage &nbsp;
                                        <input type="radio" name="coupontype" value="A" <?php if($getCouponById['coupontype']=='A'){echo "checked";}?> > Amount    
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Discount Value:</label>  
                                        <div class="col-md-9">
                                         <input type="text" class="form-control" name="discountvalue"  value="<?php echo $getCouponById['discountvalue'];?>"/>    
                                            
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label class="col-md-3 control-label">Minimum Condition:</label>  
                                        <div class="col-md-9">
                                             <input type="radio" name="mincondition" value="CV" <?php if($getCouponById['mincondition']=='CV'){echo "checked";}?>  > Cart value &nbsp;
                                              <input type="radio" name="mincondition" value="MP" <?php if($getCouponById['mincondition']=='MP'){echo "checked";}?> > Min Pax
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Minimum Condition Value:</label>  
                                        <div class="col-md-9">
                                         <input type="text" class="form-control" name="minconditionvalue"  value="<?php echo $getCouponById['minconditionvalue'];?>"/>    
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Max Number of Uses:</label>  
                                        <div class="col-md-9">
                                         <input type="text" class="form-control" name="maxnumofuses"  value="<?php echo $getCouponById['maxnumofuses'];?>"/>    
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Booking From :</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="bookingfrom"  value="<?php echo $getCouponById['bookingfrom'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Booking To :</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="bookingto"  value="<?php echo $getCouponById['bookingto'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
                                     
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Travelling From :</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="travellingfrom"  value="<?php echo $getCouponById['travellingfrom'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Travelling To :</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="travellingto"  value="<?php echo $getCouponById['travellingto'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Applicable To:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="applicableto123" onchange="show_divat(this.value);"  id="applicableto123" disabled>
                                                <option value="0">Select</option>
                                                <option value="1" <?php if($getCouponById['applicableto']=='1'){echo "selected";}?> >City</option>
                                                <option value="2" <?php if($getCouponById['applicableto']=='2'){echo "selected";}?> >Tour Category</option>
                                                <option value="3" <?php if($getCouponById['applicableto']=='3'){echo "selected";}?> >Tour </option>
                                                
                                            </select>                           
                                            <input type="hidden" name="applicableto" id="applicableto" value="<?php if( isset($getCouponById['applicableto']) && $getCouponById['applicableto']!= "" ){ echo $getCouponById['applicableto']; }  ?>"/>
                                        </div>                        
                                     </div> 
                                    
                                    
                                    <div class="form-group" id="at1" <?php if($getCouponById['applicableto']!='1'){?> style="display: none;"  <?php }?>  >
                                        <label class="col-md-3 control-label">Select City:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="dropdwonid" id="dropdwonid_addCity" >
                                               <?php 
                                                 foreach($getSystemCity as $SystemCity)
                                                      { ?>  
                                                <option value="<?php echo $SystemCity['id'];?>"   ><?php echo $SystemCity['cityname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
                                        </div>  
                                        <span style="margin-left:100px;"><button type="button" class="btn btn-primary" name="btn_js_addCity" onClick="js_addCity()">Add</button></span>                      
                                     </div> 
                                    
                                    <div class="form-group" id="at_tour_category" <?php if($getCouponById['applicableto']=='2'){?> style="display: block;"<?php }else {?>style="display: none;" <?php }?>>
                                        <label class="col-md-3 control-label">Select City:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="dropdwonid_tour_category" id="dropdwonid_addCity_tour_category" onChange="City_addTourCategory(this.value)">
                                             <option value="0">Select City</option>
                                               <?php 
                                                 foreach($getSystemCity as $SystemCity)
                                                      { ?>  
                                                <option value="<?php echo $SystemCity['id'];?>"><?php echo $SystemCity['cityname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
                                        </div>    
                                        <!-- <span style="margin-left:100px;"><button type="button" class="btn btn-primary" name="btn_js_addCity" onClick="js_addCity()">Add</button></span>-->                    
                                     </div> 
                                    <div class="form-group" id="at_tour" <?php if($getCouponById['applicableto']=='3'){?> style="display: block;"<?php }else {?>style="display: none;"<?php }?>>
                                        <label class="col-md-3 control-label">Select City:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="dropdwonid_tour" id="dropdwonid_addCity_tour" onChange="City_addTour(this.value)">
                                             <option value="0">Select City</option>
                                               <?php 
                                                 foreach($getSystemCity as $SystemCity)
                                                      { ?>  
                                                <option value="<?php echo $SystemCity['id'];?>"><?php echo $SystemCity['cityname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
                                        </div> 
                                        </div> 
                                    <div class="form-group"  id="at2"  <?php if($getCouponById['applicableto']!='2'){?> style="display: none;"  <?php }?> >
                                        <label class="col-md-3 control-label">Select Tour Category:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="dropdwonid" id="dropdwonid_addTourCategory">
                                        <?php 
                                                 /*foreach($getSystemTC as $SystemTC)
                                                      { ?>  
                                                <option value="<?php echo $SystemTC['id'];?>"   ><?php echo $SystemTC['tourcatname'];?></option>
                                                <?php }*/ ?>
                                                
                                                
                                            </select>                           
                                            
                                        </div> 
                                        <span style="margin-left:100px;"><button type="button" class="btn btn-primary" name = "btn_js_addTourCategory" onClick="js_addTourCategory()">Add</button></span>                                           
                                     </div> 
                                    

                                   <div class="form-group"  id="at3"  <?php if($getCouponById['applicableto']!='3'){?> style="display: none;"  <?php }?> >
                                        <label class="col-md-3 control-label">Select Tour:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="dropdwonid" id="dropdwonid_addTour">

                                                 <?php 
                                                /* foreach($getSystemTour as $SystemTour)
                                                      { ?>  
                                                <option value="<?php echo $SystemTour['id'];?>"  ><?php echo $SystemTour['tourname'];?></option>
                                                <?php } */?>
                                                
                                                
                                            </select>                           
                                            
                                        </div>
                                        <span style="margin-left:100px;"><button type="button" class="btn btn-primary" name="btn_js_addTour" onClick="js_addTour()">Add</button></span>                        
                                     </div>
  <br/><br/><br/>
<div name="table_city" <?php if($getCouponById['applicableto']!='1'){?> style="display: none;"  <?php }?> id="table_city">
              <table class="table " style=" margin-bottom: -8px; margin-left: 16px; max-width: 100%; width: 96%;" id="table_addCity">
                     <thead>
                             <tr>
                             		<th>S.No.</th>	
                                     <th>City Name</th>
                                    <th>Actions</th>
                              </tr>
                     </thead>
                     <?php if($getCouponById['applicableto'] == '1') {
						 $countCity = 1;
						 		if (isset($getCouponById['dropdwonid']) && is_array($getCouponById['dropdwonid']) && count($getCouponById['dropdwonid'])>0) {
									
									foreach ($getCouponById['dropdwonid'] as $key => $value) {
										$getCouponCity = Users::getSystemCityById($value);
										
						?>
                        <tr id='rowCity<?php echo $value; ?>'>
                         <td style='width:60px; text-align: left;'><?php echo $countCity; ?></td><td style='width: 110px; text-align: left;'><input type='hidden' name="addCityarray[]" value='<?php echo $value; ?>' size='3' ><?php if (isset($getCouponCity['cityname']) && $getCouponCity['cityname']!= "") echo $getCouponCity['cityname']; ?></td>
                         <td style='width:60px; text-align: left;'><img src='img/minus.png' width='35px' height='35px' onclick='delete_row(<?php echo $value; ?>,"table_addCity")'/></td>
                         </tr>
                        <?php				
									$countCity++; }
									
								}
						 		
						 
						 
					 }?>
                    
            </table>

	</div>
	
<div name="table_tourCategory" id="table_tourCategory" <?php if($getCouponById['applicableto']!='2'){?> style="display: none;"  <?php }?>>
              <table class="table " style=" margin-bottom: -8px; margin-left: 16px; max-width: 100%; width: 96%;" id="table_addTourCategory">
                     <thead>
                             <tr>
                             		<th>S.No.</th>	
                                     <th>Tour Category Name</th>
                                    <th>Actions</th>
                              </tr>
                     </thead>
                     <?php if($getCouponById['applicableto'] == '2') {
						 $countCity = 1;
						 		if (isset($getCouponById['dropdwonid']) && is_array($getCouponById['dropdwonid']) && count($getCouponById['dropdwonid'])>0) {
									
									foreach ($getCouponById['dropdwonid'] as $key => $value) {
										$getCouponCity = Users::getTourCategoryById($value);
										
						?>
                        <tr id='rowTC<?php echo $value; ?>'>
                         <td style='width:60px; text-align: left;'><?php echo $countCity; ?></td><td style='width: 110px; text-align: left;'><input type='hidden' name='addTourCategory[]' value='<?php echo $value; ?>' size='3' ><?php if (isset($getCouponCity['tourcatname']) && $getCouponCity['tourcatname']!= "") echo $getCouponCity['tourcatname']; ?></td>
                         <td style='width:60px; text-align: left;'><img src='img/minus.png' width='35px' height='35px' onclick='delete_row(<?php echo $value; ?>,"table_addTourCategory")'/></td>
                         </tr>
                        <?php				
									$countCity++; }
									
								}
						 		
						 
						 
					 }?>

            </table>

	</div>
    <div name="table_tour" id = "table_tour"  <?php if($getCouponById['applicableto']!='3'){?> style="display: none;"  <?php }?>>
              <table class="table " style=" margin-bottom: -8px; margin-left: 16px; max-width: 100%; width: 96%;" id="table_addTour">
                     <thead>
                             <tr>
                             		<th>S.No.</th>	
                                     <th>Tour Name</th>
                                    <th>Actions</th>
                              </tr>
                     </thead>
                     <?php if($getCouponById['applicableto'] == '3') {
						 $countCity = 1;
						 		if (isset($getCouponById['dropdwonid']) && is_array($getCouponById['dropdwonid']) && count($getCouponById['dropdwonid'])>0) {
									
									foreach ($getCouponById['dropdwonid'] as $key => $value) {
										$getCouponCity = Users::getTourByID($value);
										
						?>
                        <tr id='row<?php echo $value; ?>'>
                         <td style='width:60px; text-align: left;'><?php echo $countCity; ?></td><td style='width: 110px; text-align: left;'><input type='hidden' name="addTourarray[]" value='<?php echo $value; ?>' size='3' ><?php if (isset($getCouponCity['tourname']) && $getCouponCity['tourname']!= "") echo $getCouponCity['tourname']; ?></td>
                         <td style='width:60px; text-align: left;'><img src='img/minus.png' width='35px' height='35px' onclick='delete_row(<?php echo $value; ?>,"table_addTour")'/></td>
                         </tr>
                        <?php				
									$countCity++; }
									
								}
						 		
						 
						 
					 }?>

            </table>

	</div>
	<br/><br/><br/>

 
	
	
 
                                   <!-- <div class="btn-group">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate_coupon.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        
                                    </div> -->
                                    <div class="btn-group">
                                       <button name="submit" value="1" class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                    
                                    <div class="btn-group">
                                        <a href="coupon.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div> 
                                    
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div> 
                                        
                                    </div>
                        
                        
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>     
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  
                <script>
               
				<!-- Add Tour Start -->			
				
				
<!-- End Add Tour -->				
<!-- Add Tour Category Start -->				
				
				
				
				
<!--              Add Tour Category End -->				
				
				
			/*	function delete_row(id,tablename)
                        {
							//alert(tablename);
                                var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
									document.getElementById("row"+id+"").outerHTML="";
                                                                   
                                }
                                else
                                {
                                        return false;
                                }
								var tablelength = document.getElementById(tablename).rows.length;
								//alert(tablelength);
								if(tablelength === 1) {
									//alert(document.getElementById("applicableto123").disabled);
									document.getElementById("applicableto123").disabled = false;
									//document.getElementById("applicableto123").style.display = "block";
									
									

								}
                        }
						*/
                function show_divat(val){
                    //alert(val);
                    if(val==1){
                     document.getElementById('at1').style.display = "block";
                     document.getElementById('at2').style.display = "none";
                     document.getElementById('at3').style.display = "none";
                    }
                    if(val==2){
                     document.getElementById('at1').style.display = "none";
                     document.getElementById('at2').style.display = "block";
                     document.getElementById('at3').style.display = "none";
                    }
                    if(val==3){
                     document.getElementById('at1').style.display = "none";
                     document.getElementById('at2').style.display = "none";
                     document.getElementById('at3').style.display = "block";
                    }
                    
                 }
                function City_addTourCategory(cityid) {
	selectCity = document.getElementById('dropdwonid_addTourCategory');
	//document.getElementById('tourcatid').innerHTML = "";
	//$("#tourcatid").selectpicker('refresh');
$.ajax({
						url: "lib/scripts/php/all/ajaxTourCategoryOnCityId.php",
						type: "POST",
						data:'cityid='+cityid,
						success: function(data){
						//alert(data);	
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var status = duce.status;
											var tourcatoptions =duce.tourcatoptions;
                                    												
								   }
								   if (status == 1 ) {
									   selectCity.innerHTML = tourcatoptions;
									   $("#dropdwonid_addTourCategory").selectpicker('refresh');
									   //document.getElementById('at2').style.display = "block";
								   }else {
									   alert("Select City");
									   
								   }
								   
		
											
											} 
										   
						       
				   });

}
function City_addTour(cityid) {
	selectCity = document.getElementById('dropdwonid_addTour');
	//document.getElementById('tourcatid').innerHTML = "";
	//$("#tourcatid").selectpicker('refresh');
$.ajax({
						url: "lib/scripts/php/all/ajaxTourOnCityId.php",
						type: "POST",
						data:'cityid='+cityid,
						success: function(data){
						//alert(data);	
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var status = duce.status;
											var tourcatoptions =duce.touroptions;
                                    												
								   }
								   if (status == 1 ) {
									   selectCity.innerHTML = tourcatoptions;
									   $("#dropdwonid_addTour").selectpicker('refresh');
									   //document.getElementById('at3').style.display = "block";
								   }else {
									   alert("Select City");
									   
								   }
								   
		
											
											} 
										   
						       
				   });

}
                </script>
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<?php	require_once("footer.php");
?>
<script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
 <script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>

<script>
function delete_row(id,tablename){
                    noty({
                        text: 'Are you sure Want to delete This',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									if(tablename == 'table_addTour') {
									document.getElementById("row"+id+"").outerHTML="";
									}else if(tablename == 'table_addTourCategory') {
									document.getElementById("rowTC"+id+"").outerHTML="";	
									}else if(tablename == 'table_city') {
										document.getElementById("rowCity"+id+"").outerHTML="";
									}
                                   // noty({text: 'You clicked "Ok" button', layout: 'topRight', type: 'success'});
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					var tablelength = document.getElementById(tablename).rows.length;
								//alert(tablelength);
								if(tablelength === 1) {
									//alert(document.getElementById("applicableto123").disabled);
									document.getElementById("applicableto123").disabled = false;
								}                                                    
                }
				
				function js_addTour() {
					//alert("addTour");
					document.getElementById("applicableto").value	= document.getElementById("applicableto123").value;
					document.getElementById("applicableto123").disabled = true;
					document.getElementById("table_tourCategory").style.display = "none";
					document.getElementById("table_city").style.display = "none";
					document.getElementById("table_tour").style.display = "block";
					var addTourID=document.getElementById("dropdwonid_addTour").value; 
					if(Number(addTourID) > 0 ) {
					$.ajax({
						url: "lib/scripts/php/all/ajaxCouponAddTour.php",
						type: "POST",
						data:'TourId='+addTourID,
						success: function(data){
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var tourname = duce.tourname;
                                    												
								   }
								   //alert(cityname);
		
		
							  
							  var data_table='table_addTour';
		
											var table=document.getElementById(data_table);
											var table_len=(table.rows.length);
											if(document.getElementById("row"+addTourID)){
												 noty({
                        text: 'Tour Already Exist',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									return false;
									
                                   // noty({text: 'You clicked "Ok" button', layout: 'topRight', type: 'success'});
                                }
                                },
                               
                                
                            ]
                    })
    //alert("Element exists");
}else {
											var row = table.insertRow(table_len).outerHTML="<tr id='row"+addTourID+"'>\n\<td style='width:60px; text-align: left;'>"+table_len+"</td><td style='width: 110px; text-align: left;'><input type='hidden' name=addTourarray[] value='"+addTourID+"' size='3' >"+tourname+"</td>\n\
																			  <td style='width:60px; text-align: left;'><img src='img/minus.png' width='35px' height='35px' onclick='delete_row("+table_len+",\"table_addTour\")'/></td>\n\
										  </tr>";
		
											
}} 
										   
						       
				   });
					}else{
						noty({
                        text: 'Select City or Tour ',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									return false;
									
                                   // noty({text: 'You clicked "Ok" button', layout: 'topRight', type: 'success'});
                                }
                                },
                               
                                
                            ]
                    })
						
					}
					//alert(addTour);
					
				}
				function js_addCity() {
					//alert("123456");
						document.getElementById("applicableto").value	= document.getElementById("applicableto123").value;
						//alert(document.getElementById("applicableto123").value);
					      document.getElementById("applicableto123").disabled = true;
					
					document.getElementById("table_tour").style.display = "none";
					document.getElementById("table_tourCategory").style.display = "none";
                    document.getElementById("table_city").style.display = "block";
                    var addCityID=document.getElementById("dropdwonid_addCity").value; 
						
				
				
					$.ajax({
						url: "lib/scripts/php/all/ajaxCouponAddCity.php",
						type: "POST",
						data:'CityId='+addCityID,
						success: function(data){
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var cityname = duce.cityname;
                                    												
								   }
								   //alert(cityname);
		
		if(document.getElementById("rowCity"+addCityID)){
												 noty({
                        text: 'City Already Exist',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									return false;
									
                                   // noty({text: 'You clicked "Ok" button', layout: 'topRight', type: 'success'});
                                }
                                },
                               
                                
                            ]
                    })
    //alert("Element exists");
}else {
							  
							  var data_table='table_addCity';
							  table_name_send='table_addCity';
		
											var table=document.getElementById(data_table);
											var table_len=(table.rows.length);
											var row = table.insertRow(table_len).outerHTML="<tr id='rowCity"+addCityID+"'>\n\<td style='width:60px; text-align: left;'>"+table_len+"</td><td style='width: 110px; text-align: left;'><input type='hidden' name=addCityarray[] value='"+addCityID+"' size='3' >"+cityname+"</td>\n\
																			  <td style='width:60px; text-align: left;'><img src='img/minus.png' width='35px' height='35px' onclick='delete_row("+table_len+",\"table_addCity\")'/></td>\n\
										  </tr>";
		
											
} } 
										   
						       
				   });

										
				}
				function js_addTourCategory() {
						document.getElementById("applicableto").value	= document.getElementById("applicableto123").value;
					document.getElementById("applicableto123").disabled = true;
					//alert("addTourCategory");
					
					//document.getElementById("applicableto").disabled = true;
					document.getElementById("table_tour").style.display = "none";
					document.getElementById("table_city").style.display = "none";

					document.getElementById("table_tourCategory").style.display = "block";
					
					
					var addTourCategoryID=document.getElementById("dropdwonid_addTourCategory").value; 
					if(Number(addTourCategoryID) > 0 ) {
					$.ajax({
						url: "lib/scripts/php/all/ajaxCouponAddTourCategory.php",
						type: "POST",
						data:'TourCategoryID='+addTourCategoryID,
						success: function(data){
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var tourcatname = duce.tourcatname;
                                    												
								   }
								   //alert(cityname);
		if(document.getElementById("rowTC"+addTourCategoryID)){
												 noty({
                        text: 'Tour Category Already Exist',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									return false;
									
                                   // noty({text: 'You clicked "Ok" button', layout: 'topRight', type: 'success'});
                                }
                                },
                               
                                
                            ]
                    })
    //alert("Element exists");
}else {
		
							  
							  var data_table='table_addTourCategory';
		
											var table=document.getElementById(data_table);
											var table_len=(table.rows.length);
											var row = table.insertRow(table_len).outerHTML="<tr id='rowTC"+addTourCategoryID+"'>\n\<td style='width:60px; text-align: left;'>"+table_len+"</td><td style='width: 110px; text-align: left;'><input type='hidden' name=addTourCategory[] value='"+addTourCategoryID+"' size='3' >"+tourcatname+"</td>\n\
																			  <td style='width:60px; text-align: left;'><img src='img/minus.png' width='35px' height='35px' onclick='delete_row("+table_len+",\"table_addTourCategory\")'/></td>\n\
										  </tr>";
		
											
}} 
										   
						       
				   });
					}else {
						noty({
                        text: 'Select City or Tour Category ',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									return false;
									
                                   // noty({text: 'You clicked "Ok" button', layout: 'topRight', type: 'success'});
                                }
                                },
                               
                                
                            ]
                    })
						
					}
					//alert(addTourCategory);	
				}
</script>
	<script type="text/javascript" src="js/settings.js"></script>
            
            <script type="text/javascript" src="js/plugins.js"></script>        
            <script type="text/javascript" src="js/actions.js"></script>	