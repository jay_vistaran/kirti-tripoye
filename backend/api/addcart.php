<?php $data = file_get_contents("php://input");
//echo "<pre>";
//print_r($_POST);die;
$json_decode_result = json_decode($data,true);
//echo "<pre>";
//print_r($json_decode_result);die;
 /*
 * Following code will create a new cart
 * All cart details are read from HTTP Post Request
 * Made By : D
 * DATE:20-05-2017
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
// array for JSON response
$response = array();
function cartDetailById($id) {
	$sql = "select * from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;
	$result = MySQL::query($sql);
	return $result; 
}
function deletecartById($id) {
	$sql = "delete from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;;die;
	$result = MySQL::query($sql);
	//return $result; 
}
function cartExist($id) {
	$sql = "select count(*) as cartcount from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;  
	$result = MySQL::query($sql,true);
	return $result; 
}
function cartExistDetail($id) {
	$sql = "select * from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;  
	$result = MySQL::query($sql,true);
	return $result; 
}
if (isset($json_decode_result['CartDetail']) && is_array($json_decode_result['CartDetail']) && count($json_decode_result['CartDetail'])>0 && isset($json_decode_result['cart_user_id']) && $json_decode_result['cart_user_id']!= "") {
	$_POST['cart_user_id'] =$json_decode_result['cart_user_id'];
	$_POST['CartDetail'] =$json_decode_result['CartDetail'];
	//echo "<pre>";
//print_r($_POST['CartDetail']);die;
	deletecartById($_POST['cart_user_id']);
	foreach ($_POST['CartDetail'] as $key => $value ) {
		//echo "<pre>";
//print_r($value['FullTotal']);die;
		if(isset($value['TourId'])  && isset($value['TourOptId']) && isset($value['ComboId']) && isset($value['type']) && isset($value['TourName']) && isset($value['TourDate']) && isset($value['TransferName']) &&isset($value['TourOptName']) && isset($value['AgeType']) &&isset($value['AdultPrice']) &&isset($value['ChildPrice']) && isset($value['Discount']) && isset($value['DiscountType']) && isset($value['DisAdultPrice']) && isset($value['DisChildPrice']) &&isset($value['TourTime']) &&isset($value['AdultQuantity']) && isset($value['ChildQuantity']) && isset($value['InfantQuantity']) && isset($value['SingleTotal']) && isset($value['SingleDisTotal']) && isset($value['FullTotal']) && isset($value['DisFullTotal']) && isset($value['cartitem_uniqueId']) && isset($value['SessionId']) && isset($value['TransferId']) && isset($value['TransferType']) && isset($value['currencyId'])) {
			//echo "hello";die;
	if ($value['TourId'] == "" ){
		$TourId = 0;
	}else{
		$TourId = $value['TourId'];
	}
	
	if ($value['TourOptId'] == "" ){
		$TourId = 0;
	}else{
		$TourOptId = $value['TourOptId'];
	}
	
	if ($value['ComboId'] == "" ){
		$ComboId = 0;
	}else{
		$ComboId = $value['ComboId'];
	}
	
	if ($value['type'] == "" ){
		$type = 0;
	}else{
		$type = $value['type'];
	}
	
	
	
	if ($value['AdultPrice'] == "" ){
		$AdultPrice = 0;
	}else{
		$AdultPrice = $value['AdultPrice'];
	}
	
	if ($value['ChildPrice'] == "" ){
		$ChildPrice = 0;
	}else{
		$ChildPrice = $value['ChildPrice'];
	}
	if ($value['Discount'] == "" ){
		$Discount = 0;
	}else{
		$Discount = $value['Discount'];
	}
	if ($value['DiscountType'] == "" ){
		$DiscountType = 0;
	}else{
		$DiscountType = $value['DiscountType'];
	}
	if ($value['DisAdultPrice'] == "" ){
		$DisAdultPrice = 0;
	}else{
		$DisAdultPrice = $value['DisAdultPrice'];
	}
	if ($value['DisChildPrice'] == "" ){
		$DisChildPrice = 0;
	}else{
		$DisChildPrice = $value['DisChildPrice'];
	}
	if ($value['AdultQuantity'] == "" ){
		$AdultQuantity = 0;
	}else{
		$AdultQuantity = $value['AdultQuantity'];
	}
	if ($value['ChildQuantity'] == "" ){
		$ChildQuantity = 0;
	}else{
		$ChildQuantity = $value['ChildQuantity'];
	}
	
	if ($value['InfantQuantity'] == "" ){
		$InfantQuantity = 0;
	}else{
		$InfantQuantity = $value['InfantQuantity'];
	}
	if ($value['SingleTotal'] == "" ){
		$SingleTotal = 0;
	}else{
		$SingleTotal = $value['SingleTotal'];
	}
	if ($value['SingleTotal'] == "" ){
		$SingleTotal = 0;
	}else{
		$SingleTotal = $value['SingleTotal'];
	}
	if ($value['SingleDisTotal'] == "" ){
		$SingleDisTotal = 0;
	}else{
		$SingleDisTotal = $value['SingleDisTotal'];
	}
	if ($value['SingleDisTotal'] == "" ){
		$SingleDisTotal = 0;
	}else{
		$SingleDisTotal = $value['SingleDisTotal'];
	}
	if ($value['FullTotal'] == "" ){
		$FullTotal = 0;
	}else{
		$FullTotal = $value['FullTotal'];
	}
	if ($value['DisFullTotal'] == "" ){
		$DisFullTotal = 0;
	}else{
		$DisFullTotal = $value['DisFullTotal'];
	}
	
	
	
	$SQL = "INSERT INTO `tour_cart` (`TourId`, `TourOptId`, `ComboId`,`type`, `TourName`, `TourDate`,`TransferName`, `TourOptName`, `AgeType`,`AdultPrice`, `ChildPrice`, `Discount`, `DiscountType`, `DisAdultPrice`,`DisChildPrice`, `TourTime`, `AdultQuantity`,`ChildQuantity`, `InfantQuantity`, `SingleTotal`,`SingleDisTotal`, `FullTotal`, `DisFullTotal`,cart_user_id,cart_create_update_time,cartitem_uniqueId,SessionId,TransferId,TransferType,currency_id)
               VALUES ( '" . $TourId . "', '" . $TourOptId . "', '" . $ComboId . "', '" . $type . "', '" . addslashes($value['TourName']) . "', '" . addslashes($value['TourDate']) . "','" . addslashes($value['TransferName']) . "','" . addslashes($value['TourOptName']) . "','" . addslashes($value['AgeType']) . "', '" . $AdultPrice . "', '" . $ChildPrice . "', '" . $Discount . "', '" . $DiscountType . "', '" . $DisAdultPrice . "', '" . $DisChildPrice . "', '" . addslashes($value['TourTime']). "','" . $AdultQuantity . "', '" . $ChildQuantity . "', '" . $InfantQuantity . "', '" . $SingleTotal . "', '" . $SingleDisTotal . "', '" . $FullTotal . "','" . $DisFullTotal . "','".$_POST['cart_user_id']."','".date('Y-m-d h:i:s')."','".$value['cartitem_uniqueId']."',".$value['SessionId'].",".$value['TransferId'].",'".$value['TransferType']."' ,'".$value['currencyId']."')";
$result = MySQL::query($SQL);
//echo $SQL;
//echo "<hr/>";
}
	}
//die;
		$cartDetail = cartDetailById($_POST['cart_user_id']);
		$cart_create_update_time = "";
		$cartUSERID ="";
		if (isset($cartDetail) && is_array($cartDetail) && count($cartDetail)>0) {
			foreach($cartDetail as $keya  => $valuea ){
				$cartUSERID = $valuea['cart_user_id'];
				$cart_create_update_time = $valuea['cart_create_update_time'];
				unset( $valuea['cart_user_id']);
				unset( $valuea['cart_create_update_time']);
				$cartDetailnew[$keya] =$valuea;
			}
		}
		//echo "<pre>";print_r($cartDetailnew);die;
        $response["success"] = 1;
        $response["message"] = "Tour Added Successfully in Cart.";
		$response["cartDetail"] = $cartDetailnew;
		$response["cart_create_update_time"] = $cart_create_update_time;
		$response["cart_user_id"] = $cartUSERID;
	echo json_encode($response);
}else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}

?>