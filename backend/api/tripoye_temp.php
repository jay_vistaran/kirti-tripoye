<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; background-color:#f0f2ea; margin:0; padding:0; color:#333333;">

<table width="100%" bgcolor="#f0f2ea" cellpadding="0" cellspacing="0" border="0">
    <tbody>
        <tr>
            <td style="padding:40px 0;">
                <!-- begin main block -->
                <table cellpadding="0" cellspacing="0" width="608" border="0" align="center">
                    <tbody>
                        <tr>
                            <td>
                                <a href="http://www.tripoye.com/" style="display:block; width:120px; height:100px; margin:0 auto 30px;">
                                    <img src="http://www.tripoye.com/backend/temp_img/appicon.png" width="100" height="100" alt="tripoye" style="display:block; border:0; margin:0;" align="middle">
                                </a>
                                <p style="margin:0 0 36px; text-align:center; font-size:14px; line-height:20px; text-transform:uppercase; color:#626658;">
                                    what is the most fascinating thing about summer?
                                </p>
                                <!-- begin wrapper -->
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td width="8" height="4" colspan="2" style="background:url(http://demo.artlance.ru/email/shadow-top-left.png) no-repeat 100% 100%;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td height="4" style="background:url(http://demo.artlance.ru/email/shadow-top-center.png) repeat-x 0 100%;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td width="8" height="4" colspan="2" style="background:url(http://demo.artlance.ru/email/shadow-top-right.png) no-repeat 0 100%;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                        </tr>
                                        
                                        <tr>
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-left-top.png) no-repeat 100% 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td colspan="3" rowspan="3" bgcolor="#FFFFFF" style="padding:0 0 30px;">
                                                <!-- begin content -->
                                                <img src="http://www.tripoye.com/backend/temp_img/Florida.jpeg" width="600" height="200" alt="summer‘s coming trimm your sheeps" style="display:block; border:0; margin:0 0 44px; background:#eeeeee;" align="middle">
                                                <p style="margin:0 30px 33px;; text-align:center; text-transform:uppercase; font-size:24px; line-height:30px; font-weight:bold; color:#484a42;">
                                                    Order Summary
                                                </p>
                                                <p style="margin:0; border-top:2px solid #e5e5e5; font-size:5px; line-height:5px; margin:0 30px 29px;">&nbsp;</p>
                                                <a href="http://www.tripoye.com/" style="display:block; width:50px; height:30px; margin:0 28px;">
                                                    <img src="http://www.tripoye.com/backend/temp_img/logo.png" width="100" height="30" alt="tripoye" style="display:block; border:0; margin:0;">
                                                </a><br>
                                                <!-- begin articles -->
                                                <div>
                                                    <p style="margin:0 30px 33px; font-size:18px; font-weight:bold; color:#484a42;">
                                                    Hello Rajkumar Gaikwad,
                                                    </p>
                                                    <p style="margin:0 30px 33px; color:#484a42;">
                                                    Thanks for booking with tripoye! Here are the details of your order.
                                                    </p>
                                                    <p style="margin:0 30px 33px; color:#484a42;">
                                                    Your voucher will be sent in a separate email once your booking is confirmed.
                                                    </p><br>
                                                    <p style="margin:0 30px 33px; color:#484a42;">
                                                    <strong>Order Number: 6991ee24</strong> Booking Date: 2017-08-05     
                                                    </p>

                                                <p style="margin:0; border-top:2px solid #e5e5e5; font-size:5px; line-height:5px; margin:0 30px 29px;">&nbsp;</p><br>
                                                </div>

                                                <div style="width:100%">
                                                    <div style="width:50%; float: left;">
                                                      <img src="http://www.tripoye.com/backend/temp_img/Florida.jpeg" width="300" height="250" alt="tripoye" style="display:block; border:0; margin:0;">
                                                    </div>
                                                    <div style="width:50%; float: left;">
                                                        <p style="margin:0 30px 33px; font-size:18px; font-weight:bold; color:#484a42;">
                                                        I Love Rome Hop On Hop Off Panoramic Bus Tour
                                                        </p>
                                                        <p style="margin:0 30px 33px; color:#484a42;">
                                                        <strong>Package</strong> Panoramic Bus Tour 
                                                        <strong>Tickets</strong> - 24h
                                                        </p>
                                                        <p style="margin:0 30px 33px; color:#484a42;">
                                                        <strong>Date</strong> 2017-09-30
                                                        </p>
                                                        <p style="margin:0 30px 33px; color:#484a42;">
                                                        <strong>Units</strong> 1 x Adult    
                                                        </p> 
                                                        <p style="margin:0; border-top:2px solid #e5e5e5; font-size:5px; line-height:5px; margin:0 30px 29px;">&nbsp;</p>
                                                        <p style="margin:0 30px 33px; color:#484a42;">
                                                        <strong>Order Total:</strong> US$ 25.0    
                                                        </p> 
                                                        <p style="margin:0; border-top:2px solid #e5e5e5; font-size:5px; line-height:5px; margin:0 30px 29px;">&nbsp;</p>
                                                        <p style="margin:0 30px 33px; color:#484a42;">
                                                        <strong>Amount Paid:</strong> US$ 25.0    
                                                        </p> 
                                                        <p style="margin:0; border-top:2px solid #e5e5e5; font-size:5px; line-height:5px; margin:0 30px 29px;">&nbsp;</p>
                                                    </div>

                                                </div>
                                                <!-- /end articles -->
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tbody>
                                                        <tr valign="top">
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td>
                                                                <p style="margin:0 0 4px; font-weight:bold; color:#333333; font-size:14px; line-height:22px;">Tripoye</p>
                                                                <p style="margin:0; color:#333333; font-size:11px; line-height:18px;">
                                                                    301 Clematis. Suite 3000, West Palm Beach, FL 33401<br>
                                                                    Help &amp; Support Center: 981-981-9811<br>
                                                                    Website: <a href="http://www.tripoye.com/" style="color:#6d7e44; text-decoration:none; font-weight:bold;">www.tripoye.com</a>
                                                                </p>
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td width="120">
                                                                <a href="https://www.facebook.com/websuccessagency" style="float:left; width:24px; height:24px; margin:6px 8px 10px 0;">
                                                                    <img src="http://www.tripoye.com/backend/temp_img/facebook.png" width="24" height="24" alt="facebook" style="display:block; margin:0; border:0; background:#eeeeee;">
                                                                </a>
                                                                <a href="https://twitter.com/mywebsuccess" style="float:left; width:24px; height:24px; margin:6px 8px 10px 0;">
                                                                    <img src="http://www.tripoye.com/backend/temp_img/twitter.png" width="24" height="24" alt="twitter" style="display:block; margin:0; border:0; background:#eeeeee;">
                                                                </a>
                                                                <a href="https://www.linkedin.com/company/web-success-agency" style="float:left; width:24px; height:24px; margin:6px 8px 10px 0;;">
                                                                    <img src="http://www.tripoye.com/backend/temp_img/linkedin.png" width="24" height="24" alt="tumblr" style="display:block; margin:0; border:0; background:#eeeeee;">
                                                                </a>
                                                                <a href="https://plus.google.com/+Websuccessagency" style="float:left; width:24px; height:24px; margin:6px 0 10px 0;">
                                                                    <img src="http://www.tripoye.com/backend/temp_img/google-plus.png" width="24" height="24" alt="rss" style="display:block; margin:0; border:0; background:#eeeeee;">
                                                                </a>
                                                                <p style="margin:0; font-weight:bold; clear:both; font-size:12px; line-height:22px;">
                                                                    <a href="http://www.tripoye.com/" style="color:#6d7e44; text-decoration:none;">Visit website</a><br>
                                                                    <a href="http://www.tripoye.com/" style="color:#6d7e44; text-decoration:none;">Mobile version</a>
                                                                </p>
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- end content --> 
                                            </td>
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-right-top.png) no-repeat 0 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td width="4" style="background:url(http://demo.artlance.ru/email/shadow-left-center.png) repeat-y 100% 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td width="4" style="background:url(http://demo.artlance.ru/email/shadow-right-center.png) repeat-y 0 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                        </tr>
                                        
                                        <tr> 
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-left-bottom.png) repeat-y 100% 100%;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-right-bottom.png) repeat-y 0 100%;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                        </tr>
                                 
                                        <tr>
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-bottom-corner-left.png) no-repeat 100% 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-bottom-left.png) no-repeat 100% 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td height="4" style="background:url(http://demo.artlance.ru/email/shadow-bottom-center.png) repeat-x 0 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-bottom-right.png) no-repeat 0 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                            <td width="4" height="4" style="background:url(http://demo.artlance.ru/email/shadow-bottom-corner-right.png) no-repeat 0 0;"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- end wrapper-->
                                <p style="margin:0; padding:34px 0 0; text-align:center; font-size:11px; line-height:13px; color:#333333;">
                                    Don‘t want to recieve further emails? You can unsibscribe <a href="http://www.tripoye.com/" style="color:#333333; text-decoration:underline;">here</a>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- end main block -->
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>
