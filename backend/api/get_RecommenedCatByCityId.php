<?php
 
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
 
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

// array for JSON response
$response = array();
 
// check for required fields
    if (isset($_POST['cityid']) && $_POST['cityid']!='') {
       
        $SQL = "SELECT b. *
                            FROM `toprecommened_cat` a
                            LEFT JOIN tour_category b ON a.tourcatId = b.id
                            WHERE a.cityid ='".$_POST['cityid']."' AND a.tourcatId = b.id
                            ORDER BY a.torder ASC";
        $result=MySQL::query($SQL);
        // check if row inserted or not
        if ($result) {
        // successfully inserted into database
            $response["success"] = 1;
            $response["message"] = "All Recommened City.";
            $response["recommened_cat"] = 1;
            //$response["CityDetail"] = $result;
         }
        else{
             $response["success"] = 0;
             $response["recommened_cat"] = 0;
             $response["message"] = "No Recommened City found in the database";
         }
        $SQL1 = "SELECT *
                            FROM `tour_category` 
                            WHERE cityid ='".$_POST['cityid']."'
                            ORDER BY tourcatname ASC";
        $result1=MySQL::query($SQL1);
        // check if row inserted or not
        if ($result1) {
        // successfully inserted into database
            $response["success"] = 1;
            $response["message"] = "All Tour City.";
            $response["Tour_cat"] = 1;
            $response["CityDetail"] = array_merge($result,$result1);
         }
        else{
             $response["success"] = 0;
             $response["Tour_cat"] = 0;
             $response["message"] = "No Tour Category found in the database";
         }
        
        //print_r($response);die;
        echo json_encode($response);
        
 } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}  
?>