<?php 
 /*
 * Following code will For Coupon Check API
 * All coupon details are read from HTTP POST Request
 * Made By : D
 * DATE: 8-06-2017
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
$today = date('Y-m-d');
$todaystrtotime = strtotime($today); 


// array for JSON response
$response = array();
$tourTotal = array();
$comboTotal = array();
$CouponUsed = 0;
$overAllTotal = 0;
/****                                        
	function For Coupon Info

****/

function couponInfo($couponCode) {
	$SQL = "SELECT * FROM tour_coupon WHERE couponcode  = '".$couponCode."' LIMIT 1";
	//echo $SQL;die;
    $result = MySQL::query($SQL,true);
	 if (isset($result) && is_array($result) && count($result)>0 ){
		 return $result;
	 }else {
		  return false;
	 }
}

/**********************************************************/
	



/****                                        
	function For Check Coupon Type

****/

function couponType($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		 return $couponinfo['coupontype'];
	 }else {
		  return false;
	 }
}

/**********************************************************/

/****                                        
	function For Coupon Discount Value

****/

function couponDiscountValue($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		 return $couponinfo['discountvalue'];
	 }else {
		  return false;
	 }
}

/**********************************************************/	

/****                                        
	function For Coupon Minimum Condition type 

****/

function couponMinimumConditionType($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		 return $couponinfo['mincondition'];
	 }else {
		  return false;
	 }
}

/**********************************************************/

/****                                        
	function For Coupon Minimum Condition value

****/

function couponMinimumConditionValue($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		 return $couponinfo['minconditionvalue'];
	 }else {
		  return false;
	 }
}

/**********************************************************/


/****                                        
	function For Coupon Maximum uses value

****/

function couponMaximumUses($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		 return $couponinfo['maxnumofuses'];
	 }else {
		  return false;
	 }
}

/**********************************************************/

/****                                        
	function For Coupon Booking  Date value

****/

function couponBookingDate($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		$bookingarray['bookingfrom'] = $couponinfo['bookingfrom'];
		$bookingarray['bookingto'] = $couponinfo['bookingto'];
		 return $bookingarray;
	 }else {
		  return false;
	 }
}

/**********************************************************/

/****                                        
	function For Coupon Travelling  Date value

****/

function couponTravellingDate($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		$travellingarray['travellingfrom'] = $couponinfo['travellingfrom'];
		$travellingarray['travellingto'] = $couponinfo['travellingto'];
		 return $travellingarray;
	 }else {
		  return false;
	 }
}

/**********************************************************/

/****                                        
	function For Coupon Applicable To

****/

function couponApplicableTo($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		
		 return $couponinfo['applicableto'];
	 }else {
		  return false;
	 }
}

/**********************************************************/
/****                                        
	function For Coupon Applicable Value

****/

function couponApplicableValue($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		
		 return  $couponinfo['dropdwonid'];
	 }else {
		  return false;
	 }
}

/**********************************************************/

/****                                        
	function For maximum number of uses 

****/

function couponMaximumNumberUsed($couponinfo=array()) {
	
	if (isset($couponinfo) && is_array($couponinfo) && count($couponinfo)>0 ){
		 return  $couponinfo['coupon_uses'];
	 }else {
		  return false;
	 }
}

/**********************************************************/

/**
	Cart Functions

**/
function cartDetailTourById($id) {
	$sql = "select TourId,ComboId,type,FullTotal,DisFullTotal,sum(AdultQuantity),sum(ChildQuantity),MIN(AdultPrice),TourDate,Discount,DiscountType,cart_user_id from tour_cart where cart_user_id = '".$id."' AND type = 0 GROUP BY TourId";
	//echo $sql;die;
	$result = MySQL::query($sql);
	return $result; 
}
function cartDetailComboById($id) {
	$sql = "select TourId,ComboId,type,FullTotal,DisFullTotal,sum(AdultQuantity),sum(ChildQuantity),MIN(AdultPrice),TourDate,Discount,DiscountType,cart_user_id from tour_cart where cart_user_id = '".$id."' AND type = 1 GROUP BY TourId,ComboId";
	//echo $sql;die;
	$result = MySQL::query($sql);
	return $result; 
}

function cartMinimumtouroptionValueTour($tourid,$id) {
	$sql = "select MIN( CAST( DisAdultPrice AS DECIMAL( 8, 2 )  )  ) as minDisAdultPrice  from tour_cart where cart_user_id = '".$id."' AND type = 0 AND TourId=".$tourid." AND DisAdultPrice <> 0";
	//echo $sql;
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
	
}
function cartMinimumtouroptionValueCombo($tourid,$id,$comboid) {
	$sql = "select MIN( CAST( DisAdultPrice AS DECIMAL( 8, 2 )  )  ) as minDisAdultPrice  from tour_cart where cart_user_id = '".$id."' AND type = 0 AND TourId=".$tourid." AND DisAdultPrice <> 0 AND ComboId = ".$comboid."";
	//echo $sql;
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
	
}

/**
	Discount Value
*/

function CouponDiscountValueCart($amount,$type,$couponinfo,$typetour,$tourid,$id,$comboid) {
	if ($type == 'F') {
		
		$coupondisvalue = couponDiscountValue($couponinfo);
		if($coupondisvalue){
			$amountless = $amount-$coupondisvalue;  
			return $amountless;
		}else {
			return $amount;
		}
	}else if ($type == 'P') {
		
		$coupondisvalue = couponDiscountValue($couponinfo);
		if($coupondisvalue){
			$percentagecoupondisvalue = round($amount*$coupondisvalue/100,2);
			$amountless = $amount-$percentagecoupondisvalue;  
			return $amountless;
		}else {
			return $amount;
		}
	}elseif ($type == 'A') {
		
		$coupondisvalue = couponDiscountValue($couponinfo);
		if($typetour == 1 ) {
			
			  $cartMinimumtouroptionValueTour	= cartMinimumtouroptionValueCombo($tourid,$id,$comboid);
		}else {
		 $cartMinimumtouroptionValueTour = cartMinimumtouroptionValueTour($tourid,$id);
		}
		//print_r($cartMinimumtouroptionValueTour);
		if($coupondisvalue){
		$amountAdiscount = $amount-$cartMinimumtouroptionValueTour['minDisAdultPrice'];
		$newamount =$amountAdiscount+$coupondisvalue;
		return $newamount;
		}else {
			return $amount;
		}
		
	}else{
		return $amount;
	}
}

/**********************************************************/

/**
	Tour Category function
*/

function tourCategory($tourid) {
	$sql = "select tourcatid from tour where id = '".$tourid."' LIMIT 1";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	if (isset($result) && is_array($result) && count($result)>0 ){
		
		 return  $result['tourcatid'];
	 }else {
		  return "NA";
	 }
}

/**********************************************************/

/**
	Tour City function
*/

function tourCity($tourid) {
	$sql = "select cityid from tour where id = '".$tourid."' LIMIT 1";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	if (isset($result) && is_array($result) && count($result)>0 ){
		
		 return $result['cityid'];
	 }else {
		  return "NA";
	 }
}

/**********************************************************/

//print_r( strtotime( cartDetailComboById($_POST['cart_user_id'])[0]['TourDate']));
//echo "<Hello>";
//echo strtotime('22-06-2017');die;
/**

API response 

**/

if (isset($_POST['couponcode']) && $_POST['couponcode']!= "" && isset($_POST['cart_user_id']) && $_POST['cart_user_id']!= "") {
	
/*       Coupon code isn't reside in db                 */
                                           
	if (couponInfo($_POST['couponcode'])) {
		
			$couponinfo = couponInfo($_POST['couponcode']);
		/*
			maximum number of uses
		
		*/
		
		if(couponMaximumNumberUsed($couponinfo) <= couponMaximumUses($couponinfo) ) {
				
			
		}else {
		$response["success"] = 0;
 		$response["message"] = "Maximum number uses got";
 		echo json_encode($response);die;
		}
		
	/*******************************************************************************/
	
	/*
			Booking Date
		
		*/
		$couponBookingdateArray = couponBookingDate($couponinfo);
		$Bookingstart = strtotime($couponBookingdateArray['bookingfrom']);
		$Bookingend = strtotime($couponBookingdateArray['bookingto']); 
		//echo $Bookingstart;echo "<hr/>";echo $Bookingend;echo "<hr/>";echo $todaystrtotime;
		//die;
		
		if($todaystrtotime>=$Bookingstart && $todaystrtotime<=$Bookingend) {
			
		}else {
		$response["success"] = 0;
 		$response["message"] = "Booking time over";
 		echo json_encode($response);die;
		}
		
		
		
	/*******************************************************************************/
	
	/**
			Check Coupon For Tour
	
	**/
	$cartDetailTour = cartDetailTourById($_POST['cart_user_id']);
	//print_r($cartDetailTour);
		if(isset($cartDetailTour) && is_array($cartDetailTour) && count($cartDetailTour)>0 ) {
			foreach($cartDetailTour as $cartDetailTourK => $cartDetailTourV ) {
				$overAllTotal+=$cartDetailTourV['DisFullTotal']; 
		/**
			MINIMUM CONDITION
	
	**/
				$couponMinimumConditionType =	couponMinimumConditionType($couponinfo);
				if($couponMinimumConditionType == 'CV') {
					if(couponMinimumConditionValue($couponinfo)){
						$couponMinimumConditionValue = couponMinimumConditionValue($couponinfo);
						
					}else{
						$couponMinimumConditionValue = 0;
					}
					
					if($couponMinimumConditionValue>$cartDetailTourV['DisFullTotal']) {
					  $tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
					}else{
			/**
			Travelling Date
			**/
			$coupontravellingdateArray = couponTravellingDate($couponinfo);
		   	$Travellingstart = strtotime($coupontravellingdateArray['travellingfrom']);
		  	$Travellingend = strtotime($coupontravellingdateArray['travellingto']); 
			$tourdatestrtotime=strtotime($cartDetailTourV['TourDate']); 
			if($tourdatestrtotime>=$Travellingstart && $tourdatestrtotime<=$Travellingend) {
				/**
					start coupon code
				*/
				if (couponApplicableTo($couponinfo)) {
					
					$couponApplicableTo = couponApplicableTo($couponinfo);
				/**
					For Tour
				*/
				
				
				
				if($couponApplicableTo == 3 ) {
					
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					if(in_array($cartDetailTourV['TourId'],$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue =CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$tourTotal[$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
						//echo $cartDetailTourV['TourId'];die;
					}else{
						$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
					//print_r($couponApplicableValueArray);die;
						
				}elseif($couponApplicableTo == 2){
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCategory = tourCategory($cartDetailTourV['TourId']); 
					if(in_array($tourCategory,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$tourTotal[$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}elseif($couponApplicableTo == 1) {
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCity = tourCity($cartDetailTourV['TourId']); 
					if(in_array($tourCity,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$tourTotal[$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}else{
					$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
				}
				
				
				
				/*******************************************************************************/	
					
					
					
				}else {
					
				}
				
				
				
				
				
				/*******************************************************************************/	
			
			}else {
					$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
			}
			/*******************************************************************************/		
						
					}
					
					
				}elseif($couponMinimumConditionType == 'MP'){
					
					if(couponMinimumConditionValue($couponinfo)){
						$couponMinimumConditionValue = couponMinimumConditionValue($couponinfo);
						
					}else{
						$couponMinimumConditionValue = 0;
					}
					
					if(($cartDetailTourV['sum(AdultQuantity)']+$cartDetailTourV['sum(ChildQuantity)'])<$couponMinimumConditionValue) {
					  $tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
					}else{
			/**
			Travelling Date
			**/
			$coupontravellingdateArray = couponTravellingDate($couponinfo);
		   	$Travellingstart = strtotime($coupontravellingdateArray['travellingfrom']);
		  	$Travellingend = strtotime($coupontravellingdateArray['travellingto']); 
			$tourdatestrtotime=strtotime($cartDetailTourV['TourDate']); 
			if($tourdatestrtotime>=$Travellingstart && $tourdatestrtotime<=$Travellingend) {
				/**
					start coupon code
				*/
				if (couponApplicableTo($couponinfo)) {
					
					$couponApplicableTo = couponApplicableTo($couponinfo);
				/**
					For Tour
				*/
				
				
				
				if($couponApplicableTo == 3 ) {
					
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					if(in_array($cartDetailTourV['TourId'],$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$tourTotal[$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
						//echo $cartDetailTourV['TourId'];die;
					}else{
						$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
					//print_r($couponApplicableValueArray);die;
						
				}elseif($couponApplicableTo == 2){
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCategory = tourCategory($cartDetailTourV['TourId']); 
					if(in_array($tourCategory,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$tourTotal[$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}elseif($couponApplicableTo == 1) {
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCity = tourCity($cartDetailTourV['TourId']); 
					if(in_array($tourCity,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$tourTotal[$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}else{
					$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
				}
				
				
				
				/*******************************************************************************/	
					
					
					
				}else {
					
				}
				
				
				
				
				
				/*******************************************************************************/	
			
			}else {
					$tourTotal[$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
			}
			/*******************************************************************************/		
						
					}
					
					
				
				}else{
					$response["success"] = 0;
 					$response["message"] = "Coupon invalid";
 					echo json_encode($response);die;
					
				}
	
	/*******************************************************************************/	
			}
		}
		
	/**
			Check Coupon For Combo
	
	**/
	
	$cartDetailCombo = cartDetailComboById($_POST['cart_user_id']);
	//print_r($cartDetailCombo);die;
		if(isset($cartDetailCombo) && is_array($cartDetailCombo) && count($cartDetailCombo)>0 ) {
			foreach($cartDetailCombo as $cartDetailTourK => $cartDetailTourV ) {
		/**
			MINIMUM CONDITION
	
	**/
				$overAllTotal+=$cartDetailTourV['DisFullTotal']; 
				$couponMinimumConditionType =	couponMinimumConditionType($couponinfo);
				if($couponMinimumConditionType == 'CV') {
					if(couponMinimumConditionValue($couponinfo)){
						$couponMinimumConditionValue = couponMinimumConditionValue($couponinfo);
						
					}else{
						$couponMinimumConditionValue = 0;
					}
					
					if($couponMinimumConditionValue>$cartDetailTourV['DisFullTotal']) {
					  $comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
					}else{
			/**
			Travelling Date
			**/
			$coupontravellingdateArray = couponTravellingDate($couponinfo);
		   	$Travellingstart = strtotime($coupontravellingdateArray['travellingfrom']);
		  	$Travellingend = strtotime($coupontravellingdateArray['travellingto']); 
			$tourdatestrtotime=strtotime($cartDetailTourV['TourDate']); 
			if($tourdatestrtotime>=$Travellingstart && $tourdatestrtotime<=$Travellingend) {
				/**
					start coupon code
				*/
				if (couponApplicableTo($couponinfo)) {
					
					$couponApplicableTo = couponApplicableTo($couponinfo);
				/**
					For Tour
				*/
				
				
				
				if($couponApplicableTo == 3 ) {
					
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					if(in_array($cartDetailTourV['TourId'],$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue =CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
						//echo $cartDetailTourV['TourId'];die;
					}else{
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
					//print_r($couponApplicableValueArray);die;
						
				}elseif($couponApplicableTo == 2){
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCategory = tourCategory($cartDetailTourV['TourId']); 
					if(in_array($tourCategory,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}elseif($couponApplicableTo == 1) {
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCity = tourCity($cartDetailTourV['TourId']); 
					if(in_array($tourCity,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}else{
					$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
				}
				
				
				
				/*******************************************************************************/	
					
					
					
				}else {
					
				}
				
				
				
				
				
				/*******************************************************************************/	
			
			}else {
					$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
			}
			/*******************************************************************************/		
						
					}
					
					
				}elseif($couponMinimumConditionType == 'MP'){
					
					if(couponMinimumConditionValue($couponinfo)){
						$couponMinimumConditionValue = couponMinimumConditionValue($couponinfo);
						
					}else{
						$couponMinimumConditionValue = 0;
					}
					
					if(($cartDetailTourV['sum(AdultQuantity)']+$cartDetailTourV['sum(ChildQuantity)'])<$couponMinimumConditionValue) {
					  $comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
					}else{
			/**
			Travelling Date
			**/
			$coupontravellingdateArray = couponTravellingDate($couponinfo);
		   	$Travellingstart = strtotime($coupontravellingdateArray['travellingfrom']);
		  	$Travellingend = strtotime($coupontravellingdateArray['travellingto']); 
			$tourdatestrtotime=strtotime($cartDetailTourV['TourDate']); 
			if($tourdatestrtotime>=$Travellingstart && $tourdatestrtotime<=$Travellingend) {
				/**
					start coupon code
				*/
				if (couponApplicableTo($couponinfo)) {
					
					$couponApplicableTo = couponApplicableTo($couponinfo);
				/**
					For Tour
				*/
				
				
				
				if($couponApplicableTo == 3 ) {
					
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					if(in_array($cartDetailTourV['TourId'],$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
						//echo $cartDetailTourV['TourId'];die;
					}else{
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
					//print_r($couponApplicableValueArray);die;
						
				}elseif($couponApplicableTo == 2){
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCategory = tourCategory($cartDetailTourV['TourId']); 
					if(in_array($tourCategory,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}elseif($couponApplicableTo == 1) {
					$couponApplicableValue = couponApplicableValue($couponinfo);
					$couponApplicableValueArray = explode(",",$couponApplicableValue);
					$tourCity = tourCity($cartDetailTourV['TourId']); 
					if(in_array($tourCity,$couponApplicableValueArray)) {
						$touramount = $cartDetailTourV['DisFullTotal'];
						$coupontype = couponType($couponinfo);
						$coupondiscounttourvalue = CouponDiscountValueCart($touramount,$coupontype,$couponinfo,$cartDetailTourV['type'],$cartDetailTourV['TourId'],$cartDetailTourV['cart_user_id'],$cartDetailTourV['ComboId']);
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = round($coupondiscounttourvalue,2);
						$CouponUsed++;
					}else{
						$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
						
					}
					
				}else{
					$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
				}
				
				
				
				/*******************************************************************************/	
					
					
					
				}else {
					
				}
				
				
				
				
				
				/*******************************************************************************/	
			
			}else {
					$comboTotal[$cartDetailTourV['ComboId']][$cartDetailTourV['TourId']] = $cartDetailTourV['DisFullTotal'];
			}
			/*******************************************************************************/		
						
					}
					
					
				
				}else{
					$response["success"] = 0;
 					$response["message"] = "Coupon invalid";
 					echo json_encode($response);die;
					
				}
	
	/*******************************************************************************/	
			}
		}
	
	
	/*******************************************************************************/	
	$tourTotalsum = 0;
	$comboTotalsum = 0;
	if( isset($tourTotal) && is_array($tourTotal) && count($tourTotal)>0 ) {
		$tourTotalsum =array_sum($tourTotal);
	}else{
		$tourTotalsum =0;
	}
	if( isset($comboTotal) && is_array($comboTotal) && count($comboTotal)>0 ) {
		foreach($comboTotal as $comboTotalV) {
			$comboTotalsum+=array_sum($comboTotalV);
		}
		
	}else{
		$comboTotalsum = 0;
	}
	$totalSum = $tourTotalsum+$comboTotalsum;
	//print_r($tourTotal);
	//echo $CouponUsed;
	//print_r($comboTotal);
	//die;
	if ($overAllTotal>$totalSum) {
		$response["success"] = 1;
 		$response["message"] = "Coupon valid";
		$response["total"] = $totalSum ;
		$response["discount"] = $overAllTotal - $totalSum; 
 		echo json_encode($response);die;
	}else{
		$response["success"] = 0;
 		$response["message"] = "Coupon invalid";
 		echo json_encode($response);
	}
	//echo $totalSum;die;
	
	
	
	}else{
		$response["success"] = 0;
 		$response["message"] = "Coupon invalid";
 		echo json_encode($response);
	}
/**********************************************************/	

}else {
 $response["success"] = 0;
 $response["message"] = "Required field(s) is missing";
 echo json_encode($response);	
	
}
	
   
?>