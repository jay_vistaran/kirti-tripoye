<?php
 
/*
 * Following code 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 24-11-2016
 */
 
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

// array for JSON response
$response = array();
 
// check for required fields
   if (isset($_POST['cityid']) && $_POST['cityid']!='') {
       
        $SQL = "SELECT * FROM `tour_category` WHERE cityid = '" . $_POST['cityid'] . "' ORDER BY `id` ASC";
        $result=MySQL::query($SQL);
        // check if row inserted or not
        if ($result) {
        // successfully inserted into database
        $response["successTC"] = 1;
        $response["messageTC"] = "All Tour Category .";
        $response["upto"] = 4;
        $response["allTourCategory"] = $result;
       // $response["allTourCategory"]["upto"]=4;
        
        // echoing JSON response
        //echo json_encode($response);
        }
        else{
             $response["successTC"] = 0;
             $response["messageTC"] = "No  Tour Category found in the database";
 
        // echoing JSON response
        //echo json_encode($response);
        }
        /////Top Recommened Category///
        
        $SQL1 = "SELECT a . * , b.*
                                    FROM `toprecommened_cat` a
                                    LEFT JOIN tour_category b ON a.`tourcatId` = b.id WHERE a.cityid = '" . $_POST['cityid'] . "' order by a.torder ASC";
        $result1=MySQL::query($SQL1);
        // check if row inserted or not
        if ($result1) {
        // successfully inserted into database
        $response["successTRC"] = 1;
        $response["messageTRC"] = "All Top Recommened Category .";
        $response["upto"] = 5;
        $response["topRecommenedCategory"] = $result1;
        // echoing JSON response
        //echo json_encode($response);
        }
        else{
             $response["successTRC"] = 0;
             $response["messageTRC"] = "No  Top Recommened Category found in the database";
 
        // echoing JSON response
        //echo json_encode($response);
        }
        
        
         /////Top Selling Category///
        
        $SQL2 = "SELECT a.* , b.*
                                    FROM `topselling_cat` a
                                    LEFT JOIN tour_category b ON a.`tourcatId` = b.id WHERE a.cityid = '" . $_POST['cityid'] . "' order by a.torder ASC";
        $result2=MySQL::query($SQL2);
        // check if row inserted or not
        if ($result2) {
        // successfully inserted into database
        $response["successTSC"] = 1;
        $response["messageTSC"] = "All Top Selling Category .";
        $response["upto"] = 6;
        $response["topSellingCategory"] = $result2;
        // echoing JSON response
        //echo json_encode($response);
        }
        else{
             $response["successTSC"] = 0;
             $response["messageTSC"] = "No  Top Selling Category found in the database";
 
        // echoing JSON response
        //echo json_encode($response);
        }
        echo json_encode($response);
        
        
        
 } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}  
?>