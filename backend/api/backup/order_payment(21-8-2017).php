<?php  header('Content-Type: application/json');
//echo "<pre>";
//print_r($json_decode_result);die;
 /*
 * Following code will set payment detail of order
 * Made By : D
 * DATE:20-06-2017
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
// array for JSON response
$response = array();
function cartDetailById($id) {
	$sql = "select * from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;
	$result = MySQL::query($sql);
	return $result; 
}
function deletecartById($id) {
	$sql = "delete from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;;die;
	$result = MySQL::query($sql);
	//return $result; 
}
function cartExist($id) {
	$sql = "select count(*) as cartcount from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;  
	$result = MySQL::query($sql,true);
	return $result; 
}
function cartExistDetail($id) {
	$sql = "select * from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;  
	$result = MySQL::query($sql,true);
	return $result; 
}
if (isset($_POST['order_id']) && $_POST['order_id']>0) {
	if(isset($_POST['id'])  && isset($_POST['mode']) && isset($_POST['status']) && isset($_POST['unmappedstatus']) && isset($_POST['key']) && isset($_POST['txnid']) && isset($_POST['transaction_fee']) &&isset($_POST['amount']) && isset($_POST['cardCategory']) &&isset($_POST['discount']) &&isset($_POST['additional_charges']) && isset($_POST['addedon']) && isset($_POST['productinfo']) && isset($_POST['email']) && isset($_POST['hash']) &&isset($_POST['field1']) &&isset($_POST['field2']) && isset($_POST['field3']) && isset($_POST['field4']) && isset($_POST['field9']) && isset($_POST['payment_source']) && isset($_POST['PG_TYPE']) && isset($_POST['bank_ref_no']) && isset($_POST['ibibo_code']) && isset($_POST['error_code']) && isset($_POST['Error_Message']) && isset($_POST['name_on_card']) && isset($_POST['issuing_bank']) && isset($_POST['card_type']) && isset($_POST['is_seamless']) && isset($_POST['surl']) && isset($_POST['furl'])) {
		$SQL = "UPDATE   `order_tripoye`  
		set payment_id = '".$_POST['id']."', 
		payment_mode = '".$_POST['mode']."',
		payment_status = '".$_POST['status']."', 
		payment_unmappedstatus = '".$_POST['unmappedstatus']."', 
		payment_key = '".$_POST['key']."',
		payment_txnid = '".$_POST['txnid']."',
		payment_transaction_fee = '".$_POST['transaction_fee']."',
		payment_amount = '".$_POST['amount']."',
		payment_cardCategory = '".$_POST['cardCategory']."',
		payment_discount = '".$_POST['discount']."',
		payment_additional_charges = '".$_POST['additional_charges']."',
		payment_addedon = '".$_POST['addedon']."',
		payment_productinfo = '".$_POST['productinfo']."',
		payment_email = '".$_POST['email']."',
		payment_hash = '".$_POST['hash']."',
		payment_field1 = '".$_POST['field1']."',
		payment_field2 = '".$_POST['field2']."',
		payment_field3 = '".$_POST['field3']."',
		payment_field4 = '".$_POST['field4']."',
		payment_field9 = '".$_POST['field9']."',
		payment_payment_source = '".$_POST['payment_source']."',
		payment_PG_TYPE = '".$_POST['PG_TYPE']."',
		payment_bank_ref_no = '".$_POST['bank_ref_no']."',
		payment_ibibo_code = '".$_POST['ibibo_code']."',
		payment_error_code = '".$_POST['error_code']."',
		payment_Error_Message = '".$_POST['Error_Message']."',
		payment_name_on_card = '".$_POST['name_on_card']."',
		payment_issuing_bank = '".$_POST['issuing_bank']."',
		payment_card_type = '".$_POST['card_type']."',
		payment_is_seamless = '".$_POST['is_seamless']."',
		payment_surl = '".$_POST['surl']."',
		payment_furl = '".$_POST['furl']."',
		order_status = 1
		WHERE `order_id` = " .$_POST['order_id'] . "";
		//echo $SQL;die;
		$res=MySQL::query($SQL,true);
        $response["success"] = 1;
		$response["message"] = "Payment detail added";
		echo json_encode($response);
		
	}else{
		 $response["success"] = 0;
    	 $response["message"] = "Required field(s) is missing";
 		 echo json_encode($response);
	}
}else {
	$response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 	echo json_encode($response);
}
?>