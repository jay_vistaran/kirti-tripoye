<?php header('Content-Type: application/json');
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
$today = date('Y-m-d');
	   $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid = $getSystemDefaultSeason['id'];
		}else {
			$sid = 0;
		}
$response = array();
   if (isset($_POST['cityid']) && $_POST['cityid']!='') {
       $SQL = "SELECT a.id,a.tourname,a.countryid,a.cityid,a.tourcatid,a.tlocation, b.countryname, c.cityname, d.tourcatname
                                FROM `tour` a
                                LEFT JOIN country b ON a.`countryid` = b.id
                                LEFT JOIN city c ON a.`cityid` = c.id
                                LEFT JOIN tour_category d ON a.`tourcatid` = d.id
                                WHERE a.cityid = '" . $_POST['cityid'] . "' and a.recommended = 1 AND a.`countryid` = b.id AND a.`cityid` = c.id AND a.`tourcatid` = d.id
                                ORDER BY a.`tourname` ASC";
       
        $result=MySQL::query($SQL);
        if ($result) {  
            $newTC =array();
            foreach ($result as $key => $allTour) {
                $SQL1="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                            WHERE `TourId` ='".$allTour['id']."'
                            ORDER BY galleryorder ASC LIMIT 1
                             ";
                $result11=MySQL::query($SQL1,true);
    			if (isset($result11) && is_array($result11) && count($result11)>0 ) {
    			$result[$key]['gallery_img'] = $result11['gallery_img'];
    			$result[$key]['gallery_img_url'] = $result11['gallery_img_url'];
    			}else {
    				$result[$key]['gallery_img'] = "";
    				$result[$key]['gallery_img_url'] = "";
    			}

    			//for gettying the tour likes
				$result[$key]['tour_like'] =0;
				if (isset($_POST['UserId']) && !empty($_POST['UserId'])) {
					$SQL141="SELECT tlike as tour_like FROM `tour_like` WHERE 1 AND TourId = ".$allTour['id']." AND UserId = ".$_POST['UserId']."";
		            $result1147=MySQL::query($SQL141,true);
					if ($result1147['tour_like'] != ""){
						$result[$key]['tour_like'] =$result1147['tour_like'];
					}
				}
                /*$result['tour_like'] =0;
                if (isset($_POST['UserId']) && !empty($_POST['UserId'])) {
                    $SQL141="SELECT tlike as tlike FROM `tour_like` WHERE 1 AND TourId = ".$allTour['id']." AND UserId=".$_POST['UserId']."";
                    $result1147=MySQL::query($SQL141,true);
                    if ($result1147['tlike'] != ""){
                        $result['tour_like'] =$result1147['tlike'];
                    }
                }*/
    			
    			$result11 = array();
    			$SQL1="SELECT tds.sellAP,tds.sellCP,tds.discount,tds.discountType FROM tour_otheroption toh inner join tour_add_services tds on toh.id = tds.tourOtherOptionId 
                            WHERE toh.TourId = '".$allTour['id']."' AND toh.base_pack = 1 AND tds.seasonId = ".$sid." AND toh.id = tds.tourOtherOptionId";
                $result11=MySQL::query($SQL1,true);
    			if (isset($result11) && is_array($result11) && count($result11) > 0 ) {
    				$result[$key]['sellAP'] =$result11['sellAP'];
    				$result[$key]['sellCP'] =$result11['sellCP'];
    				$result[$key]['discount'] =$result11['discount'];
    				$result[$key]['discountType'] =$result11['discountType'];
    					if($result['discountType'] == 0 ) {
    						$result[$key]['discountType'] = 0;
    					}else {
    						$result[$key]['discountType'] = 1;
    					}
    			}else{
    				$result[$key]['sellAP'] = "";
    				$result[$key]['sellCP'] = "";
    				$result[$key]['discount'] = "";
    				$result[$key]['discountType'] = "";
    			}
            } 
            $response["success"] = 1;
            $response["message"] = "All Best Tour .";
            $response["bestTour"] =$result;
            echo json_encode($response);
        }
        else{
            $response["success"] = 0;
            $response["message"] = "No Best Tour found in the database";
            echo json_encode($response);
        }
  } else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}   
?>