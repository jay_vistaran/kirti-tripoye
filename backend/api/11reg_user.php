<?php
 
/*
 * Following code will create a new User
 * All user details are read from HTTP Post Request
 * Made By : D
 * DATE: 12-03-2016
 */
 
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

// array for JSON response
$response = array();
 
// check for required fields
if ( isset($_POST['user_email']) && isset($_POST['user_password']) ) {
 
    $user_email = $_POST['user_email'];
    $user_password = $_POST['user_password'];
     $user_status = 1;
   /* $user_name = $_POST['user_name'];
    $user_surname = $_POST['user_surname'];
    $user_address = $_POST['user_address'];
    $user_dob = $_POST['user_dob'];
    $user_mobile = $_POST['user_mobile'];
    $latitiude = $_POST['latitiude'];// added by 20-5
    $longitude = $_POST['longitude'];// added by 20-5
   
    $user_role = 1;*/
 
        $getNewDocumentAndritz = Users::getSystemUsers();//for global
        $website_users=count($getNewDocumentAndritz)+1;
        $new_password = encryptPassword($user_password,$website_users);
    
    $Users = Users::getUserByEmailId($user_email);
//dump($Users);//die;

    if(empty($Users))
    {    
        $SQL = "INSERT INTO `users` (`user_email`, `user_password`, `user_status`)
               VALUES ( '" . $user_email . "', '" . $new_password . "', '" . $user_status . "')";
        $result = MySQL::query($SQL);
        $insertID = mysql_insert_id();
        // check if row inserted or not
        if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "User successfully created.";
        $getSystemUsers = Users::getUserByIdForAPI($insertID);
        $response["Users"] = $getSystemUsers;
        //$response["User_id"] = $insertID;
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
    
    
    }else{
        // required field is missing
    $response["success"] = 0;
    $response["message"] = "Email already exist";
    // echoing JSON response
    echo json_encode($response);
    }
    
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>