<?php //header('Content-Type: application/json');
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
date_default_timezone_set("Asia/Kolkata");
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

$response = array();
   if (isset($_POST['UserId']) && $_POST['UserId']!='') {
      	$SQL = "SELECT order_tripoye.order_id,order_tripoye.booking_ref_num,order_tripoye.payment_amount,order_tripoye.total,order_tripoye.date_added as orderdate,rateofcurrency.currencycode,rateofcurrency.currencysymbol,order_status.name as order_status, order_tripoye.payment_txnid as txnid from order_tripoye as order_tripoye
	LEFT JOIN rateofcurrency as rateofcurrency ON order_tripoye.currency_id = rateofcurrency.id
	LEFT JOIN order_status as order_status ON order_tripoye.order_status = order_status.order_status_id
	WHERE order_tripoye.customer_id= '".$_POST['UserId']."' ORDER BY order_tripoye.order_id DESC";
        $result=MySQL::query($SQL);
        if ($result) {
            foreach ($result as $keyOrder => $allOrder) {
            	if(empty($allOrder['currencycode'])){
            		$result[$keyOrder]['currencycode'] = 'INR';
            	}
            	if(empty($allOrder['currencysymbol'])){
            		$result[$keyOrder]['currencysymbol']  = '₹';
            	}
                $SqlOrderItem = "SELECT order_item.TourId,order_item.TourOptId,order_item.TourName,order_item.TourDate,order_item.TourOptName, tour_galleryrimage.gallery_img,tour_galleryrimage.gallery_img_url, order_item.AdultQuantity, order_item.ChildQuantity, order_item.InfantQuantity
                from order_item as order_item
                LEFT JOIN tour_galleryrimage as tour_galleryrimage ON order_item.TourId = tour_galleryrimage.TourId
                WHERE order_item.order_id='".$allOrder['order_id']."' GROUP BY order_item.TourId ORDER BY order_item.order_item_id DESC";
        	$resultOrderItem=MySQL::query($SqlOrderItem);
        	$result[$keyOrder]['tour_list'] = $resultOrderItem;
        	/*$result[$keyOrder]['TourId'] = $resultOrderItem[0]['TourId'];
        	$result[$keyOrder]['TourName'] = $resultOrderItem[0]['TourName'];
        	$SQLImage="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                            WHERE `TourId` ='".$resultOrderItem['TourId']."'
                            ORDER BY galleryorder ASC LIMIT 1
                             ";
                $resultImage=MySQL::query($SQLImage,true);
                if (isset($resultImage) && is_array($resultImage) && count($resultImage)>0 ) {
    			$result[$keyOrder]['gallery_img'] = $resultImage['gallery_img'];
    			$result[$keyOrder]['gallery_img_url'] = $resultImage['gallery_img_url'];
		}else {
			$result['gallery_img'] = "";
			$result['gallery_img_url'] = "";
		}*/
                
            }
            $response["success"] = 1;
            $response["message"] = "Order List.";
            $response["orderlist"] =$result;
            //print_r($response);die;
            echo json_encode($response);
        }
        else{
            $response["success"] = 0;
            $response["message"] = "No Order found in the database for this user";
            echo json_encode($response);
        }
  } else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}   
?>