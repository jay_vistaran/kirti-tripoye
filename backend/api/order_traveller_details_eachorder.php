<?php
 
/*
 * Following code insert the tour operator details 
 * All user details are read from HTTP POST Request
 * Made By : Abhinav
 * DATE: 17-07-2017
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

// array for JSON response
$response = array();
// check for required fields
   if (isset($_POST['order_id']) && $_POST['order_id']!='' && isset($_POST['combo_id']) && $_POST['combo_id']!='' && isset($_POST['name']) && $_POST['name']!='' && isset($_POST['lastname']) && $_POST['lastname']!='' && isset($_POST['remark']) && $_POST['remark']!=''  && isset($_POST['email']) && $_POST['email']!='' && isset($_POST['phone']) && $_POST['phone']!='') {
           
        $SQL = "INSERT INTO `order_traveller_detail` (`order_id`, `comboId`,`name`,`lastname`,`remark`, `email`, `phone`)
               VALUES ( " . $_POST['order_id'] . ", " . $_POST['combo_id'] . ", '" . $_POST['name'] . "', '" . $_POST['lastname'] . "', '" . $_POST['remark'] . "', '" . $_POST['email'] . "', '" . $_POST['phone'] . "')";
        $result=MySQL::query($SQL);
        $response["success"] = 1;
        $response["message"] = "Sucessfully Added Traveller Details.";
 
        // echoing JSON response
       	echo json_encode($response);
 } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}  
//dump($response);
?>