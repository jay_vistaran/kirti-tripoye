<?php 
 /*
 * Following code will Confirm Cart
 * All user details are read from HTTP POST Request
 * Made By : D
 * DATE: 17-03-2016
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");

// array for JSON response
$response = array();
function cartDetailById($id) {
	$sql = "select * from tour_cart where cart_id = ".$id."";
	$result = MySQL::query($sql,true);
	return $result; 
}
function checkPrice($TourId,$TourOptionId,$seasonid) {
	$sql = "select sellAP,sellCP,discount,discountType from tour_add_services where TourId = ".$TourId." AND tourOtherOptionId =".$TourOptionId." AND seasonId = ".$seasonid."";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
}
function checkTransferPrice($TourId,$TransferId,$seasonid) {
	$sql = "select sellAP,sellCP,discount,discountType,msellAP from tour_price where TourId = ".$TourId." AND tourTransOptionId =".$TransferId." AND seasonId = ".$seasonid."";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
}
function cartExist($id) {
	$sql = "select count(*) as cartcount from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;  
	$result = MySQL::query($sql,true);
	return $result; 
}
function cartExistDetail($id) {
	$sql = "select * from tour_cart where cart_user_id = '".$id."'";
	//echo $sql;die;  
	$result = MySQL::query($sql);
	return $result; 
}
function sharingTransferOptionSameTourOptionCart($TourId,$seasonid,$user_id) {
	$sql = "select sum(SingleTotal) as SingTotal ,sum(SingleDisTotal) as SingDisTotal,sum(AdultQuantity) as AdultQuantity,sum(ChildQuantity) as ChildQuantity from tour_cart where TourId = ".$TourId." AND SessionId = ".$seasonid." AND cart_user_id ='".$user_id."'  AND ComboId = 0";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
}
function privateTransferOptionSameTourOptionCart($TourId,$seasonid,$user_id) {
	$sql = "select sum(SingleTotal) as SingTotal ,sum(SingleDisTotal) as SingDisTotal,sum(AdultQuantity) as AdultQuantity,sum(ChildQuantity) as ChildQuantity from tour_cart where TourId = ".$TourId." AND SessionId = ".$seasonid." AND cart_user_id ='".$user_id."' AND ComboId = 0";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
}
function checkTransferPricePrivate($TourId,$seasonid,$TransferId,$nmember) {
	$sql = "select sellAP,sellCP,discount,discountType,msellAP from tour_price where TourId = ".$TourId." AND tourTransOptionId =".$TransferId." AND seasonId = ".$seasonid." AND minpax <= ".$nmember. " AND maxpax >= ".$nmember." LIMIT 1";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
}
/*          Combo Function                                   */

function sharingTransferOptionSameTourOptionCartCombo($TourId,$seasonid,$user_id,$combo_id) {
	$sql = "select sum(SingleTotal) as SingTotal ,sum(SingleDisTotal) as SingDisTotal,sum(AdultQuantity) as AdultQuantity,sum(ChildQuantity) as ChildQuantity from tour_cart where TourId = ".$TourId." AND SessionId = ".$seasonid." AND cart_user_id ='".$user_id."'  AND ComboId = '".$combo_id."'";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
}

function privateTransferOptionSameTourOptionCartCombo($TourId,$seasonid,$user_id,$combo_id) {
	$sql = "select sum(SingleTotal) as SingTotal ,sum(SingleDisTotal) as SingDisTotal,sum(AdultQuantity) as AdultQuantity,sum(ChildQuantity) as ChildQuantity from tour_cart where TourId = ".$TourId." AND SessionId = ".$seasonid." AND cart_user_id ='".$user_id."' AND ComboId = '".$combo_id."'";
	//echo $sql;die;
	$result = MySQL::query($sql,true);
	return $result; 
}

/*          Combo Function                                   */

if (isset($_POST['cart_user_id'])  && $_POST['cart_user_id']!= "") {
	$cartDeatil = cartExistDetail($_POST['cart_user_id']);

	if (isset($cartDeatil) && is_array($cartDeatil) && count($cartDeatil)>0 ) {
		foreach ($cartDeatil as $key => $value ) {
			if ($value['type'] == 0 ) { 
			$checkPrice = checkPrice($value['TourId'],$value['TourOptId'],$value['SessionId']);
			
			if (isset($checkPrice) && is_array($checkPrice) && count($checkPrice) > 0 ) {
				if($value['AgeType'] == 1 ) {
					if ($checkPrice['sellAP'] == $value['AdultPrice'] &&  $checkPrice['discount'] == $value['Discount'] && $checkPrice['discountType'] == $value['DiscountType'] ) {
					$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;
					
			}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
			}
			
			if ($value['TransferType'] == 1 ){

				$transferPrice = checkTransferPrice($value['TourId'],$value['TransferId'],$value['SessionId']);
				if (isset($transferPrice) && is_array($transferPrice) && count($transferPrice) > 0 ) {
					$sharingTransferOptionSameTourOptionCart = sharingTransferOptionSameTourOptionCart($value['TourId'],$value['SessionId'],$_POST['cart_user_id']);
					$SingTotal =$sharingTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$sharingTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$sharingTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$sharingTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$transferPrice['sellAP'];
					$transferPriceChild =$transferPrice['sellCP']; 
					$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);
					$fullTotal = $TotaltransferPriceAdult+$SingTotal+$TotaltransferPriceChild;
					if ( $fullTotal == $value['FullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					if($transferPrice['discountType'] == 0) {
					$transferPriceAdultDis = round(($transferPrice['sellAP']-($transferPrice['sellAP']*$transferPrice['discount'])/100),2); 
					$transferPriceChildDis = round(($transferPrice['sellCP']-($transferPrice['sellCP']*$transferPrice['discount'])/100),2);
					}else {
						$transferPriceAdultDis =$transferPrice['sellAP']-$transferPrice['discount'];
						$transferPriceChildDis =$transferPrice['sellCP']-$transferPrice['discount'];
					}
					
					$TotaltransferPricedis =(round($AdultQuantity*$transferPriceAdultDis,2))+(round($transferPriceChildDis*$ChildQuantity,2));
					$fullTotaldis = round($TotaltransferPricedis+$SingDisTotal,2);	
					if ( $fullTotaldis == $value['DisFullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 2) {

				$privateTransferOptionSameTourOptionCart = privateTransferOptionSameTourOptionCart($value['TourId'],$value['SessionId'],$_POST['cart_user_id']);
				$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
				$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$nmember = $privateTransferOptionSameTourOptionCart['AdultQuantity'] + $privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$checkTransferPricePrivate = checkTransferPricePrivate($value['TourId'],$value['SessionId'],$value['TransferId'],$nmember);
				
				if (isset($checkTransferPricePrivate) && is_array($checkTransferPricePrivate) && count($checkTransferPricePrivate) > 0 ) {
				
					$SingTotal =$privateTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$privateTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$checkTransferPricePrivate['sellAP'];
					$transferPriceChild =$checkTransferPricePrivate['sellCP']; 
					$msellp =$checkTransferPricePrivate['msellAP'];
					
					/*if($checkTransferPricePrivate['discountType'] == 0) {
						$transferPriceAdultDis = round(($checkTransferPricePrivate['sellAP']-($checkTransferPricePrivate['sellAP']*$checkTransferPricePrivate['discount'])/100),2); 
					}else {
						$transferPriceAdultDis =$checkTransferPricePrivate['sellAP']-$checkTransferPricePrivate['discount'];
					}
					
					
					
					
					$averageTransferPrice = round($transferPriceAdultDis/$nmember,2);
					if($averageTransferPrice<$msellp) {
						$TotaltransferPriceAdult =round($msellp*$nmember,2);
					}else {
						$TotaltransferPriceAdult =$transferPriceAdultDis;
					}*/
					// Average Price Change Logic
					if($checkTransferPricePrivate['discountType'] == 0) {
						$transferPriceAdultDis = round(($checkTransferPricePrivate['sellAP']-($checkTransferPricePrivate['sellAP']*$checkTransferPricePrivate['discount'])/100),2); 
					}else {
						$transferPriceAdultDis =$checkTransferPricePrivate['sellAP']-$checkTransferPricePrivate['discount'];
					}
					
					// Average Price Change Logic
					
					
					$averageTransferPrice = round($transferPriceAdultDis/$nmember,2);

					if($averageTransferPrice<$msellp) {
						$TotaltransferPriceAdult =round($msellp*$nmember,2);

						
					if($checkTransferPricePrivate['discountType'] == 0) {
						$mselldis =round(($msellp*$checkTransferPricePrivate['discount'])/100,2);
						$mselldistotal =round($mselldis*$nmember,2);
						
						$TotaltransferPriceAdult = round(($TotaltransferPriceAdult-$mselldistotal),2); 
					}else {
						$TotaltransferPriceAdult =$TotaltransferPriceAdult-round($checkTransferPricePrivate['discount']*$nmember,2);
					}
						
					}else {
						$TotaltransferPriceAdult =$transferPriceAdultDis;
					}
					/*$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);*/
					
				    $fullTotaldis = round($TotaltransferPriceAdult+$SingDisTotal,2);
					if ($fullTotaldis == $value['DisFullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 3){
				
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 0;
			}
			
		}else {
					if ( $checkPrice['sellCP'] == $value['ChildPrice'] &&  $checkPrice['discount'] == $value['Discount'] && $checkPrice['discountType'] == $value['DiscountType'] ) {
					$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;
			}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
			}
			if ($value['TransferType'] == 1 ){
				$transferPrice = checkTransferPrice($value['TourId'],$value['TransferId'],$value['SessionId']);
				if (isset($transferPrice) && is_array($transferPrice) && count($transferPrice) > 0 ) {
					$sharingTransferOptionSameTourOptionCart = sharingTransferOptionSameTourOptionCart($value['TourId'],$value['SessionId'],$_POST['cart_user_id']);
					$SingTotal =$sharingTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$sharingTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$sharingTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$sharingTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$transferPrice['sellAP'];
					$transferPriceChild =$transferPrice['sellCP']; 
					$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);
					$fullTotal = $TotaltransferPriceAdult+$SingTotal+$TotaltransferPriceChild;
					if ( $fullTotal == $value['FullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					if($transferPrice['discountType'] == 0) {
					$transferPriceAdultDis = round(($transferPrice['sellAP']-($transferPrice['sellAP']*$transferPrice['discount'])/100),2); 
					$transferPriceChildDis = round(($transferPrice['sellCP']-($transferPrice['sellCP']*$transferPrice['discount'])/100),2);
					}else {
						$transferPriceAdultDis =$transferPrice['sellAP']-$transferPrice['discount'];
						$transferPriceChildDis =$transferPrice['sellCP']-$transferPrice['discount'];
					}
					
					$TotaltransferPricedis =(round($AdultQuantity*$transferPriceAdultDis,2))+(round($transferPriceChildDis*$ChildQuantity,2));
					$fullTotaldis = round($TotaltransferPricedis+$SingDisTotal,2);	
					if ( $fullTotaldis == $value['DisFullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 2) {
				$privateTransferOptionSameTourOptionCart = privateTransferOptionSameTourOptionCart($value['TourId'],$value['SessionId'],$_POST['cart_user_id']);
				$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
				$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$nmember = $privateTransferOptionSameTourOptionCart['AdultQuantity'] + $privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$checkTransferPricePrivate = checkTransferPricePrivate($value['TourId'],$value['SessionId'],$value['TransferId'],$nmember);
				if (isset($checkTransferPricePrivate) && is_array($checkTransferPricePrivate) && count($checkTransferPricePrivate) > 0 ) {
				
					$SingTotal =$privateTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$privateTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$checkTransferPricePrivate['sellAP'];
					$transferPriceChild =$checkTransferPricePrivate['sellCP']; 
					$msellp =$checkTransferPricePrivate['msellAP'];
					
					// Average Price Change Logic
					if($checkTransferPricePrivate['discountType'] == 0) {
						$transferPriceAdultDis = round(($checkTransferPricePrivate['sellAP']-($checkTransferPricePrivate['sellAP']*$checkTransferPricePrivate['discount'])/100),2); 
					}else {
						$transferPriceAdultDis =$checkTransferPricePrivate['sellAP']-$checkTransferPricePrivate['discount'];
					}
					
					// Average Price Change Logic
					
					
					$averageTransferPrice = round($transferPriceAdultDis/$nmember,2);
					
					if($averageTransferPrice<$msellp) {
						$TotaltransferPriceAdult =round($msellp*$nmember,2);
						
					if($checkTransferPricePrivate['discountType'] == 0) {
						$mselldis =round(($msellp*$checkTransferPricePrivate['discount'])/100,2);
						$mselldistotal =round($mselldis*$nmember,2);
						
						$TotaltransferPriceAdult = round(($TotaltransferPriceAdult-$mselldistotal),2); 
					}else {
						$TotaltransferPriceAdult =$TotaltransferPriceAdult-round($checkTransferPricePrivate['discount']*$nmember,2);
					}
						
					}else {
						$TotaltransferPriceAdult =$transferPriceAdultDis;
					}
					/*$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);*/
					
				    $fullTotaldis = round($TotaltransferPriceAdult+$SingDisTotal,2);	
					if ( $fullTotaldis == $value['DisFullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 3){
				
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 0;
			}
			
			
				}
				
				
			}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
			}
			
		}else {
			$SQL = "SELECT tours,tour_combo_price.comboDiscount   FROM tour_combo_t left join tour_combo_price on tour_combo_t.comboid = tour_combo_price.comboid  WHERE tour_combo_t.comboid ='".$value['ComboId']."' ";
			
        $resultcombo=MySQL::query($SQL,true);
		//echo "<pre>";
	//print_r($resultcombo);
	//die;
		if (isset($resultcombo) && is_array($resultcombo) && count($resultcombo)>0) {
		$comboDiscount = $resultcombo['comboDiscount'];
		if ($comboDiscount == $value['Discount']) {
			//$comboDiscount ="NA";
					$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;
		}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
		}
		/* To  Check Cart For Combo */
		
		 
			$checkPrice = checkPrice($value['TourId'],$value['TourOptId'],$value['SessionId']);
			
			if (isset($checkPrice) && is_array($checkPrice) && count($checkPrice) > 0 ) {
				if($value['AgeType'] == 1 ) {
					if ($checkPrice['sellAP'] == $value['AdultPrice']  ) {
					$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;
					
			}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
			}
			
			if ($value['TransferType'] == 1 ){
				$transferPrice = checkTransferPrice($value['TourId'],$value['TransferId'],$value['SessionId']);
				if (isset($transferPrice) && is_array($transferPrice) && count($transferPrice) > 0 ) {
					$sharingTransferOptionSameTourOptionCart = sharingTransferOptionSameTourOptionCartCombo($value['TourId'],$value['SessionId'],$_POST['cart_user_id'],$value['ComboId']);
					$SingTotal =$sharingTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$sharingTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$sharingTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$sharingTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$transferPrice['sellAP'];
					$transferPriceChild =$transferPrice['sellCP']; 
					$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);
					$fullTotal = $TotaltransferPriceAdult+$SingTotal+$TotaltransferPriceChild;
					if ( $fullTotal == $value['FullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					
					$transferPriceAdultDis = round(($transferPrice['sellAP']-($transferPrice['sellAP']*$comboDiscount)/100),2); 
					$transferPriceChildDis = round(($transferPrice['sellCP']-($transferPrice['sellCP']*$comboDiscount)/100),2);
					
					
					$TotaltransferPricedis =(round($AdultQuantity*$transferPriceAdultDis,2))+(round($transferPriceChildDis*$ChildQuantity,2));
					$fullTotaldis = round($TotaltransferPricedis+$SingDisTotal,2);	
					//echo $fullTotaldis;
					//echo "/n";
					
					if ( $fullTotaldis == $value['DisFullTotal'] ) {
						//echo "hello";
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 2) {
				$privateTransferOptionSameTourOptionCart = privateTransferOptionSameTourOptionCartCombo($value['TourId'],$value['SessionId'],$_POST['cart_user_id'],$value['ComboId']);
				$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
				$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$nmember = $privateTransferOptionSameTourOptionCart['AdultQuantity'] + $privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$checkTransferPricePrivate = checkTransferPricePrivate($value['TourId'],$value['SessionId'],$value['TransferId'],$nmember);
				if (isset($checkTransferPricePrivate) && is_array($checkTransferPricePrivate) && count($checkTransferPricePrivate) > 0 ) {
				
					$SingTotal =$privateTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$privateTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$checkTransferPricePrivate['sellAP'];
					$transferPriceChild =$checkTransferPricePrivate['sellCP']; 
					$msellp =$checkTransferPricePrivate['msellAP'];
					/*if($checkTransferPricePrivate['discountType'] == 0) {
						$transferPriceAdultDis = round(($checkTransferPricePrivate['sellAP']-($checkTransferPricePrivate['sellAP']*$checkTransferPricePrivate['discount'])/100),2); 
					}else {
						$transferPriceAdultDis =$checkTransferPricePrivate['sellAP']-$checkTransferPricePrivate['discount'];
					}
					$averageTransferPrice = round($transferPriceAdultDis/$nmember,2);
					if($averageTransferPrice<$msellp) {
						$TotaltransferPriceAdult =round($msellp*$nmember,2);
					}else {
						$TotaltransferPriceAdult =$transferPriceAdultDis;
					}*/
					// Average Price Change Logic
					
						$transferPriceAdultDis = round(($checkTransferPricePrivate['sellAP']-($checkTransferPricePrivate['sellAP']*$comboDiscount)/100),2); 
					
					
					// Average Price Change Logic
					
					
					$averageTransferPrice = round($transferPriceAdultDis/$nmember,2);
					
					if($averageTransferPrice<$msellp) {
						$TotaltransferPriceAdult =round($msellp*$nmember,2);
						
					
						$mselldis =round(($msellp*$comboDiscount)/100,2);
						$mselldistotal =round($mselldis*$nmember,2);
						
						$TotaltransferPriceAdult = round(($TotaltransferPriceAdult-$mselldistotal),2); 
					
						
					}else {
						$TotaltransferPriceAdult =$transferPriceAdultDis;
					}
					/*$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);*/
					
				    $fullTotaldis = round($TotaltransferPriceAdult+$SingDisTotal,2);	
					if ( $fullTotaldis == $value['DisFullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 3){
				$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;
				
			}
			
		}else {
					if ( $checkPrice['sellCP'] == $value['ChildPrice'] ) {
					$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;
			}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
			}
			if ($value['TransferType'] == 1 ){
				$transferPrice = checkTransferPrice($value['TourId'],$value['TransferId'],$value['SessionId']);
				if (isset($transferPrice) && is_array($transferPrice) && count($transferPrice) > 0 ) {
					$sharingTransferOptionSameTourOptionCart = sharingTransferOptionSameTourOptionCartCombo($value['TourId'],$value['SessionId'],$_POST['cart_user_id'],$value['ComboId']);
					$SingTotal =$sharingTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$sharingTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$sharingTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$sharingTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$transferPrice['sellAP'];
					$transferPriceChild =$transferPrice['sellCP']; 
					$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);
					$fullTotal = $TotaltransferPriceAdult+$SingTotal+$TotaltransferPriceChild;
					if ( $fullTotal == $value['FullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					
					$transferPriceAdultDis = round(($transferPrice['sellAP']-($transferPrice['sellAP']*$comboDiscount)/100),2); 
					$transferPriceChildDis = round(($transferPrice['sellCP']-($transferPrice['sellCP']*$comboDiscount)/100),2);
					
					
					$TotaltransferPricedis =(round($AdultQuantity*$transferPriceAdultDis,2))+(round($transferPriceChildDis*$ChildQuantity,2));
					$fullTotaldis = round($TotaltransferPricedis+$SingDisTotal,2);	
					//echo $fullTotaldis;echo "<hr/>";
					if ( $fullTotaldis == $value['DisFullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 2) {
				$privateTransferOptionSameTourOptionCart = privateTransferOptionSameTourOptionCartCombo($value['TourId'],$value['SessionId'],$_POST['cart_user_id'],$value['ComboId']);
				$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
				$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$nmember = $privateTransferOptionSameTourOptionCart['AdultQuantity'] + $privateTransferOptionSameTourOptionCart['ChildQuantity'];
				$checkTransferPricePrivate = checkTransferPricePrivate($value['TourId'],$value['SessionId'],$value['TransferId'],$nmember);
				if (isset($checkTransferPricePrivate) && is_array($checkTransferPricePrivate) && count($checkTransferPricePrivate) > 0 ) {
				
					$SingTotal =$privateTransferOptionSameTourOptionCart['SingTotal'];
					$SingDisTotal =$privateTransferOptionSameTourOptionCart['SingDisTotal'];
					$AdultQuantity =$privateTransferOptionSameTourOptionCart['AdultQuantity'];
					$ChildQuantity =$privateTransferOptionSameTourOptionCart['ChildQuantity'];
					$transferPriceAdult =$checkTransferPricePrivate['sellAP'];
					$transferPriceChild =$checkTransferPricePrivate['sellCP']; 
					$msellp =$checkTransferPricePrivate['msellAP'];
					/*if($checkTransferPricePrivate['discountType'] == 0) {
						$transferPriceAdultDis = round(($checkTransferPricePrivate['sellAP']-($checkTransferPricePrivate['sellAP']*$checkTransferPricePrivate['discount'])/100),2); 
					}else {
						$transferPriceAdultDis =$checkTransferPricePrivate['sellAP']-$checkTransferPricePrivate['discount'];
					}
					$averageTransferPrice = round($transferPriceAdultDis/$nmember,2);
					if($averageTransferPrice<$msellp) {
						$TotaltransferPriceAdult =round($msellp*$nmember,2);
					}else {
						$TotaltransferPriceAdult =$transferPriceAdultDis;
					}*/
					// Average Price Change Logic
					
						$transferPriceAdultDis = round(($checkTransferPricePrivate['sellAP']-($checkTransferPricePrivate['sellAP']*$comboDiscount)/100),2); 
					
					
					// Average Price Change Logic
					
					
					$averageTransferPrice = round($transferPriceAdultDis/$nmember,2);
					
					if($averageTransferPrice<$msellp) {
						$TotaltransferPriceAdult =round($msellp*$nmember,2);
						
					
						$mselldis =round(($msellp*$comboDiscount)/100,2);
						$mselldistotal =round($mselldis*$nmember,2);
						
						$TotaltransferPriceAdult = round(($TotaltransferPriceAdult-$mselldistotal),2); 
					
						
					}else {
						$TotaltransferPriceAdult =$transferPriceAdultDis;
					}
					/*$TotaltransferPriceAdult =round($AdultQuantity*$transferPriceAdult,2);
					$TotaltransferPriceChild =round($ChildQuantity*$transferPriceChild,2);*/
					
				    $fullTotaldis = round($TotaltransferPriceAdult+$SingDisTotal,2);	
					if ( $fullTotaldis == $value['DisFullTotal'] ) {
						
					}else {
						$response["success"] = 1;
        				$response["message"] = "Expire cart flag";
						$response["expire_flag"] = 1;
						break;
					}
					/*$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;*/
				}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
				}
				
			}elseif($value['TransferType'] == 3){
				$response["success"] = 1;
        			$response["message"] = "Expire cart flag";
					$response["expire_flag"] = 0;
				
			}
			
			
				}
				
				
			}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
			}
			
		
		
		
		
		
		
		
		
		/* To  Check Cart For Combo  */
		
						
		
		}else {
				$response["success"] = 1;
        		$response["message"] = "Expire cart flag";
				$response["expire_flag"] = 1;
				break;
		}
		}
		//echo "<pre>";
	//print_r($combo_price);
	//die;
}
echo json_encode($response); die;		
}else {
	$response["success"] = 0;
    $response["message"] = "Cart is empty";
	echo json_encode($response);
}
}else {
	$response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
	
   
?>