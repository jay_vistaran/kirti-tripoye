<?php
 
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
 
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

// array for JSON response
$response = array();
 
// check for required fields
   if (isset($_POST['cityid']) && $_POST['cityid']!='') {
       $SQL = "SELECT a. * , b.countryname, c.cityname, d.tourcatname
                                FROM `tour` a
                                LEFT JOIN country b ON a.`countryid` = b.id
                                LEFT JOIN city c ON a.`cityid` = c.id
                                LEFT JOIN tour_category d ON a.`tourcatid` = d.id
                                WHERE a.cityid = '" . $_POST['cityid'] . "' and a.recommended = 1
                                ORDER BY a.`tourname` ASC";
       
        $result=MySQL::query($SQL);
        // check if row inserted or not
        if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "All Best Tour .";
        $response["bestTour"] = $result;
        //$response["User_id"] = $insertID;
        // echoing JSON response
        echo json_encode($response);
        }
        else{
             $response["success"] = 0;
             $response["message"] = "No Best Tour found in the database";
 
    // echoing JSON response
        echo json_encode($response);
        }
  } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}   
?>