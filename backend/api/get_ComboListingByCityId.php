<?php  header('Content-Type: application/json');
 
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

// array for JSON response
$response = array();
 date_default_timezone_set("Asia/Kolkata");
 $today = date('Y-m-d');
	   $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid = $getSystemDefaultSeason['id'];
		}else {
			$sid = 0;
		}
// check for required fields
   if (isset($_POST['cityid']) && $_POST['cityid']!='' ) {
       
        $SQL = "SELECT * FROM tour WHERE cityid ='".$_POST['cityid']."' AND islive = 1";
        $result=MySQL::query($SQL);
		 //print_r($result);
        // check if row inserted or not
        if ($result) {
        // successfully inserted into database
            
        $newTC =array();
		$comboTour =array();
        foreach ($result as $allTour) {
           
            $SQL1="SELECT comboid,tours FROM `tour_combo_t`
                        WHERE FIND_IN_SET('".$allTour['id']."', tours);";
						//echo $SQL1;
						
            $result11 = MySQL::query($SQL1);
			//print_r($result11);
			//die;
			 if (isset($result11) && is_array($result11) && count($result11)>0) {
			  	foreach($result11 as $key => $value) {
					$comboTour[$value['comboid']] = $value['tours']; 
					
				}
			 }
           if (isset($result11) && is_array($result11) && count($result11)>0) {
			  	foreach($result11 as $key => $value) {
            		$newTC[] = $value['comboid'];
				}
		   }
		   
        } 
		//print_r($comboTour);
		//die;
		/**         Combo Tour name  **/
		if (isset($comboTour) && is_array($comboTour) && count($comboTour)>0){
				foreach($comboTour as $key => $value) {
            		 $SQL1="SELECT tourname,tlocation  FROM `tour`
                        WHERE id in(".$value.");";
						//echo $SQL1;
						$tmpComboTourname = MySQL::query($SQL1);
						
						 if (isset($tmpComboTourname) && is_array($tmpComboTourname) && count($tmpComboTourname) > 0 ) {
							 $tmpComboTournameUnique = array();
							 foreach($tmpComboTourname as $keyT => $valueT) {
								 $tmpComboTournameUnique[] = $valueT['tourname']; 
								 $tmpComboTourlocationUnique[$valueT['tourname']] = $valueT['tlocation']; 
							 }
						 }
					$combotourname[$key] = $tmpComboTournameUnique;
					$combotourlocation[$key] = $tmpComboTourlocationUnique;
				}
		}
		/**         Combo Tour name  **/
		/**         Combo  rating  **/
		if (isset($comboTour) && is_array($comboTour) && count($comboTour)>0){
				foreach($comboTour as $key => $value) {
					$SQL1="SELECT avg(combo_rating) as combo_rating,count(*) as total_review FROM `combo_rating`
                        WHERE `ComboId` ='".$key."'";
            $resultcomboratingTmp=MySQL::query($SQL1,true);
			
			if ($resultcomboratingTmp['combo_rating'] != ""){
				$comborating[$key] =$resultcomboratingTmp['combo_rating'];
				$comboreview[$key] =$resultcomboratingTmp['total_review'];
			}else {
				$comborating[$key] =0;
				$comboreview[$key] =0;
			}
					}
		}
		/**         Combo  rating  **/
		/**         Combo  like  **/
		if (isset($comboTour) && is_array($comboTour) && count($comboTour)>0){
				foreach($comboTour as $key => $value) {
				/*$SQL1="SELECT count(*) as combo_like FROM `combo_like` WHERE `ComboId` ='".$key."'";
					$resultcombolikeTmp=MySQL::query($SQL1,true);
					if ($resultcombolikeTmp['combo_like'] != ""){
						$combolike[$key] =$resultcombolikeTmp['combo_like'];
					}else {
						$combolike[$key] =0;
					}*/
				$combolike[$key] =0;
	            if (isset($_POST['UserId']) && !empty($_POST['UserId'])) {
	                $SQL141="SELECT clike as clike FROM `combo_like` WHERE 1 AND ComboId = ".$key." AND UserId=".$_POST['UserId']."";
	                $result1147=MySQL::query($SQL141,true);
	                if ($result1147['clike'] != ""){
	                   $combolike[$key] =$result1147['clike'];
	                }
	            }
				}
		}
		/**         Combo  like  **/
		/**         Combo  price **/
		//print_r($resultcomboratingTmp);  
        // die;
		if (isset($comboTour) && is_array($comboTour) && count($comboTour)>0){
			 // $minsellAP = array();
			 // $minsellCP = array();
				foreach($comboTour as $key => $value) {
				//print_r($value);
            	$result22 = array();
                $SQL1 = "SELECT SUM(tds.sellAP) AS sellAP,SUM(tds.sellCP) AS sellCP,SUM(tds.buyAP) AS buyAP,SUM(tds.buyCP) AS buyCP FROM tour_otheroption toh inner join tour_add_services tds on toh.id = tds.tourOtherOptionId
            	    WHERE toh.TourId  in ($value) AND toh.base_pack = 1 AND tds.seasonId = ".$sid."";
            	
            	$result22 = MySQL::query($SQL1,true);
            	$resultin = array();
                $SQLin = "SELECT tour_price.TourId,tour_price.sellAP,tour_price.sellCP,transfer_category.transfercatname,transfer_category.transferoption,tour_price.discount,tour_price.discountType
                          FROM tour_price
                          LEFT JOIN tour_transferoption ON tour_price.tourTransOptionId = tour_transferoption.id
                          LEFT JOIN transfer_category ON tour_transferoption.transfercatId = transfer_category.id WHERE tour_transferoption.TourId in ($value) AND tour_price.seasonId = '$sid' AND tour_transferoption.isdefault = '1'";
              // $resultin = MySQL::query($SQLin,true);
              // print_r($resultin);
              
              unset($resultin);
              $resultin = @mysql_query($SQLin);
            $sellAP = $sellCP = array();
            while ($row = mysql_fetch_assoc($resultin)) {
           //print_r($row);
              if ($row['transfercatname'] == 3) {
                $sellAP[] = '0';
                $sellCP[] = '0';
              }
              else {
                $sellAP[] = $row['sellAP'];
                $sellCP[] = $row['sellCP'];
              }
            }

            $totalsellAP = array_sum($sellAP);
            $totalsellCP = array_sum($sellCP);
            $minsellCP = min($sellCP);	
           
            if (isset($result22) && is_array($result22) && count($result22) > 0 ) {
            						$result23['combosellAP'] =$result22['sellAP'] + $totalsellAP;
            						$result23['combosellCP'] =$result22['sellCP'] + $totalsellCP;
            						$result23['combobuyAP'] =$result22['buyAP'];
            						$result23['combobuyCP'] =$result22['buyCP'];

            					}else{
            						$result23['combosellAP'] = "";
            						$result23['combosellCP'] = "";
            						$result23['combobuyAP'] = "";
            						$result23['combobuyCP'] = "";
            					}
		} 
	}
			//print_r($result23);die;
		 $newTC = array_unique($newTC); 
		 if (isset($newTC) && is_array($newTC) && count($newTC) > 0 ) {
				$newTCString =implode(",",$newTC);
				//echo $newTCString;die;
				
				$SQL1="SELECT tc.ComboId as comboid ,tc.comboname,tc.comboimageurl,tc.combotodate,tc.combofromdate,tc.combodesc,tcp.comboDiscount FROM `tour_combo` tc left join tour_combo_price tcp  on tc.ComboId = tcp.comboid
                        WHERE tc.ComboId in(".$newTCString.")  AND tc.recommended = 1";
						//echo $SQL1;
            $result11 = MySQL::query($SQL1);
			/**         main section **/
				if (isset($result11) && is_array($result11) && count($result11) > 0) {
						foreach($result11 as $key => $value ) {
							 if (isset($combotourname[$value['comboid']]) && is_array($combotourname[$value['comboid']])  ) {
							$result11[$key]['combotourname'] = implode(",",$combotourname[$value['comboid']]);
							 }else {
								 $result11[$key]['combotourname'] = "";
							 }
							 if (isset($combotourlocation[$value['comboid']]) && is_array($combotourlocation[$value['comboid']])  ) {
							$result11[$key]['combotourlocation'] = $combotourlocation[$value['comboid']];
							 }else {
								 $result11[$key]['combotourlocation'] = "";
							 }
							 if (isset($result23) && is_array($result23)  ) {
							$result11[$key]['comboprice'] = $result23;
							 }else {
								 $result11[$key]['comboprice'] = "";
							 }
							 if (isset($comborating[$value['comboid']])  ) {
							$result11[$key]['comborating'] = $comborating[$value['comboid']];
							 }else {
								 $result11[$key]['comborating'] = 0;
							 }
							  if (isset($comboreview[$value['comboid']])  ) {
							$result11[$key]['comboreview'] = $comboreview[$value['comboid']];
							 }else {
								 $result11[$key]['comboreview'] = 0;
							 }
							 
							   if (isset($combolike[$value['comboid']])  ) {
							$result11[$key]['combolike'] = $combolike[$value['comboid']];
							 }else {
								 $result11[$key]['combolike'] = 0;
							 }
						}
				}
				/**         main section **/
			//print_r(get_defined_vars());
			//die;
				
			 //$result11[$key]['combo_price'] = array_merge($result23);
				
		$response["success"] = 1;
        $response["message"] = "List all Combo from that city.";
        $response["ComboDetail"] = $result11;
        //$response["User_id"] = $insertID;
        // echoing JSON response
        echo json_encode($response);
		 }else {
			 $response["success"] = 0;
             $response["message"] = "No Data found in the database";
 			// echoing JSON response
        	echo json_encode($response);
		}
		 
		 
            
            
       
        }
        else{
             $response["success"] = 0;
             $response["message"] = "No Data found in the database";
 
    // echoing JSON response
        echo json_encode($response);
        }
 } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}  
?>