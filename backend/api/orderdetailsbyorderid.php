<?php header('Content-Type: application/json');
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
date_default_timezone_set("Asia/Kolkata");
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");

$organizePhone = Users::getOrganizePhone();//for global
//print_r($organizePhone);die;

$response = array();


   if (isset($_POST['UserId']) && $_POST['UserId']!='' && isset($_POST['orderId']) && $_POST['orderId']!='') {
        $SQLORDER = "SELECT order_tripoye.order_id,order_tripoye.payment_amount,order_tripoye.total,order_tripoye.date_added as orderdate,rateofcurrency.currencycode,rateofcurrency.currencysymbol,order_status.name as order_status from order_tripoye as order_tripoye
  LEFT JOIN rateofcurrency as rateofcurrency ON order_tripoye.currency_id = rateofcurrency.id
  LEFT JOIN order_status as order_status ON order_tripoye.order_status = order_status.order_status_id
  WHERE order_tripoye.order_id= '".$_POST['orderId']."' ORDER BY order_tripoye.order_id DESC";
       $resultOrder=MySQL::query($SQLORDER);
       $resultOrder = $resultOrder[0];

        if(empty($resultOrder['currencycode'])){
        $resultOrder['currencycode'] = 'INR';
  }
  if(empty($resultOrder['currencysymbol'])){
    $resultOrder['currencysymbol']  = '₹';
  }

  $sqlPDF = "SELECT * FROM order_pdf_files WHERE order_id = '".$_POST['orderId']."'";
  $resultPDF= MySQL::query($sqlPDF);
  //print_r($resultPDF);
  if (!empty($resultPDF)) {
    foreach ($resultPDF as $key => $value) {
      $pdf[] = $value['pdf'];
    }
    $resultOrder['pdf'] = implode(",",$pdf);
  } else {
    $resultOrder['pdf'] = '';
  }
  $resultOrder['pdf_url'] = "http://www.tripoye.com/backend/upload/";
  
  $resultOrder['organize_phone_number'] = $organizePhone['phone'];


  $SQLTr = "SELECT * from order_traveller_detail WHERE order_traveller_detail.order_id='".$_POST['orderId']."' ORDER BY order_treveller_detail_id DESC LIMIT 1";
      $resultTr=MySQL::query($SQLTr);
      if(isset($resultTr[0]) && !empty($resultTr[0])){
        $result = array_merge($resultOrder,$resultTr[0]);
      }else{
        $result = $resultOrder;
      }
      
     $SQL = "SELECT order_item.order_item_id,order_item.order_id,order_item.TourId,order_item.ComboId,order_item.type,order_item.TourName,order_item.TourDate,order_item.TransferName,order_item.cart_user_id,order_item.cart_create_update_time,order_item.cartitem_uniqueId,order_item.SessionId,order_item.TransferId,order_item.TransferType
           from order_item as order_item
             WHERE order_item.order_id='".$_POST['orderId']."' 
             group by order_item.TourId
             ORDER BY order_item.order_item_id DESC
             ";
        $result['tour_items']=MySQL::query($SQL);

        if ($result['tour_items']) {
            foreach ($result['tour_items'] as $keyOrder => $allOrder) {
              $SQLrefundPolicy="SELECT pages.* FROM `pages` LEFT JOIN tour ON pages.id=tour.cancelpolicy WHERE tour.id='".$allOrder['TourId']."'";
              $refundPolicy=MySQL::query($SQLrefundPolicy);
    if(empty($refundPolicy)){
                $refundPolicy[] = 'null';
                }
              foreach ($refundPolicy as $key => $policy) {
                  if($policy['pagetypeid']=='4' && $policy['refund_policy']!='null'){
                    $result['tour_items'][$keyOrder]['refundPolicy'][$key]['id']=$policy['id'];
                    $result['tour_items'][$keyOrder]['refundPolicy'][$key]['pagename']=$policy['pagename'];
                    $result['tour_items'][$keyOrder]['refundPolicy'][$key]['pagetypeid']=$policy['pagetypeid'];
                    $result['tour_items'][$keyOrder]['refundPolicy'][$key]['content']=$policy['content'];
                  $result['tour_items'][$keyOrder]['refundPolicy'][$key]['refund_policy']=json_decode($policy['refund_policy']);
                    $result['tour_items'][$keyOrder]['refundPolicy'][$key]['status']=$policy['status'];
                  }else{
                    $result['tour_items'][$keyOrder]['refundPolicy'][$key]=$policy;
                  } 
              }
              //print_r($refundPolicy);
          $SQLlocation ="SELECT tlocation FROM `tour`
                            WHERE `id` ='".$allOrder['TourId']."'";
                $resultLocation=MySQL::query($SQLlocation,true); 
                //print_r($resultLocation);die; 
              
          $SQLImage="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                            WHERE `TourId` ='".$allOrder['TourId']."'
                            ORDER BY galleryorder ASC LIMIT 1
                             ";
                $resultImage=MySQL::query($SQLImage,true);
                if (isset($resultImage) && is_array($resultImage) && count($resultImage)>0 ) {
          $result['tour_items'][$keyOrder]['gallery_img'] = $resultImage['gallery_img'];
          $result['tour_items'][$keyOrder]['gallery_img_url'] = $resultImage['gallery_img_url'];
    }else {
      $result['tour_items'][$keyOrder]['gallery_img'] = "";
      $result['tour_items'][$keyOrder]['gallery_img_url'] = "";
    } 

    $result['tour_items'][$keyOrder]['location'] = $resultLocation['tlocation'];

    $SQLOPtions = "SELECT order_item.TourOptId,order_item.TourOptName,order_item.pickup_point,order_item.AdultPrice,order_item.ChildPrice,order_item.Discount,order_item.DiscountType,order_item.DisAdultPrice,order_item.DisChildPrice,order_item.TourTime,order_item.AdultQuantity,order_item.ChildQuantity,order_item.InfantQuantity,order_item.SingleTotal,order_item.SingleDisTotal,order_item.FullTotal,order_item.DisFullTotal, vendors.vendor_email, vendors.vendor_name, vendors.vendor_surname, vendors.vendor_phone from order_item as order_item
             LEFT JOIN vendors as vendors ON order_item.vendorid=vendors.vendor_id
             WHERE order_item.order_id='".$_POST['orderId']."' AND order_item.TourId = '".$allOrder['TourId']."'
             ORDER BY order_item.order_item_id DESC
             ";
             $result['tour_items'][$keyOrder]['tour_option_items']=MySQL::query($SQLOPtions);
            }
            
            $response["success"] = 1;
            $response["message"] = "Order List.";
            $response["orderlist"] =$result;
            //print_r($response);die;
            echo json_encode($response);
        }
        else{
            $response["success"] = 0;
            $response["message"] = "No Order found in the database for this user";
            echo json_encode($response);
        }
  } else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}   
?>