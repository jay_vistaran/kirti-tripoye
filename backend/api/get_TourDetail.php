<?php header('Content-Type: application/json');
 
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
require_once("functionTourLike.php");
$today = date('Y-m-d');
	   $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid = $getSystemDefaultSeason['id'];
		}else {
			$sid = 0;
		}
// array for JSON response
$response = array();
 //$_POST['cityid']=7;
// check for required fields
    if (isset($_POST['TourId']) && $_POST['TourId']!='') {
       $SQL = "SELECT a.*, b.countryname, c.cityname, d.tourcatname,p.content as CANCEL_POLICY
                                FROM `tour` a
                                LEFT JOIN country b ON a.`countryid` = b.id
                                LEFT JOIN city c ON a.`cityid` = c.id
                                LEFT JOIN tour_category d ON a.`tourcatid` = d.id 
								LEFT JOIN pages p ON  a.`cancelpolicy` = p.id
                                WHERE a.id = '" . $_POST['TourId'] . "'";
								//echo $SQL ;
								//die;
       
        $result=MySQL::query($SQL,true);
		
        
       //print_r($result);die; 
        // check if row inserted or not
        if ($result) {
			  
			//print_r($result);die; 
			 $SQL1="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                        WHERE `TourId` ='".$result['id']."'
                        ORDER BY galleryorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result['gallery_img'] =$result11;
			$SQL1="SELECT banner_img,banner_img_url FROM `tour_bannerimage`
                        WHERE `TourId` ='".$result['id']."'
                        ORDER BY bannerorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result['banner_img'] =$result11;
			
			$SQL1="SELECT avg(tour_rating) as tour_rating,count(*) as total_review FROM `tour_rating`
                        WHERE `TourId` ='".$result['id']."'";
            $result11=MySQL::query($SQL1,true);
			if ($result1['tour_rating'] != ""){
				$result['tour_rating'] =$result11['tour_rating'];
			}else {
				$result['tour_rating'] =0;
			}
			$result['total_review'] = $result11['total_review'];
            $result['like_count'] =0;
			$SQL141="SELECT count(*) as tour_like FROM `tour_like` WHERE 1 AND TourId = ".$_POST['TourId']." AND tlike=1";
            $result1147=MySQL::query($SQL141,true);
			if ($result1147['tour_like'] != ""){
				$result['like_count'] =$result1147['tour_like'];
			}

            $result['tour_like'] =0;
            if (isset($_POST['UserId']) && !empty($_POST['UserId'])) {
                $SQL141="SELECT tlike as tlike FROM `tour_like` WHERE 1 AND TourId = ".$_POST['TourId']." AND UserId=".$_POST['UserId']."";
                $result1147=MySQL::query($SQL141,true);
                if ($result1147['tlike'] != ""){
                    $result['tour_like'] =$result1147['tlike'];
                }
            }
			$result11 = array();
			$SQL1="SELECT tds.sellAP,tds.sellCP,tds.discount,tds.discountType FROM tour_otheroption toh inner join tour_add_services tds on toh.id = tds.tourOtherOptionId 
                        WHERE toh.TourId = '".$result['id']."' AND toh.base_pack = 1 AND tds.seasonId = ".$sid."";
						//echo $SQL1;die;
            $result11=MySQL::query($SQL1,true);

			//print_r($result11);
			//die;
			$resultin = array();
              $SQLin = "SELECT tour_price.TourId,tour_price.sellAP,tour_price.sellCP,transfer_category.transfercatname,transfer_category.transferoption,tour_price.discount,tour_price.discountType
                          FROM tour_price
                          LEFT JOIN tour_transferoption ON tour_price.tourTransOptionId = tour_transferoption.id
                          LEFT JOIN transfer_category ON tour_transferoption.transfercatId = transfer_category.id WHERE tour_transferoption.TourId = '$result[id]' AND tour_price.seasonId = '$sid' AND tour_transferoption.isdefault = '1'";
              //$resultin=MySQL::query($SQLin,true);
              //print_r($resultin);die;
              
              unset($resultin);
              $resultin = @mysql_query($SQLin);

            $sellAP = $sellCP = array();
            while ($row = mysql_fetch_assoc($resultin)) {
           //print_r($row);
              if ($row['transfercatname'] == 3) {
                $sellAP[] = '0';
                $sellCP[] = '0';
              }
              else {
                $sellAP[] = $row['sellAP'];
                $sellCP[] = $row['sellCP'];
              }
            }

            $minsellAP = min($sellAP);
            $minsellCP = min($sellCP);
              unset($resultin);
              $resultin = @mysql_query($SQLin);
            while ($row = mysql_fetch_assoc($resultin)) {
              
              if ($row['sellAP'] == $minsellAP) {
                $transfer_disAP = $row['discount'];
                $transfer_disTAP = $row['discountType'];
              }
            }
            if ($transfer_disTAP == 1) {
                $tour_transfer_priceAP =  $minsellAP - $transfer_disAP; 
            } 
            if ($transfer_disTAP == 0) {
                $tour_transfer_priceAP =  $minsellAP - (($minsellAP*$transfer_disAP)/100); 
            } 
            if ($result11['discountType'] == 1) {
              $tour_option_priceAP  = $result11['sellAP'] - $result11['discount'];  
              $tour_option_priceCP  = $result11['sellCP'] - $result11['discount'];    
                  
                     } 
              if ($result11['discountType'] == 0) {
              $tour_option_priceAP  = $result11['sellAP'] - (($result11['sellAP']*$result11['discount'])/100);  
              $tour_option_priceCP  = $result11['sellCP'] - (($result11['sellCP']*$result11['discount'])/100);  
                  
                     }  
                     //print $tour_transfer_priceAP.'aaa'; die;      
            // $tour_option_priceAP         
            if (empty($result11)) {
                        $result11 = array(
                            'sellAP' => "0",
                            'sellCP' => "0",
                            'discount' => "0",
                            'discountType' => "0"
                        );
                     }
            if (isset($result11) && is_array($result11) && count($result11) > 0) {
              $result['sellAP_with_discount'] = $tour_option_priceAP + $tour_transfer_priceAP;
              $result['sellCP_with_discount'] = $result11['sellCP'] + $minsellCP;
              $result['sellAP'] = $result11['sellAP'] + $minsellAP;
              $result['sellCP'] = $result11['sellCP'] + $minsellCP;
    
            }
            else {
              $result['sellAP'] = "";
              $result['sellCP'] = "";
              $result['discount'] = "";
              $result['discountType'] = "";
            }
        // successfully inserted into database
            
            
        /*$newTC =array();
        foreach ($result as $allTour) {
           
            $SQL1="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                        WHERE `TourId` ='".$allTour['id']."'
                        ORDER BY galleryorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result[$key]['gallery_img'] =$result11;
			$SQL1="SELECT banner_img,banner_img_url FROM `tour_bannerimage`
                        WHERE `TourId` ='".$allTour['id']."'
                        ORDER BY bannerorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result[$key]['banner_img'] =$result11;
        }*/
		//print_r($result);die; 
        //dump($newTC);
       // $newTC[0]['CANCEL_POLICY']  = preg_replace("/&#?[a-z0-9]{2,8};/i","",html_entity_decode(str_replace(PHP_EOL, '', htmlspecialchars(strip_tags($newTC[0]['CANCEL_POLICY'])))));
        $result['impinformation'] = str_replace(array("\n", "\t", "\r"), '', $result['impinformation']);
       	$result['itinerarydescription'] = str_replace(array("\n", "\t", "\r"), '', $result['itinerarydescription']);
        $result['tourdescription'] = str_replace(array("\n", "\t", "\r"), '', $result['tourdescription']);
       	$result['inclusion'] = str_replace(array("\n", "\t", "\r"), '', $result['inclusion']);
        $response["success"] = 1;
        $response["message"] = "All Best Tour .";
        $response["bestTour"] =$result;
        //$response["User_id"] = $insertID;
        //echoing JSON response
		//print_r($response);die;
		echo json_encode($response);
        //$restgg = json_encode($response);
		//$json = utf8_encode($restgg);
		//$json = json_decode($json);
		//echo 
		//print_r($json);die;

        }
        else{
             $response["success"] = 0;
             $response["message"] = "No Best Tour found in the database";
 
    // echoing JSON response
        echo json_encode($response);
        }
  } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}   

?>