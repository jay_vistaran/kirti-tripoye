<?php 
$data = file_get_contents("php://input");
//echo "<pre>";
//print_r($data);die;
$json_decode_result = json_decode($data);
//echo "<pre>";
//print_r($json_decode_result);die;
 /*
 * Following code will create a new cart
 * All cart details are read from HTTP Post Request
 * Made By : D
 * DATE:20-05-2017
 */
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
$response = array();
if (isset($json_decode_result->order_id) &&
   $json_decode_result->order_id!= "" && isset($json_decode_result->operator_name) && $json_decode_result->operator_name!= "" && isset($json_decode_result->operator_lastname) && 
   $json_decode_result->operator_lastname!= "" && isset($json_decode_result->remark) && isset($json_decode_result->operator_email) &&
   $json_decode_result->operator_email!= "" && isset($json_decode_result->operator_phone) && $json_decode_result->operator_phone!= "") {
	

	
	$order_id =$json_decode_result->order_id;
	$operator_name =$json_decode_result->operator_name;
	$operator_lastname =$json_decode_result->operator_lastname;
	$remark =$json_decode_result->remark;
	$operator_email =$json_decode_result->operator_email;
	$operator_phone =$json_decode_result->operator_phone;

	//print $order_id;

	if (isset($json_decode_result->pickupPoint) && is_array($json_decode_result->pickupPoint) && count($json_decode_result->pickupPoint)>0) {
		
		$obj = $json_decode_result->pickupPoint;
		$pickupPoint = json_decode(json_encode($obj), True);
		//print_r($pickupPoint);

		foreach ($pickupPoint as $key => $value ) {
				
				$SQL = "update `order_item` SET pickup_point='" . $value['tour_pickup_point'] . "', ComboId='" . $value['combo_id'] . "' Where order_id = '" . $order_id . "' AND TourOptId='".$value['tour_option_id']."'";
				$result = MySQL::query($SQL);
				//var_dump($result);die;
			
		}
	}
	$SQL = "INSERT INTO `order_traveller_detail` (`order_id`,`name`,`lastname`,`remark`, `email`, `phone`)
               VALUES ( " . $order_id . ", '" . $operator_name . "', '" . $operator_lastname . "', '" . $remark . "', '" . $operator_email . "', '" . $operator_phone . "')";
    $result=MySQL::query($SQL);
	$response["success"] = 1;
	$response["message"] = "Traveller detail successfully added.";
	echo json_encode($response);
}else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}


?>