<?php  header('Content-Type: application/json');
 /*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
 
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
$combo_price_array = array();
// array for JSON response
$response = array();
$today = date('Y-m-d');
	   $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid = $getSystemDefaultSeason['id'];
		}else {
			$sid = 0;
		}
// check for required fields
   if (isset($_POST['comboid']) && $_POST['comboid']!='' ) {
       //$getSystemCombo = Users::getSystemComboById($_POST['comboid']);
        $SQL = "SELECT tours,tour_combo_price.comboDiscount   FROM tour_combo_t left join tour_combo_price on tour_combo_t.comboid = tour_combo_price.comboid  WHERE tour_combo_t.comboid ='".$_POST['comboid']."' ";
        $result=MySQL::query($SQL,true);
		if (isset($result) && is_array($result) && count($result)>0) {
		$comboDiscount = $result['comboDiscount'];
		if ($comboDiscount == NULL) {
			$comboDiscount ="NA";
		}
		// print_r($getSystemCombo);die;
		 $SQL = "SELECT a.id ,a.tourname,a.fhday,a.cutoffday,a.tlocation,a.islive,a.recommended,a.mealincluded,a.mealtext,a.impinformation,a.itinerarydescription,a.tourdescription,a.inclusion,b.countryname, c.cityname, d.tourcatname,p.content as cancel_policy FROM tour a
                                LEFT JOIN country b ON a.`countryid` = b.id
                                LEFT JOIN city c ON a.`cityid` = c.id
                                LEFT JOIN tour_category d ON a.`tourcatid` = d.id 
								LEFT JOIN pages p ON  a.`cancelpolicy` = p.id WHERE a.id in (".$result['tours'].") ";
        $result_tour=MySQL::query($SQL);
		
        // check if row inserted or not
        if (isset($result_tour) && is_array($result_tour) && count($result_tour)>0) {
        // successfully inserted into database
            
        
        foreach ($result_tour as $key => $value ) {
            $SQL1="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                        WHERE `TourId` ='".$value['id']."'
                        ORDER BY galleryorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result_tour[$key]['gallery_img'] = $result11;
			$SQL1="SELECT banner_img,banner_img_url FROM `tour_bannerimage`
                        WHERE `TourId` ='".$value['id']."'
                        ORDER BY bannerorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result_tour[$key]['banner_img'] =$result11;
			
			$SQL1="SELECT avg(tour_rating) as tour_rating,count(*) as total_review FROM `tour_rating`
                        WHERE `TourId` ='".$value['id']."'";
            $result11=MySQL::query($SQL1,true);
			if ($result11['tour_rating'] != ""){
				$result_tour[$key]['tour_rating'] =$result11['tour_rating'];
			}else {
				$result_tour[$key]['tour_rating'] =0;
			}
			$result['total_review'] = $result11['total_review'];
			$SQL1="SELECT count(*) as tour_like FROM `tour_like`
                        WHERE `TourId` ='".$value['id']."'
                                                 ";
            $result11=MySQL::query($SQL1,true);
			if ($result11['tour_like'] != ""){
				$result_tour[$key]['tour_like'] =$result11['tour_like'];
			}else {
				$result_tour[$key]['tour_like'] =0;
			}
            $SQL1="SELECT tds.sellAP,tds.sellCP,tds.discount,tds.discountType FROM tour_otheroption toh inner join tour_add_services tds on toh.id = tds.tourOtherOptionId 
                        WHERE toh.TourId = '".$value['id']."' AND toh.base_pack = 1 AND tds.seasonId = ".$sid."";
						//echo $SQL1;die;
            $result11=MySQL::query($SQL1,true);

			//print_r($result11);
			//die;
			if (isset($result11) && is_array($result11) && count($result11) > 0 ) {
				$result_tour[$key]['sellAP'] =$result11['sellAP'];
				$result_tour[$key]['sellCP'] =$result11['sellCP'];
				$result_tour[$key]['discount'] =$comboDiscount;
				$result_tour[$key]['discountType'] ="Percentage";
					
			}else{
				$result_tour[$key]['sellAP'] = "";
				$result_tour[$key]['sellCP'] = "";
				$result_tour[$key]['discount'] = "";
				$result_tour[$key]['discountType'] = "";
			}
		   
        } 
		
		 
		 $response["success"] = 1;
        $response["message"] = "Combo Tour .";
        $response["Tour"] =$result_tour;
        echo json_encode($response); 
      // print_r($result_tour);die;
        }
        else{
             $response["success"] = 0;
             $response["message"] = "No Data found in the database";
 
    // echoing JSON response
        echo json_encode($response);
        }
 } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
} 
   }else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
} 
?>