<?php header('Content-Type: application/json');
 
/*
 * Following code willget all category 
 * All user details are read from HTTP GET Request
 * Made By : D
 * DATE: 17-03-2016
 */
 
require_once("../lib/config.php");
require_once("../lib/scripts/php/functions.php");
require_once("../lib/scripts/php/new_functions.php");
date_default_timezone_set("Asia/Kolkata");
$today = date('Y-m-d');
	   $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid = $getSystemDefaultSeason['id'];
		}else {
			$sid = 0;
		}
// array for JSON response
$response = array();
 //$_POST['cityid']=7;
// check for required fields
   if (isset($_POST['TourId']) && $_POST['TourId']!='') {
       $SQL = "SELECT a. * , b.countryname, c.cityname, d.tourcatname,p.content as CANCEL_POLICY
                                FROM `tour` a
                                LEFT JOIN country b ON a.`countryid` = b.id
                                LEFT JOIN city c ON a.`cityid` = c.id
                                LEFT JOIN tour_category d ON a.`tourcatid` = d.id 
								LEFT JOIN pages p ON  a.`cancelpolicy` = p.id
                                WHERE a.id = '" . $_POST['TourId'] . "'";
								//echo $SQL ;
								//die;
       
        $result=MySQL::query($SQL,true);
		
        
       //print_r($result);die; 
        // check if row inserted or not
        if ($result) {
			 $SQL1="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                        WHERE `TourId` ='".$result['id']."'
                        ORDER BY galleryorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result['gallery_img'] =$result11;
			$SQL1="SELECT banner_img,banner_img_url FROM `tour_bannerimage`
                        WHERE `TourId` ='".$result['id']."'
                        ORDER BY bannerorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result['banner_img'] =$result11;
			
			$SQL1="SELECT avg(tour_rating) as tour_rating,count(*) as total_review FROM `tour_rating`
                        WHERE `TourId` ='".$result['id']."'";
            $result11=MySQL::query($SQL1,true);
			if ($result1['tour_rating'] != ""){
				$result['tour_rating'] =$result11['tour_rating'];
			}else {
				$result['tour_rating'] =0;
			}
			$result['total_review'] = $result11['total_review'];
			$SQL1="SELECT count(*) as tour_like FROM `tour_like`
                        WHERE `TourId` ='".$result['id']."'
                                                 ";
            $result11=MySQL::query($SQL1,true);
			if ($result1['tour_like'] != ""){
				$result['tour_like'] =$result11['tour_like'];
			}else {
				$result['tour_like'] =0;
			}
			$result11 = array();
			$SQL1="SELECT tds.sellAP,tds.sellCP,tds.discount,tds.discountType FROM tour_otheroption toh inner join tour_add_services tds on toh.id = tds.tourOtherOptionId 
                        WHERE toh.TourId = '".$result['id']."' AND toh.base_pack = 1 AND tds.seasonId = ".$sid."";
						//echo $SQL1;die;
            $result11=MySQL::query($SQL1,true);

			//print_r($result11);
			//die;
			if (isset($result11) && is_array($result11) && count($result11) > 0 ) {
				$result['sellAP'] =$result11['sellAP'];
				$result['sellCP'] =$result11['sellCP'];
				$result['discount'] =$result11['discount'];
				$result['discountType'] =$result11['discountType'];
					if($result['discountType'] == 0 ) {
						$result['discountType'] = "Percentage";
					}else {
						$result['discountType'] = "Amount";
					}
			}else{
				$result['sellAP'] = "NA";
				$result['sellCP'] = "NA";
				$result['discount'] = "NA";
				$result['discountType'] = "NA";
			}
        // successfully inserted into database
            
            
        /*$newTC =array();
        foreach ($result as $allTour) {
           
            $SQL1="SELECT gallery_img,gallery_img_url FROM `tour_galleryrimage`
                        WHERE `TourId` ='".$allTour['id']."'
                        ORDER BY galleryorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result[$key]['gallery_img'] =$result11;
			$SQL1="SELECT banner_img,banner_img_url FROM `tour_bannerimage`
                        WHERE `TourId` ='".$allTour['id']."'
                        ORDER BY bannerorder ASC
                         ";
            $result11=MySQL::query($SQL1);
			$result[$key]['banner_img'] =$result11;
        }*/
		//print_r($result);die; 
        //dump($newTC);
       // $newTC[0]['CANCEL_POLICY']  = preg_replace("/&#?[a-z0-9]{2,8};/i","",html_entity_decode(str_replace(PHP_EOL, '', htmlspecialchars(strip_tags($newTC[0]['CANCEL_POLICY'])))));

        $response["success"] = 1;
        $response["message"] = "All Best Tour .";
        $response["bestTour"] =$result;
        //$response["User_id"] = $insertID;
        // echoing JSON response
        echo json_encode($response);
        }
        else{
             $response["success"] = 0;
             $response["message"] = "No Best Tour found in the database";
 
    // echoing JSON response
        echo json_encode($response);
        }
  } else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}   
?>