<?php
	require_once("header.php");
	if(!isset($_GET['id']) || $_GET['id']=='' )
            {
                    header("location:comboadd.php"); 
            }
        $trendingid=$_GET['id'];
		$today = date('Y-m-d');

     
        $getSystemCountry = Users::getSystemCountryAproved();//for global
         $getSystemCombo = Users::getSystemComboAproved();//for global 
        
		
		$getSystemComboTour=Users::getSystemComboCityById($trendingid);
       
	    
		if (isset($getSystemComboTour['tour_order']) &&  $getSystemComboTour['tour_order']!= "") {
				$getSystemComboTour['tour_order'] = explode(",",$getSystemComboTour['tour_order']);
				
		}
		
                     
?>


<style>
.btn-primary.active {
            background-color: #e34724;
    }
.btn_red.active {
  background-color: red;
   }
   .form-inline .form-group #tourname {
	width:230px;
}
  
</style>
<link rel="stylesheet" href="tabletest/css/style.css">
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<style>
    .content {
	
    margin-left: -37px;
    margin-right: 30px;
    padding: 10px;
    text-align: center;
}
    /* Sortable ******************/
#sortable { 
	list-style: none; 
	text-align: left; 
}
#sortable li { 
	margin: 0 0 10px 0;
	height: 75px; 
	background: #dbd9d9;
	border: 1px solid #999999;
	border-radius: 5px;
	color: #333333;
}

#sortable li img {
	height: 65px;
	border: 5px solid #cccccc;
	display: inline-block;
	float: left;
}
#sortable li div {
	padding: 5px;
}
#sortable li h2 {    
	font-size: 16px;
    line-height: 20px;
}
</style>
  <script>
  /* $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
  */
  $(function() {
    $('#sortable').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function(event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            document.getElementById("toursortingorder").value = list_sortable;
    		// change order in the database using Ajax
            
        }
    }); // fin sortable
}); 
  </script>
  <?php
  $getSystemComboTourCount=Users::getSystemComboTById($comboid);  ?>
				
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2>Manage Combo Package</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                 
                  
                <div class="row">
               
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                           <form id="jvalidate_tour123" role="form" class="form-horizontal" method="post" action="lib/scripts/php/all/comboByCityEditSave.php" onsubmit="return checkMinimumTour()">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Select Combo</strong></h3>
                                   
                                </div>
                              
                                <div class="panel-body">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Select Country:</label>
                                                <div class="col-md-9">                                                                                            
                                                   
                                                     
                                                    <select class="form-control select" name="countryid" id="countryid" onChange="js_country_city(this.value)">
                                             	<option value="">Select Country</option>

                                                 <?php 
                                                 foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>"><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select> 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                         
                                          
                                            
                                           
                                                            <div class="col-md-12">
                                            
                                            <div class="form-group">                                           
                                                                                                
                                            
                                                <label class="col-md-3 control-label">Select Combo:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                     <select class="form-control select" name="comboid" id="comboid">
              
                                                <option value="">Select Combo Name</option>

                                                 <?php 
                                                 foreach($getSystemCombo as $SystemCombo)
                                                      { ?>  
                                                <option value="<?php echo $SystemCombo['comboid'];?>"><?php echo $SystemCombo['comboname'];?></option>
                                                <?php } ?>                                                
                                                
                                            </select>  
                                                    <!--<span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                           
                                            
											
											   <div class="form-group">
                                                <label class="col-md-3 control-label">Enter  Title:</label>
												  <?php 
                                                 if (isset($getSystemComboTour['title']) &&  $getSystemComboTour['title']!= "") {
				                                $titles = $getSystemComboTour['title'];
				
	                                        	}?>  
													  
                                                <div class="col-md-9"><input class="form-control" type="text" name="title" value="<?php echo $titles ?>">  
												</div>
                                                
                                                
                                            </div>
                                            
                                            </div>                 
                                          
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                            
                                                
                                                
                                            
                                                <label class="col-md-3 control-label">Select City:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                     <select class="form-control select" name="cityid" id="cityid" onChange="js_tour_category(this.value)">
              
                                                
                                                
                                                
                                            </select>  
                                                    <!--<span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                        
                                             
                                             <div class="form-group">
                                                <div class="col-md-3">                                                                                            
                                                    
                                                   
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                                 <div class="col-md-9">                                                                                            
                                                    
                                                   <span><button type="button" class="btn btn-primary btn-lg" name="btn_js_addTour" onClick="js_addCombo()">Add</button></span>
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            
                                            
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class='row'>
    
        
       <div class='col-sm-4'>    
            <h4><strong>Added Combo</strong></h4>
            <div id="message-box-tour" style="display:none" >
            <span class="alert alert-danger" style="font-size:13px"  >Please Add Minimum Two Unique Combo.</span>
            </div>
<?php if(isset($_REQUEST['msg'])){?><span class="alert alert-danger" style="font-size:13px">Please Add Minimum Two Unique Combo.</span><br/><br/><br/> <?php }?>
        </div>
       
        

        <div class='col-sm-4'>
            
        </div>
        <div class='col-sm-4'>    
           
        </div>
       
    </div>
                                        
                                    
                                    

                                
                                <table class="responstable" id="sortabletablebannerHead">
   <thead>
      <tr>
                                                <th>Combo Name</th>
                                                <th>ORDER</th>
                                                <th>Remove</th>
                                                
      </tr>
      </thead>
  
   
  
    <tbody id="select_tour_combo">
              <?php
                    if(isset($getSystemComboTour['tour_order']) && is_array($getSystemComboTour['tour_order']) && count($getSystemComboTour['tour_order'])>0){
						foreach( $getSystemComboTour['tour_order'] as $key => $value ) {
							
							$tourdetails =  Users::getComboByID($value);
							//echo "<pre>";print_r($tourdetails);
			?>		
            	<tr id="<?php echo $tourdetails['comboid']; ?>"><td><?php echo $tourdetails['comboname']; ?></td> <td><span style="background-image: url('img/drag.png');background-repeat: no-repeat;background-position: center;width: 30px;height: 30px; display: inline-block;float: left;cursor: move;"></span></td><td><a href="#" type="submit" onClick="return delete_rowbanner('<?php echo $tourdetails['comboid']; ?>');"><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td><input type="hidden" name="arraycomboname[]" value="<?php echo $tourdetails['comboid']; ?>" </tr>
							
						<?php	
						}
						
					}
			//die;
			?>        
                            
    
        
                            
    </tbody>
   
</table>
</div>
</div>
                              <div class="panel-footer">
                                    <div class='row'>
        
        <div class='col-sm-1'>
           
        </div>
        
        <div class='col-sm-7'>
            <div class="btn-group">
                                       <button name="submit"  style="display:none;" value="1" class="btn btn-primary"  id="save_btn"  type="submit">Save</button>
                                    </div>
                                
                                    
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  
<?php	require_once("footer.php");
?>
<script>	


						
	function checkMinimumTour() {
			//alert("hello");
			var countTour = document.getElementById("select_tour_combo").childElementCount;
			if (countTour<2){
				//alert(countTour);
				document.getElementById("message-box-tour").style.display="block";
				return false;
			}
			

			
		}	
		
		 $( "#select_tour_combo" ).sortable({
	update: function( event, ui ) {
		//alert("hello");
	}
		  });				
		
</script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'></script>
<script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
 <script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
<script>

function delete_rowbanner(id) {
                    noty({
                        text: 'Are you sure want to delete',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									   document.getElementById(id).outerHTML="";
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }	

						
function js_country_city(counterid) {
	
	selectCity = document.getElementById('cityid');
	
$.ajax({
						url: "lib/scripts/php/all/ajaxCityOnCountryId.php",
						type: "POST",
						data:'CountryId='+counterid,
						success: function(data){
						//alert(data);	
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			var duce = jQuery.parseJSON(data);
                                   			var status = duce.status;
											var cityoptions =duce.cityoptions;
                                    												
								   }
								   if (status == 1 ) {
									   selectCity.innerHTML = cityoptions;
									   $("#cityid").selectpicker('refresh');
									   $('#save_btn').show();
								   }else {
									  noty({
                        text: 'Select City',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									 $noty.close();
                                       
                                }
                                },
                                
                                
                            ]
                    })
									   
								   }
								   
		
											
											} 
										   
						       
				   });

}





function js_addCombo() {
	
	document.getElementById("message-box-tour").style.display="none";
	var comboid=document.getElementById("comboid").value; 
	
	if ( comboid > 0 ) {
					$.ajax({
						url: "lib/scripts/php/all/ajaxAddExclusiveCombo.php",
						type: "POST",
						data:'comboid='+comboid,
						success: function(data){
										console.log(data); 
										//alert(data);
										if(data)
                                   		{                                  
                                   			 duce = jQuery.parseJSON(data);
                                   			 comboname = duce.comboname;
                                    												
								   }
								  //alert(tourname);
		
											if (document.getElementById(comboid)) {
												 noty({
                        text: 'This Combo already exist.',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									 $noty.close();
                                       
                                }
                                },
                                
                                
                            ]
                    })
												
												//alert("This Tour already exist");
												return false;
											}
							
		
											var table = document.getElementById("sortabletablebannerHead").getElementsByTagName('tbody')[0];;
											//var table=document.getElementById("data_tableED");
                                            var table_len=(table.rows.length);
                                             var row = table.insertRow(table_len).outerHTML= '<tr id="'+comboid+'" style="height: 40px;">\n\<td >'+comboname+'</td>\n\<td><span style="background-image: url(\'img/drag.png\');background-repeat: no-repeat;background-position: center;width: 30px;height: 30px; display: inline-block;float: left;cursor: move;"></span></td>\n\<td><a href="#" type="submit" onClick=\'return delete_rowbanner('+comboid+');\'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>\n\<input type="hidden" name="arraycomboname[]" value=\''+comboid+'\'</tr>';} 
										   });
	}
}						
</script>

