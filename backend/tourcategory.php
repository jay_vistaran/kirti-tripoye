<?php
	require_once("header.php");
        //global $cleaned;
	$getSystemUsers = Users::getTourCategory();//for global
    //echo "<pre>";print_r($getSystemUsers);die;
         
?>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Tour Category</h3>
                                    <?php if(isset($_REQUEST['error']) && !empty($_REQUEST['error'])){?>
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Danger!</strong> <?php echo $_REQUEST['error'];?>
                                    </div>
                                    <?php }?>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                            <a href="tourcategoryAdd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Tour Category</button></a>
                                     </div>
                                       
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <form name="edit_cat_form" id="edit_cat_form" action="" method="post">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Tour Category City</th>
                                                <th>Tour Category</th>
                                                <th>Single Width Image</th>
                                                <th>Double Width Image</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getSystemUsers as $NewSystemUsers){?>    
                                            <tr>
                                                <td><?php echo $NewSystemUsers['id'];?></td>
                                                <td><?php echo $NewSystemUsers['cityname'];?></td>
                                                <td><?php echo $NewSystemUsers['tourcatname'];?></td>
                                                <td><img src="<?php echo $NewSystemUsers['singlewidth_url'];?>" width="100" /></td>
                                                <td><img src="<?php echo $NewSystemUsers['doublewidth_url'];?>" width="100" /></td>
                                                <td><?php if($NewSystemUsers['status']==1){echo "Active";}else{echo "InActive";};?></td>
                                                
                                                <td><a href="tourcategoryEdit.php?id=<?php echo $NewSystemUsers['id'];?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                                                    
                                                    <a href="#" type="submit" onClick='return deletemode(<?php echo $NewSystemUsers['id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table></form>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                            

                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->  


             <script type="text/javascript">
                
                        function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
                                        window.location.href="lib/scripts/php/all/tourcategory-actions.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }
</script>

<?php	require_once("footer.php");
?>

<script type="text/javascript">
    window.setTimeout(function() {
    $(".alert").fadeTo(10000, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
</script>