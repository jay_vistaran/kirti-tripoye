<?php
	require_once("header.php");
        //global $cleaned;
	$getSystemUsers = Users::getSystemUsers();//for global
                     
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Country</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Add New Country</h4>
                                <form id="jvalidate_city" role="form" class="form-horizontal" action="lib/scripts/php/all/country.php">
                                <div class="panel-body">                                    
                                                 
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate City Name, Please Try Another City name</span> <?php }?>
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Country Name:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="countryname"/>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Description:</label>  
                                        <div class="col-md-9">
                                           <textarea class="form-control" rows="5" name="description"></textarea>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>                                                                                    
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate_city.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>


                
                
                
              
                






