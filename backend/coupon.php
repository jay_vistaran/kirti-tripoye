<?php
	require_once("header.php");
        //global $cleaned;
         
        $getSystemCountry = Users::getSystemCountryAproved();//for global
         
        $getSystemCity = Users::getSystemCityAproved();//for global
         
        $getSystemTC= Users::getTourCategoryAproved();//for global
        
        $getSystemCoupon= Users::getCoupon();//for global
?>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Coupon Management</h3>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                            <a href="couponadd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Coupon</button></a>
                                     </div>
                                       
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                   
                                    
                                    
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Coupon Code</th>
                                                <th>Coupon Value</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=0;
                                  foreach($getSystemCoupon as $Coupon){?>    
                                            <tr>
                                                <td><?php echo $Coupon['id'];?></td>
                                                
                                                <td><?php echo $Coupon['couponcode'];?></td>
                                                <td><?php echo $Coupon['discountvalue']; ?></td>
                                                <td><?php echo $Coupon['bookingfrom'];?></td>
                                                <td><?php echo $Coupon['bookingto'];?></td>
                                               
                                                <td><a href="couponedit.php?id=<?php echo $Coupon['id'];?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                                                    
                                                    <a href="#" type="submit" onClick='return deletemode(<?php echo $Coupon['id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table>
                                    
                                    
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                            

                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->  


             <script type="text/javascript">
                
                      /*  function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. Becouse if you delete Tour here then releted Tour Detail will be deleted. ");
                                if(conf)
                                {
                                       window.location.href="lib/scripts/php/all/coupondelete.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }
						*/
</script>

<?php	require_once("footer.php");
?>
<script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
<script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
 <script type="text/javascript">
                
                       /* function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. Becouse if you delete Tour here then releted Tour Detail will be deleted. ");
                                if(conf)
                                {
                                       window.location.href="lib/scripts/php/all/deletetour.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }*/
						function deletemode(id){
                    noty({
                        text: 'Are you sure Want to delete This. Becouse if you delete Tour here then releted Tour Detail will be deleted. ',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									window.location.href="lib/scripts/php/all/coupondelete.php?id="+id;
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }
</script>