<?php
	require_once("header.php");
        //global $cleaned;
	$getSystemUsers = Users::getSystemSeason();//for global
       
         
?>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Manage Season</h3>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                         <a href="seasonAdd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add New Season</button></a>
                                     </div>
                                      
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <form name="edit_cat_form" id="edit_cat_form" action="" method="post">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Season Name</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Effective Date</th>
                                               
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getSystemUsers as $NewSystemUsers){?>    
                                            <tr>
                                                <td><?php echo $NewSystemUsers['id'];?></td>
                                                <td><?php echo $NewSystemUsers['season_name']; ?></td>
                                                <td><?php echo $NewSystemUsers['fromdate'];?></td>
                                                <td><?php echo $NewSystemUsers['todate'];?></td>
                                                <td><?php echo $NewSystemUsers['effectivedate'];?></td>
                                              
                                                <td><a href="seasonEdit.php?id=<?php echo $NewSystemUsers['id'];?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                                                    
                                                    <a href="#" type="submit" onClick='return deletemode(<?php echo $NewSystemUsers['id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table></form>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                            

                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->  


             <script type="text/javascript">
                
                        function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This ");
                                if(conf)
                                {
                                        window.location.href="lib/scripts/php/all/season-actions.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }
</script>

<?php	require_once("footer.php");
?>