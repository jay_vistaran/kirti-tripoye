<?php
	require_once("header.php");
         //global $cleaned;
        $cleaned = clean($_GET);
        //dump($cleaned);
	$getSystemCity = Users::getSystemCityById($cleaned['id']);//for global
        
	$getSystemCountry = Users::getSystemCountryAproved();//for global
                     
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> City</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Edit City</h4>
                                <form id="jvalidate_city"  method="post" enctype="multipart/form-data" role="form" class="form-horizontal" action="lib/scripts/php/all/cityEdit.php">
                                <div class="panel-body">                                    
                                                 
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate City Name, Please Try Another City name</span> <?php }?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">City Name:</label>  
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control" name="city_id" value="<?php echo $getSystemCity['id'];?>"/>
                                            <input type="text" class="form-control" name="cityname" value="<?php echo $getSystemCity['cityname'];?>"/>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Select Country:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="countryid">
                                                <option value="">Select Country</option>
                                                 <?php 
                                                 foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>" <?php if($getSystemCity['countryid']==$SystemCountry['id']){?> selected="selected" <?php }?> ><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div> 
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">File</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class=" btn-primary" name="filename" id="filename" title="Browse file"/>
                                             <img src="lib/tourimages/city/<?php echo $getSystemCity['cityimage'];?>" width="100" />
                                            <input type="hidden" name="old_img" id="gimage" value="<?php echo $getSystemCity['cityimage'];?>"">
                                            <span class="help-block">Image ratio 16:9</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Heading 1:</label>  
                                        <div class="col-md-9">
                                           <textarea class="form-control" rows="5" name="description"><?php echo $getSystemCity['description'];?></textarea>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Heading 2:</label>  
                                        <div class="col-md-9">
                                           <textarea class="form-control" rows="5" name="description2"><?php echo $getSystemCity['description2'];?></textarea>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Show On Landing Page:</label>
                                        <div class="col-md-3">
                                            <input type="checkbox" name="showLpage" id="none" value="1"  <?php if($getSystemCity['showLpage']==1){ ?> checked="checked" <?php } ?> >
                                           
                                        </div>                        
                                     </div> 
                                    
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="status">
                                                <option value="1" <?php if($getSystemCity['status']==1){?> selected="selected" <?php }?> >Active</option>
                                                <option value="0" <?php if($getSystemCity['status']==0){?> selected="selected" <?php }?> >Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>
                                    <div class="panel-heading" style="padding-left: 155px;">                                
                                               
                                                     <a href="topsellingcategory.php?cityid=<?php echo $cleaned['id'];?>" style="float: left; padding-right: 10px;"> <div class="btn btn-success btn-block" style="width: 160px;"> Top Selling Category</div></a>

                                                     <a href="recommendedcategory.php?cityid=<?php echo $cleaned['id'];?>" style="float: left; padding-right: 10px;"> <div class="btn btn-success btn-block" style="width: 160px;">Recommended Category</div></a>
                                                
                                     </div>
                                    <div class="btn-group pull-right" style="padding-top: 10px;">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate_city.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>


                
                
                
              
                






