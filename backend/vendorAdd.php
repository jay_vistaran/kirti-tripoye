<?php
	require_once("header.php");
    $getSystemCountry = Users::getSystemCountryAproved();//for Country
    //echo "<pre>";print_r($getSystemCountry);die; 
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Vendors</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Add New Vendor</h4>
                                <form id="jvalidateVendorAdd" role="form" class="form-horizontal" action="lib/scripts/php/all/vendors.php">
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Select Country:</label>
                                        <div class="col-md-9">          
                                            <select class="form-control select" name="countryid" onChange="js_country_city(this.value)">
                                                <option value="">Select Country</option>
                                                <?php 
                                                foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>"><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>   
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Select City:</label>
                                        <div class="col-md-9">               
                                            <select class="form-control select" name="cityid" id="cityid" onChange="js_tour_category(this.value)"> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">First Name:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="vendor_name"/>
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div>
                                    <div class="form-group">                                        
                                        <label class="col-md-3 control-label">Last name:</label>          
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="vendor_surname"/>                                        
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Company:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="vendor_company"/>
                                            <span class="help-block">min size = 2, max size = 20</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="" name="vendor_email" class="form-control"/>                                        
                                            <span class="help-block">required email</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Phone:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="" name="vendor_phone" class="form-control"/>                                        
                                            <span class="help-block">required Phone</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Skype Id:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="" name="vendor_skypeid" class="form-control"/>                                        
                                            <span class="help-block">required Skype Id</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Whatssap No:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="" name="vendor_watsappno" class="form-control"/>                                        
                                            <span class="help-block">required Whatssap No</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Office Address:</label>
                                        <div class="col-md-9">
                                            <textarea type="text" value="" name="vendor_officeaddress" class="form-control"></textarea>                                        
                                            <span class="help-block">required Office Address</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="vendor_status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>                           
                                            
                                        </div>                        
                                     </div>  
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidateVendorAdd.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");?>
<script>
function js_country_city(counterid) {
    selectCity = document.getElementById('cityid');
    //document.getElementById('tourcatid').innerHTML = "";
    $("#tourcatid").selectpicker('refresh');
$.ajax({
                        url: "lib/scripts/php/all/ajaxCityOnCountryId.php",
                        type: "POST",
                        data:'CountryId='+counterid,
                        success: function(data){
                        //alert(data);  
                                        console.log(data); 
                                        //alert(data);
                                        if(data)
                                        {                                  
                                            var duce = jQuery.parseJSON(data);
                                            var status = duce.status;
                                            var cityoptions =duce.cityoptions;
                                                                                    
                                   }
                                   if (status == 1 ) {
                                       selectCity.innerHTML = cityoptions;
                                       $("#cityid").selectpicker('refresh');
                                   }else {
                                       alert("Select Country");
                                       
                                   }
                                   
        
                                            
                                            } 
                                           
                               
                   });

}
</script>


                
                
                
              
                






