<?php
	require_once("header.php");
        //global $cleaned;
	$getSystemVendors = Users::getSystemVendors();
?>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Vendors</h3>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                         <a href="vendorAdd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add New Vendor</button></a>
                                     </div>
                                      
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <form name="edit_cat_form" id="edit_cat_form" action="" method="post">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Skype ID</th>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Company</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getSystemVendors as $NewSystemVendors){?>    
                                            <tr>
                                                <td><?php echo $NewSystemVendors['vendor_id'];?></td>
                                                <td><?php echo $NewSystemVendors['vendor_name'] . '&nbsp;' .$NewSystemVendors['vendor_surname'];?></td>
                                                <td><?php echo $NewSystemVendors['vendor_email'];?></td>
                                                <td><?php echo $NewSystemVendors['vendor_phone'];?></td>
                                                <td><?php echo $NewSystemVendors['vendor_skypeid'];?></td>
                                                <td><?php $CountryName = Users::getCountryNameByID($NewSystemVendors['countryid']);
                                                  echo $CountryName['countryname'];?></td>
                                                <td><?php $CityName = Users::getCityNameByID($NewSystemVendors['cityid']);
                                                  echo $CityName['cityname'];?></td>
                                                <td><?php echo $NewSystemVendors['vendor_company'];?></td>
                                                <td><a href="vendorEdit.php?id=<?php echo $NewSystemVendors['vendor_id'];?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                                                    
                                                    <a href="#" type="submit" onClick='return deletemode(<?php echo $NewSystemVendors['vendor_id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table></form>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                            

                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->  


             <script type="text/javascript">
                
                        function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This ");
                                if(conf)
                                {
                                        window.location.href="lib/scripts/php/all/vendors-actions.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }
</script>

<?php	require_once("footer.php");
?>