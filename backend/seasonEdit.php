<?php
	require_once("header.php");
        //global $cleaned;
        $cleaned = clean($_GET);
        //dump($cleaned);
        //$getAdminRoles = Users::getAdminRoles();//for global
	$SystemUsers = Users::getSeasonById($cleaned['id']);//for global
       // dump($SystemUsers);
        
       
        if($SystemUsers['weekdaystatus']==0)
                       {
                          $weekdaystatus0='checked="checked"';
                          $weekdayschk1='';
                          $weekdayschk2='';
                          $weekdayschk3='';
                          $weekdayschk4='';
                          $weekdayschk5='';
                          $weekdayschk6='';
                          $weekdayschk7='';
                       }
       
         if($SystemUsers['weekdaystatus']==1)
                       {
                           $weekdaystatus1='checked="checked"';
                           $weekdays=unserialize($SystemUsers['weekdays']);
                           //var_export($weekdays);
//                           if($weekdays[0]==1){$weekdayschk1='checked="checked"';}
//                           if($weekdays[1]==2){$weekdayschk2='checked="checked"';}
//                           if($weekdays[2]==3){$weekdayschk3='checked="checked"';}
//                           if($weekdays[3]==4){$weekdayschk4='checked="checked"';}
//                           if($weekdays[4]==5){$weekdayschk5='checked="checked"';}
//                           if($weekdays[5]==6){$weekdayschk6='checked="checked"';}
//                           if($weekdays[6]==7){$weekdayschk7='checked="checked"';}
                                                 
                       }
        if($SystemUsers['weekdaystatus']==2)
                       {
                           $weekdaystatus1='';
                           $weekdaystatus0='';
                           
                           $weekdays=unserialize($SystemUsers['weekdays']);
                           //var_export($weekdays);
                           
//                           if($weekdays[0]==1){$weekdayschk1='checked="checked"';}
//                           if($weekdays[1]==2){$weekdayschk2='checked="checked"';}
//                           if($weekdays[2]==3){$weekdayschk3='checked="checked"';}
//                           if($weekdays[3]==4){$weekdayschk4='checked="checked"';}
//                           if($weekdays[4]==5){$weekdayschk5='checked="checked"';}
//                           if($weekdays[5]==6){$weekdayschk6='checked="checked"';}
//                           if($weekdays[6]==7){$weekdayschk7='checked="checked"';}
                                                 
                       }
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Season</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Edit Season</h4>
                                <form id="jvalidateSeason" role="form" class="form-horizontal" action="lib/scripts/php/all/seasonEdit.php">
                                <div class="panel-body" style="width: 750px;"> 
                                     <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate Season Name, Please Try Another Season name</span> <?php }?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Season Name:</label>  
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control" name="season_id" value="<?php echo $SystemUsers['id'];?>"/>
                                            <input type="text" class="form-control" name="season_name"  value="<?php echo $SystemUsers['season_name'];?>"/>
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div>
                                                
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">From Date :</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="fromdate"  value="<?php echo $SystemUsers['fromdate'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">To Date:</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="todate"  value="<?php echo $SystemUsers['todate'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
                                     
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Effective Date :</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="effectivedate"  value="<?php echo $SystemUsers['effectivedate'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Week Days :</label>
                                        <div class="col-md-9">
                                            <input type="checkbox" name="select_all" <?php echo $weekdaystatus1;?> id="select_all" value="11">  All  &nbsp;
                                            <input type="checkbox" name="none" id="none"  <?php echo $weekdaystatus0;?> value="10">  None
                                        </div>
                                    </div>-->
                                   
<!--                                     <div class="form-group">
                                        <label class="col-md-3 control-label">&nbsp; </label>
                                        <div class="col-md-3" style="width: 555px;">-->
                                            
                                         <?php 
                                             
                                         /* $mydays = array(
                                              1 => 'Monday', 2 => 'Tuesday', 
                                              3 =>'Wednesday', 4 =>'Thursday', 
                                              5 =>'Friday', 6 =>'Saturday', 7 =>'Sunday');
                                          
                                         foreach ($mydays as $id => $wname) {
                                             
                                             if(!empty($weekdays))
                                                 {
                                                        $chk='';

                                                        foreach ($weekdays as $key => $user_role) 
                                                               {

                                                                   if($id==$user_role)
                                                                       {
                                                                         $chk='checked="checked"';

                                                                       }

                                                               }
                                             
                                                 }
                                            ?>    
                                           
                                            <input class="wbox" type="checkbox"  <?php echo $chk;?>  name="weekdays[]" value="<?php echo $id;?>"> <?php echo $wname;?>
                                           
                                            <?php echo "&nbsp;"; }
                                            */
                                            
                                            ?>       
                                           
<!--                                            <input class="wbox" type="checkbox" <?php echo $weekdayschk1;?> name="weekdays[]" value="1"> Monday &nbsp;
                                            <input class="wbox" type="checkbox" <?php echo $weekdayschk2;?> name="weekdays[]" value="2"> Tuesday &nbsp;
                                            <input class="wbox" type="checkbox" <?php echo $weekdayschk3;?> name="weekdays[]" value="3"> Wednesday &nbsp;
                                            <input class="wbox" type="checkbox" <?php echo $weekdayschk4;?> name="weekdays[]" value="4"> Thursday &nbsp;
                                            <input class="wbox" type="checkbox" <?php echo $weekdayschk5;?> name="weekdays[]" value="5"> Friday &nbsp;
                                            <input class="wbox" type="checkbox" <?php echo $weekdayschk6;?> name="weekdays[]" value="6"> Saturday &nbsp;
                                            <input class="wbox" type="checkbox" <?php echo $weekdayschk7;?> name="weekdays[]" value="7"> Sunday &nbsp;
                                            -->
                                           
                                            
<!--                                        </div>                        
                                    </div>-->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>  
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");


?>

                <script>

jQuery("#select_all").change(function(){  //"select all" change
    var status = this.checked; // "select all" checked status
    jQuery('.wbox').each(function(){ //iterate all listed checkbox items
        this.checked = status; //change ".checkbox" checked status
        jQuery('.none').checked=false;
    });
});

jQuery("#none").change(function(){  //"select all" change
    var status = this.checked; // "select all" checked status
    jQuery('.wbox').each(function(){ //iterate all listed checkbox items
        this.checked = false; //change ".checkbox" checked status
        jQuery("#select_all")[0].checked = false; //change "select all" checked status to false
    });
    
   
})
jQuery('.wbox').change(function(){ //".checkbox" change
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(this.checked == false){ //if this item is unchecked
        jQuery("#select_all")[0].checked = false; //change "select all" checked status to false
        jQuery("#none")[0].checked = false; //change "select all" checked status to false
    }
    
    
    //check "select all" if all checkbox items are checked
    if (jQuery('.wbox:checked').length == jQuery('.wbox').length ){
        jQuery("#select_all")[0].checked = true; //change "select all" checked status to true
    }
});
</script>


                
                
                
              
                






