<?php
	require_once("header.php");
         //global $cleaned;
        $cleaned = clean($_GET);
        //dump($cleaned);
	$getSystemCity = Users::getTransferCategoryById($cleaned['id']);//for global
	
                     
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Transfer Option</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Edit Transfer Option</h4>
                                <form id="jvalidate_tourcategory" role="form" class="form-horizontal" action="lib/scripts/php/all/transfercategoryEdit.php">
                                <div class="panel-body">                                    
                                                 
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate Transfer Category Name, Please Try Another Transfer Category name</span> <?php }?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tour Option Name:</label>  
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control" name="transfercat_id" value="<?php echo $getSystemCity['id'];?>"/>
                                            <input type="text" class="form-control" name="transferoption" value="<?php echo $getSystemCity['transferoption'];?>"/>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label class="col-md-3 control-label">Transfer Category:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="transfercatname">
                                                <option value="1" <?php if($getSystemCity['transfercatname']==1){?> selected="selected" <?php }?>>Sharing</option>
                                                <option value="2" <?php if($getSystemCity['transfercatname']==2){?> selected="selected" <?php }?>>Private</option>
                                                <option value="3" <?php if($getSystemCity['transfercatname']==3){?> selected="selected" <?php }?>>Ticket Only</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label">Description:</label>  
                                        <div class="col-md-9">
                                           <textarea class="form-control" rows="5" name="description"><?php echo $getSystemCity['description'];  ?></textarea>
                                            
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="status">
                                                <option value="1" <?php if($getSystemCity['status']==1){?> selected="selected" <?php }?> >Active</option>
                                                <option value="0" <?php if($getSystemCity['status']==0){?> selected="selected" <?php }?> >Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>                                                                                    
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate_tourcategory.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>


                
                
                
              
                






