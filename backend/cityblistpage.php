<?php
	require_once("header.php");
        //global $cleaned;
        
	if(!isset($_GET['CityId']) || $_GET['CityId']=='' )
            {
                    header("location:citybanner.php"); 
            }
        $CityId=$_GET['CityId'];
        
        $getLandingAppBanner = Users::getCityAppBanner($CityId);//for global
        $getCityNameById = Users::getCityNameById($CityId);//for global
        //dump($getCityNameById);
                       
?>
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<style>
    .content {
	
    margin-left: -37px;
    margin-right: 30px;
    padding: 10px;
    text-align: center;
}
    /* Sortable ******************/
#sortable { 
	list-style: none; 
	text-align: left; 
}
#sortable li { 
	margin: 0 0 10px 0;
	height: 75px; 
	background: #dbd9d9;
	border: 1px solid #999999;
	border-radius: 5px;
	color: #333333;
}

#sortable li img {
	height: 65px;
	border: 5px solid #cccccc;
	display: inline-block;
	float: left;
}
#sortable li div {
	padding: 5px;
}
#sortable li h2 {    
	font-size: 16px;
    line-height: 20px;
}


    
</style>
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Manage App Banner -> City Banner -> <?php echo $getCityNameById['cityname']; ?> Banner</h2>
                </div>
                <!-- END PAGE TITLE -->                
                 

<!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                
                       
                        
                                    <div class="panel-heading">
                                       
                                         <a href="addbanner.php?CityId=<?php echo $CityId;?>" style="float: left; padding-right: 10px;"> <div class="btn btn-success btn-block" style="width: 160px;">Add Banner</div></a>
                                                
                                     </div>
                        
                                  <div class="panel-body">
                                    <form name="edit_cat_form" id="edit_cat_form" action="" method="post">
                                   

             <table class="table " style=" margin-bottom: -8px; margin-left: 16px; max-width: 100%; width: 50%;">
                     <thead>
                             <tr>
                                    <th>S.No.</th>
                                    <th>Image Name</th>
                                    <th>Image</th>
                                    <th>Order</th>
                                    <th>Actions</th>
                              </tr>
                     </thead>
            </table>
           <div class="content" style="width: 55%;">
               <ul id="sortable">   
            
            <?php
            if(count($getLandingAppBanner)>0){
            foreach ($getLandingAppBanner as $LandingAppBanner) {
            ?>
           
            <li id="<?php echo $LandingAppBanner['id']; ?>" style="height: 77px;">
              
               <div style="float: left; font-size: 12px; padding-left: 7px; width: 107px;">  <?php echo $LandingAppBanner['id']; ?></div>
               <div style="float: left;font-size: 12px; padding-left: 7px; width: 155px;">  <?php echo $LandingAppBanner['img_name'];?></div>
            
               <div style="float: left;font-size: 12px; padding-left: 7px; width: 155px;"> <img src="<?php echo $LandingAppBanner['app_img_url'];?>" width="100" /> </div>
              
               <div style="float: left;font-size: 12px; padding-left: 7px; width: 111px;">   <span style="
                                            background-image: url('img/drag.png');
                                            background-repeat: no-repeat;
                                            background-position: center;
                                            width: 30px;
                                            height: 30px; 
                                            display: inline-block;
                                            float: left;
                                            cursor: move;">
                                            </span>
               </div>
               <div style="float: left;font-size: 12px; padding-left: 7px; width: 90px;">
                   <a href="editappbanner.php?id=<?php echo $LandingAppBanner['id'];?>&CityId=<?php echo $CityId;?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                           <a href="#" type="submit" onClick='return delete_row(<?php echo $LandingAppBanner['id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a>
               </div>
               
            </li>
            
            
            <?php
            }
            }
            else{
                echo "No Record Found In Database.";
            }
            ?>
        </ul>
        </div><!-- content --> 
                                      
            
            </form>
                                </div>
                        
                                   
                                 
                                    <div class="btn-group">
                                        <a href="appbanner.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div>                                                                                                                         

                                             
                                    </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>                                
                    
                </div>
  <!-- PAGE CONTENT WRAPPER --> 
  
  <script type="text/javascript">
   $(function() {
    $('#sortable').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function(event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            //var cityid=document.getElementById("cityid").value; 
    		// change order in the database using Ajax
            $.ajax({
                url: 'lib/scripts/php/all/ajaxAppBSetOrder.php',
                type: 'POST',
                data: {list_order:list_sortable},
                success: function(data) {
                    //finished
                }
            });
        }
    }); // fin sortable
}); 
    
        function delete_row(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
                                    $.ajax({
                                            url: "lib/scripts/php/all/ajaxAppBannerDelete.php",
                                            type: "POST",
                                            data:'id='+id,
                                            success: function(data){
                                                                        document.getElementById(id).outerHTML="";
                                                                   }        
                                           });    
                                }
                                else
                                {
                                        return false;
                                }
                                
                        }  
                        
        
  
   
</script>
<?php	require_once("footer.php");
?>


                
                
                
              
                






