<?php
	require_once("header.php");
        //global $cleaned;
        $today = date('Y-m-d');
	if(!isset($_GET['TourId']) || $_GET['TourId']=='' )
            {
                    header("location:createTour1.php"); 
            }
        $TourId=$_GET['TourId'];
        
        $getTourBannerImage = Users::getTourBannerImageBYTourId($TourId);//for Banner
        
        $getTourGalleryImage = Users::getTourGalleryImageBYTourId($TourId);//for Gallery
		 $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid =$getSystemDefaultSeason['id'];
		}
		else{
			$sid =0;
		} 
         
        //dump($getTourBannerImage);                   
?>

 <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
	<script>
	$(document).ready(function() {
		$('.nav-tabs > li > a').click(function(event){
		event.preventDefault();//stop browser to take action for clicked anchor

		//get displaying tab content jQuery selector
		var active_tab_selector = $('.nav-tabs > li.active > a').attr('href');

		//find actived navigation and remove 'active' css
		var actived_nav = $('.nav-tabs > li.active');
		actived_nav.removeClass('active');

		//add 'active' css into clicked navigation
		$(this).parents('li').addClass('active');

		//hide displaying tab content
		$(active_tab_selector).removeClass('active');
		$(active_tab_selector).addClass('hide');

		//show target tab content
		var target_tab_selector = $(this).attr('href');
		$(target_tab_selector).removeClass('hide');
		$(target_tab_selector).addClass('active');
	     });
	  });
	</script>
    <link rel="stylesheet" href="tabletest/css/style.css"> 

		<style>
			/** Start: to style navigation tab **/
			.nav {
			  margin-bottom: 18px;
			  margin-left: 0;
			  list-style: none;
			}

			.nav > li > a {
			  display: block;
			}

			.nav-tabs{
			  *zoom: 1;
			}

			.nav-tabs:before,
			.nav-tabs:after {
			  display: table;
			  content: "";
			}

			.nav-tabs:after {
			  clear: both;
			}

			.nav-tabs > li {
			  float: left;
			}

			.nav-tabs > li > a {
			  padding-right: 12px;
			  padding-left: 12px;
			  margin-right: 2px;
			  line-height: 14px;
			}

			.nav-tabs {
			  border-bottom: 1px solid #ddd;
			}

			.nav-tabs > li {
			  margin-bottom: -1px;
			}

			.nav-tabs > li > a {
			  padding-top: 8px;
			  padding-bottom: 8px;
			  line-height: 18px;
			  border: 1px solid transparent;
			  -webkit-border-radius: 4px 4px 0 0;
				 -moz-border-radius: 4px 4px 0 0;
					  border-radius: 4px 4px 0 0;
			}

			.nav-tabs > li > a:hover {
			  border-color: #eeeeee #eeeeee #dddddd;
			}

			.nav-tabs > .active > a,
			.nav-tabs > .active > a:hover {
			  color: #555555;
			  cursor: default;
			  background-color: #ffffff;
			  border: 1px solid #ddd;
			  border-bottom-color: transparent;
			}

			li {
			  line-height: 18px;
			}

			.tab-content.active{
				display: block;
			}

			.tab-content.hide{
				display: none;
			}
.mactive {
            background-color: #e34724;
    }

			/** End: to style navigation tab **/
		</style>
                
                
<!--     for shorting           -->

<style>
ul.topnav {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #29343f;
  border-radius: 5px;
}

ul.topnav li {float: left;}

ul.topnav li a {
  display: inline-block;
  color: #f2f2f2;
  text-align: center;
  padding: 10px 35px;
  text-decoration: none;
  transition: 0.3s;
  font-size: 14px;
}

ul.topnav li a:hover {background-color: #e34724;}

ul.topnav li.icon {display: none;}

@media screen and (max-width:680px) {
  ul.topnav li:not(:first-child) {display: none;}
  ul.topnav li.icon {
    float: right;
    display: inline-block;
  }
}

@media screen and (max-width:680px) {
  ul.topnav.responsive {position: relative;}
  ul.topnav.responsive li.icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  ul.topnav.responsive li {
    float: none;
    display: inline;
  }
  ul.topnav.responsive li a {
    display: block;
    text-align: left;
  }
}
</style>
<style>
    .content {
	
    margin-left: -37px;
    margin-right: 30px;
    padding: 10px;
    text-align: center;
}
    /* Sortable ******************/
            #sortableB { 
                    list-style: none; 
                    text-align: left; 
            }
          /*  #sortabletablebanner { 
                    margin: 0 0 10px 0;
                    height: 75px; 
                    background: #dbd9d9;
                    border: 1px solid #999999;
                    border-radius: 5px;
                    color: #333333;
					
    border-collapse:separate;
    border-spacing:0 5px;

            }
			 #sortabletablegallary { 
                    margin: 0 0 10px 0;
                    height: 75px; 
                    background: #dbd9d9;
                    border: 1px solid #999999;
                    border-radius: 5px;
                    color: #333333;
					
    border-collapse:separate;
    border-spacing:0 5px;

            }*/

            #sortableB li img {
                    height: 65px;
                    border: 5px solid #cccccc;
                    display: inline-block;
                    float: left;
            }
            #sortableB li div {
                    padding: 5px;
            }
            #sortableB li h2 {    
                    font-size: 16px;
                line-height: 20px;
            }
            
/*            for gallery*/

            #sortableG { 
                    list-style: none; 
                    text-align: left; 
            }
            #sortableG li { 
                    margin: 0 0 10px 0;
                    height: 75px; 
                    background: #dbd9d9;
                    border: 1px solid #999999;
                    border-radius: 5px;
                    color: #333333;
            }

            #sortableG li img {
                    height: 65px;
                    border: 5px solid #cccccc;
                    display: inline-block;
                    float: left;
            }
            #sortableG li div {
                    padding: 5px;
            }
            #sortableG li h2 {    
                    font-size: 16px;
                line-height: 20px;
            }


    
</style>
<style>
ul.topnav {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #29343f;
  border-radius: 5px;
}

ul.topnav li {float: left;}

ul.topnav li a {
  display: inline-block;
  color: #f2f2f2;
  text-align: center;
  padding: 10px 35px;
  text-decoration: none;
  transition: 0.3s;
  font-size: 14px;
}

ul.topnav li a:hover {background-color: #e34724;}

ul.topnav li.icon {display: none;}

@media screen and (max-width:680px) {
  ul.topnav li:not(:first-child) {display: none;}
  ul.topnav li.icon {
    float: right;
    display: inline-block;
  }
}

@media screen and (max-width:680px) {
  ul.topnav.responsive {position: relative;}
  ul.topnav.responsive li.icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  ul.topnav.responsive li {
    float: none;
    display: inline;
  }
  ul.topnav.responsive li a {
    display: block;
    text-align: left;
  }
}
</style>               
<!--  for shorting               -->
                
                
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2></span> Manage Tour</h2>
                </div>
                <!-- END PAGE TITLE -->                
                 

<!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                
                                <ul class="topnav" id="myTopnav">
 <li class="navtour"><a href="createTour1edit.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Tour Detail</a></li>
                                                <li class="navtour"><a href="createTour2.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Tour Timing</a></li>
                                                <li class="navtour mactive"><a href="createTour3.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Tour Image</a></li>
                                                <li class="navtour"><a href="createTour4.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Transfer Option</a></li>
                                                <li class="navtour"><a href="createTour5.php?TourId=<?php echo $TourId;?>&sId=<?php echo $sid;?>" class="navtour"  style=" font-weight: bold;">Buying Price</a></li>
                                                <li class="navtour"><a href="createTour6.php?TourId=<?php echo $TourId;?>&sId=<?php echo $sid;?>" class="navtour"  style=" font-weight: bold;">Selling Price</a></li>
                                                <li class="navtour"><a href="createTour7.php?TourId=<?php echo $TourId;?>&sId=<?php echo $sid;?>" class="navtour"   style="font-weight: bold;">Discount</a></li>
  <li class="icon">
   <a href="javascript:void(0);" style="font-size:15px;" onclick="mymenuFunction()">☰</a>
  </li>
</ul>
                                    <!--<div class="col-md-8" >
                                            <div class="btn-group btn-group-justified">  
                                                <a href="createTour1edit.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Detail</a>
                                                <a href="createTour2.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Timing</a>
                                                <a href="createTour3.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg active"  style=" font-weight: bold;">Tour Image</a>
                                                <a href="createTour4.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Transfer Option</a>
                                                <a href="createTour5.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Buying Price</a>
                                                <a href="createTour6.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Selling Price</a>
                                                <a href="createTour7.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Discount</a>
                                            </div>                                         
                                    </div> -->
                        
                        <br> <br>  <br>
                        <br>
                        
                                    <div class="panel-heading">                                
                                                <div class="col-md-4" >
                                                     <a href="createTourBanner.php?TourId=<?php echo $TourId;?>" style="float: left; padding-right: 10px;"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Banner Image</button></a>

                                                     <a href="createTourGallery.php?TourId=<?php echo $TourId;?>"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Gallery Image</button></a>
                                                </div>
                                     </div>
                        
                                    <div class="panel-body">
                                       <input type="hidden" class="form-control" name="TourId" id="TourId" value="<?php echo $TourId;?>"/>         

                                            <div style="padding-top: 50px;">
                                                    <ul class="nav nav-tabs">
                                                            <li class="active">
                                                                    <a href="#tab1">Banner Image</a>
                                                            </li>
                                                            <li>
                                                                    <a href="#tab2">Gallery Image</a>
                                                            </li>

                                                    </ul>
                                            </div>
                                                  
                                            <!--   Content in tab 1-->      
                <section id="tab1" class="tab-content active">
                    <div style="margin-bottom: 10px;">

      <table class="responstable" id="sortabletablebannerHead">
   <thead>
      <tr>
        <th>S.No.</th>
                                                <th>Banner Name</th>
                                                <th>Image</th>
                                                <th>Order</th>
                                                <th>Actions</th>
      </tr>
      </thead>
  
    <!--<tfoot>
      <tr class="ui-state-default">
        <th colspan="4">Original</th>
        <th colspan="4">table footer</th>
        <th colspan="4">table footer</th>
        <th colspan="4">table footer</th>
        <th colspan="4">table footer</th>
        <th colspan="4">Current Pos</th>
      </tr>
    </tfoot>-->
  
    <tbody id="sortabletablebanner">
                     <?php 
                        $i=1;
                        foreach($getTourBannerImage as $TourBannerImage)
                            {
                 ?> 
                     <tr id="rowb<?php echo $TourBannerImage['id']; ?>">              
                    <td width="20px;"> <?php echo $TourBannerImage['id'];?></td>
                    <td> <?php echo $TourBannerImage['bannername'];?></td>
                    <td> <img src="<?php echo $TourBannerImage['banner_img_url'];?>" width="100"/> </td>
                    <td><span style="background-image: url('img/drag.png');
                                            background-repeat: no-repeat;
                                            background-position: center;
                                            width: 30px;
                                            height: 30px; 
                                            display: inline-block;
                                            float: left;
                                            cursor: move;">
                                            </span>
                    </td>
                    <td>
                        <a href="#" type="submit" onClick='return delete_rowbanner(<?php echo $TourBannerImage['id'];?>,<?php echo $TourId;?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a>
                    </td>
                      </tr>                     
               
                                        <?php  $i++;
                            }?>    
                            
    
        
                            
    </tbody>
   
</table>
                                   <!-- <table class="table">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Banner Name</th>
                                                <th>Image</th>
                                                <th>Order</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                       
                                      </table>
                        
            <div class="content">
               <ul id="sortableB">
                <?php 
                        $i=1;
                        foreach($getTourBannerImage as $TourBannerImage)
                            {
                 ?> 
                <li id="<?php echo $TourBannerImage['id']; ?>" style="height: 76px;"> 
                   
                    <div style="float: left; font-size: 12px; padding-left: 7px; width: 185px;"> <?php echo $TourBannerImage['id'];?></div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 380px;"> <?php echo $TourBannerImage['bannername'];?></div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 260px;"> <img src="lib/tourimages/banner/<?php echo $TourBannerImage['banner_img'];?>" width="100"/> </div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 211px;"><span style="
                                            background-image: url('img/drag.png');
                                            background-repeat: no-repeat;
                                            background-position: center;
                                            width: 30px;
                                            height: 30px; 
                                            display: inline-block;
                                            float: left;
                                            cursor: move;">
                                            </span>
                    </div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 20px;">
                        <a href="#" type="submit" onClick='return delete_rowbanner(<?php echo $TourBannerImage['id'];?>,<?php echo $TourId;?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a>
                    </div>
                                           
                 </li> 
                                        <?php  $i++;
                            }?>    
                                      
                  </ul>
            </div>                    
                                    



                                                    </div>-->
                                            </section>
                                                  
                                                  
         <section id="tab2" class="tab-content hide">
                                                   
                                                           
                                           <table class="responstable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Gallery Name</th>
                                                <th>Image</th>
                                                <th>Order</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                       
                                      
                  <tbody id="sortabletablegallary">      
               
                <?php 
                        $i=1;
                        foreach($getTourGalleryImage as $TourGalleryImage)
                            {
                 ?> 
                <tr id="rowg<?php echo $TourGalleryImage['id']; ?>" > 
                   <td width="20px;"> <?php echo $TourGalleryImage['id'];?></td>
<td><?php echo $TourGalleryImage['galleryname'];?></td>
                    <td> <img src="<?php echo $TourGalleryImage['gallery_img_url'];?>" width="100"/> </td>
                    <td><span style="
                                            background-image: url('img/drag.png');
                                            background-repeat: no-repeat;
                                            background-position: center;
                                            width: 30px;
                                            height: 30px; 
                                            display: inline-block;
                                            float: left;
                                            cursor: move;">
                                            </span>
                    </td>
                    <td>
                        <a href="#" type="submit" onClick='return delete_rowgallery(<?php echo $TourGalleryImage['id'];?>,<?php echo $TourId;?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a>
                    </td>
                                           
                 </tr>
                                        <?php  $i++;
                            }?>    
                                      
                 
                   </tbody>               
                                                        
                                                   
                                                        
                                </table>                   
                                            </section>                                         
                                                  
                                            <!--   Content in tab 2-->      
                                      <!--      <section id="tab2" class="tab-content hide">
                                                    <div>
                                                           
                                           <table class="table">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Gallery Name</th>
                                                <th>Image</th>
                                                <th>Order</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                       
                                      </table>
                        
            <div class="content">
               <ul id="sortableG">
                <?php 
                        $i=1;
                        foreach($getTourGalleryImage as $TourGalleryImage)
                            {
                 ?> 
                <li id="<?php echo $TourGalleryImage['id']; ?>" style="height: 76px;"> 
                   
                    <div style="float: left; font-size: 12px; padding-left: 7px; width: 185px;"> <?php echo $TourGalleryImage['id'];?></div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 380px;"> <?php echo $TourGalleryImage['galleryname'];?></div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 260px;"> <img src="lib/tourimages/gallery/<?php echo $TourGalleryImage['gallery_img'];?>" width="100"/> </div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 211px;"><span style="
                                            background-image: url('img/drag.png');
                                            background-repeat: no-repeat;
                                            background-position: center;
                                            width: 30px;
                                            height: 30px; 
                                            display: inline-block;
                                            float: left;
                                            cursor: move;">
                                            </span>
                    </div>
                    <div style="float: left;font-size: 12px; padding-left: 7px; width: 20px;">
                        <a href="#" type="submit" onClick='return delete_rowgallery(<?php echo $TourGalleryImage['id'];?>,<?php echo $TourId;?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a>
                    </div>
                                           
                 </li> 
                                        <?php  $i++;
                            }?>    
                                      
                  </ul>
            </div>                       
                                                        
                                                   
                                                        
                                                    </div>
                                            </section>-->




                                    <div class="btn-group">
                                        <a href="createTour4.php?TourId=<?php echo $TourId;?>"><button name="submit" value="1" class="btn btn-primary" type="submit">Save & Next</button></a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="managetour.php"><button name="savexit" value="1" class="btn btn-primary" type="submit">Save & Exit</button></a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="managetour.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div>
 
                                            

                                                                                                                                                                        

                                             
                                    </div>
                        
                        
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>                                
                    
                </div>
  <!-- PAGE CONTENT WRAPPER --> 
  
  <script type="text/javascript">
      $( "table #sortabletablebanner" ).sortable( {
		  
	update: function( event, ui ) {
		
		var rowID = new Array();
		var indexID = new Array();
		var TourId=document.getElementById("TourId").value; 
		//alert(data);
		//alert($(this).children().attr('id'));
        $(this).children().each(function(index) {
		//alert(id);
		//alert( 'index = ' + index + '; id = ' + $(row).attr( 'id' ) );
		   rowID.push( $(this).find('td').first().text());
		   indexID.push(index + 1)
			
    });
	rowIDS =rowID.toString();
	indexIDS =indexID.toString();
	 //alert(indexIDS);
//alert(indexIDS);
	 
    		// change order in the database using Ajax
            $.ajax({
                url: 'lib/scripts/php/all/ajaxC3BSetOrder.php',
                type: 'POST',
                data: 'TourId='+TourId+'&rowID='+rowIDS+'&indexID='+indexIDS,
                success: function(data) {
					//alert(data);
                    //finished
                }
            });
  }
});
   $( "table #sortabletablegallary" ).sortable( {
		  
	update: function( event, ui ) {
		
		var rowIDG = new Array();
		var indexIDG = new Array();
		var TourId=document.getElementById("TourId").value; 
		//alert(data);
		//alert($(this).children().attr('id'));
        $(this).children().each(function(index) {
		//alert(id);
		//alert( 'index = ' + index + '; id = ' + $(row).attr( 'id' ) );
		   rowIDG.push( $(this).find('td').first().text());
		   indexIDG.push(index + 1)
			
    });
	rowIDSG =rowIDG.toString();
	indexIDSG =indexIDG.toString();
	// alert(rowIDSG);
//alert(indexIDSG);
	 
    		// change order in the database using Ajax
            $.ajax({
                url: 'lib/scripts/php/all/ajaxC3GSetOrder.php',
                type: 'POST',
                data: 'TourId='+TourId+'&rowID='+rowIDSG+'&indexID='+indexIDSG,
                success: function(data) {
					//alert(data);
                    //finished
                }
            });
  }
});
      
    $(function() {
    $('#sortableB').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function(event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            var TourId=document.getElementById("TourId").value; 
    		// change order in the database using Ajax
            $.ajax({
                url: 'lib/scripts/php/all/ajaxC3BSetOrder.php',
                type: 'POST',
                data: {list_order:list_sortable,TourId},
                success: function(data) {
                    //finished
                }
            });
        }
    }); // fin sortable
    
    
    $('#sortableG').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function(event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            var TourId=document.getElementById("TourId").value; 
    		// change order in the database using Ajax
            $.ajax({
                url: 'lib/scripts/php/all/ajaxC3GSetOrder.php',
                type: 'POST',
                data: {list_order:list_sortable,TourId},
                success: function(data) {
                    //finished
                }
            });
        }
    }); // fin sortable
    
    
}); 
      
      
      
      
      
         
  
   
</script>
<?php	require_once("footer.php");
?>

<script src='http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'></script>
<script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
 <script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script> 
 <script type='text/javascript' src='js/plugins/noty/layouts/center.js'></script>            
 <script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>
 <script>
 /*function delete_rowbanner(id,TourId)
                        {
							//alert(id);
                                var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
                                    $.ajax({
                                            url: "lib/scripts/php/all/ajaxTBannerDelete.php",
                                            type: "POST",
                                            data:'TourId='+TourId+'&id='+id,
                                            success: function(data){
                                                                        document.getElementById("rowb"+id).outerHTML="";
                                                                   }        
                                           });    
                                }
                                else
                                {
                                        return false;
                                }
                                
                        } */ 
function delete_rowbanner(id,TourId){
                    noty({
                        text: 'Are you sure want to delete',
                        layout: 'center',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									$.ajax({
                                            url: "lib/scripts/php/all/ajaxTBannerDelete.php",
                                            type: "POST",
                                            data:'TourId='+TourId+'&id='+id,
                                            success: function(data){
                                                                        document.getElementById("rowb"+id).outerHTML="";
                                                                   }        
                                           }); 
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }						
                        
         /*function delete_rowgallery(id,TourId)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
                                    $.ajax({
                                            url: "lib/scripts/php/all/ajaxTGalleryDelete.php",
                                            type: "POST",
                                            data:'TourId='+TourId+'&id='+id,
                                            success: function(data){
                                                                        document.getElementById("rowg"+id).outerHTML="";
                                                                   }        
                                           });    
                                }
                                else
                                {
                                        return false;
                                }
                                
                        } */
						
function delete_rowgallery(id,TourId){
                    noty({
                        text: 'Are you sure Want to delete This',
                        layout: 'center',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									 $.ajax({
                                            url: "lib/scripts/php/all/ajaxTGalleryDelete.php",
                                            type: "POST",
                                            data:'TourId='+TourId+'&id='+id,
                                            success: function(data){
                                                                        document.getElementById("rowg"+id).outerHTML="";
                                                                   }        
                                           });
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }	
	function mymenuFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}					
 </script>             
 
                






