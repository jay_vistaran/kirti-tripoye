<?php
	require_once("header.php");
        //global $cleaned;
	$getSystemUsers = Users::getSystemCountry();//for global
      
         
?>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Country</h3>
                                    <?php if(isset($_REQUEST['error']) && !empty($_REQUEST['error'])){?>
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Danger!</strong> <?php echo $_REQUEST['error'];?>
                                    </div>
                                    <?php }?>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                            <a href="countryAdd.php"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add New Country</button></a>
                                     </div>
                                       
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <form name="edit_cat_form" id="edit_cat_form" action="" method="post">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Country Name</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getSystemUsers as $NewSystemUsers){?>    
                                            <tr>
                                                <td><?php echo $NewSystemUsers['id'];?></td>
                                                
                                                <td><?php echo $NewSystemUsers['countryname'];?></td>
                                                <td><?php if($NewSystemUsers['status']==1){echo "Active";}else{echo "InActive";};?></td>
                                                
                                                <td><a href="countryEdit.php?id=<?php echo $NewSystemUsers['id'];?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                                                    
                                                    <a href="#" type="submit" onClick='return deletemode(<?php echo $NewSystemUsers['id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table></form>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                            

                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->  


             <script type="text/javascript">
                
                        function deletemode(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. Becouse if you delete country here then releted city will be deleted. ");
                                if(conf)
                                {
                                        window.location.href="lib/scripts/php/all/country-actions.php?id="+id;
                                }
                                else
                                {
                                        return false;
                                }
                        }
</script>

<?php	require_once("footer.php");
?>

<script type="text/javascript">
    window.setTimeout(function() {
    $(".alert").fadeTo(10000, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
</script>