<?php

	require_once("header.php");

         //global $cleaned;

        $cleaned = clean($_GET);

        //dump($cleaned);

	$getSystemCity = Users::getSystemPagesById($cleaned['id']);//for global

	

          $getPageType= Users::getPageTypeAproved();//for global            

?>

  

<!-- PAGE TITLE -->

                <div class="page-title">                    

                    <h2><span class="fa fa-arrow-circle-o-left"></span> Pages</h2>

                </div>

                <!-- END PAGE TITLE -->                

                

                <!-- PAGE CONTENT WRAPPER -->

                <div class="page-content-wrap">                

                

                    <div class="row">

                        

                        <div class="col-md-11">                        



                            <!-- START JQUERY VALIDATION PLUGIN -->

                            <div class="block">

                                <h4>Edit Page</h4>

                                <form id="jvalidate_city" role="form" class="form-horizontal" action="lib/scripts/php/all/pageEdit.php">

                                <div class="panel-body">                                    

                                                 

                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate Page Name, Please Try Another Page name</span> <?php }?>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">Page Name:</label>  

                                        <div class="col-md-9">

                                            <input type="hidden" class="form-control" name="page_id" value="<?php echo $getSystemCity['id'];?>"/>

                                            <input type="text" class="form-control" name="pagename" value="<?php echo $getSystemCity['pagename'];?>"/>

                                            <span class="help-block">min size = 20, max size = 25</span>

                                        </div>

                                    </div>

                                    

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">Page Type:</label>

                                        <div class="col-md-3">

                                            <select class="select" name="pagetypeid">

                                               <?php 

                                                 foreach($getPageType as $PageType)

                                                      { ?>  

                                                <option value="<?php echo $PageType['id'];?>" <?php if($getSystemCity['pagetypeid']==$PageType['id']){?> selected="selected" <?php }?>    ><?php echo $PageType['pagetype'];?></option>

                                                <?php } ?>

                                                

                                                

                                            </select>                           

                                            

                                        </div>                        

                                     </div> 

                                    

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">Description:</label>  

                                        <div class="col-md-9">

                                            <textarea class="ckeditor" rows="5" name="content" style="width:793px;height:100px;"><?php echo $getSystemCity['content'];?></textarea>

                                           

                                            <span class="help-block">min size = 20, max size = 25</span>

                                        </div>

                                    </div>



                                    <?php if(!empty($getSystemCity['refund_policy'])){ ?> 

                                    <div class="form-group" id="refund_duration">
                                        <label class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-9">

                                            <table class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    <th>Sr.No.</th>
                                                    <th>From Duration</th>
                                                    <th>To Duration</th>
                                                    <th>Percentage</th>
                                                    <th></th>
                                                    <th></th>
                                                  </tr>
                                                </thead>
                                                <tbody class="tr_perent">
                                                <?php $refund_policy=json_decode($getSystemCity['refund_policy']);
                                                   if(!empty($refund_policy)){
                                                    foreach ($refund_policy as $key => $rfp) { ?>

                                                <tr>
                                                    <td><?=($key+1)?></td>
                                                    <td><input type="number" min="0" value="<?=$rfp->from_duration?>" class="form-control" name="refund_policy[<?=$key?>][from_duration]" id="from_duration"></td>
                                                    <td><input type="number" min="0" value="<?=$rfp->to_duration?>" class="form-control" name="refund_policy[<?=$key?>][to_duration]" id="to_duration"></td>
                                                    <td><input type="number" min="0" value="<?=$rfp->percentage?>" class="form-control" name="refund_policy[<?=$key?>][percentage]"></td>
                                                    <td><button type="button" class="btn btn-primary add_more" count="<?=($key+1)?>">+ ADD</button></td>
                                                    <td><?php if($key>='1'){ echo '<button type="button" class="btn btn-primary remove">x REMOVE</button>'; }else{  echo '-'; } ?></td>
                                                </tr>
                                                            
                                                <?php } }else{ ?> 

                                                <tr>
                                                    <td>1</td>
                                                    <td><input type="number" min="0" value="0" class="form-control" name="refund_policy[0][from_duration]" id="from_duration"></td>
                                                    <td><input type="number" min="0" value="0" class="form-control" name="refund_policy[0][to_duration]" id="to_duration"></td>
                                                    <td><input type="number" min="0" value="0" class="form-control" name="refund_policy[0][percentage]"></td>
                                                    <td><button type="button" class="btn btn-primary add_more" count="1">+ ADD</button></td>
                                                    <td>-</td>
                                                  </tr>

                                                <?php } ?>
                                                  
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>

                                    <?php } ?>

                                     <div class="form-group">

                                        <label class="col-md-2 control-label">Status:</label>

                                        <div class="col-md-3">

                                            <select class="select" name="status">

                                                <option value="1" <?php if($getSystemCity['status']==1){?> selected="selected" <?php }?> >Active</option>

                                                <option value="0" <?php if($getSystemCity['status']==0){?> selected="selected" <?php }?> >Inactive</option>

                                                

                                            </select>                           

                                            

                                        </div>                        

                                     </div>                                                                                    

                                    <div class="btn-group pull-right">

                                        <button class="btn btn-primary" type="button" onClick="jvalidate_city.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>

                                        <button style="margin-left: 5px;" class="btn btn-primary" type="submit">Submit</button>

                                    </div>                                                                                                                          

                                </div>                                               

                                </form>

                            <!-- END JQUERY VALIDATION PLUGIN -->

                            </div>

                        </div>

                    </div>



                 

                        

                </div>

                <!-- END PAGE CONTENT WRAPPER -->  





<script type="text/javascript">
    $(document).on("click",".add_more",function(){
        var count=parseInt($(this).attr('count'));
        $(".tr_perent tr:last-child").after('<tr><td>'+(count+1)+'</td><td><input type="number" min="0" value="0" class="form-control" name="refund_policy['+count+'][from_duration]" id="from_duration"></td><td><input type="number" min="0" value="0" class="form-control" name=refund_policy['+count+'][to_duration]" id="to_duration"></td><td><input type="number" min="0" value="0" class="form-control" name="refund_policy['+count+'][percentage]"></td><td><button type="button" class="btn btn-primary add_more" count="'+(count+1)+'">+ ADD</button></td><td><button type="button" class="btn btn-primary remove">x REMOVE</button></td></tr>');
    }); 

    $(document).on("click",".remove",function(){
        console.log($(this).parents().closest('tr').remove());
    });


    $(function() {

      $('.select').on('change', function(){
        var selected = $(this).find("option:selected").val();
        if(selected=='4'){
            $("#refund_duration").removeClass("hidden");
        }else{
            if($("#refund_duration").hasClass('hidden')){

            }else{
                 $("#refund_duration").addClass("hidden");
            }
        }
      });
      
    });

    var content = CKEDITOR.replace( 'content', {

        filebrowserBrowseUrl      : '<?php echo SYSTEM; ?>ckfinder/ckfinder.html',

        filebrowserImageBrowseUrl : '<?php echo SYSTEM; ?>ckfinder/ckfinder.html?type=Images',

        filebrowserFlashBrowseUrl : '<?php echo SYSTEM; ?>ckfinder/ckfinder.html?type=Flash',

        filebrowserUploadUrl      : '<?php echo SYSTEM; ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

        filebrowserImageUploadUrl : '<?php echo SYSTEM; ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

        filebrowserFlashUploadUrl : '<?php echo SYSTEM; ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

    });

    CKFinder.setupCKEditor( content, '../' );

    

</script>





<?php	require_once("footer.php");

?>





                

                

                

              

                













