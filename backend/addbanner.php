<?php
	require_once("header.php");
        //global $cleaned;
	
       if(isset($_GET['CityId']) || $_GET['CityId']!=0 )
            {
                    $CityId=$_GET['CityId'];
        
                    $getCityNameById = Users::getCityNameById($CityId);//for global
            }
      
                     
?>

<!-- PAGE TITLE -->
                <div class="page-title"> 
                    <?php if(!isset($_GET['CityId']) || $_GET['CityId']=='' ){?>
                    <h2><span class="fa fa-arrow-circle-o-left"></span>Manage App Banner -> Landing Page Banner</h2>
                     <?php }else{?>
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Manage App Banner -> City Banner -> <?php echo $getCityNameById['cityname']; ?> Banner</h2>
                     <?php }?>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                 <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                        
                        
                                    <div class="panel-heading">                                
                                                <div class="col-md-4" >
                                                    <h4><strong>Add Banner</strong></h4>

                                                     
                                                </div>
                                     </div>
                        
                                    <div class="panel-body">
                                             
                                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                
                                <form id="jvalidate_aappbanner"  method="post" role="form"  enctype="multipart/form-data" class="form-horizontal" action="lib/scripts/php/all/addbanner1.php">
                                <div class="panel-body">                                    
                                                 
                                   <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate App Banner Image Name, Please Try Another Name</span> <?php }?>
                                   <input type="hidden" class="form-control" name="cityid" value="<?php echo $_GET['CityId'];?>"/>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Image Name:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="img_name"/>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                    
                                     
                                    
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">File</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class=" btn-primary" name="filename12345" id="filename" title="Browse file"/>
                                            <span class="help-block">Image ratio 16:9</span>
                                        </div>
                                    </div>
                                    
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Heading 1:</label>  
                                        <div class="col-md-9">
                                           <textarea class="form-control" rows="5" name="heading1"></textarea>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Heading 2:</label>  
                                        <div class="col-md-9">
                                           <textarea class="form-control" rows="5" name="heading2"></textarea>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                  
                                    
                                    <div class="btn-group">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate_aappbanner.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        
                                    </div> 
                                    
                                    <div class="btn-group">
                                        <button name="savexit" value="1" class="btn btn-primary" type="submit">Save </button>
                                    </div>
                                    <div class="btn-group">
                                        <?php if(!isset($_GET['CityId']) || $_GET['CityId']=='' ){?>
                                        <a href="landingpagebanner.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                        <?php }else{?>
                                        <a href="cityblistpage.php?CityId2314=<?php echo $CityId;?>"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                        <?php }?>
                                    </div> 
                                    
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div> 
                                        
                                    </div>
                        
                        
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>     
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  


<?php	require_once("footer.php");
?>


