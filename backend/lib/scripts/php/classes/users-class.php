<?php
	class Users
	{	
		function construct()
		{
			//
		}
		
		
		function getSystemUsers($pageLimit, $setLimit)
		{
			$SQL = "SELECT * FROM `users` WHERE `user_account_disabled` = '0' ORDER BY `user_name`, `user_surname` ASC LIMIT ".$pageLimit." , ".$setLimit;
			return MySQL::query($SQL);
		}		
				
		//lucky
		function getSystemUsersForRating()
		{
			$SQL = "SELECT * FROM `users` WHERE `user_account_disabled` = '0' ORDER BY `user_name`";//.$pageLimit." , ".$setLimit;
			return MySQL::query($SQL);
		}
		
		
		function getSystemVendors()
		{
			$SQL = "SELECT * FROM `vendors` ORDER BY `vendor_name`, `vendor_surname` ASC";
			return MySQL::query($SQL);
		}
		function getSystemVendorsById($id)
		{
			$SQL = "SELECT * FROM `vendors` WHERE id= '$id'";
			return MySQL::query($SQL);
		}
		function getOrderStatus()
		{
			$SQL = "SELECT * FROM `order_status` ORDER BY `name` ASC";
			return MySQL::query($SQL);
		}
		function getOrderStatusById($id)
		{
			$SQL = "SELECT * FROM `order_status` where order_status_id = '".$id."'";
			return MySQL::query($SQL);
		}
		public function getSystemOrders()
		{
			$SQL = "SELECT order_tripoye.*, users.*,users.user_id as usertableid, tour_coupon.couponcode
			FROM order_tripoye 
    		LEFT JOIN users 
        	ON order_tripoye.customer_id = users.user_email
        	LEFT JOIN tour_coupon 
        	ON order_tripoye.coupon_applied = tour_coupon.id
        	ORDER BY order_tripoye.order_id DESC";
			$result =  MySQL::query($SQL);
			return $result;
		}
		public function getSystemordersById($orderid)
		{
			$SQL = "SELECT order_tripoye.*, users.*,users.user_id as usertableid, tour_coupon.couponcode
					FROM order_tripoye 
		    		LEFT JOIN users 
		        	ON order_tripoye.customer_id = users.user_email
		        	LEFT JOIN tour_coupon 
		        	ON order_tripoye.coupon_applied = tour_coupon.id
		        	WHERE order_tripoye.order_id = ".$orderid."";
			$result =  MySQL::query($SQL);
			return $result;
		}
		public function getSystemOrderItemByOrderId($orderId){
			$SQL = "SELECT order_item.*, tour_otheroption.optionName, tour.tourname, tour_combo.comboname as comboname
					FROM order_item 
    				LEFT JOIN tour_otheroption 
        			ON order_item.TourOptId = tour_otheroption.id
    				LEFT JOIN tour
        			ON order_item.TourId = tour.id
        			LEFT JOIN tour_combo
        			ON order_item.ComboId = tour_combo.comboid
        			WHERE order_item.order_id = ".$orderId."";
			$result =  MySQL::query($SQL);
			return $result;
		}
		public function getSystemTravelerByOrderIdOld($orderId){
			$SQL = "SELECT order_traveller_detail.*, tour_otheroption.optionName
					FROM `order_traveller_detail` 
					LEFT JOIN tour_otheroption 
        			ON order_traveller_detail.OptId = tour_otheroption.id
					WHERE 1 ANd order_id = ".$orderId." 
					GROUP BY order_traveller_detail.optId";
			$result =  MySQL::query($SQL);
			echo "<pre>";print_r($result);die;
			return $result;
		}
		public function getSystemTravelerByOrderId($orderId){
			$SQL = "SELECT * FROM `order_traveller_detail` WHERE order_id = '".$orderId."'";
			$result =  MySQL::query($SQL);
			return $result;
		}
		function getUserById($userID)
		{
			$SQL = "SELECT * FROM `users` WHERE `user_id` = '" . $userID . "'";
			return MySQL::query($SQL, true);
		}
		function getVendorById($vendorID)
		{
			$SQL = "SELECT * FROM `vendors` WHERE `vendor_id` = '" . $vendorID . "'";
			return MySQL::query($SQL, true);
		}
                ///added by 20/5/2016///
                function getUserByIdForAPI($userID)
		{
			$SQL = "SELECT user_id,user_email,user_name,user_surname,user_address,user_dob,user_mobile,user_status,latitiude,longitude,user_role FROM `users` WHERE `user_id` = '" . $userID . "'";
			return MySQL::query($SQL, true);
		}
        

        function getUserByEmailId($emailID)
		{
			$SQL = "SELECT * FROM `users` WHERE `user_email` = '" . $emailID . "'";
			return MySQL::query($SQL, true);
		}


		function getUserBySocialId($SocialId){
			$SQL = "SELECT * FROM `social_account_data` WHERE `social_id` = '".$SocialId."'";
			return MySQL::query($SQL, true);
		}


		function getVendorByEmailId($emailID)
		{
			$SQL = "SELECT * FROM `vendors` WHERE `vendor_email` = '" . $emailID . "'";
			return MySQL::query($SQL, true);
		}
        function deleteUserById($id)
		{   
                        $SQL = "DELETE FROM `users` WHERE `user_id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
		function deleteVendorById($id)
		{   
            $SQL = "DELETE FROM `vendors` WHERE `vendor_id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function getUserNameById($userID)
		{
			$SQL = "SELECT user_name , user_surname  FROM `users` WHERE `user_id` = '" . $userID . "'";
			return MySQL::query($SQL, true);
		}
                
                function getAdminRoles()
		{
			$SQL = "SELECT * FROM `admin_role` ORDER BY `id` ASC";
			return MySQL::query($SQL);
		}
                #########Country Functions=======================########
                                
                function getSystemCountry()
		{
			$SQL = "SELECT * FROM `country` ORDER BY `countryname` ASC";
			return MySQL::query($SQL);
		}
                function getSystemCountryAproved()
		{
			$SQL = "SELECT * FROM `country` where status=1 ORDER BY `countryname` ASC";
			return MySQL::query($SQL);
		}
                function getSystemCountryById($id)
		{
			$SQL = "SELECT * FROM `country` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
                function getSystemCountryByNmae($cityname)
		{
			$SQL = "SELECT * FROM `country` WHERE `countryname` = '" . $cityname . "'";
			return MySQL::query($SQL);
		}
                function deleteCountryById($id)
		{   
                        $SQL = "DELETE FROM `country` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                #########City Functions=======================########
                                
                function getSystemCity()
		{
			$SQL = "SELECT * FROM `city` ORDER BY `cityname` ASC";
			return MySQL::query($SQL);
		}
                function getSystemCityAproved()
		{
			$SQL = "SELECT * FROM `city` where status=1 ORDER BY `cityname` ASC";
			return MySQL::query($SQL);
		}
                function getSystemCityById($id)
		{
			$SQL = "SELECT * FROM `city` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
                function getCityByCountryId($id)
		{
			$SQL = "SELECT * FROM `city` WHERE `countryid` = '" . $id . "'";
			return MySQL::query($SQL);
		}
		function getCityByCountryIdAddtour($id)
		{
			$SQL = "SELECT * FROM `city` WHERE `countryid` = '" . $id . "'";
			return MySQL::query($SQL);
		}
		 function getCityByCountryIdApproved($id)
		{
			$SQL = "SELECT * FROM `city` WHERE `countryid` = '" . $id . "' AND status=1";
			return MySQL::query($SQL, true);
		}
                function getSystemCityByNmae($cityname,$countryid)
		{
			$SQL = "SELECT * FROM `city` WHERE `cityname` = '" . $cityname . "' and `countryid` = '" . $countryid . "'";
			return MySQL::query($SQL);
		}
                function deleteCityById($id)
		{   
                        $SQL = "DELETE FROM `city` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function deleteCityByCountryId($id)
		{   
                        $SQL = "DELETE FROM `city` WHERE `countryid` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function getPopulerCity()
		{
			//$SQL = "SELECT * FROM `populercity` ORDER BY `id` ASC";
                        $SQL = "SELECT a . * , b.cityname
                                    FROM `populercity` a
                                    LEFT JOIN city b ON a.`cityid` = b.id order by a.torder ASC";
			return MySQL::query($SQL);
		}
                function deletePopulerCityById($id,$cityid)
		{   
                        $SQL = "DELETE FROM `populercity` WHERE `id` = '" . $id . "'  LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function deletePCityByCityId($id,$cityid)
		{   
                        $SQL = "DELETE FROM `populercity` WHERE `cityid` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function getCityShowFP()
		{
			$SQL = "SELECT * FROM `city` where showLpage=1 ORDER BY `cityname` ASC";
			return MySQL::query($SQL);
		}
                function getFPCity()
		{
			//$SQL = "SELECT * FROM `populercity` ORDER BY `id` ASC";
                        $SQL = "SELECT a . * , b.cityname
                                    FROM `fpcity` a
                                    LEFT JOIN city b ON a.`cityid` = b.id order by a.torder ASC";
			return MySQL::query($SQL);
		}
                function deleteFPCityById($id,$cityid)
		{   
                        $SQL = "DELETE FROM `fpcity` WHERE `id` = '" . $id . "'  LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function deleteFPCityByCityId($id,$cityid)
		{   
                        $SQL = "DELETE FROM `fpcity` WHERE `cityid` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                #########Tour Category Functions=======================########
                                
                function getTourCategory()
		{
			$SQL = "SELECT tour_category.*, city.cityname FROM `tour_category` as `tour_category`  
			LEFT JOIN city ON tour_category.cityid=city.id
			ORDER BY `tourcatname` ASC";
			return MySQL::query($SQL);
		}
		function getTourBytourcatid($tourcatid)
		{
			$SQL = "SELECT * FROM `tour` WHERE `tourcatid`='".$tourcatid."'";
			return MySQL::query($SQL);
		}
                function getTourCategoryAproved()
		{
			$SQL = "SELECT * FROM `tour_category` where status=1 ORDER BY `tourcatname` ASC";
			return MySQL::query($SQL);
		}
                function getTourCategoryByNmae($cityname)
		{
			$SQL = "SELECT * FROM `tour_category` WHERE `tourcatname` = '" . $cityname . "'";
			return MySQL::query($SQL);
		}

			function getTourCategoryByName($cityname,$cityid)
		{
			$SQL = "SELECT * FROM `tour_category` WHERE `tourcatname` = '$cityname' AND cityid = '$cityid'";
			return MySQL::query($SQL);
		}		
                
                 function deleteTourCategoryById($id)
		{   
                        $SQL = "DELETE FROM `tour_category` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function deleteTourCategoryByCityId($id)
		{   
                        $SQL = "DELETE FROM `tour_category` WHERE `cityid` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
		 
                function getTourCategoryById($id)
		{
			$SQL = "SELECT * FROM `tour_category` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
                function getTourCategoryByCityId($cityid)
		{
			$SQL = "SELECT * FROM `tour_category` WHERE `cityid` = '" . $cityid . "'";
			return MySQL::query($SQL);
		}
		function getTourByCityId($cityid)
		{
			$SQL = "SELECT * FROM `tour` WHERE `cityid` = '" . $cityid . "'";
			return MySQL::query($SQL);
		}
		function getTourCategoryByCityIdAproved($cityid)
		{
			$SQL = "SELECT * FROM `tour_category` WHERE `cityid` = '" . $cityid . "' AND status=1";
			return MySQL::query($SQL);
		}
                function getTourTopSellingById($cityid)
		{
			$SQL = "SELECT a . * , b.tourcatname
                                    FROM `topselling_cat` a
                                    LEFT JOIN tour_category b ON a.`tourcatId` = b.id
                                    WHERE b.`cityid` = '" . $cityid . "' order by a.torder ASC";
			//echo $SQL = "SELECT * FROM `topselling_cat` WHERE `cityid` = '" . $cityid . "'";
			return MySQL::query($SQL);
		}
                function deleteTourTopSellingById($id,$cityid)
		{   
                        $SQL = "DELETE FROM `topselling_cat` WHERE `id` = '" . $id . "' and `cityid`= '" . $cityid . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function getTourRecommendedCatById($cityid)
		{
			$SQL = "SELECT a . * , b.tourcatname
                                    FROM `toprecommened_cat` a
                                    LEFT JOIN tour_category b ON a.`tourcatId` = b.id
                                    WHERE b.`cityid` = '" . $cityid . "' order by a.torder ASC";
			
			return MySQL::query($SQL);
		}
                function deleteTourRecommendedCatById($id,$cityid)
		{   
                        $SQL = "DELETE FROM `toprecommened_cat` WHERE `id` = '" . $id . "' and `cityid`= '" . $cityid . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function deleteTourRecommendedByCityId($id)
		{   
                        $SQL = "DELETE FROM `toprecommened_cat` WHERE `cityid` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function deleteTourTopSellingByCityId($id)
		{   
                        $SQL = "DELETE FROM `topselling_cat` WHERE `cityid` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                
                #########Transfer Category Functions=======================########
                                
                function getTransferCategory()
		{
			$SQL = "SELECT * FROM `transfer_category` ORDER BY `transferoption` ASC";
			return MySQL::query($SQL);
		}
                function getTransferCategoryByNmae($cityname)
		{
			$SQL = "SELECT * FROM `transfer_category` WHERE `transfercatname` = '" . $cityname . "'";
			return MySQL::query($SQL);
		}
                function getTransferCategoryById($id)
		{
			$SQL = "SELECT * FROM `transfer_category` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
               function deleteTransferCategoryById($id)
		{   
                        $SQL = "DELETE FROM `transfer_category` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                
                
              
                #########Pages Functions=======================########
                                
                function getSystemPages()
		{
			$SQL = "SELECT * FROM `pages` ORDER BY `pagename` ASC";
			return MySQL::query($SQL);
		}
        

        function getSystemPages2()
		{
			$SQL = "SELECT a.id, a.pagename, b.pagetype, a.pagetypeid, a.content, a.status
                                FROM `pages` a
                                LEFT JOIN pagetype b ON a.pagetypeid = b.id
                                ORDER BY a.`pagename` ASC";
			return MySQL::query($SQL);
		}
		
        function getSystemPagesByNmae($cityname)
		{
			$SQL = "SELECT * FROM `pages` WHERE `pagename` = '" . $cityname . "'";
			return MySQL::query($SQL);
		}
                function getSystemPagesById($id)
		{
			$SQL = "SELECT * FROM `pages` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
                function deletePageById($id)
		{   
                        $SQL = "DELETE FROM `pages` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function getPageTypeAproved()
		{
			$SQL = "SELECT * FROM `pagetype` where status=1 ORDER BY `pagetype` ASC";
			return MySQL::query($SQL);
		}
                /// here page type 4 means cancel policy
                function getPageCancelPolicyAproved()
		{
			$SQL = "SELECT * FROM `pages` where status=1 and pagetypeid =4 ORDER BY `pagename` ASC";
			return MySQL::query($SQL);
		}
                 /// here page type 5 means term and condition
                function getPageTermConditionAproved()
		{
			$SQL = "SELECT * FROM `pages` where status=1 and pagetypeid =5 ORDER BY `pagename` ASC";
			return MySQL::query($SQL);
		}
                
                
                #########Season Functions=======================########
                                
                function getSystemSeason()
		{
			$SQL = "SELECT * FROM `season` ORDER BY `season_name` ASC";
			return MySQL::query($SQL);
		}
		function getSystemDefaultSeason($todate)
		{
			$SQL = "SELECT * FROM season WHERE STR_TO_DATE(`todate`, '%Y-%m-%d') >= '".$todate."'
AND STR_TO_DATE(`fromdate`, '%Y-%m-%d') <= '".$todate."' AND status = 1 LIMIT 1";
			return MySQL::query($SQL,true);
		}
                
                function getSeasonByNmae($cityname)
		{
			$SQL = "SELECT * FROM `season` WHERE `season_name` = '" . $cityname . "'";
			return MySQL::query($SQL);
		}
                function getSeasonById($userID)
		{
			$SQL = "SELECT * FROM `season` WHERE `id` = '" . $userID . "'";
			return MySQL::query($SQL, true);
		}
				function getOrganizePhone()
		{
			$SQL = "SELECT * FROM `organize_phone`";
			return MySQL::query($SQL, true);
		}
                 function deleteSeasonById($id)
		{   
                        $SQL = "DELETE FROM `season` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                #########Rate fo exchange Functions=======================########
                                
                function getRateOfCurrency()
		{
			$SQL = "SELECT * FROM `rateofcurrency` ORDER BY `currencyname` ASC";
			return MySQL::query($SQL);
		}
                 function getCurrencyByNmae($cityname)
		{
			$SQL = "SELECT * FROM `rateofcurrency` WHERE `currencyname` = '" . $cityname . "'";
			return MySQL::query($SQL);
		}
                function deleteCurrencyById($id)
		{   
                        $SQL = "DELETE FROM `rateofcurrency` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                
                #########Tour Functions=======================########
                 function getTourExcludedDate($id)
		{
			$SQL = "SELECT * FROM `tour_excludeddate` WHERE `TourId` = '" . $id . "' ORDER BY `id` ASC";
			return MySQL::query($SQL);
		}
		
		function getTourIncludedDate($id)
		{
			$SQL = "SELECT * FROM `tour_includeddate` WHERE `TourId` = '" . $id . "' ORDER BY `id` ASC";
			return MySQL::query($SQL);
		}
                function deleteTourExcludedDateByID($id)
		{   
                        $SQL = "DELETE FROM `tour_excludeddate` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
		function deleteTourIncludedDateByID($id)
		{   
                        $SQL = "DELETE FROM `tour_includeddate` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function getTourOtherOption($id)
		{
			$SQL = "SELECT * FROM `tour_otheroption` WHERE `TourId` = '" . $id . "' ORDER BY `optionorder` ASC";
			return MySQL::query($SQL);
		}
		function getTourOtherOptionId($id)
		{
			$SQL = "SELECT id FROM `tour_otheroption` WHERE `TourId` = '" . $id . "' ORDER BY `optionorder` ASC";
			return MySQL::query($SQL);
		}
                function deleteTourOtherOptionInTableByID($id)
		{   
                        $SQL = "DELETE FROM `tour_otheroption` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                 function getTourByName($tourname)
		{
			$SQL = "SELECT * FROM `tour` WHERE `tourname` = '" . $tourname . "'";
			return MySQL::query($SQL);
		}
                function getTourMealIncluded ($id)
		{
			$SQL = "SELECT mealincluded  FROM `tour` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                
                function getTourBannerImageBYTourId($id)
		{
			$SQL = "SELECT * FROM `tour_bannerimage` WHERE `TourId` = '" . $id . "' ORDER BY `bannerorder` ASC";
			return MySQL::query($SQL);
		}
                function deleteTourBannerImageByID($id)
		{   
                        $SQL = "DELETE FROM `tour_bannerimage` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                 function getTourBannerImageByName($cityname,$TourId)
		{
			$SQL = "SELECT * FROM `tour_bannerimage` WHERE `bannername` = '" . $cityname . "' and `TourId` = '" . $id . "'";
			return MySQL::query($SQL);
		}
                function getTourGalleryImageByName($cityname,$TourId)
		{
			$SQL = "SELECT * FROM `tour_galleryrimage` WHERE `galleryname` = '" . $cityname . "' and `TourId` = '" . $id . "'";
			return MySQL::query($SQL);
		}
                function getTourGalleryImageBYTourId($id)
		{
			$SQL = "SELECT * FROM `tour_galleryrimage` WHERE `TourId` = '" . $id . "' ORDER BY `galleryorder` ASC";
			return MySQL::query($SQL);
		}
                 function getTourBannerMaxId($TourId)
		{
			$SQL = "SELECT *
                                FROM `tour_bannerimage`
                                WHERE `TourId` ='" . $TourId . "'
                                ORDER BY bannerorder DESC  limit 0,1";
			return MySQL::query($SQL,true);
		}
                
                function getTourBannerUpId($id,$TourId)
		{
			$SQL = "SELECT * FROM `tour_bannerimage` WHERE `bannerorder` = '" . $id . "' and `TourId` = '" . $TourId . "'";
			return MySQL::query($SQL,true);
		}
                
                function getTourBannerUpOneId($id,$TourId)
		{
	                $SQL = "SELECT * FROM `tour_bannerimage` WHERE `id`  < '" . $id . "' and `TourId` = '" . $TourId . "' ORDER BY `bannerorder` DESC limit 0,1";
			return MySQL::query($SQL,true);
		}
                
                function getTourBannerDwonOneId($id,$TourId)
		{
	               echo $SQL = "SELECT * FROM `tour_bannerimage` WHERE `id`  > '" . $id . "' and `TourId` = '" . $TourId . "' ORDER BY `bannerorder` DESC limit 0,1";
			return MySQL::query($SQL,true);
		}
                
                function deleteTourGalleryImageByID($id)
		{   
                        $SQL = "DELETE FROM `tour_galleryrimage` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function getTourTransferOption($id)
		{
			$SQL = "SELECT a . * , b.transfercatname, b.transferoption
                                FROM `tour_transferoption` a
                                LEFT JOIN transfer_category b ON a.`transfercatId` = b.id
                                WHERE a.`TourId` = '" . $id . "'
                                ORDER BY a.`tforder` ASC";
			return MySQL::query($SQL);
		}
                function deleteTourTransferOptionByID($id)
		{   
                        $SQL = "DELETE FROM `tour_transferoption` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                
                function getTourTransferIsDefaultByID($TourId)
		{   
                        $SQL = "SELECT *  FROM `tour_transferoption` WHERE `TourId` = '" . $TourId . "' AND `isdefault`=1 LIMIT 1";
			return MySQL::query($SQL,true);
		}
                
                function getSystemTour()
		{
			$SQL = "SELECT a. * , b.countryname, c.cityname, d.tourcatname
                                FROM `tour` a
                                LEFT JOIN country b ON a.`countryid` = b.id
                                LEFT JOIN city c ON a.`cityid` = c.id
                                LEFT JOIN tour_category d ON a.`tourcatid` = d.id
                                ORDER BY a.`tourname` ASC";
			return MySQL::query($SQL);
		}
                function getSystemTourIsLive()
		{
			$SQL = "SELECT a. * , b.countryname, c.cityname, d.tourcatname
                                FROM `tour` a
                                LEFT JOIN country b ON a.`countryid` = b.id
                                LEFT JOIN city c ON a.`cityid` = c.id
                                LEFT JOIN tour_category d ON a.`tourcatid` = d.id
                                WHERE a.islive=1
                                ORDER BY a.`tourname` ASC";
			return MySQL::query($SQL);
		}
                function getTourByID($id)
		{
			$SQL = "SELECT * FROM `tour` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
		function getTourRating()
		{
			$SQL = "SELECT tour_rating.*,tour.tourname,users.user_name,users.user_surname,users.user_id FROM `tour_rating`  INNER JOIN tour on tour_rating.TourId = tour.id INNER JOIN users on tour_rating.UserId = users.user_id";
			return MySQL::query($SQL);
		}
		function getTourRatingByid($id)
		{
			$SQL = "SELECT tour_rating.*,tour.tourname,users.user_name,users.user_surname,users.user_id FROM `tour_rating`  INNER JOIN tour on tour_rating.TourId = tour.id INNER JOIN users on tour_rating.UserId = users.user_id where tour_rating.id =".$id."";
			return MySQL::query($SQL,true);
		}
		
		function getComboRating()
		{
			$SQL = "SELECT combo_rating.*,tour_combo.comboname,users.user_name,users.user_surname,users.user_id FROM `combo_rating`  INNER JOIN tour_combo on combo_rating.ComboId = tour_combo.comboid INNER JOIN users on combo_rating.UserId = users.user_id";
			return MySQL::query($SQL);
		}
		function getComboRatingByid($id)
		{
			$SQL = "SELECT combo_rating.*,tour_combo.comboname,users.user_name,users.user_surname,users.user_id FROM `combo_rating`  INNER JOIN tour_combo on combo_rating.ComboId = tour_combo.comboid INNER JOIN users on combo_rating.UserId = users.user_id where combo_rating.id =".$id."";
			return MySQL::query($SQL,true);
		}

                function deleteTourPriceBYTransferOptionID($id)
		{   
                        $SQL = "DELETE FROM `tour_price` WHERE `tourTransOptionId` = '" . $id . "'";
			            return MySQL::query($SQL,true);
		}
		
                function getTourPrice($seasonId,$TourId)
		{
			$SQL = "SELECT a . * , c.transfercatname, c.transferoption,b.transfercatId 
                                FROM `tour_price` a
                                LEFT JOIN tour_transferoption b ON a.`tourTransOptionId` = b.id
                                LEFT JOIN transfer_category c ON b.`transfercatId` = c.id
                                WHERE a.subTPid=0 and a.`seasonId` = '" . $seasonId . "' and a.`TourId` = '" . $TourId . "'
                                ORDER BY a.`id` ASC";
			//echo $SQL;die;				
			return MySQL::query($SQL);
		}
		function getComboDiscount($seasonId,$ComboId)
		{
			$SQL = "SELECT * FROM `tour_combo_price` WHERE  `seasonId` = '" . $seasonId . "' and `ComboId` = '" . $ComboId . "' AND tourId = 0";
			//echo $SQL;die;				
			return MySQL::query($SQL);
		}
		
		function getTourPriceTourID($TourId)
		{
			$SQL = "SELECT a . * , c.transfercatname, c.transferoption,b.transfercatId 
                                FROM `tour_price` a
                                LEFT JOIN tour_transferoption b ON a.`tourTransOptionId` = b.id
                                LEFT JOIN transfer_category c ON b.`transfercatId` = c.id
                                WHERE a.subTPid=0  and a.`TourId` = '" . $TourId . "'
                                ORDER BY a.`TourId` ASC";
			return MySQL::query($SQL);
		}
                
                function getTourPriceById($id)
		{
			$SQL = "SELECT a . id, a.subTPid, a.TourId, a.tourTransOptionId, a.seasonId,  c.transferoption
                                FROM `tour_price` a
                                LEFT JOIN tour_transferoption b ON a.`tourTransOptionId` = b.id
                                LEFT JOIN transfer_category c ON b.`transfercatId` = c.id
                                WHERE a.`id` = '" . $id . "'
                                ORDER BY a.`id` ASC";
			return MySQL::query($SQL, true);
		}
                function deleteTourPrcieSubByID($id)
		{   
                        $SQL = "DELETE FROM `tour_price` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function getTourPriceSubById($id)
		{
			$SQL = "SELECT a . * , c.transfercatname, c.transferoption
                                FROM `tour_price` a
                                LEFT JOIN tour_transferoption b ON a.`tourTransOptionId` = b.id
                                LEFT JOIN transfer_category c ON b.`transfercatId` = c.id
                                WHERE a.`subTPid` = '" . $id . "'
                                ORDER BY a.`id` ASC";
			return MySQL::query($SQL);
		}
                function getTourPriceSubIdById($id)
		{
			$SQL = "SELECT a . * , c.transfercatname, c.transferoption
                                FROM `tour_price` a
                                LEFT JOIN tour_transferoption b ON a.`tourTransOptionId` = b.id
                                LEFT JOIN transfer_category c ON b.`transfercatId` = c.id
                                WHERE a.`subTPid` = '" . $id . "'
                                ORDER BY a.`id` DESC
                                LIMIT 0 , 1";
			return MySQL::query($SQL, true);
		}
                function getToursubTPidByID($id)
		{
			$SQL = "SELECT subTPid FROM `tour_price` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
                function getTourPriceMaxPaxByID($id,$ide)
		{
			$SQL = "SELECT MAX(maxpax) as maxpax FROM `tour_price` WHERE (`subTPid` = '" . $id . "' OR `id` = '" . $id . "' ) AND id <> '" . $ide . "'";
			return MySQL::query($SQL, true);
		}
                function getTourPriceBySeasonID($id,$TourId)
		{
			$SQL = "SELECT * FROM `tour_price` WHERE `seasonId` = '" . $id . "' and `TourId` = '" . $TourId . "'";
			return MySQL::query($SQL);
		}
                function getTourAddServicesBySeasonID($id,$TourId)
		{
			$SQL = "SELECT * FROM `tour_add_services` WHERE `seasonId` = '" . $id . "'  and `TourId` = '" . $TourId . "' AND status=1";
			return MySQL::query($SQL);
		}
                function deleteTourById($id)
		{   
                        $SQL = "DELETE FROM `tour` WHERE `id` = '" . $id . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function deleteTourOtherOptionByTourId($id)
		{   
                        $SQL = "DELETE FROM `tour_otheroption` WHERE `TourId` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function deleteTourBannerByTourId($id)
		{   
                        $SQL = "DELETE FROM `tour_bannerimage` WHERE `TourId` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function deleteTourGalleryByTourId($id)
		{   
                        $SQL = "DELETE FROM `tour_galleryrimage` WHERE `TourId` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function deleteTourTOptionByTourId($id)
		{   
                        $SQL = "DELETE FROM `tour_transferoption` WHERE `TourId` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function deleteTourPriceByTourId($id)
		{   
                        $SQL = "DELETE FROM `tour_price` WHERE `TourId` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function getTourAddServices($id,$TourId)
		{
			$SQL = "SELECT a . * , b.optionName
                                FROM `tour_add_services` a
                                LEFT JOIN `tour_otheroption` b ON a.`tourOtherOptionId` = b.id
                                WHERE a .`seasonId` = '" . $id . "' and a .`TourId` = '" . $TourId . "' AND a.status=1
                                ORDER BY a.`id` ASC";
			return MySQL::query($SQL);
		}
                
                ###############General ###############
                
                
                function getCountryNameByID($id)
		{
			$SQL = "SELECT countryname FROM `country` where `id` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function getCityNameByID($id)
		{
			$SQL = "SELECT cityname FROM `city` where `id` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                function getTCNameByID($id)
		{
			$SQL = "SELECT tourcatname FROM `tour_category` where `id` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                
                ###############General ###############
                
                
                function getLandingAppBanner()
		{
			$SQL = "SELECT * FROM `app_banner` WHERE cityid =0 ORDER BY `appborder` ASC";
			return MySQL::query($SQL);
		}
                function getCityAppBanner($id)
		{
			$SQL = "SELECT * FROM `app_banner` WHERE cityid = '" . $id . "' ORDER BY `appborder` ASC";
			return MySQL::query($SQL);
		}
                  function getAppBannerByName($tourname)
		{
			$SQL = "SELECT * FROM `app_banner` WHERE `img_name` = '" . $tourname . "'";
			return MySQL::query($SQL);
		}
                function getAppBannerById($id)
		{
			$SQL = "SELECT * FROM `app_banner` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
               function deleteAppBannerById($id)
		{   
                        $SQL = "DELETE FROM `app_banner` WHERE `id` = '" . $id . "'  LIMIT 1";
			return MySQL::query($SQL,true);
		}
                function getAppBannerTourById($id)
		{
			$SQL = "SELECT * FROM `app_banner_tour` WHERE `appbid` = '" . $id . "'";
			return MySQL::query($SQL);
		}
                function getTourNameById($id)
		{
			$SQL = "SELECT tourname FROM `tour` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                 function deleteAppBannerTourById($id)
		{   
                        $SQL = "DELETE FROM `app_banner_tour` WHERE `id` = '" . $id . "'  LIMIT 1";
			return MySQL::query($SQL,true);
		}
                
                ########################################
                #Get Coupon Management####
                function getCoupon()
		{
			$SQL = "SELECT * FROM `tour_coupon`  ORDER BY `id` ASC";
			return MySQL::query($SQL);
		}
                function getCouponCodeByName($tourname)
		{
			$SQL = "SELECT * FROM `tour_coupon` WHERE `couponcode` = '" . $tourname . "'";
			return MySQL::query($SQL);
		}
                function getCouponById($id)
		{
			$SQL = "SELECT * FROM `tour_coupon` WHERE `id` = '" . $id . "'  ORDER BY `id` ASC";
			return MySQL::query($SQL,true);
		}
                function deleteCouponTourById($id)
		{   
                        $SQL = "DELETE FROM `tour_coupon` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL,true);
		}
                #
                #########################################
                #Tour like function####
               
        function getTourLike($TourId,$UserId)
		{
			$SQL = "SELECT * FROM `tour_like` WHERE `TourId` = '" . $TourId . "'  and `UserId` = '" . $UserId . "'";
			return MySQL::query($SQL,true);
		}
		 function getTourRate($TourId,$UserId)
		{
			$SQL = "SELECT * FROM `tour_rating` WHERE `TourId` = '" . $TourId . "'  and `UserId` = '" . $UserId . "'";
			return MySQL::query($SQL,true);
		}
		function getComboRate($ComboId,$UserId)
		{
			$SQL = "SELECT * FROM `combo_rating` WHERE `ComboId` = '" . $ComboId . "'  and `UserId` = '" . $UserId . "'";
			return MySQL::query($SQL,true);
		}
		function getComboLike($ComboId,$UserId)
		{
			$SQL = "SELECT * FROM `combo_like` WHERE `ComboId` = '" . $ComboId . "'  and `UserId` = '" . $UserId . "'";
			return MySQL::query($SQL,true);
		}
             #tour function 
                function getSystemTourComboAproved()
		{
			$SQL = "SELECT * FROM `tour_combo` ORDER BY `comboid` ASC";
			return MySQL::query($SQL);
		}
                function getComboByName($tourname)
		{
			$SQL = "SELECT * FROM `tour_combo` WHERE `comboname` = '" . $tourname . "'";
			return MySQL::query($SQL);
		}
                function deleteCombo1ById($id)
		{   
                        $SQL = "DELETE FROM `tour_combo` WHERE `comboid` = '" . $id . "'";
			            MySQL::query($SQL,true);
						$SQL = "DELETE FROM `tour_combo_t` WHERE `comboid` = '" . $id . "'";
			            MySQL::query($SQL,true);
						$SQL = "DELETE FROM `tour_combo_price` WHERE `comboid` = '" . $id . "'";
			            MySQL::query($SQL,true);
		}
                function getSystemComboById($id)
		{
			$SQL = "SELECT * FROM `tour_combo` WHERE `comboid` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
                function getSystemComboTById($id)
		{
			$SQL = "SELECT * FROM `tour_combo_t` WHERE `comboid` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
		
		
		
		/////.....Lakshmikant....START
		
		          function getCityByID($id)
		{
			$SQL = "SELECT * FROM `city` WHERE `id` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
		
		
		        function getSystemTourByCatId($id)
		{  
			$SQL = "SELECT t.id,t.tourname FROM tour as t WHERE `tourcatid` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
		
		
		        function getSystemTrendingById($id)
		{  //trending_city
			$SQL = "SELECT * FROM `section_manager` WHERE `trendingid` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
		
		      function getSystemComboAproved()
		{
			$SQL = "SELECT * FROM `tour_combo`  ORDER BY `comboname` ASC";
			return MySQL::query($SQL);
		}
		
		
		         function getComboByID($id)
		{
			$SQL = "SELECT * FROM `tour_combo` WHERE `comboid` = '" . $id . "'";
			return MySQL::query($SQL, true);
		}
		
		
		       function getSystemHotSellingByCityId($id)
		{  
	    	$name="HOT_SELLING_BY_CITY"; 
			$SQL = "SELECT  c.id,c.cityname,coun.countryname FROM `city` c 
			
			LEFT JOIN `country` coun ON c.`countryid` = coun.id   
			
			WHERE c.id='" . $id . "' AND  `section_name` = '" . $name . "'  ";
                               
			return MySQL::query($SQL, true);
		}
		
		
		       function getSystemHotSellingById($id)
		{   
		    $name="HOT_SELLING_BY_CITY";
			$SQL = "SELECT * FROM `section_manager_city` WHERE `city_id` = '" . $id . "'  AND  `section_name` = '" . $name . "' ";
			return MySQL::query($SQL, true);
		}
		
		
		
		     function getSystemCollectionById($id)
		{   
		    $name="COLLECTION_BY_CITY";
			$SQL = "SELECT * FROM `section_manager_city` WHERE `city_id` = '" . $id . "'  AND  `section_name` = '" . $name . "' ";
			return MySQL::query($SQL, true);
		}
		
		
	   function getSystemCreatedSectionByCityId($id)
		{   
		    $name="CREATED_SECTION_BY_CITY";
			$SQL = "SELECT * FROM `section_manager_city` WHERE `city_id` = '" . $id . "'  AND  `section_name` = '" . $name . "' ";
			return MySQL::query($SQL, true);
		}
		
		
		
		
		 function getSystemComboCityById($id)
		{   
		    $name="EXCLUSIVE_COMBO_BY_CITY";
			$SQL = "SELECT * FROM `section_manager_city` WHERE `city_id` = '" . $id . "'  AND  `section_name` = '" . $name . "' ";
			return MySQL::query($SQL, true);
		}  
		
		
		
		      function getSystemHotSellingCity()
		{  
		     $name="HOT_SELLING_BY_CITY";
		     $SQL = "SELECT   smc.section_id,c.id,c.cityname,coun.countryname
                                FROM `section_manager_city` smc
                                LEFT JOIN `city` c ON smc.`city_id` = c.id
								
								 LEFT JOIN `country` coun ON c.`countryid` = coun.id
                                
								WHERE smc.section_name = '" . $name . "'
								
                                ORDER BY c.`id` ASC";
			return MySQL::query($SQL);
		}
		
		
		
		
		  function getSystemCreatedSectionCity()
		{  
		     $name="CREATED_SECTION_BY_CITY";
		     $SQL = "SELECT  DISTINCT c.id,c.cityname,coun.countryname
                                FROM `section_manager_city` smc
                                LEFT JOIN `city` c ON smc.`city_id` = c.id
								
								 LEFT JOIN `country` coun ON c.`countryid` = coun.id
                                
								WHERE smc.section_name = '" . $name . "'
								
                                ORDER BY c.`id` ASC";
			return MySQL::query($SQL);
		}
		
		
		
		     function getSystemCreatedTourByCatId($cat_id,$city_id)
		{   
		    $name="CREATED_SECTION_TOUR_BY_CITY";
			$SQL = "SELECT * FROM `created_section_tour` WHERE `city_id` = '" . $city_id . "' AND  `tour_cat_id` = '" . $cat_id . "' AND  `section_name` = '" . $name . "' ";
			return MySQL::query($SQL, true);
		}
		
		
		
		
		      function getSystemCollectionCity()
		{  
		     $name="COLLECTION_BY_CITY";
		     $SQL = "SELECT  smc.section_id,c.id,c.cityname,coun.countryname
                                FROM `section_manager_city` smc
                                LEFT JOIN `city` c ON smc.`city_id` = c.id
								
								 LEFT JOIN `country` coun ON c.`countryid` = coun.id
                                
								WHERE smc.section_name = '" . $name . "'
								
                                ORDER BY c.`id` ASC";
			return MySQL::query($SQL);
		}
		
		
		
		
		      function getSystemComboCity()
		{  
		 $name="EXCLUSIVE_COMBO_BY_CITY";
		   $SQL = "SELECT  smc.section_id,c.id,c.cityname,coun.countryname
                                FROM `section_manager_city` smc
                                LEFT JOIN `city` c ON smc.`city_id` = c.id
								
								 LEFT JOIN `country` coun ON c.`countryid` = coun.id
                                WHERE smc.section_name = '" . $name . "'
                                ORDER BY c.`id` ASC";
			return MySQL::query($SQL);
		}
		
		
		
		      function getSystemNewsLetter()
		{  
		     $SQL = "SELECT * FROM `newsletter` ";
		                  
			return MySQL::query($SQL);
		}
		
		
		
		     function deleteNewslettter($newsletter_id)
		{   
		       
                $SQL = "DELETE FROM `newsletter` WHERE `newsletter_id` = '" . $newsletter_id . "'  LIMIT 1";
			return MySQL::query($SQL,true);
		}
		
		
		
		
		       function deleteHoSellingByCityId($id)
		{   
		       $name="HOT_SELLING_BY_CITY";
                $SQL = "DELETE FROM `section_manager_city` WHERE `city_id` = '" . $id . "' AND  `section_name` = '" . $name . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
		
		
		      function deleteCreatedSectionByCity($id)
		{   
		       $name="CREATED_SECTION_BY_CITY";
                $SQL = "DELETE FROM `section_manager_city` WHERE `city_id` = '" . $id . "' AND  `section_name` = '" . $name . "' LIMIT 1";
			return MySQL::query($SQL,true);
		}
		
		
		      function deleteCreatedSectionMultipleTourByCity($id)
		{   
		       
                $SQL = "DELETE FROM `created_section_tour` WHERE `city_id` = '" . $id . "' ";
			return MySQL::query($SQL,true);
		}
		
		
		     function deleteCreatedSectionTourCategoryANDRelatedTourCity($id)
		{   
		     
                $SQL = "DELETE FROM `created_section_tour` WHERE `tour_cat_id` = '" . $id . "' ";
			    return MySQL::query($SQL,true);
		}
		
		
		
		
		     function deleteCollectionByCityId($id)
		{   
		       $name="COLLECTION_BY_CITY";
                $SQL = "DELETE FROM `section_manager_city` WHERE `city_id` = '" . $id . "' AND  `section_name` = '" . $name . "' LIMIT 1";
			   return MySQL::query($SQL,true);
		}
		
		
		
		      function deleteComboByCityId($id)
		{   
		      $name="EXCLUSIVE_COMBO_BY_CITY";
             $SQL = "DELETE FROM `section_manager_city` WHERE `city_id` = '" . $id . "'  AND  `section_name` = '" . $name . "'  LIMIT 1";
			return MySQL::query($SQL,true);
		}
		
		
		

		function displayPaginationBelow($per_page,$page,$tablename){
       $page_url="?";
        $SQL = "SELECT COUNT(*) as totalCount FROM ".$tablename;
        $rec =  MySQL::query($SQL);
        $total = $rec[0]['totalCount'];
        $adjacents = "2"; 

        $page = ($page == 0 ? 1 : $page);  
        $start = ($page - 1) * $per_page;                               
        
        $prev = $page - 1;                          
        $next = $page + 1;
        $setLastpage = ceil($total/$per_page);
        $lpm1 = $setLastpage - 1;
        
        $setPaginate = "";
        if($setLastpage > 1)
        {   
            $setPaginate .= "<ul class='setPaginate'>";
                    $setPaginate .= "<li class='setPage'>Page $page of $setLastpage</li>";
            if ($setLastpage < 7 + ($adjacents * 2))
            {   
                for ($counter = 1; $counter <= $setLastpage; $counter++)
                {
                    if ($counter == $page)
                        $setPaginate.= "<li><a class='current_page'>$counter</a></li>";
                    else
                        $setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";                  
                }
            }
            elseif($setLastpage > 5 + ($adjacents * 2))
            {
                if($page < 1 + ($adjacents * 2))        
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        if ($counter == $page)
                            $setPaginate.= "<li><a class='current_page'>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";                  
                    }
                    $setPaginate.= "<li class='dot'>...</li>";
                    $setPaginate.= "<li><a href='{$page_url}page=$lpm1'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}page=$setLastpage'>$setLastpage</a></li>";      
                }
                elseif($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                {
                    $setPaginate.= "<li><a href='{$page_url}page=1'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}page=2'>2</a></li>";
                    $setPaginate.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                    {
                        if ($counter == $page)
                            $setPaginate.= "<li><a class='current_page'>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";                  
                    }
                    $setPaginate.= "<li class='dot'>..</li>";
                    $setPaginate.= "<li><a href='{$page_url}page=$lpm1'>$lpm1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}page=$setLastpage'>$setLastpage</a></li>";      
                }
                else
                {
                    $setPaginate.= "<li><a href='{$page_url}page=1'>1</a></li>";
                    $setPaginate.= "<li><a href='{$page_url}page=2'>2</a></li>";
                    $setPaginate.= "<li class='dot'>..</li>";
                    for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++)
                    {
                        if ($counter == $page)
                            $setPaginate.= "<li><a class='current_page'>$counter</a></li>";
                        else
                            $setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";                  
                    }
                }
            }
            
            if ($page < $counter - 1){ 
                $setPaginate.= "<li><a href='{$page_url}page=$next'>Next</a></li>";
                $setPaginate.= "<li><a href='{$page_url}page=$setLastpage'>Last</a></li>";
            }else{
                $setPaginate.= "<li><a class='current_page'>Next</a></li>";
                $setPaginate.= "<li><a class='current_page'>Last</a></li>";
            }

            $setPaginate.= "</ul>\n";       
        }
        //echo $setLastpage;die;
        return $setPaginate;
    } 
		
		
		
                
                #
                ###############END OF THE CLASS#######
	}
?>