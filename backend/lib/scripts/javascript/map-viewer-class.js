/* ---------- Pure Google Maps Viewer (Not KML Depentant) ---------- */
/* If multiple elements are found with the constructor selector then only the first in the DOM is used */
/* ----------  ---------- */

function MapView(selector)
{
	/* ---------- Variables ---------- */
	this.map = null;
	this.client = "all";
	this.markers = new Array(new Array());
	this.lines = new Array(new Array());
	this.polygons = new Array(new Array());
	this.settings = 
		{
			center: new google.maps.LatLng(29.2, 0), 
			zoom: 2, 
			mapTypeId: google.maps.MapTypeId.ROADMAP, 
			panControl: false, 
			streetViewControl: false, 
			scaleControl: false, 
			scaleControlOptions:
			{
				position: google.maps.ControlPosition.TOP_LEFT, 
				style: google.maps.ScaleControlStyle.DEFAULT
			}, 
			mapTypeControlOptions: 
			{
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE],//
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.TOP_RIGHT
			}, 
			zoomControlOptions:
			{
				style: google.maps.ZoomControlStyle.DEFAULT,
				position: google.maps.ControlPosition.LEFT
			}
		};
	this.selector = selector;
	
	/* ---------- Used for clickHandler to stop double click triggering click ---------- */
	this.doubeClicked = false;
	this.clickEvent = null;
		
		
	/* ---------- Methods ---------- */
	this.init = function()
	{
		this.map = new google.maps.Map($(this.selector).get(0), this.settings);
		
		if (this.selector == "#gMapWrapper")
		{
			google.maps.event.addListener(this.map, 'zoom_changed', function()
			{
				if (view.map.zoom < 2)
				{
					view.map.setZoom(2);
				}
				else if (view.map.zoom > 14)
				{
					view.map.setZoom(14);
				}
			});
		}
	}
	
	this.updateSettings = function(key, value)
	{
		this.settings[key] = value;
	}
	
	this.clearMarkers = function(layerID)
	{
		var markers = this.markers[layerID];
		
		if (markers)
		{
			this.hideMarkers(layerID);
			markers.length = 0;
		}
	}
	
	this.hideMarkers = function(layerID)
	{
		var markers = this.markers[layerID];
		
		if (markers)
		{
			for (i in markers)
			{
				markers[i].setMap(null);
			}
		}
	}
	
	this.showMarkers = function(layerID)
	{
		var markers = this.markers[layerID];
		
		if (markers)
		{
			for (i in markers)
			{
				markers[i].setMap(this.map);
			}
		}
	}
	
	this.clearLines = function(layerID)
	{
		var lines = this.lines[layerID];
		
		if (lines)
		{
			this.hideLines(layerID);
			lines.length = 0;
		}
	}
	
	this.hideLines = function(layerID)
	{
		var lines = this.lines[layerID];
		
		if (lines)
		{
			for (i in lines)
			{
				lines[i].setMap(null);
			}
		}
	}
	
	this.hidePolygons = function(layerID)
	{
		var polygons = this.polygons[layerID];
		
		if (polygons)
		{
			for (i in polygons)
			{
				polygons[i].setMap(null);
			}
		}
		
		$("#gMapLoader").css("visibility", "hidden");
	}
	
	this.showLines = function(layerID)
	{
		var lines = this.lines[layerID];
		
		if (lines)
		{
			for (i in lines)
			{
				lines[i].setMap(this.map);
			}
		}
	}
	
	this.showPolygons = function(layerID)
	{
		var polygons = this.polygons[layerID];
		
		if (polygons)
		{
			for (i in polygons)
			{
				polygons[i].setMap(this.map);
			}
		}
		
		$("#gMapLoader").css("visibility", "hidden");
	}
	
	this.zoomToMarkers = function(layerID)
	{
		var markers = this.markers[layerID];
		var bounds = new google.maps.LatLngBounds();
		
		if (markers)
		{
			for (i in markers)
			{
				bounds.extend(markers[i].position);
			}
			
		}
		
		this.map.fitBounds(bounds);
	}
	
	this.fetchPoiLayer = function(client, type, id)
	{
		this.client = client;
		$("#gMapLoader").css("visibility", "visible");
		
		var markers = new Array();
		this.markers[0] = markers;
		
		$.ajax(
		{
			type: "POST", 
			url: "../admin/lib/scripts/php/fetchPoiData.php", 
			data: 
			{
				client: client, 
				type: type, 
				id: id
			}, 
			success: function(response)
			{
				var data = JSON.parse(response);
				var map = this.map;
				
				for (i in data)
				{
					var poi = data[i];
					
					if (client == "omv")
					{
						poi.risk_rating = poi.rating;
					}
					
					switch (poi.risk_rating)
					{
						case "1":
							colour = "82BC00";
							break;
						case "2":
							colour = "00833D";
							break;
						case "3":
							colour = "F4DC00";
							break;
						case "4":
							colour = "F68026";
							break;
						case "5":
							colour = "E31C23";
							break;
						default:
							colour = "666666";
							break;
					}
					
					var icon = new google.maps.MarkerImage("http://www.drum-cussac.info/images/map-icons/" + colour + "/" + poi.icon + ".png",
						new google.maps.Size(32, 37),
						new google.maps.Point(0, 0),
						new google.maps.Point(16, 37));
					
					var shadow = new google.maps.MarkerImage("http://www.drum-cussac.info/images/map-icons/shadow.png",
						new google.maps.Size(54, 37),
						new google.maps.Point(0, 0),
						new google.maps.Point(16, 37));
						
					var shape = {
						coord: [29,0,30,1,31,2,31,3,31,4,31,5,31,6,31,7,31,8,31,9,31,10,31,11,31,12,31,13,31,14,31,15,31,16,31,17,31,18,31,19,31,20,31,21,31,22,31,23,31,24,31,25,31,26,31,27,31,28,31,29,30,30,29,31,23,32,22,33,21,34,20,35,19,36,12,36,11,35,10,34,9,33,8,32,2,31,1,30,0,29,0,28,0,27,0,26,0,25,0,24,0,23,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,0,5,0,4,0,3,0,2,1,1,2,0,29,0],
						type: "poly"
					};

					var marker = new google.maps.Marker(
					{
						position: new google.maps.LatLng(poi.lat, poi.lng),
						map: map, 
						icon: icon, 
						shadow: shadow, 
						shape: shape,
						title: poi.title, 
						zIndex: (parseInt(poi.id) * 1000) + parseInt(poi.assign_id)
					});
					
					google.maps.event.addListener(marker, 'dblclick',  function(event)
					{
						doubleClicked = true;
					});
					
					google.maps.event.addListener(marker, "click", function(event)
					{
						doubleClicked = false;
						clickEvent = event;
						setTimeout("view.handleMarkerClick(" + this.zIndex + ")", 200);
					});
					
					markers.push(marker);
				}
			}, 
			complete: function()
			{
                            //setTimeout(view.showMarkers(0), 2000);
                            $("#gMapLoader").css("visibility", "hidden");
			}
		});
	}
	
	this.fetchGeofenceLayer = function(client, type, id)
	{
		this.client = client;
		
		$("#gMapLoader").css("visibility", "visible");
		var polygons = new Array();
		this.polygons[0] = polygons;
				
		$.ajax(
		{
			type: "POST", 
			url: "../admin/lib/scripts/php/fetchGeofenceData.php", 
			data:
			{
				client: client, 
				type: type, 
				id: id
			}, 
			success: function(response)
			{
				var data = JSON.parse(response);
				var map = this.map;
				
				for (i in data)
				{
					var geofence = data[i];
					var paths = new Array();
					
					for (j in geofence.p)
					{
						var path = geofence.p[j];
						var coords = new Array();
						
						for (k in path)
						{
							var point = path[k];
							coords.push(new google.maps.LatLng(point.y, point.x));
						}
						
						paths.push(coords);
					}
					
					var polygon = new google.maps.Polygon(
					{
						paths: paths,
						strokeColor: "#000000",
						strokeOpacity: 0.8,
						strokeWeight: 0.5,
						fillColor: geofence.c,
						fillOpacity: 0.5, 
						zIndex: geofence.id
					});
					
					google.maps.event.addListener(polygon, 'dblclick',  function(event)
					{
						doubleClicked = true;
					});
					
					google.maps.event.addListener(polygon, "click", function(event)
					{
						doubleClicked = false;
						clickEvent = event;
						setTimeout("view.handleClick(" + this.zIndex + ")", 200);
					});
					
					polygons.push(polygon);
				}
			}, 
			complete: function()
			{
				if (autoShow === true)
				{
					view.showPolygons(0);
				}
				else
				{
					$("#gMapLoader").css("visibility", "hidden");
				}
			}
		});
	}
	
	/* ---------- Click Handler Methods ---------- */
	this.handleClick = function(id)
	{
		if (doubleClicked === false)
		{
			page = (this.client == "all") ? "country-report.php" : "omv-report.php";
			location.href = page + "?countryid=" + id;
		}
	}
	
	this.handleMarkerClick = function(id)
	{
		if (doubleClicked === false)
		{
			alertID = Math.floor(id / 1000);
			
			full = id.toString();			
			countryID = full.substr(full.length - 3);
			
			while (countryID.substr(0, 1) == '0' && countryID.length > 1)
			{
				countryID = countryID.substr(1, 9999);
			}
			
			page = (this.client == "all") ? "country-alert.php" : "omv-alert.php";
			location.href = page + "?countryid=" + countryID + "&alert=" + alertID;
		}
	}
	
	/* ---------- Test Methods ---------- */
}