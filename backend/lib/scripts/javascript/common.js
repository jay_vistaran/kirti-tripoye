function showFilter(val)
{
	$("#"+val).slideToggle();	
}

jQuery.fn.fadeToggle = function(speed, easing, callback) {
   return this.animate({opacity: 'toggle'}, speed, easing, callback);
}; 

function check_user_access(column,link)
{
	$.post('lib/scripts/php/check_user_access.php',{column:column},function(data){

		if(data == 'true')
		{
			location.href=link
		}
		else
		{
			$.prompt(data);
		}
	})	
}

function call_cascade(aCompany,aCase){
	$.post('lib/modules/Cases/includes/popup-call-cascade.php',{id_company:aCompany,id_case:aCase, idCase:aCase},function(data){
		$.prompt(data,{prefix:'callcascade'});
	})	
}

function PrintArray(array)
{
	for (var i in array)
	{
		document.write(array[i] + "<br />");	
	}
}

function QueryString(FullURL)
{
	if (FullURL.indexOf("?") > -1)
	{
		QueryStr = FullURL.split("?");
		VariableArray = QueryStr[1].split("&");
		
		Variables = new Array();
		for (var ArrayObjects in VariableArray)
		{
			Vars = VariableArray[ArrayObjects].split("=");
			Variables.push(Vars);
		}
		return Variables;
		// Returns the format Variables[VarName][VarData]
	}
	else
	{
		return false;
	}
}

function GetQueryVariable(VarName)
{
	var VarArray = QueryString(document.URL);

	for (var Count = 0; Count < VarArray.length; Count++)
	{
		if (VarArray[Count][0] == VarName)
		{
			return VarArray[Count][1];	
		}
	}
	
	return false;
}


function InputNumber(event)
{
	if ( window.event ) // IE
	{
		keynum = event.keyCode;
	}
	else if ( event.which ) // Netscape/Firefox/Opera
	{
		keynum = event.which;
	}
	
	if (keynum != undefined && keynum != 8)
	{
		if ((keynum < 48 || keynum > 57) && keynum != 45 && keynum != 46)
		{
			return false;	
		}
		else
		{
			return true;	
		}
	}
}

function view_admin()
{
	
	function mycallbackfunc(v,m,f){
		if(f.pin == "1978"){
			location.href='landing.php?sec=main-navigation&pg=admin'
		}
	}
	
	function focusInput(){
		$('#pin').focus();	
	}
	
	var str = ''
	str += '<h3>Admin Authentication</h3>'
	str += '<table border="0" class="tableForLayout" cellpadding="0" cellspacing="0">'
	str += '<tr>'
	str += '<td class="label" style="width:88px;">'
	str += 'Pin Number:'
	str += '</td>'
	str += '<td>'
	str += '<input name="pin" id="pin" type="password" style="width:100px;" />'
	str += '</td>'
	str += '</tr>'
	str += '</table><br clear="all"/>'
	$.prompt(str,{ 
			 callback: mycallbackfunc, 
			 prefix:'admin_pin',
			 loaded:focusInput
	})
}

function pageLoader(){
	$('body').append('<div class="pageLoading">Loading...</div>');	
}

function destroyLoader(){
	$('.pageLoading').remove();	
}

function addCompany(){

	var txt = 'Enter Account Name:<br /><input type="text" id="name" name="name" value="" />';

	function mycallbackform(v,m,f){
		  if(v == true){
			  var n = f.name;
				$.post("lib/modules/companies/scripts/php/check-company-name.php", {name:n},
					function(data){
						if(data == 0){
							location.href='landing.php?sec=companies&pg=companies-details&str='+n;	
						}else{
							location.href='landing.php?sec=companies&pg=view-companies&str='+n;	
						}
					});	
		  }else{
			jQuery.prompt.close();  
		  }
	}
	
	$.prompt(txt,{
		  callback: mycallbackform,
		  buttons: { Ok:true, Cancel:false }
	});
	
}
/****FROM VALIDATION***/
function validateForm(y){
	var x = 0;
	$(".requiredField").each(function()
	{
		// -1 for combo boxes
		if(this.value == "" || this.value == "-1")
		{
			$(this).css('border-color','red');
			x = 1;
		} 
		else 
		{
			$(this).css('border-color','#666666');
		}
	});
	if(x === 1)
	{
		//$.prompt('Please enter all required fields marked with an *');
		return false;
	}
	x = "document." + y + ".submit()";
	eval(x);
}

function validateFormNoSubmit(formName)
{
	var valid = true;
	
	$("form[name='" + formName + "'] .requiredField").each(function()
	{
		if($(this).val() == "")
		{
			$(this).css('border-color','red');
			valid = false;
		} 
		else 
		{
			$(this).css('border-color','#666666');
		}
	});

	return valid;
}

/****FROM VALIDATION***/
function validatePromptForm(formname, f)
{
	var x = 0;
	$('form[name='+formname+'] .requiredField').each(function()
	{																								   
		if (f[String($(this).attr("name"))] == "" || f[String($(this).attr("name"))] == "-1")
		{
			$(this).css('border','1px solid red');
			x = 1;
		} 
		else 
		{
			$(this).css('border',' 1px solid #666666');
		}
	});
		
	if (x === 1)
	{
		return false;
	}
	else
	{
		return true;
	}
	
}
function validateInput(inputData){
	var x = 0;
	// -1 for combo boxes
	if(this.value == "" || this.value == "-1")
	{
		x = 1;
	} 
	if(x === 1)
	{
		//$.prompt('Please enter all required fields marked with an *');
		return false;
	}
	return true;
}

function setnewfield(x, input, table, tableJoin, table2){
	var m = x.value;
	$("#"+input).autocomplete("lib/scripts/php/auto-complete.php?table="+ table + "&tableJoin=" + tableJoin + "&table2=" + table2 + "&q2=" + m);
}

function copyAddress(){
	var x = "";
	x = x + $("#addressLine1").val() + "\n";
	x = x + $("#addressLine2").val() + "\n";
	x = x + $("#addressLine3").val() + "\n";
	x = x + $("#country").val() + "\n";
	x = x + $("#region").val() + "\n";
	x = x + $("#town").val() + "\n";
	x = x + $("#postcode").val() + "\n";
	document.getElementById('holdtext').innerText = x;
	Copied = document.getElementById('holdtext').createTextRange();
	Copied.execCommand("Copy");
}

function addContact(){
	
	function checkContactAction(v,m,f){
	
		var clicked = v;
		if(v == "Next"){
			var action = f.company;
			if(action == "1"){
				location.href='landing.php?sec=companies&pg=view-companies';
			} else if(action == "2"){
				//location.href='landing.php?sec=companies&pg=companies-details';
				jQuery.prompt.close();
				addCompany();
			} else if(action == "3"){
				location.href='landing.php?sec=companies&pg=contact-details&company=1&office=2';
			} else {
				jQuery.prompt.close();			
			}
		} else {
			jQuery.prompt.close();	
		}
	
	}
	
	jQuery.post("lib/modules/contacts/includes/popup-add-contact.php", function(data){
		$.prompt(data, {
			//prefix:'addcontact',
			buttons: { Cancel: 'Cancel', Next: 'Next' },
			callback: checkContactAction
			
		});																		   
	})
	
}

function selectAll(clicked,what)
{
	var y = $(clicked).attr("checked");
	if(y == true){
		$("."+what).each(function(){
			$(this).attr("checked",true);						  
		});
	} else {
		$("."+what).each(function(){
			$(this).attr("checked",false);						  
		});
	}
}

/*-------------TASKS-------------*/
function addTaskPop(id){
	
	function checkTask(v,m,f){
		
		if(v){
		
			var context = $('#hiddenContext').val();
			var taskFor = f.hiddenID;
			var status = f.taskStatus;
			var type = f.taskType;
			var managedBy = f.managedBy;
			var name = f.taskName;
			var due = f.dueBy;
			var allocate = f.taskFor;
			var note = f.taskNotes;
			var taskID = f.hiddenTask;
			
			var error = false;
			
			$('#taskType').css('border','1px solid #666666');
			$('#managedBy').css('border','1px solid #666666');
			$('#taskName').css('border','1px solid #666666');
			$('#dueBy').css('border','1px solid #666666');
			$('#taskFor').css('border','1px solid #666666');
			$('#taskNotes').css('border','1px solid #666666');
			
			if(type == "" ){
				$('#taskType').css('border','1px solid red');
				error = true;
			}
			
			if(managedBy == "" ){
				$('#managedBy').css('border','1px solid red');
				error = true;
			}
			
			if(name == "" ){
				$('#taskName').css('border','1px solid red');
				error = true;
			}
			
			if(due == "" ){
				$('#dueBy').css('border','1px solid red');
				error = true;
			}
			
			if(taskFor == "" ){
				$('#taskFor').css('border','1px solid red');
				error = true;
			}
			
			if(note == "" ){
				$('#taskNotes').css('border','1px solid red');
				error = true;
			}
			
			if(error){
					
			}else{
		
				$.post("lib/modules/clients/scripts/php/add-task.php", {taskID:taskID, taskName:name, taskNotes:note, taskStatus:status, taskType:type, managedBy:managedBy, taskDue:due, contactID:taskFor, context:context, taskFor:allocate}, 
				function(data){
					if(data != ""){
						var qwe = location.href;
						qwe = qwe.replace(/&task=1/,"");
						location.href = qwe;
					}else{
					}
					return false;
				});
				
			}
		
		}else{
			jQuery.prompt.close();		
		}
	
	}
	
	var context = jQuery('#hiddenContext').val();
	jQuery.post("lib/modules/clients/includes/popup-add-task.php", {id:id,context:context}, function(data){
		$.prompt(data, {
			prefix: 'widescreen',
			top: '13%',
			buttons: {'Add Task':true, Cancel:false},
			submit: checkTask
		});	
	})	
	
}

function editTask(task){
	
	function updateTask(v,m,f){
		
		var context = $('#hiddenContext').val();
		var taskFor = f.hiddenID;
		var status = f.taskStatus;
		var type = f.taskType;
		var managedBy = f.managedBy;
		var name = f.taskName;
		var due = f.dueBy;
		var allocate = f.taskFor;
		var note = f.taskNotes;
		var taskID = f.hiddenTask;
		
		if(v){
			$.post("lib/modules/clients/scripts/php/add-task.php", {taskID:taskID, taskName:name, taskNotes:note, taskStatus:status, taskType:type, managedBy:managedBy, taskDue:due, contactID:taskFor, context:context, taskFor:allocate}, 
			function(data){
				if(data != ""){
					var qwe = location.href;
					qwe = qwe.replace(/&task=1/,"");
					location.href = qwe;
				}else{
				}
				return false;
			});
			}else{
			jQuery.prompt.close();		
		}
	
	}
	
	jQuery.post("lib/modules/clients/includes/popup-add-task.php", {ref:task}, function(data){
		$.prompt(data, {
			prefix: 'widescreen',
			top: '13%',
			buttons: {'Update Task':'Update Task', Cancel: false},
			callback: updateTask
		});																		   
	})	
}



function taskActions(){

	function checkAction(v,m,f){
		var toDelete = "";
		var action = f.action;
		if(action == "delete"){
			$(".chk").each(function(){
				x = $(this).attr("checked");
				b = $(this).val();
				if(x == true){
					toDelete = toDelete + b + ",";
				}						  
			});
			$.post("lib/modules/clients/scripts/php/delete-tasks.php", {strDelete:toDelete}, 
			function(data){
				if(data != ""){
					location.reload(true);
				}
			});	
		} else if(action == "open") {
			$(".chk").each(function(){
				x = $(this).attr("checked");
				b = $(this).val();
				if(x == true){
					toDelete = toDelete + b + ",";
				}						  
			});
			$.post("lib/modules/clients/scripts/php/open-tasks.php", {strDelete:toDelete}, 
			function(data){
				if(data != ""){
					location.reload(true);
				}
			});	
		} else if(action == "close") {
			$(".chk").each(function(){
				x = $(this).attr("checked");
				b = $(this).val();
				if(x == true){
					toDelete = toDelete + b + ",";
				}						  
			});
			$.post("lib/modules/clients/scripts/php/close-tasks.php", {strDelete:toDelete}, 
			function(data){
				if(data != ""){
					location.reload(true);
				}
			});	
		} else {
			//$.prompt("Functionality TBC");
		}
	}
	
		var toDelete = '';
		$(".chk").each(function(){
			x = $(this).attr("checked");
			b = $(this).val();
			if(x == true){
				toDelete = toDelete + b + ",";
			}						  
		});
		if(toDelete == ''){
			$.prompt("<h3 class='warningHeader'>Warning</h3><p>You must select at least one task to action.</p>");
		} else {
			$.post("lib/modules/clients/includes/actions-tasks.php", function(data){
				$.prompt(data, {
					callback: checkAction,
					buttons: {'Cancel':false, 'Ok':true}
				});
			})
		}
	
}

//*****MESSAGE CENTER******//
function selectLetter(x){
	$.post("lib/modules/message-center/scripts/php/get-letter.php", { id:x },
		function(data){
			$('#headerImage').attr("src","images/message-center/header/"+data.header)
			$('#message-office-address').html(data.address);
			$('#message-footer').html(data.footer);
	}, "json");
}

function selectTemplate(x){
	$.post("lib/modules/message-center/scripts/php/get-template.php", { id:x },
		function(data){
			$('iframe').contents().find('#tinymce').html(data.contents);
	}, "json");
}

function getContents(type){
	var txt = 'Please enter a '+ type +' name:<br /><input type="text" id="name" name="name" value="" style="border:1px solid #999999;width:150px;" /><span id="nameError"></span>';
  	
	if(type == 'template'){
		$("#letterForm").attr("action","lib/modules/message-center/scripts/php/save-as-template.php");
		$("#letterForm").attr("target","_self"); 
	}else{
		$("#letterForm").attr("action","lib/modules/message-center/scripts/php/save-as-draft.php"); 
		$("#letterForm").attr("target","_self"); 
	}
	
	function mycallbackform(v,m,f){
		if(v == true){
			//$.prompt(f.name);
			$('#hiddenName').val(f.name);
			document.getElementById('letterForm').submit();
		}
	}
	
	function mysubmitfunc(v,m,f){
		if(v == true){
			an = m.children('#name');
			if(f.name == ""){
				an.css("border","solid #ff0000 1px");
				$("#nameError").html('&nbsp;&nbsp;<span style="color:#ff0000;">*Required field</span>')
				return false;
			}
			return true;
		} else {
			jQuery.prompt.close();
		}
	}
	
	$.prompt(txt,{
		  callback: mycallbackform,
		  submit: mysubmitfunc,
		  buttons: { Ok:true, Cancel:false }
	});
	//document.getElementById('templateForm').submit();
}

function sendEmail(){
	//VALIDATE FIELDS FIRST
  var from = $('#from').val();
  var to = $('#to').val();
	var subject = $('#subject').val();
  
  if ( !from ) {
    $.prompt('Please enter a valid email in the from field!', {callback:function(){$('#from_annoninput input').focus();}});
  } else if ( !to ) {
    $.prompt('Please enter a valid email in the to field!', {callback:function(){$('#to_annoninput input').focus();}});
  } else if ( subject == "" ) {
		$.prompt('Please enter a subject!', {callback:function(){$('#subject').focus();}});
	} else {
    //SEND EMAIL
    $("#letterForm").attr("action","lib/modules/message-center/scripts/php/send-email.php"); 
    $("#letterForm").attr("target","_self");
    document.getElementById('letterForm').submit();
  }
  
  return(false);
}

function previewAsEmail(){

	var style = $('#emailStyle').val();
	switch(style){
		
		case "1":
			$("#letterForm").attr("action","lib/modules/message-center/scripts/php/email-gen.php"); 
		break;
		
		case "2":
			$("#letterForm").attr("action","lib/modules/message-center/scripts/php/newsletter-gen.php"); 
		break;
		 
		case "3":
			$("#letterForm").attr("action","lib/modules/message-center/scripts/php/promo-gen.php"); 
		break;
		default:
			$("#letterForm").attr("action","lib/modules/message-center/scripts/php/email-gen.php"); 
		break;

	}	
	$("#letterForm").attr("target","_blank");
	document.getElementById('letterForm').submit();

}

function previewAsPDF(){

	$("#letterForm").attr("action","lib/modules/message-center/scripts/php/pdf-gen.php"); 
	$("#letterForm").attr("target","_blank");
	document.getElementById('letterForm').submit();

}

function previewAsPDF2(){

	$("#letterForm").attr("action","lib/modules/message-center/scripts/php/pdf-gen-v2.php"); 
	$("#letterForm").attr("target","_blank");
	document.getElementById('letterForm').submit();

}

function savePDF(x){

	var txt = 'Please enter a name for the letter:<br /><input type="text" id="name" name="name" value="" style="border:1px solid #999999;width:150px;" /><span id="nameError"></span>';

	$("#letterForm").attr("action","lib/modules/message-center/scripts/php/pdf-gen-v2.php?save=1"); 
	$("#letterForm").attr("target","_blank");
	
	function mycallbackform(v,m,f){
		if(v == true){
			//$.prompt(f.name);
			$('#hiddenName').val(f.name);
			document.getElementById('letterForm').submit();
			
			setTimeout(function(){ 
			
				if(x == 1){
					var company = $("#companyID").val();
					var office = $("#officeID").val();
					var contact = $("#contactID").val();
					location.href='landing.php?sec=companies&pg=contact-details&company='+company+'&office='+office+'&contact='+contact+'&doc='+f.name;
				}
			
			}, 2000);

			
			

		}
	}
	
	function mysubmitfunc(v,m,f){
		if(v == true){
			an = m.children('#name');
			var doc = f.name;
			if(f.name == ""){
				an.css("border","solid #ff0000 1px");
				$("#nameError").html('&nbsp;&nbsp;<span style="color:#ff0000;">*Required field</span>')
				return false;
			}
			return true;
		} else {
			jQuery.prompt.close();
		}
	}
	
	$.prompt(txt,{
		  callback: mycallbackform,
		  submit: mysubmitfunc,
		  buttons: { Ok:true, Cancel:false }
	});
}

function addressBook(field,book){
	
	var orig = field;
	
	function mycallbackform(v,m,f){
		
		if(orig == "from"){
			$("#from").find('option').remove().end();
			$('#fromrow td ul li').remove().end();	
		}
		
		var field = f.currentField;
		var emails = f.popFrom;
		//$('#'+field).val(emails);
		 
		//NEED TO LOOP THROUGH VALUES
		var mySplitResult = emails.split(";")
		for(i = 0; i < mySplitResult.length; i++){
			if(mySplitResult[i] == ""){
			}else{
				$('#'+field).get(0).addItem(mySplitResult[i],mySplitResult[i]);
			}
		}
		
	}
	
	x = 1;
	$.post("lib/modules/message-center/includes/pop-up-addressbook.php", {field:field,book:book},
		function(data){
			$.prompt(data,{
				callback: mycallbackform,
				prefix: 'widescreen',
				top:'10%',
				buttons: { Ok:true, Cancel:false }	
			});
			
		var str = '';
		if(book == "All"){
			getEmailAddresses('All','',str);
		}else if(book == "Modulift"){
			getEmailAddresses('Modulift','',str);
		}else{
			var company = $('#hiddenCompanyID').val();
			getEmailAddresses('Company',company,str);
		}
		
	});	
}

function updatePaper(x){
	$.post("lib/modules/message-center/includes/drop-down-template.php", {id:x},
		function(data){
			$('#templateArea').html(data);
		});	
}

function selectContacts(field){
	$('#popFrom').val('');
	$("#contactsTable tr td input.chk").each(function(){
		if($('#'+this.id).attr('checked') == true){
			$('#popFrom').val($('#popFrom').val() + this.value + ";");
		}
	});	
}

function searchEmailAccounts(){
	var x = $('#currentBook').val();
 	var y = $('#search').val();
 	selectEmailAccounts(x,y);
}

function selectEmailAccounts(book,str){
	$('#currentBook').val(book);
	if(book == "All"){
		getEmailAddresses('All','',str);
	}else if(book == "Modulift"){
		getEmailAddresses('Modulift','',str);
	}else if(book == "Company"){
		var company = $('#hiddenCompanyID').val();
		getEmailAddresses('Company',company,str);
	}
}

function getEmailAddresses(book,id,str){
	$.post("lib/modules/message-center/scripts/php/get-email-addresses.php", {id:id,book:book,str:str},
		function(data){
			$('#addressbook-wrapper').html(data);
		});	
}

function loadAttachments(){
	var company = $('#hiddenCompanyID').val();
	var toDelete = "";
	
	function mysubmitfunc(v,m,f){
		
		if(v == true){
		
			$(".chk").each(function(){
				x = $(this).attr("checked");
				b = $(this).val();
				if(x == true){
				//	toDelete = toDelete + b + ",";
				//}
				//NEED TO LOOP THROUGH VALUES
				//var mySplitResult = toDelete.split(",")
				//for(i = 0; i < mySplitResult.length; i++){
				//	if(mySplitResult[i] == ""){
				//	}else{
				//		$('#attachments').get(0).addItem(mySplitResult[i],mySplitResult[i]);
				//	}
				$('#attachments').get(0).addItem(b,b);
				}
				jQuery.prompt.close();
			});
		
		}else{
			jQuery.prompt.close();	
		}
		
	}
	
	$.post("lib/modules/message-center/includes/pop-up-attachments.php", {company:company},
		function(data){
			$.prompt(data,{
				top:'10%',
				prefix: 'widescreen',
				submit:mysubmitfunc,
				buttons: { Attach:true, Cancel:false }	
			});	
		});
			
}
	
	function selectNarrow(x){
		if(x!=""){
			location.href='landing.php?sec=companies&pg=view-companies&nar='+x;	
		}
	}
	
	function selectNarrow2(x){
		if(x!=""){
			location.href='landing.php?sec=companies&pg=view-offices&nar='+x;	
		}
	}
	
	function selectNarrow3(x){
		if(x!=""){
			location.href='landing.php?sec=companies&pg=view-contacts&nar='+x;	
		}
	}
	
	function updatePrices(){
		var y = 0;
		$('.hiddenPrice').each(function(){
			y = y + parseFloat($(this).val());
		});	
		$('#total').html(y.toFixed(2));
	}
	
	function updateWeights(){
		var y = 0;
		$('.hiddenWeight').each(function(){
			y = y + parseFloat($(this).val());
		});	
		$('#weight').html(y.toFixed(2));
	}
	
	function recMod(){
		
		function reccommend(v,m,f){
			
			
			
			if(v == true){
				
				pageLoader();
				var angle = f.angle;
				var span = f.span;
				var loadLimmit = f.loadLimmit;
				var un = $("#hiddenUnit").val();
				
				$.post("lib/modules/products/scripts/php/reccommend-mod.php", {angle:angle,span:span,loadLimmit:loadLimmit,un:un},
				function(data){
					
					if(data == "error"){
						 $.post("lib/modules/products/includes/error-mod.php", 
							function(data){
						 	$.prompt(data);
						 	destroyLoader();
						});
					}else{
					
					$.post("lib/modules/products/scripts/php/get-mod-parts.php", {mod:data},
					function(data){
						$('#recArea').append(data);
						//updatePrices();
						//updateWeights();
						destroyLoader();
						//completeSummary();
						//updateAllPrices();
						newUpdatePrices();
						$('#recArea').show();
					});
					
					}
				});
				
			}
		}
		
		$.post("lib/modules/products/includes/pop-up-reccommend-mod.php", {},
		function(data){
			$.prompt(data,{
				top:'10%',
				callback:reccommend,
				buttons: { Ok:true, Cancel:false }	
			});	
		});
		
	}
	
	
	
	
		function addOpPopUp(x){
		
		if(x == 0){
			x = "";
		}
		
		var context = jQuery('#hiddenContext').val();
		var id = jQuery('#hiddenID').val();
		
		function addOpportunity(v,m,f){
			
			if(v == "Save Opportunity"){
			
				var name = f.name;
				var raisedFor = f.raisedFor;
				var value = f.value;
				var type = f.type;
				var notes = f.notes;
				var probability = f.probability;
				var converted = f.converted;
				var ref = f.ref;
				
				$.post("lib/modules/companies/scripts/php/add-opportunity.php", {id:id, context:context, name:name, raisedFor:raisedFor, value:value, type:type, notes:notes, probability:probability, ref:ref, converted:converted}, 
				function(data){
					if(data != ""){
						location.reload(true);
					}else{
					}
					return false;
				});
			
			}
			
		}
		
		jQuery.post("lib/modules/companies/includes/popup-add-opportunity.php", {id:id,context:context,ref:x}, function(data){
			$.prompt(data, {
				prefix: 'widescreen',
				top: '13%',
				buttons: {'Save Opportunity':'Save Opportunity', Cancel: false},
				callback: addOpportunity
			});																		   
		})	
	
	}
	
	/***************
	POSTCODE LOOKUP FUNCTIONALITY 
	***************/
	
	var account_code='GHCOA11112';
	var license_code='MW41-EA71-MJ73-KU25';
	var machine_id='';
	
	
	function pcaByPostcodeBegin()
	   {
		  var postcode = document.getElementById("postcode").value;
		  var scriptTag = document.getElementById("pcaScriptTag");
		  var headTag = document.getElementsByTagName("head").item(0);
		  var strUrl = "";
		  
		  
		  
		  //Build the url
		  strUrl = "http://services.postcodeanywhere.co.uk/inline.aspx?";
		  strUrl += "&action=lookup";
		  strUrl += "&type=by_postcode";
		  strUrl += "&postcode=" + escape(postcode);
		  strUrl += "&account_code=" + escape(account_code);
		  strUrl += "&license_code=" + escape(license_code);
		  strUrl += "&machine_id=" + escape(machine_id);
		  strUrl += "&callback=pcaByPostcodeEnd";
		  
		  //Make the request
		  if (scriptTag)
			 {
				//The following 2 lines perform the same function and should be interchangeable
				headTag.removeChild(scriptTag);
				//scriptTag.parentNode.removeChild(scriptTag);
			 }
		  scriptTag = document.createElement("script");
		  scriptTag.src = strUrl
		  scriptTag.type = "text/javascript";
		  scriptTag.id = "pcaScriptTag";
		  headTag.appendChild(scriptTag);
		  
		  
		  
	   }

	function pcaByPostcodeEnd()
	   {
		  
		  
		  //Test for an error
		  if (pcaIsError)
			 {
				//Show the error message
				document.getElementById("selectaddress").style.display = 'none';
				document.getElementById("btnFetch").style.display = 'none';
				alert(pcaErrorMessage);
			 }
		  else
			 {
				//Check if there were any items found
				if (pcaRecordCount==0)
				   {
					  document.getElementById("selectaddress").style.display = 'none';
					  document.getElementById("btnFetch").style.display = 'none';
					  alert("Sorry, no matching items found. Please try another postcode.");
				   }
				else
				   {
					  
					$.post("lib/modules/companies/includes/popup-find-address.php",  
					function(data){
						
						function callbackfunction(){
							pcaFetchBegin();
							jQuery.prompt.close();
						}
						
						$.prompt(data, {submit:callbackfunction});
						
						
						document.getElementById("selectaddress").style.display = '';
					  	//document.getElementById("btnFetch").style.display = '';
						 for (i=document.getElementById("selectaddress").options.length-1; i>=0; i--){
						  document.getElementById("selectaddress").options[i] = null;
						}
					  for (i=0; i<pca_id.length; i++){
						document.getElementById("selectaddress").options[document.getElementById("selectaddress").length] = new Option(pca_description[i], pca_id[i]);
					  }
					});
					  
					  
					  
					 
				   }
			 }
	   }
	
	function pcaFetchBegin()
	   {
		  var address_id = $("#selectaddress").val();
		  var scriptTag = document.getElementById("pcaScriptTag");
		  var headTag = document.getElementsByTagName("head").item(0);
		  var strUrl = "";

		  //Build the url
		  strUrl = "http://services.postcodeanywhere.co.uk/inline.aspx?";
		  strUrl += "&action=fetch";
		  strUrl += "&id=" + escape(address_id);
		  strUrl += "&account_code=" + escape(account_code);
		  strUrl += "&license_code=" + escape(license_code);
		  strUrl += "&machine_id=" + escape(machine_id);
		  strUrl += "&callback=pcaFetchEnd";

		  //Make the request
		  if (scriptTag)
			 {
				//The following 2 lines perform the same function and should be interchangeable
				headTag.removeChild(scriptTag);
				//scriptTag.parentNode.removeChild(scriptTag);
			 }
		  scriptTag = document.createElement("script");
		  scriptTag.src = strUrl
		  scriptTag.type = "text/javascript";
		  scriptTag.id = "pcaScriptTag";
		  headTag.appendChild(scriptTag);
		  
		  //document.forms[0]["selectaddress"].style.display = 'none';
		  //document.forms[0]["btnFetch"].style.display = 'none';
	   }

	function pcaFetchEnd()
	   {
		  //Test for an error
		  if (pcaIsError)
			 {
				//Show the error message
				alert(pcaErrorMessage);
			 }
		  else
			 {
				//Check if there were any items found
				if (pcaRecordCount==0)
				   {
					  alert("Sorry, no matching items found");
				   }
				else
				   {
					  				  
					  $("#addressLine1").val(pca_line1[0]);
					  $("#addressLine2").val(pca_line2[0]);
					  $("#addressLine3").val(pca_line3[0]);
					  $("#town").val(pca_post_town[0]);
					  $("#region").val(pca_county[0]);
					  
					  //~document.forms[0]["company"].value = '' + pca_organisation_name[0];
					  //~document.forms[0]["line1"].value = '' + pca_line1[0];
					  //~document.forms[0]["line2"].value = '' + pca_line2[0];
					  //~document.forms[0]["line3"].value = '' + pca_line3[0];
					  //~document.forms[0]["line4"].value = '' + pca_line4[0];
					  //~document.forms[0]["line5"].value = '' + pca_line5[0];
					  //~document.forms[0]["town"].value = '' + pca_post_town[0];
					  //~document.forms[0]["county"].value = '' + pca_county[0];
					  //~document.forms[0]["postcode"].value = '' + pca_postcode[0];
				   }
			 }
	   }
		
// URL Hash Functions
function HashString(FullURL)
{
	QueryStr = FullURL.split("#");	
	VariableArray = QueryStr[1].split("&");
	
	Variables = new Array();
	for (var ArrayObjects in VariableArray)
	{
		Vars = VariableArray[ArrayObjects].split("=");
		Variables.push(Vars);
	}
	return Variables;
	// Returns the format Variables[VarName][VarData]
}

function GetHashVariable(VarName)
{
	if (location.hash)
	{
		var VarArray = HashString(location.hash);
	
		for (var Count = 0; Count < VarArray.length; Count++)
		{
			if (VarArray[Count][0] == VarName)
			{
				return VarArray[Count][1];	
			}
		}
		
		return false;
	}
	return false;
}

// select vessel
function selectVessel(alertType)
{
	page = (alertType == "test") ? "vessel-schedule" : "vessel-alerts";
	$.post("lib/modules/Maritime/includes/select-vessel.php", {"type": alertType}, function(data)
	{
		$.prompt(data, {callback: Action, buttons: {"Ok":true, "Cancel":false}});
	});
	
	function Action(v, m, f)
	{
		if (v == true && f.vessel != undefined && f.vessel != "")
		{
			location.href = "landing.php?sec=Maritime&pg=" + page + "&id=" + f.vessel + "#auto=true";
		}
	}		
}

// select vessel based on company
function selectVessel(alertType, company_id)
{
	page = (alertType == "test") ? "vessel-schedule" : "vessel-alerts";
	$.post("lib/modules/Maritime/includes/select-vessel.php", {"type": alertType, company_id:company_id}, function(data)
	{
		$.prompt(data, {callback: Action, buttons: {"Ok":true, "Cancel":false}});
	});
	
	function Action(v, m, f)
	{
		if (v == true && f.vessel != undefined && f.vessel != "")
		{
			location.href = "landing.php?sec=Maritime&pg=" + page + "&id=" + f.vessel + "#auto=true";
		}
	}		
}

function viewAutoCompleteList(type)
{
	$.ajax(
	{
		type: "POST", 
		url: "lib/scripts/php/viewAutoCompleteList.php", 
		data: "type=" + type, 
		success: function(data)
		{
			$.prompt(data, {callback: actions, buttons: {"Select": true, "Cancel": false}});
		}
	});
	
	function actions(v, m, f)
	{
		if (v == true)
		{
			switch (type)
			{
				case "company_types":
					$("input#type").val(f.option);
					break;
				case "maritime_clients":
					$("input#clientname").val(f.option);
					break;
			}
		}
	}
}

function capitalise(string)
{
	return string.charAt(0).toUpperCase() + string.slice(1);
}

function zeroPad(num, count)
{
	var numZeropad = num + '';
	while(numZeropad.length < count) 
	{
		numZeropad = "0" + numZeropad;
	}
	return numZeropad;
}

function updateCrumbs(val)
{
	$("#breadcrumb").html(val.replace(/ > /g, " &rsaquo; "));	
}