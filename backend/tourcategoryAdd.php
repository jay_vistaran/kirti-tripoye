<?php
	require_once("header.php");
        //global $cleaned;
	//$getSystemUsers = Users::getSystemUsers();//for global
            $getSystemCity = Users::getSystemCityAproved();//for global          
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Tour Category</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Add New Tour Category</h4>
                                <form id="jvalidate_tourcategory" method="post" role="form"  enctype="multipart/form-data" class="form-horizontal" action="lib/scripts/php/all/tourcategory.php">
                                <div class="panel-body">                                    
                                                 
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate category for the selected city, Category name must be unique for the city.</span> <?php }?>
                                   <div class="form-group">
                                        <label class="col-md-3 control-label">Select City:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="cityid">
                                                
                                                 <?php 
                                                 foreach($getSystemCity as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>"><?php echo $SystemCountry['cityname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tour Category Name:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="tourcatname"/>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Single Width Image</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class=" btn-primary" name="filename" id="filename" title="Browse file"/>
                                            <span class="help-block">Image ratio 1:1</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Double Width Image</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class=" btn-primary" name="filename1" id="filename1" title="Browse file"/>
                                            <span class="help-block">Image ratio 16:9</span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>                                                                                    
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate_tourcategory.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>


                
                
                
              
                






