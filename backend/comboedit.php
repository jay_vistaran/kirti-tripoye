<?php
	require_once("header.php");
        //global $cleaned;
	$getCancelPolicyAproved= Users::getPageCancelPolicyAproved();//for CancelPolicy
        
         $cleaned = clean($_GET);
		 if(!isset($_GET['id']) || $_GET['id']=='' )
            {
                    header("location:comboadd.php"); 
            }
        $comboid=$_GET['id'];
        //dump($cleaned);
	$getSystemCombo = Users::getSystemComboById($cleaned['id']);//for global
            
      $today = date('Y-m-d');
	  $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid =$getSystemDefaultSeason['id'];
		}
		else{
			$sid =0;
		}
                     
?>
<style>
.btn-primary.active {
            background-color: #e34724;
    }
.btn_red.active {
  background-color: red;
   }
   .form-inline .form-group input[type=text] {
	width:230px;
}
  
</style>
<!-- PAGE TITLE -->
                <div class="page-title"> 
                   
                    <h2>Manage Combo</h2>
                    
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">  
               
                 
                 
                <div class="row">
                 <div class="panel panel-default"> 
                <div class="panel-body">
                <div class="col-md-8" >
                


                                            <div class="btn-group btn-group-justified">  
                                                <a href="comboedit.php?id=<?php echo $comboid; ?>" class="btn btn-primary btn-lg active"  style="font-weight: bold;">Combo Details</a>
                                                <a href="comboedit1.php?id=<?php echo $comboid; ?>" class="btn btn-primary btn-lg"  style="font-weight: bold;">Select Tour</a>
                                                <a href="comboadd2.php?id=<?php echo $comboid; ?>&sId=<?php echo $sid; ?>" class="btn btn-primary btn-lg"  style="font-weight: bold;">Combo Pricing</a>
                                            </div>                                         
                                    </div>
                       <div class="col-md-4" ></div>            
                       </div>
                       </div>             
                                    
                
               
                                            
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate_aappbanner"  method="post" role="form"  enctype="multipart/form-data" class="form-horizontal" action="lib/scripts/php/all/comboedit.php">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Edit Combo</strong></h3>
                                    <!--<ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul> -->
                                </div>
                                <!--<div class="panel-body">
                                   
                                </div>-->
                                <div class="panel-body">                                                                        
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate App Banner Image Name, Please Try Another Name</span> <?php }?>
                                   <input type="hidden" class="form-control" name="comboid" value="<?php echo $_GET['id'];?>"/>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Combo Name:</label>
                                                <div class="col-md-9">                                                                                            
                                                   
                                                    <input type="text" class="form-control" name="comboname" value="<?php echo $getSystemCombo['comboname'];?>" required/>
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">From Date:</label>
                                                <div class="col-md-9">                                            
                                                     <input type="text" class="form-control datepicker" id="todate" name="combofromdate"  value="<?php echo $getSystemCombo['combofromdate'];?>" required/>
                                            
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cancel Policy:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                     <select class="form-control select" name="combocancelpolicy">

                                                 <?php 
                                                 foreach($getCancelPolicyAproved as $CancelPolicyAproved)
                                                      { ?>  
                                                <option value="<?php echo $CancelPolicyAproved['id'];?>" <?php if($getSystemCombo['combocancelpolicy']==$CancelPolicyAproved['id']){?> selected="selected" <?php }?>><?php echo $CancelPolicyAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                             
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            
                                             
                                            
                                            
                                            
                                              <div class="form-group">
                                            
                                                
                                                
                                            
                                                <label class="col-md-3 control-label">Image</label>
                                                <div class="col-md-9">                                                                                            
<img src="lib/tourimages/appbanner/<?php echo $getSystemCombo['comboimage'];?>" width="100" />
                                            <input type="hidden" name="old_img" id="gimage" value="<?php echo $getSystemCombo['comboimage'];?>"  >
                                            <input type="file" class=" btn-primary" name="filename" id="filename" title="Browse file" />                                                    <span class="help-block">Image ratio 16:9</span> 
                                                </div>
                                            </div>   
                                             <div class="form-group">
                                            
                                                
                                                
                                            
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-9">                                                                                            
                                                    <label class="checkbox"><input type="checkbox" name="recommended" class="icheckbox" value="1" <?php if($getSystemCombo['recommended']==1){?>  checked="checked" <?php }?>/>  Recommended to Bestselling</label>
                                                    <!--<span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>                        
                                            
                                             
                                           
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">To Date:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                   <input type="text" class="form-control datepicker" id="todate" name="combotodate"  value="<?php echo $getSystemCombo['combotodate'];?>" required/>
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                             
                                             
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description:</label>
                                                <div class="col-md-9">                                            
                                                       <textarea class="form-control" rows="5" name="combodesc"><?php echo $getSystemCombo['combodesc'];?></textarea>               
                                                    </div>                                            
                                                    
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        
                                    </div>
                                    <br/><br/><br/>

                                </div>
                                <div class="panel-footer">
                                    <div class='row'>
        
        <div class='col-sm-1'>
           
        </div>
        
        <div class='col-sm-7'>
            <div class="btn-group">
                                       <button name="submit" value="1" class="btn btn-primary" type="submit">Save & Next</button>
                                    </div>
                                    <div class="btn-group">
                                        <button name="savexit" value="1" class="btn btn-primary" type="submit">Save & Exit</button>
                                    </div>
                                    <div class="btn-group">
                                        <a href="combo.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div> 
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>
                <!-- END PAGE CONTENT WRAPPER -->  


<?php	require_once("footer.php");
?>


