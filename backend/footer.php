 </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="landing.php?action=logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
       <!-- <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>   -->
        
        
        
        
          <script type='text/javascript' src='js/plugins/bootstrap/bootstrap-datepicker.js'></script> 
           
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-colorpicker.js"></script>
          <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type='text/javascript' src='js/plugins/bootstrap/bootstrap-select.js'></script>        

        <script type='text/javascript' src='js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='js/plugins/validationengine/jquery.validationEngine.js'></script>        

        <script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>                

        <script type='text/javascript' src='js/plugins/maskedinput/jquery.maskedinput.min.js'></script>
        
        
        
        
        
        <script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="js/plugins/morris/morris.min.js"></script>       
        <script type="text/javascript" src="js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="js/plugins/rickshaw/rickshaw.min.js"></script>
        <script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
        <script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
                       
        <script type="text/javascript" src="js/plugins/owl/owl.carousel.min.js"></script>                 
        
        <script type="text/javascript" src="js/plugins/moment.min.js"></script>
        
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="js/settings.js"></script>
        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>
        
        <script type="text/javascript" src="js/demo_dashboard.js"></script>
        <!-- END TEMPLATE -->
        
        
        <script type="text/javascript">
            var jvalidate = $("#jvalidate").validate({
                ignore: [],
                rules: {                                            
                        email: {
                                required: true,
                                email: true
                        },
                        
                        user_password: {
                                required: true,
                                minlength: 5,
                                maxlength: 10
                        },
                        're-password': {
                                required: true,
                                minlength: 5,
                                maxlength: 10,
                                equalTo: "#password2"
                        },
                        user_name: {
                                required: true,
                                minlength: 2,
                                maxlength: 10
                        },
                        user_surname: {
                                required: true,
                                minlength: 2,
                                maxlength: 10
                        },
                        user_mobile: {
                                required: true,
                                 minlength: 2,
                                maxlength: 10
                        },
                        date: {
                                required: true,
                                date: true
                        }
                    }                                        
                });                                    

        </script>

        <script type="text/javascript">
            var jvalidateVendorAdd = $("#jvalidateVendorAdd").validate({
                ignore: [],
                rules: {                                            
                        vendor_email: {
                                required: true,
                                email: true
                        },
                        vendor_name: {
                                required: true,
                                minlength: 2,
                                maxlength: 10
                        },
                        vendor_surname: {
                                required: true,
                                minlength: 2,
                                maxlength: 10
                        },
                        vendor_phone: {
                                required: true,
                                 minlength: 2,
                                maxlength: 10
                        },
                        countryid: {
                                required: true
                        },
                        cityid: {
                                required: true
                        },
                        vendor_skypeid: {
                                required: true,
                                 minlength: 2
                        },
                        vendor_watsappno: {
                                required: true,
                                 minlength: 2,
                                maxlength: 10
                        },
                        vendor_officeaddress: {
                                required: true,
                                 minlength: 10
                        },
                        vendor_company: {
                                required: true,
                                 minlength: 2,
                                maxlength: 20
                        }
                    }                                        
                });
        </script>

        <script type="text/javascript">
            var jvalidateVendateEdit = $("#jvalidateVendateEdit").validate({
                ignore: [],
                rules: {                                            
                        vendor_email: {
                                required: true,
                                email: true
                        },
                        vendor_name: {
                                required: true,
                                minlength: 2,
                                maxlength: 10
                        },
                        vendor_surname: {
                                required: true,
                                minlength: 2,
                                maxlength: 10
                        },
                        vendor_phone: {
                                required: true,
                                 minlength: 2,
                                maxlength: 10
                        },
                        countryid: {
                                required: true
                        },
                        cityid: {
                                required: true
                        },
                        vendor_skypeid: {
                                required: true,
                                 minlength: 2
                        },
                        vendor_watsappno: {
                                required: true,
                                 minlength: 2,
                                maxlength: 10
                        },
                        vendor_officeaddress: {
                                required: true,
                                 minlength: 10
                        },
                        vendor_company: {
                                required: true,
                                 minlength: 2,
                                maxlength: 20
                        }
                    }                                        
                });
        </script>
        
        <script type="text/javascript">
            var jvalidateUserEdit = $("#jvalidateUserEdit").validate({
                ignore: [],
                rules: {                                            
                        email: {
                                required: true,
                                email: true
                        },
                        
                        user_name: {
                                required: true
                        },
                        user_surname: {
                                required: true
                        },
                        user_mobile: {
                                required: true,
                                 minlength: 2,
                                maxlength: 10
                        },
                        date: {
                                required: true,
                                date: true
                        }
                    }                                        
                });                                    

        </script>
        <script type="text/javascript">
            var jvalidate_tour = $("#jvalidate_tour").validate({
                ignore: [],
                rules: {                                            
                       
                        tourname: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        <script type="text/javascript">
            var jvalidate_coupon = $("#jvalidate_coupon").validate({
                ignore: [],
                rules: {                                            
                       
                        couponcode: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        <script type="text/javascript">
            var jvalidate_city = $("#jvalidate_city").validate({
                ignore: [],
                rules: {                                            
                       
                        cityname: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        
        <script type="text/javascript">
            var jvalidate_location = $("#jvalidate_location").validate({
                ignore: [],
                rules: {                                            
                       city_id: {
                                required: true,
                                 city_id: true
                        },
                        area: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        
        
        <script type="text/javascript">
            var jvalidate_category = $("#jvalidate_category").validate({
                ignore: [],
                rules: {                                            
                       cat_name: {
                                required: true
                        },
                       cat_keyword: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        
        <script type="text/javascript">
            var jvalidate_subcategory = $("#jvalidate_subcategory").validate({
                ignore: [],
                rules: {
                       city_id: {
                                required: true
                        },
                       subcat_name: {
                                required: true
                        },
                       subcat_keyword: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        
        
        <script type="text/javascript">
            var jvalidate_ads = $("#jvalidate_ads").validate({
                ignore: [],
                rules: {
                       bannername: {
                                required: true
                        },
                       bannerorder: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        
         <script type="text/javascript">
            var jvalidate_gads = $("#jvalidate_gads").validate({
                ignore: [],
                rules: {
                       galleryname: {
                                required: true
                        },
                       galleryorder: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        
        <!--Added By ME Dated: 15-9-2016  -->
        <script type="text/javascript">
            var jvalidate_city = $("#jvalidate_tourcategory").validate({
                ignore: [],
                rules: {                                            
                       
                        tourcatname: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        
        <script type="text/javascript">
            var jvalidateSeason = $("#jvalidateSeason").validate({
                ignore: [],
                rules: {                                            
                                               
                        season_name: {
                                required: true
                        },
                        fromdate: {
                                required: true,
                                date: true
                        },
                        todate: {
                                required: true,
                                date: true
                        },
                        effectivedate: {
                                required: true,
                                date: true
                        }
                    }                                        
                });                                    

        </script>
        
        <script type="text/javascript">
            var jvalidate_currency = $("#jvalidate_currency").validate({
                ignore: [],
                rules: {                                            
                                               
                        currencyname: {
                                required: true
                        },
                        currencysymbol: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
         <script type="text/javascript">
            var jvalidate_currency = $("#jvalidate_aappbanner").validate({
                ignore: [],
                rules: {                                            
                                               
                        img_name: {
                                required: true
                        }
                    }                                        
                });                                    

        </script>
        <!--Added By ME Dated: 15-9-2016  -->
        
        
    <!-- END SCRIPTS -->         
    </body>
</html>