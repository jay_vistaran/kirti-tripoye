<?php
	require_once("header.php");
        //global $cleaned;
	$getCancelPolicyAproved= Users::getPageCancelPolicyAproved();//for CancelPolicy
            
      
                     
?>
<style>
.btn-primary.active {
            background-color: #e34724;
    }
.btn_red.active {
  background-color: red;
   }
   .form-inline .form-group input[type=text] {
	width:230px;
}
  
</style>

<!-- PAGE TITLE -->
                <div class="page-title"> 
                   
                    <h2></span>Manage Combo</h2>
                    
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">  
               
                 
                 
                <div class="row">
                 <div class="panel panel-default"> 
                <div class="panel-body">
                <div class="col-md-8">
                


                                            <div class="btn-group btn-group-justified">  
                                               <a href="#" class="btn btn-primary btn-lg active"  style=" font-weight: bold;">Combo Details</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Select Tour</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Combo Pricing</a>
                                            </div>                                         
                                    </div>
                       <div class="col-md-4" ></div>            
                       </div>
                       </div>             
                                    
                
               
                                            
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate_aappbanner"  method="post" role="form"  enctype="multipart/form-data" class="form-horizontal" action="lib/scripts/php/all/addcombo.php">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add Combo</strong></h3>
                                    <!--<ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul> -->
                                </div>
                                <!--<div class="panel-body">
                                   
                                </div>-->
                                <div class="panel-body">                                                                        
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate Combo Name, Please Try Another Name</span> <?php }?>
                                   <input type="hidden" class="form-control" name="cityid" value="<?php echo $_GET['CityId'];?>"/>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Combo Name:</label>
                                                <div class="col-md-9">                                                                                            
                                                   
                                                    <input type="text" class="form-control" name="comboname" required/> 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">From Date:</label>
                                                <div class="col-md-9">                                            
                                                    <input type="text" class="form-control datepicker" id="todate" name="combofromdate" required/>
                                            
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cancel Policy:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                     <select class="form-control select" name="combocancelpolicy">

                                                 <?php 
                                                 foreach($getCancelPolicyAproved as $CancelPolicyAproved)
                                                      { ?>  
                                                <option value="<?php echo $CancelPolicyAproved['id'];?>"><?php echo $CancelPolicyAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                             
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            
                                             
                                            
                                            
                                                                         
                                             <div class="form-group">
                                            
                                                
                                                
                                            
                                                <label class="col-md-3 control-label">Image</label>
                                                <div class="col-md-9">                                                                                            
                                                    <input type="file" class=" btn-primary" name="filename" id="filename" title="Browse file" required/>
                                                   <span class="help-block">Image ratio 16:9</span>
                                                </div>
                                            </div>
                                             
                                           
                                             <div class="form-group">
                                            
                                                
                                                
                                            
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-9">                                                                                            
                                                    <label class="checkbox"><input type="checkbox" name="recommended" class="icheckbox" value="1"/>		Recommended to Bestselling</label>
                                                    <!--<span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                            
                                           
                                            
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            
                                           
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">To Date:</label>
                                                <div class="col-md-9">                                                                                            
                                                    
                                                   <input type="text" class="form-control datepicker" id="todate" name="combotodate" required/>
 
                                                   <!-- <span class="help-block">Select box example</span> -->
                                                </div>
                                            </div>
                                             
                                             
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description:</label>
                                                <div class="col-md-9">                                            
                                                       <textarea class="form-control" rows="5" name="combodesc"></textarea>                
                                                    </div>                                            
                                                    
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        
                                    </div>
                                    <br/><br/><br/>

                                </div>
                                <div class="panel-footer">
                                    <div class='row'>
        
        <div class='col-sm-1'>
           
        </div>
        
        <div class='col-sm-7'>
            <div class="btn-group">
                                       <button name="submit" value="1" class="btn btn-primary" type="submit">Save & Next</button>
                                    </div>
                                    <div class="btn-group">
                                        <button name="savexit" value="1" class="btn btn-primary" type="submit">Save & Exit</button>
                                    </div>
                                    <div class="btn-group">
                                        <a href="combo.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div> 
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>
              
                
                <!-- END PAGE CONTENT WRAPPER -->  


<?php	require_once("footer.php");
?>


