<?php
	require_once("header.php");
        //global $cleaned;
	$getSystemUsers = Users::getSystemUsers();//for Users
        
        $getSystemCountry = Users::getSystemCountryAproved();//for Country
         
        $getSystemCity = Users::getSystemCityAproved();//for City
         
        $getSystemTC= Users::getTourCategoryAproved();//for TourCategory
          
        $getCancelPolicyAproved= Users::getPageCancelPolicyAproved();//for CancelPolicy
        
        $getPageTermConditionAproved= Users::getPageTermConditionAproved();//for CancelPolicy
                     
?>
<script type="text/javascript" 	src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".flip1").click(function() {
			$(".panel1").slideToggle("slow");
		});
                
                $(".flip2").click(function() {
			$(".panel2").slideToggle("slow");
		});
                
                $(".flip3").click(function() {
			$(".panel3").slideToggle("slow");
		});
                
                $(".flip4").click(function() {
			$(".panel4").slideToggle("slow");
		});
	});
        
</script>
<style>
.btn-primary.active {
            background-color: limegreen;
    }
.btn_red.active {
  background-color: red;
   }
</style>
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Manage Tour</h2>
                </div>
                <!-- END PAGE TITLE -->                
                 

<!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">  
               
                 
                 
                <div class="row">
                 <div class="panel panel-default"> 
                <div class="panel-body">
                <div class="col-sm-8" >
                                            <div class="btn-group btn-group-justified">  
                                                <a href="createTour1.php" class="btn btn-primary btn-lg active"  style=" font-weight: bold;">Tour Detail</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Timing</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Image</a>
                                                <a href="#" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Transfer Option</a>
                                                <a href="createTour5.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Buying Price</a>
                                                <a href="createTour6.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Selling Price</a>
                                                <a href="createTour7.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Discount</a>
                                            </div>                                         
                                    </div>
                       </div>
                       </div>             
                                     <div class="panel-heading">                                
                                                <div class="col-sm-4" >
                                                    <h4><strong>Add New Tour</strong></h4>

                                                     
                                                </div>
                                     </div> 
                
               
                <div class="panel panel-default">                                  
                <div class="panel-body">
                    
    <!-- first part start here -->
    <form id="jvalidate_tour" role="form" class="form-inline" action="lib/scripts/php/all/createTour1.php">
    <div class='row'>
        <div class='col-sm-4'>    
            <div class='form-group'>
                <label for="user_title">Select Country:</label>
                 <select class="select" name="countryid">

                                                 <?php 
                                                 foreach($getSystemCountry as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>"><?php echo $SystemCountry['countryname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>    
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_firstname">Select City:</label>
                <select class="select" name="cityid">
                                               <?php 
                                                 foreach($getSystemCity as $SystemCity)
                                                      { ?>  
                                                <option value="<?php echo $SystemCity['id'];?>"><?php echo $SystemCity['cityname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_lastname">Select Tour Category:</label>
                <select class="select" name="tourcatid">
                                        <?php 
                                                 foreach($getSystemTC as $SystemTC)
                                                      { ?>  
                                                <option value="<?php echo $SystemTC['id'];?>"><?php echo $SystemTC['tourcatname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>         
            </div>
        </div>
    </div>
    <br/> <br/> <br/> 
    <!-- first part End here --> 
    <!-- first II start -->
    
     <div class='row'>
        <div class='col-sm-4'>    
            <div class='form-group'>
                <label for="user_title">Full Day / Half Day:</label>
                 <select class="select" name="fhday">
                                                <option value="1">Full Day</option>
                                                <option value="0">Half Day</option>
                                                
                                            </select>     
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_firstname">Tour Name:</label>
                <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger">Duplicate Tour Name, Please Try Another Tour name</span> <?php }?>
                <input type="text" class="form-control" style = "width:200x;" name="tourname"/>
                <span class="help-block">min size = 20, max size = 25</span>
                                                           
                                            
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_lastname">Cancel Policy:</label>
                 <select class="select" name="cancelpolicy">

                                                 <?php 
                                                 foreach($getCancelPolicyAproved as $CancelPolicyAproved)
                                                      { ?>  
                                                <option value="<?php echo $CancelPolicyAproved['id'];?>"><?php echo $CancelPolicyAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>           
            </div>
        </div>
        
    </div>
    
    <!-- first second end here -->
    <!-- Second part start -->
    
    <br/> <br/> <br/>
    <!--- second part end -->
    <!-- third part start -->
     <div class='row'>
        
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_firstname">Term & Condition:</label>
                 <select class="select" name="termcond">

                                                 <?php 
                                                 foreach($getPageTermConditionAproved as $TermConditionAproved)
                                                      { ?>  
                                                <option value="<?php echo $TermConditionAproved['id'];?>"><?php echo $TermConditionAproved['pagename'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                              
                                            
            </div>
        </div>
        
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for="user_lastname">CutOff Day:</label>
                <input type="text" max="10" class="form-control numericOnly" name="cutoffday" value=""/>
                                            <span class="help-block">min size = 20, max size = 25</span>     
            </div>
        </div>
        <div class='col-sm-4'>    
            <div class='form-group'>
            <label class="checkbox-inline"><input type="checkbox" value="">Is Live</label>
			<label class="checkbox-inline"><input type="checkbox" value="">Recommended to Bestselling</label>
			<label class="checkbox-inline"><input type="checkbox" value="">Meal Included</label>
                
            </div>
        </div>
    </div>
    <br/> <br/> <br/>
    <div class="panel-group accordion">
                                    
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColOne">
                                               + Important Information
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColOne">
                                       <textarea class="ckeditor" rows="5" name="content1" ></textarea>
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColTwo">
                                                + Itinerary Description
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColTwo">
                                        <textarea class="ckeditor" rows="5" name="content2" ></textarea>
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColThree">
                                                + Description
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColThree">
                                          <textarea class="ckeditor" rows="5" name="content3" ></textarea>
		
                                    </div>                                
                                </div>
                                </div>
                                
                                     <div class="col-md-12">
                          
                            <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="#accOneColFour">
                                               + Inclusion
                                            </a>
                                        </h4>
                                    </div>                                
                                    <div class="panel-body" id="accOneColFour">
                                          <textarea class="ckeditor" rows="5" name="content4" ></textarea>
		
                                    </div>                                
                                </div>
                            </div>
                            </div>
    <!-- Thrid Part End                   -->
    <div class='row'>
        
        <div class='col-sm-1'>
           
        </div>
        
        <div class='col-sm-7'>
            <div class="btn-group">
                                       <button name="submit" value="1" class="btn btn-primary" type="submit">Save & Next</button>
                                    </div>
                                    <div class="btn-group">
                                        <button name="savexit" value="1" class="btn btn-primary" type="submit">Save & Exit</button>
                                    </div>
                                    <div class="btn-group">
                                        <a href="managetour.php"><span class="btn btn-primary" type="submit">Cancel</span></a>
                                    </div> 
        </div>
        <div class='col-sm-4'>    
            
        </div>
    </div>
   
                                    
    
    </form>

                    </div>    
                    </div>                            
                    </div>
                </div> 
  <!-- PAGE CONTENT WRAPPER --> 
  
  <script type="text/javascript">
      
      function save_row_addser(id)
            {

              tourOtherOptionId='tourOtherOptionId_'+id;      
              buyAP='buyAP_'+id;
              buyCP='buyCP_'+id;
              sellAP='sellAP_'+id;
              sellCP='sellCP_'+id;
              discount='discount_'+id;
              discountType='discountType_'+id;
              
              
              mydiv='#predownA_'+id;
             var TourId=document.getElementById("TourId").value;
             var seasonId=document.getElementById("seasonId").value;
             var tourOtherOptionId=document.getElementById(tourOtherOptionId).value;
             var buyAP=document.getElementById(buyAP).value;
             var buyCP=document.getElementById(buyCP).value;
             var sellAP=document.getElementById(sellAP).value;
             var sellCP=document.getElementById(sellCP).value;
             var discount=document.getElementById(discount).value;
             var discountType=document.getElementById(discountType).value;

                                $.ajax({
                                            url: "lib/scripts/php/all/ajaxAdditionalServicesSave.php",
                                            type: "POST",
                                            data:'TourId='+TourId+'&id='+id+'&tourOtherOptionId='+tourOtherOptionId+'&buyAP='+buyAP+'&buyCP='+buyCP+'&sellAP='+sellAP+'&sellCP='+sellCP+'&discount='+discount+'&discountType='+discountType+'&seasonId='+seasonId,
                                            success: function(data){

                                            if(data==1)
                                               {                                   

                                               $(mydiv).show();
                                               $(mydiv).fadeOut(4000);

                                                } 

                                            }        
                               });


            }
  
      
      
      
      function add_seasion(id)
        {     ///alert(id);
        var TourId=document.getElementById("TourId").value;
        //alert(TourId);
        //var tourToption=document.getElementById("tourToption").value;
                if(id!=''){
                               $.ajax({
                                        url: "lib/scripts/php/all/ajaxAddTourSeason.php",
                                        type: "POST",
                                        data:'TourId='+TourId+'&seasonId='+id,
                                        success: function(data){
                                            //console.log(data);
                                            //alert(data);
                                       window.location.href = "createTour5.php?TourId="+TourId+"&sId="+id;   
                                        }        
		                });
                           }
                 else
                           {
                               alert('Please select the season first');
                            return false;
                           }
        
              
                   
 
        } 

        function add_row(id)
        {
                
              tourTransOptionId='tourTransOptionId_'+id;      
             var TourId=document.getElementById("TourId").value;
             var seasonId=document.getElementById("seasonId").value;
             var tourTransOptionId=document.getElementById(tourTransOptionId).value;
             
                

                 $.ajax({
				url: "lib/scripts/php/all/ajaxTransferOptionAddSub.php",
				type: "POST",
				data:'TourId='+TourId+'&id='+id+'&tourTransOptionId='+tourTransOptionId+'&seasonId='+seasonId,
				success: function(data){
                                console.log(data); 
                                
                                if(data)
                                   {                                  
                                    var duce = jQuery.parseJSON(data);
                                    var nid = duce.id;
                                    var subTPid = duce.subTPid;
                                    var TourId = duce.TourId;
                                    var tourTransOptionId = duce.tourTransOptionId;
                                    var transferoption = duce.transferoption;
                                    var seasonId = duce.seasonId;
                                     
                                    var img ="img/minus.png";
                                    var imgsave ="img/save.png";
                                    
                                    var sek1 ="<option value='1' >Amount</option><option value='0'>Percentage</option>";
                                   


                                    var data_table='data_table_'+id;

                                    var table=document.getElementById(data_table);
                                    var table_len=(table.rows.length);
                                    var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'>\n\
                                   <td style='width: 110px; text-align: center;'><input type='hidden' id='tourTransOptionId_"+nid+"' value='"+tourTransOptionId+"' size='3' >"+transferoption+"</td>\n\
                                   <td style='width: 72px; text-align: center;'><input type='text' id='minpax"+nid+"' size='3' ></td>\n\
                                   <td style='width: 72px; text-align: center;'><input type='text' id='maxpax"+nid+"' size='3' ></td>\n\
                                   <td style='width: 129px; text-align: center;'><input type='text' id='buyAP"+nid+"' size='6' ></td>\n\
                                   <td style='width: 129px; text-align: center;'><input type='text' id='buyCP"+nid+"' size='6' ></td>\n\
                                   <td style='width: 129px; text-align: center;'><input type='text' id='sellAP"+nid+"' size='6' ></td>\n\
                                   <td style='width: 129px; text-align: center;'><input type='text' id='sellCP"+nid+"' size='6' ></td>\n\
                                   <td style='width: 85px; text-align: center;'><input type='text' id='discount"+nid+"' size='6' ></td>\n\
                                   <td style='width:110px; text-align: center;'><select id='discountType"+nid+"'>"+sek1+"</select></td>\n\
                                   <td style='width:60px; text-align: center;'><input type='button' id='del_button"+nid+"' style='background-image: url("+img+");border: 0 none;height: 36px;width: 29px;' value='' class='delete' onclick='delete_row("+table_len+")'></td>\n\
                                   <td style='width:60px; text-align: center;'><div class='alertdiva' id='predown_"+nid+"' style=' display: none;' > Updated Successfully</div><input type='button' id='save_button"+nid+"' style='background-image: url("+imgsave+");border: 0 none;height: 36px;width: 29px;' value='' class='delete' onclick='save_row("+nid+")'></td></tr>";

                                    
                                    } 
                                   
				}        
		   });



               
        }
  
        function save_row(id)
            {

              tourTransOptionId='tourTransOptionId_'+id;      
              minpax='minpax'+id;
              maxpax='maxpax'+id;
              buyAP='buyAP'+id;
              buyCP='buyCP'+id;
              sellAP='sellAP'+id;
              sellCP='sellCP'+id;
              discount='discount'+id;
              discountType='discountType'+id;
              
              
              mydiv='#predown_'+id;
              mydivmax='#predownmax_'+id;
              mydivmax2='#predownmax2_'+id;
             var TourId=document.getElementById("TourId").value;
             var seasonId=document.getElementById("seasonId").value;
             var tourTransOptionId=document.getElementById(tourTransOptionId).value;
             var maxpax=document.getElementById(maxpax).value; 
             var minpax=document.getElementById(minpax).value;
             var buyAP=document.getElementById(buyAP).value;
             var buyCP=document.getElementById(buyCP).value;
             var sellAP=document.getElementById(sellAP).value;
             var sellCP=document.getElementById(sellCP).value;
             var discount=document.getElementById(discount).value;
             var discountType=document.getElementById(discountType).value;

                                $.ajax({
                                            url: "lib/scripts/php/all/ajaxTransferOptionSave.php",
                                            type: "POST",
                                            data:'TourId='+TourId+'&id='+id+'&tourTransOptionId='+tourTransOptionId+'&maxpax='+maxpax+'&minpax='+minpax+'&buyAP='+buyAP+'&buyCP='+buyCP+'&sellAP='+sellAP+'&sellCP='+sellCP+'&discount='+discount+'&discountType='+discountType+'&seasonId='+seasonId,
                                            success: function(data){

                                            if(data==1)
                                               {                                   

                                               $(mydiv).show();
                                               $(mydiv).fadeOut(4000);

                                               }
                                            if(data==2)
                                               {                                   
                                               alert('Please enter Grater value in MinPax in the comparetion of old MaxPax. ');
                                               $(mydivmax).show();
                                               $(mydivmax).fadeOut(4000);

                                               }
                                            if(data==3)
                                               {                                   
                                               alert('Maxpax value should be greter then Minpax. ');
                                               $(mydivmax2).show();
                                               $(mydivmax2).fadeOut(4000);

                                               }

                                            }        
                               });


            }
  
   
function delete_row(id)
                        {
                                var conf
                                conf=confirm("Are you sure Want to delete This. ");
                                if(conf)
                                {
                                    $.ajax({
                                            url: "lib/scripts/php/all/ajaxTransferOptionDelete.php",
                                            type: "POST",
                                            data:'id='+id,
                                            success: function(data){
                                                                        document.getElementById("row"+id+"").outerHTML="";
                                                                   }        
                                           });    
                                }
                                else
                                {
                                        return false;
                                }
                                
                        }   
  
   
</script>
<?php	require_once("footer.php");
?>

