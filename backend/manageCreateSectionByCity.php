<?php
	require_once("header.php");
        //global $cleaned;
        
                $getSystemCity = Users::getSystemCreatedSectionCity();//for global
            
       
         
        
?>
  <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Manage Create Section City</h3>
                                    <ul class="panel-controls">
                                        <div class="col-md-4" >
                                            <a href="createSectionAddByCity.php"> <button class="btn btn-success btn-block" style="width: 190px;"><span class="fa fa-plus"></span> Add Create Section City</button></a>
                                     </div>
                                       
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                               
                                    </br>
                                    
                                    
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                               
                                                <th>Tour City</th>
												  <th>Tour Country</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                  $i=1;
                                  foreach($getSystemCity as $CityName){?>    
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                 <td><?php echo $CityName['cityname'];?></td>
												    <td><?php echo $CityName['countryname'];?></td>
                                               
                                                   
                                                 
                                              
                                                <td><a href="createSectionByCityEdit.php?id=<?php echo $CityName['id'];?>"><span class="input-group-addon" style="width: 10px"><span class="fa fa-pencil"></span></span></a>
                                                    
                                                    <a href="#" type="submit" onClick='return deletemode(<?php echo $CityName['id'];?>);'><span class="input-group-addon" style="width: 10px"><span class="fa fa-times"></span></span></a></td>
                                            </tr>
                                        <?php  $i++; }?>    
                                            
                                        </tbody>
                                    </table>
                                    
                                    
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                            

                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->  


            

<?php	require_once("footer.php");
?>
<script type='text/javascript' src='js/plugins/noty/jquery.noty.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topCenter.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topLeft.js'></script>
<script type='text/javascript' src='js/plugins/noty/layouts/topRight.js'></script>            
<script type='text/javascript' src='js/plugins/noty/themes/default.js'></script>

 <script type="text/javascript">
                
                     
						function deletemode(id){
                    noty({
                        text: 'Are you sure Want to delete This. Becouse if you delete Tour here then releted Tour Detail will be deleted. ',
                        layout: 'topCenter',
                        buttons: [
                                {addClass: 'btn btn-success btn-clean', text: 'Ok', onClick: function($noty) {
									
                                    $noty.close();
									window.location.href="lib/scripts/php/all/deleteCreatedSectionCity.php?id="+id;
                                }
                                },
                                {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                                    $noty.close();
									return false;
                                    //noty({text: 'You clicked "Cancel" button', layout: 'topRight', type: 'error'});
                                    }
                                }
                            ]
                    })
					
					
					                                                  
                }
</script>
