<?php
	require_once("header.php");
         //global $cleaned;
        $cleaned = clean($_GET);
        //dump($cleaned);
	$getSystemCity = Users::getTourCategoryById($cleaned['id']);//for global
        
        $getCityAproved = Users::getSystemCityAproved();//for global 
	
                     
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Tour Category</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Edit Tour Category</h4>
                                <form id="jvalidate_tourcategory"  method="post" enctype="multipart/form-data" role="form" class="form-horizontal" action="lib/scripts/php/all/tourcategoryEdit.php">
                                <div class="panel-body">                                    
                                                 
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate City Name, Please Try Another City name</span> <?php }?>
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Select City:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="cityid">
                                                
                                                 <?php 
                                                 foreach($getCityAproved as $SystemCountry)
                                                      { ?>  
                                                <option value="<?php echo $SystemCountry['id'];?>" <?php if($SystemCountry['id']==$getSystemCity['cityid']){?> selected="selected" <?php }?>><?php echo $SystemCountry['cityname'];?></option>
                                                <?php } ?>
                                                
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div> 
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tour Category Name:</label>  
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control" name="tourcat_id" value="<?php echo $getSystemCity['id'];?>"/>
                                            <input type="text" class="form-control" name="tourcatname" value="<?php echo $getSystemCity['tourcatname'];?>"/>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">File</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class=" btn-primary" name="filename" id="filename" title="Browse file"/>
                                             <img src="<?php echo $NewSystemUsers['singlewidth_url'];?>" width="100" />
                                            <input type="hidden" name="old_img" id="gimage" value="<?php echo $getSystemCity['singlewidth_img'];?>"">
                                            <span class="help-block">Image ratio 1:1</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">File</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class=" btn-primary" name="filename1" id="filename1" title="Browse file"/>
                                             <img src="<?php echo $NewSystemUsers['doublewidth_url'];?>" width="100" />
                                            <input type="hidden" name="old_img1" id="gimage1" value="<?php echo $getSystemCity['doublewidth_img'];?>"">
                                            <span class="help-block">Image ratio 16:9</span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="status">
                                                <option value="1" <?php if($getSystemCity['status']==1){?> selected="selected" <?php }?> >Active</option>
                                                <option value="0" <?php if($getSystemCity['status']==0){?> selected="selected" <?php }?> >Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>                                                                                    
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate_tourcategory.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>


                
                
                
              
                






