<?php
	require_once("header.php");
        //global $cleaned;
		$today = date('Y-m-d');
	if(!isset($_GET['TourId']) || $_GET['TourId']=='' )
            {
                    header("location:createTour1.php"); 
            }
        $TourId=$_GET['TourId'];
		 $getSystemDefaultSeason = Users::getSystemDefaultSeason($today);
		if (isset($getSystemDefaultSeason) && is_array($getSystemDefaultSeason) && count($getSystemDefaultSeason)>0){
			$sid =$getSystemDefaultSeason['id'];
		}
		else{
			$sid =0;
		} 
                     
?>
  <style>
.btn-primary.active {
            background-color: #e34724;    }
.btn_red.active {
  background-color: red;
   }
    .mactive {
            background-color: #e34724;
    }
</style> 
<style>
ul.topnav {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #29343f;
  border-radius: 5px;
}

ul.topnav li {float: left;}

ul.topnav li a {
  display: inline-block;
  color: #f2f2f2;
  text-align: center;
  padding: 10px 35px;
  text-decoration: none;
  transition: 0.3s;
  font-size: 14px;
}

ul.topnav li a:hover {background-color: #e34724;}

ul.topnav li.icon {display: none;}

@media screen and (max-width:680px) {
  ul.topnav li:not(:first-child) {display: none;}
  ul.topnav li.icon {
    float: right;
    display: inline-block;
  }
}

@media screen and (max-width:680px) {
  ul.topnav.responsive {position: relative;}
  ul.topnav.responsive li.icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  ul.topnav.responsive li {
    float: none;
    display: inline;
  }
  ul.topnav.responsive li a {
    display: block;
    text-align: left;
  }
}
</style>
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2>Manage Tour</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                 <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                
                                    <div class="col-md-12">
                                          <!-- <div class="btn-group btn-group-justified">  
                                                <a href="createTour1edit.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Detail</a>
                                                <a href="createTour2.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Tour Timing</a>
                                                <a href="createTour3.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg active"  style=" font-weight: bold;">Tour Image</a>
                                                <a href="createTour4.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Transfer Option</a>
                                                <a href="createTour5.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Buying Price</a>
                                                <a href="createTour6.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Selling Price</a>
                                                <a href="createTour7.php?TourId=<?php echo $TourId;?>" class="btn btn-primary btn-lg"  style=" font-weight: bold;">Discount</a>
                                            </div>  -->
                                            <ul class="topnav" id="myTopnav">
 <li class="navtour"><a href="createTour1edit.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Tour Detail</a></li>
                                                <li class="navtour"><a href="createTour2.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Tour Timing</a></li>
                                                <li class="navtour mactive"><a href="createTour3.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Tour Image</a></li>
                                                <li class="navtour"><a href="createTour4.php?TourId=<?php echo $TourId;?>" class="navtour"  style=" font-weight: bold;">Transfer Option</a></li>
                                               <li class="navtour"><a href="createTour5.php?TourId=<?php echo $TourId;?>&sId=<?php echo $sid;?>" class="navtour"  style=" font-weight: bold;">Buying Price</a></li>
                                                <li class="navtour"><a href="createTour6.php?TourId=<?php echo $TourId;?>&sId=<?php echo $sid;?>" class="navtour"  style=" font-weight: bold;">Selling Price</a></li>
                                                <li class="navtour"><a href="createTour7.php?TourId=<?php echo $TourId;?>&sId=<?php echo $sid;?>" class="navtour"   style="font-weight: bold;">Discount</a></li>
  <li class="icon">
   <a href="javascript:void(0);" style="font-size:15px;" onclick="mymenuFunction()">☰</a>
  </li>
</ul>                                             
                                    </div>
                        
                        <br> <br>  <br>
                        
                                    <div class="panel-heading">                                
                                                <div class="col-md-4" >
                                                    <a href="createTourGallery.php?TourId=<?php echo $TourId;?>"> <button class="btn btn-success btn-block" style="width: 160px;"><span class="fa fa-plus"></span> Add Gallery Image</button></a>

                                                     
                                                </div>
                                     </div>
                        
                                    <div class="panel-body">
                                             
                                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Add New Gallery Image</h4>
                                <form id="jvalidate_gads" method="post" enctype="multipart/form-data" role="form" class="form-horizontal" action="lib/scripts/php/all/createTourGallery.php">
                                <div class="panel-body">                                    
                                     <input type="hidden" class="form-control" name="TourId" id="TourId" value="<?php echo $TourId;?>"/>            
                                    <?php if(isset($_REQUEST['msg'])){?><span class="label label-danger" style="margin-left: 159px;">Duplicate Category Name, Please Try Another Category name</span> <?php }?>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Gallery Name:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="galleryname"/>
                                            <span class="help-block">min size = 20, max size = 25</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Gallery Image</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class=" btn-primary" name="filename" id="filename" title="Browse file"/>
                                            <span class="help-block">Image ratio 16:9</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Gallery Order:</label>
                                        <div class="col-md-9">
                                            <input type="text" name="galleryorder" class="form-control"/>  
                                           
                                        </div>
                                    </div>
                                    
                                                                         
                                    
                                                                                                                        
                                    <div class="btn-group pull-right">
                                       <!-- <button class="btn btn-primary" type="button" onClick="jvalidate_gads.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>-->
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div> 
                                        
                                    </div>
                        
                        
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>     
                    
                    
                    
                    
                    
                    
                    
                    
                    
              

                 
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  


<?php	require_once("footer.php");
?>

<script>
function mymenuFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}	
</script>
