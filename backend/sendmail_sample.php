<?php
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 * This uses traditional id & password authentication - look at the gmail_xoauth.phps
 * example to see how to use XOAUTH2.
 * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
 */
//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'lib/phpmailer/src/Exception.php';
require 'lib/phpmailer/src/PHPMailer.php';
require 'lib/phpmailer/src/SMTP.php';

function sendMail($from, $from_name, $to, $to_name, $body, $altBody = '') {

	// SET SMTP CREDENTIALS / CONFIGS HERE
	$smtp_username = "no-reply@tripoye.com";
	$smtp_password = "iY@!gNtR*1";
	$smtp_host = 'smtp.tripoye.com';	
	$smtp_scheme = 'tls';
	$smtp_scheme_port = 587;

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	$mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	    )
	);
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	// $mail->SMTPDebug = 2;
	//Set the hostname of the mail server
	$mail->Host = $smtp_host;
	// use
	// $mail->Host = gethostbyname('smtp.gmail.com');
	// if your network does not support SMTP over IPv6
	//Set the encryption system to use - ssl (deprecated) or tls
	$mail->SMTPSecure = $smtp_scheme;
	//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	$mail->Port = $smtp_scheme_port;
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;
	//Username to use for SMTP authentication - use full email address for gmail
	$mail->Username = $smtp_username;
	//Password to use for SMTP authentication
	$mail->Password = $smtp_password;

	//Set who the message is to be sent from
	$mail->setFrom($from, $from_name);
	//Set an alternative reply-to address
	// $mail->addReplyTo('replyto@example.com', 'First Last');
	//Set who the message is to be sent to
	$mail->addAddress($to, $to_name);
	//Set the subject line
	$mail->Subject = 'PHPMailer GMail SMTP test';
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	$mail->msgHTML($body);
	//Replace the plain text body with one created manually
	$mail->AltBody = $altBody;
	//Attach an image file
	// $mail->addAttachment('lib//images/phpmailer_mini.png');
	//send the message, check for errors
	if (!$mail->send()) {
	    return $mail->ErrorInfo;
	} else {
	    return true;
	    //Section 2: IMAP
	    //Uncomment these to save your message in the 'Sent Mail' folder.
	    #if (save_mail($mail)) {
	    #    echo "Message saved!";
	    #}
	}
}
