<?php
	require_once("header.php");
        //global $cleaned;
        $cleaned = clean($_GET);
        //dump($cleaned);
        $getAdminRoles = Users::getAdminRoles();//for global
	$SystemUsers = Users::getUserById($cleaned['id']);//for global
        //dump($SystemUsers);
        $Selected_user_role=unserialize($SystemUsers['user_role']);
       // var_export($Selected_user_role);
        
               
?>
  
<!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Users</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        
                        <div class="col-md-6">                        

                            <!-- START JQUERY VALIDATION PLUGIN -->
                            <div class="block">
                                <h4>Edit User</h4>
                                <form id="jvalidateUserEdit" role="form" class="form-horizontal" action="lib/scripts/php/all/users.php">
                                <div class="panel-body">                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $SystemUsers['user_email'];?>" name="email" class="form-control"/>                                        
                                            <span class="help-block">required email</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Password:</label>                                        
                                        <div class="col-md-9">
                                            <input type="password" class="form-control" name="user_password" id="password2" value=""/>                                        
                                            <span class="help-block">min size = 5, max size = 10</span>
                                        </div>
                                    </div>                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Confirm Password:</label>                                       
                                        <div class="col-md-9">
                                            <input type="password" class="form-control" name="re-password"/>
                                            <span class="help-block">required same value as Password</span>
                                        </div>
                                    </div>               
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">First Name:</label>  
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="user_name" value="<?php echo $SystemUsers['user_name'];?>"/>
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div>
                                    <div class="form-group">                                        
                                        <label class="col-md-3 control-label">Last name:</label>          
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="user_surname" value="<?php echo $SystemUsers['user_surname'];?>"/>                                        
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div> 
                                     <div class="form-group">                                        
                                        <label class="col-md-3 control-label">User Mobile:</label>          
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="user_mobile" value="<?php echo $SystemUsers['user_mobile'];?>"/>                                        
                                            <span class="help-block">min size = 2, max size = 10</span>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Date of birth:</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control datepicker" name="date" value="<?php echo $SystemUsers['user_dob'];?>"/>
                                            <span class="help-block">required date</span>
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Address</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control" rows="5" name="user_address"><?php //echo $SystemUsers['user_address'];?></textarea>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Role:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="user_role">
                                                <option value="0" <?php //if($SystemUsers['user_role']==0){?> selected="selected" <?php //}?> >User</option>
                                                <option value="1" <?php //if($SystemUsers['user_role']==1){?> selected="selected" <?php //}?>>Administrator</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                    </div> -->
                                    
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Role:</label>
                                        <div class="col-md-3">
                                            
                                           <?php 
                                           
                                           
                                           
                                           foreach ($getAdminRoles as $value) {
                                               $chk='';
                                              
                                                foreach ($Selected_user_role as $key => $user_role) 
                                                    {
                                                       
                                                        if($value['id']==$user_role)
                                                            {
                                                              $chk='checked="checked"';
                                                             
                                                            }
                                                     
                                                    }
                                                       
     
                                            ?>    
                                           
                                            <input type="checkbox"   <?php echo $chk;?>    name="user_role[]" value="<?php echo $value['id'];?>"> <?php echo $value['name'];?>
                                           
                                            <?php  echo "<br>"; }?> 
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Status:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="user_status">
                                                <option value="1" <?php if($SystemUsers['user_status']==1){?> selected="selected" <?php }?>>Active</option>
                                                <option value="0" <?php if($SystemUsers['user_status']==0){?> selected="selected" <?php }?>>Inactive</option>
                                                
                                            </select>                           
                                            
                                        </div>                        
                                     </div>
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="button" onClick="jvalidate.resetForm();$('#gender').next('.bootstrap-select').removeClass('error').removeClass('valid')">Hide prompts</button>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>                                                                                                                          
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                            </div>
                        </div>
                    </div>

                 
                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  



<?php	require_once("footer.php");
?>


                
                
                
              
                






