### What is this repository for? ###

* Repo configuration and set up deployment on AWS for development

### Who do I talk to if I have queries? ###
[skype:jay@vistaran.tech](v)

## Setting up repo

Install git and clone the source code:
`git clone https://<YOUR BITBUCKET USERNAME>@bitbucket.org/<YOUR BITBUCKET USERNAME>/tripoye.com.git`

You can get this URL from Overview page of repository.

## Setting Up AWS

1. Install AWS Clin `pip install awscli --upgrade --user` and add path variable of script folder. [More Info](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install.html)
2. Clone the repo in XAMPP or WAMP and in new command prompt go to folder using command prompt after installation
3. Run `eb --version` to check version of eb cli 
4. Run `eb init` to initialize elastic beanstalk project then select **Asia Pacific - Mumbai Region**
5. Add aws access id and secret key `AKIAJKCZICXTBV2R2OZA` and `k9NOA8y2vtcH3to4JX49x8iBGdzhSoPZtdW+hqvf`
6. Then select Application to use in our case select **My First Elastic Beanstalk Application**
7. It will ask for configuring code commit - Select `N`.
8. Thats it. Now you will have default environment set up in `.elasticbeanstalk/config.yml`. You will find all details there.

## Deploying code online

When you have made your changes `add` and `commit` your files in repository and run following commands to deploy new code:

`eb deploy` and wait for it to finish. You will be able to see status of deployment right there on console. **Thats all you need to do basically!!!** No more file uploads using FTP.

Once deployed `push` your changes to repository.

## SSH to Instance

HOST: `ec2-user@ec2-13-126-25-71.ap-south-1.compute.amazonaws.com`
Private Key save with following cotents and use **Putty** to connect.

```
PuTTY-User-Key-File-2: ssh-rsa
Encryption: none
Comment: imported-openssh-key
Public-Lines: 6
AAAAB3NzaC1yc2EAAAADAQABAAABAQCK6qmYxZdE6nMWNlfHfzUrAOpo9GyZBZuL
HDxsfTevYiXCrc349+qFZx1mftKHBOH9h7ku3ozMbKbOJMS5zloUmd9aBADW2IsQ
kVhdYEGIZmFC7Wt9LneHjQ8PwdbT1hVYfYLC2JeY+TFCLattWylUZl8r0oDPFAXa
pD2IhgPlhrJnlG5a0LR/OV6Efqh0m9n76Nj8hoAEhoBhVdiYE86mKHmbYzf5SDId
Mg2Wm+8i4AMYxM/wdPgy+qP/9gR82ssY8PD9eoxTNNeizzyeAhJ1VGOK8aD8c8az
Z3lhGMI6n+OO4jr9g7jeRKSjhFxviUmqumhKawp83QR4XOrPZm75
Private-Lines: 14
AAABAG4UHWgtyvyVO7UyQsSXDK6S/y1D4novc138dfUSFHopr/pgd4L+ERy85RM/
E9xiBiI81oVJXRDwWA2qHSmPVtpURCS0X2XgGfJ6+iakC8nmNtQuBqsfkyAaqwC1
fWJHloqUinWT5C0X6eyi++CgCFQU+v1ixSdSmoDy/L+eUqAhubbBLmPrtrxj52WL
PAlHQRE9l/E4q4/iPDg0JtU7DZm8ByWaKPcXvI5nvpbcT6f5PvKeBwu4ZFOx/txR
aR656ATSXOoBhC0n5+wRDL2CMRKdG+7grVcquNQlerJlEMsWeTM92Zeq34B9hiQB
936/81y0splAmZ+Lz8/Ul/y4Z8EAAACBAN0ze99qflkLa90TXQ+RCUCwt/ZHjtiB
Vu3jyzdE7eZKwK6exlGGE7QM3dJkfYpu+MlKTJw4Ju5v/WNOaAc0qhEaaIjRQWyo
eJoev7R06gIZTrYheFJAvFvn6nYk1eVZHGZADh6WEqcbLjuvQSPMbi+dd2XIMdFP
3DeKOHl+YeZlAAAAgQCgxU4vmKoW84D7E2Dib1aGo1FV8f3ru/wbhOUAwOjOmfF1
1gSk5qGdNTNMD8Z1Pqy6QDfpsInm61xIBG2vHmmZhlvqEt+XwbTcRDBg1H0qYiTM
uXKLDTsTLFoY/C21lP3HmOT3P2liJ+at4YyxfFzPz1i+GNWS2luG0IEKKibDBQAA
AIEAxREJNJSRrJiqAvln5tg8L1xH8rhxl+n4JqrbY9g6d8JVpX7FXDoWhgrorZJ+
Giq5TqFll1c73FlA2VvnqdxbqfIQN1lEVomvQIYMwNp8tQLzLMlD28KRvJZEDe+K
l98aQ2rVdF8sIVtkX96QTiLY8JQFkcKizDBvKem58veaGB0=
Private-MAC: 99510d9055427f35ea0abd0b96c2f2b99f9e1a5b

```