$(document).ready(function(){
		
	$('[data-toggle="tooltip"]').tooltip(); 

	
	$(window).on('resize scroll',function()
	{
		if ($(window).width() < 991) 
		{
			$('.product_details .fixed_price_div_hidden').fadeOut(500);
			$('.product_details .fixed_price_div').fadeOut(500);
			$('.product_image .hidden_navbar').fadeOut(200);
			$('.product_description .price_div').slideDown(500);
		}
		else if ($(window).scrollTop() > 150)
		{
			$('.product_details .fixed_price_div').fadeOut(500);
			$('.product_details .fixed_price_div_hidden').fadeIn(500);
			$('.product_image .hidden_navbar').fadeIn(200);
		}
		else
		{
			$('.product_details .fixed_price_div_hidden').fadeOut(500);
			$('.product_details .fixed_price_div').fadeIn(500);
			$('.product_image .hidden_navbar').fadeOut(200);
			$('.product_description .price_div').slideUp(500);
		}
	});
	
	$('.product_image a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top-50
    }, 800);
	$('.black_text').css('color','#333');
	$(this).css('color','rgb(72,208,55)');
    return false;
	});
	
	$('.book_now_div .book_now').click(function(){
		
		$('.date_time_overlay').fadeIn(300);
		$('.date_time_overlay .date_time').animate({ 'right': 0, 'opacity': '1' }, { queue: false, duration: 400 });
				
	});
	$('.date_time_overlay .close_box').click(function(){
		
		$('.date_time_overlay').fadeOut(300);
		$('.date_time_overlay .date_time').animate({ 'right': -550, 'opacity': '1' }, { queue: false, duration: 400 });
		
	});
	
	var flag = 1;
	$('.leave_msg .show_msg').click(function(){
		if (flag == 1) 
		{
			$('.leave_msg').animate({ 'bottom': 0, 'opacity': '1' }, { queue: false, duration: 400 });
			flag = 0;
		}
		else 
		{
            $('.leave_msg').animate({ 'bottom': -355, 'opacity': '1' }, { queue: false, duration: 400 });
            flag = 1;
        }
        return false;
		
	});
	
	$('.promo_code .btn').click(function(){
		
		$('.hidden_msg').fadeIn(200);
		$('.hidden_msg_error').fadeIn(200);
		
	});
	
/*----------------------------------------- REVIEWS SLIDER--------------------------------------*/	
	$("#review_slideshow > div:gt(0)").hide();

    startInterval();
	
	function startInterval() {
		interval = setInterval(function() {
			sliderUp()
		}, 10000);
	}

	function stopInterval() {
		clearInterval(interval);
	}

	function sliderUp() {
		stopInterval();
		var $slide = $('#review_slideshow > div.current');
		$slide
			.fadeOut(0)
			.removeClass('current');

		if ($slide.is(':last-child')) {
			$slide
				.siblings(':first')
				.addClass('current')
				.fadeIn(200);
		} else {
			$slide
				.next()
				.addClass('current')
				.fadeIn(200);
		}

		// .appendTo('#review_slideshow');
		startInterval();
	};

	function sliderDown() {
		stopInterval();
			var $slide = $('#review_slideshow > div.current');
		$slide
			.fadeOut(0)
			.removeClass('current');

		if ($slide.is(':first-child')) {
			$slide
				.siblings(':last')
				.addClass('current')
				.fadeIn(200);
		} else {
			$slide
				.prev()
				.addClass('current')
				.fadeIn(200);
		}
		
		startInterval();
	};
	$('#sliderUp').click(function() {
		sliderUp();
	});
	$('#sliderDown').click(function() {
		sliderDown();
	})
/*----------------------------------------- REVIEWS SLIDER Ends--------------------------------------*/	
	

});